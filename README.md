# ilabs, I am master

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

---

# DO ONCE FOR INSTALLATION OF FIREBASE

dart pub global activate flutterfire_cli
curl -sL https://firebase.tools | bash

# FOR EACH PROJECT

create a project in firebase console by the name "iLabs"
Also activate FirestoreDatabase from firebase console.

flutter create ilabs

console: firebase login

flutter pub add firebase_core
flutter pub add cloud_firestore
flutterfire configure
===================================
Should not need to do the following in : project\android\app\build.gradle
defaultConfig {
// TODO: Specify your own unique Application ID (https://developer.android.com/studio/build/application-id.html).
applicationId "com.example.ilabs"
minSdkVersion 21// flutter.minSdkVersion <<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>
targetSdkVersion 30//flutter.targetSdkVersion <<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>
versionCode flutterVersionCode.toInteger()
versionName flutterVersionName
}

===================================
Now in code:

import 'package:firebase_core/firebase_core.dart';
import 'package:ilabs/firebase_options.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() async {
print("Starting");
WidgetsFlutterBinding.ensureInitialized();
await Firebase.initializeApp(
options: DefaultFirebaseOptions.currentPlatform,
);
await addUser();
// runApp(const MyApp());
}

Future<void> addUser() {
// Call the user's CollectionReference to add a new user
CollectionReference users = FirebaseFirestore.instance.collection('users');
return users
.add({
'full_name': "Salman Khan", // John Doe
'company': "NUST CHI", // Stokes and Sons
'age': 22 // 42
})
.then((value) => print("User Added"))
.catchError((error) => print("Failed to add user: $error"));
}
=========================================
11:10 Firebase working.
going to add item from the app.
