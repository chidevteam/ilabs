
UNASSIGNED
=============
- Swipe to delete.
- Different landscape views - use sliver bar in Landscape

- Moving from Trial account to actual account, limit the size of invoices, etc.

IKRAM
======
- Changes in the region are not reflected on the invoice i.e date and currency 30 min to 1 hour 
   started working on it @ 12:55
      changes in region to be displayed
      - Changes in the region are not reflected on the invoice i.e date and currency =======done
      1- On the main screen where all the invoices are displayed(all, pending, paid) =======done
      2- In the invoice info section where data and due date are selected (date)  =======done
      3- In the item section where the item's unit cost and total after discount is etc is displayed (currency) ====== done
      4- In the payment details section (discount, tax, client payment, total and balance due )(currency)  ====== done
      5- In the client payment section, screen date and currency changes in both will be reflected ===== pending
      6- In the signature date in a particular format is selected when signature is saved  ====== pending
- Saving notes in user defaults is not working (30 mins)  ====== done
- Marking invoice paid (whenever you add an item mark it as unpaid ) (30 min) ====== done
- by default items are taxable(change state of taxable toggle button)  (needs to work on ) ====== done



DAY 2
- Delete invoice (30 mins) ====== done
- Notes and business logo get deleted whenever a client is added/deleted multiples time (fixed but needs review) 1 hour =======done
- duplicating an invoice (1 hour, if images, then give warning that images won't be copied) ===== done 
- sending invoicesvia link is yet to be implemented  (2 hours) =========== done in 1 hour
- payment detail section (client payment ) (2-3hours) (when client payment will be added ,,, check if total< clientPayment sum than mark invoice as paid)
- delete in the item does not work properly (30 mins ) 
- adding images does not work properly (show galary also) (30min)
- List of inovice only 50 are being loaded, same with estimates, load more.

Bugs to be fixed:
- Overall discount doesn't work when per item tax is applied
- Updating the tax doesn't seem to work in an invoice to especially exclusive and exclusive
- Unable to edit invoice tittle
- Invoices have an unpaid amount in front of the
- balance due doesn't show the correct balance

DAY 3:
- Estimate fixes? (8 to 10 hours )
- Document the test cases for taxes.
- sending invoices via email is yet to be implemented  (2 hours)

AHMAD
=====================================================
    /invoices/upload-pdf/:id -> POST Request
    @Params
    id: invoice id

    @Authorization
    Token Required

    @Body
    file: pdf file
- In choose template screen (getting pdf from server) circular and square logo and two of the fonts do not work
- PDF at least one template working
- Invoice preview 
- Open in pdf with other applications (1 hour or more )
- The option to delete the logo is not there (30 mins to 1 hour )
- Images: UPload is slow.
https://dev.to/ashishrawat2911/handling-network-calls-and-exceptions-in-flutter-54me
- API call failure handling RetryClient()

ALI HASSAN
===========
- Printing an invoice (1 hour )
- Adding multiple images in the invoice (1 day)
  > should show galary also
  > cropping takes too much time


SALMAN
======
- SETTINGS: allow the user to only change the number in the invoice number field and title in the respective (30 mins)
- Dates should not be editable by hand.
- Add a pop up on delete invoice so user can click confirm delete
- in add_invoice_vu click floating action button and upon click on email should open new screen for email.
- have a look at invoice send button and modify its design

OMAIR
======
day 1:
- History  - 2 HR, 
- Proper implementation of signatures (also add save as default button in signature paint screen)
- Forget password implementation (3 to 4 hours )
day 2:

ZOBIA
======
- Call signature upload api after userdefault patch ---- WAITING FOR ZOBIA'S FEEDBACK
- Explain how we will move from trial account to actual account.




The difference in invoice and estimate 
========================================
https://invoicelabs.bit.ai/docs/view/qj6Ck7Y8vZDOu7E8
- On coming back from add new invoice/estimate, the invoice should be present
- Estimates should goto /estimates end point instead of /invoices
- Estimates are of two types open and closed. 
- When an estimate is converted to an invoice it is marked as closed  
  otherwise it stays open. On the other hand, invoices are either paid or unpaid 
- Estimate no and titles are stored separately 
- Estimate doesn't have a due date and payment terms in them whereas invoices do 
- Estimate doesn't have a partial payment and balance due section whereas invoices do 
- Estimates have "make invoice button" whereas invoices have "mark paid button"
- Add client / item should be possible in AddInvoice/Estimte
- original app can't understand the new format of our invoices
- Couldn't find the API to upload estimate buisiness image, failing in WEB





Reduce size of APK.
Download files And show in webview for info sections. (30 min - )

- Search in item / client functional, I have hidden the button in appbar


Second phase :
=================
Crop functionality speed improve
Image support in duplication of invoice
Offline functional
Frequent items and clients 
Payment Integration with stripe 
import client from contacts




========================================

For payments in-app purchase
email: hamzachidev@gmail.com
password: Test@1234.


com .invoiceslab.invoice
com. example.ilabs



=====================================
- How to handle resolution of images (signature, item images)
  > Low resolution images in app.
  > Signature
- Favorite Items/Clients - pending


Low priority / Fine Tuning
- Firebase support
- Indication that we are working offline
  > when should we sync data
  > https://dev.to/ashishrawat2911/handling-network-calls-and-exceptions-in-flutter-54me
  > API call failure handling RetryClient()

Salman
=======
- UI issues Different landscape views - use sliver bar in Landscape

Ali
- Theme Dark / Light
- Platform Widgets

===================================================================
Ahmad
- incorporate compute function  > in api call

Ikram
- iOS build

Difference in PDFs
- Titles are different (Estimate no, Estimate to, Estimate from)
- Due date, balance due is not displayed in the estimates

=====================================================================
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Master detail view
UBL app UI

============================================

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

- Ads
- QR code scanner
- Image picker module
- Camera and Gallery
- Share with: What's app, ....
- Show our app in accepting documents
- File upload module
- File download module (PDF)
- File download any
- Language support
- Platform widget
- Dark and light mode
- Accessiblity mode
- GPS
- https://pub.dev/packages/speech_to_text

Ahmad:s
- Table and Form
- autogenerate the table and form
- autogenerate complete view
- Signature
- prints logged to a screen in production mode

Atiq
- Offline Mode: AutoSync
- IP Camera module (remote monitoring)

Munib
- Make a post/get proper backend (backend development)
==========================================================
Integration/Unit Test
==========================================================

* UI ok
** API integrated
*** validations/ fully done
**** lanscape ok

INVOICE FORM
  Invoice info                                * 
  Add Business Info                           * 
  Add Client to Invoice                       *
  Add Items to Invoice                        *
  Add Photos to Invoice                       *
  Add Payment Method to Invoice               *
  Add Notes to Invoice                        *
  Add Sign                                    *
  Send Message (what's app, other)            *
  Send Email                                  *
  Mark paid
  
Screens:
- Invoices                                    *
    All, Outstanding, Paid                    *
    Add Invoice                               *
- Estimates                                   *
    All, Open, Closed                         *
- Clients                                     * 
- Items                                       * DONE
- Drawer                                      * 
  Business Info                               * DONE
  Choose Template  
  Region                                      * DONE
  Update Subscription                         *
  Switch Account                              *
  Contact Us                                  *
  Help                                        * DONE
  Terms of Use                                * DONE
  Privacy Policy                              * DONE
  Log out                                     
  Setting                                     *
    Payment Instructions                      * DONE TESTED (AH/IH)
    Tax                                       * 
    Default Notes                             * DONE TESTED (AH/IH)
    Default Days                              * DONE
    Default Signature                         * DONE TESTED (AH/IH)
    Invoice and Estimate Number               *
    Customize                                 *
    Default Email Message                     * DONE TESTED (AH/IH)
- Welcome / Splash                            *
- Login and Register                          *
- Create Account                              *
- Forget password                             *

- Reports                                     * 
  Client                                      * DONE
  Invoices                                    *

  Preview                                       -
  History                                       ?
  
- 
===========================================================
-===================================================
If form is small, we will have editble fields
if form is big and has multiple small models, we will make compact view of each submode
and none of them will be editable, even the normal fields will also appear as readonly

SMALL MODELS:
Following code will be generated:
- A TableView if we have a list of objects
- A basic view of Model (Card view)
- A Form, which cn be invoked by clicking on plus (empty form or cell of table or card)


- A simple table, on clicking we open the small view which shows readonly info (detail)
- on clicking the detail we 
=========================================
)