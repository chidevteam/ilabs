import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'firebase_options.dart';

const Color darkBlue = Color.fromARGB(255, 18, 32, 47);

void main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
  } catch (e, st) {
    debugPrint("$e");
    debugPrint("$st");
  }

  // The first step to using Firebase is to configure it so that
  // our code can find the Firebase project on the servers. This
  // is not a security risk, as explained here: https://stackoverflow.com/a/37484053
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform
      //  const FirebaseOptions(
      //     apiKey: "AIzaSyAhE5iTdU1MflQxb4_M_uHiXJR9EC_mE_I",
      //     authDomain: "nanochat.firebaseapp.com",
      //     projectId: "firebase-nanochat",
      //     messagingSenderId: '137230848633',
      //     appId: '1:137230848633:web:89e9b54f881fa0b843baa8')
      );

  // We sign the user in anonymously, meaning they get a user ID
  // without having to provide credentials. While this doesn't
  // allow us to identify the user, this would still allow us to
  // for example associate data in the database with each user.
  // await FirebaseAuth.instance.signInAnonymously();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(scaffoldBackgroundColor: darkBlue),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          // We use a stream builder to both read the initial data from the database and
          // listen to updates to that data in realtime. The database we use is called
          // Firestore, and we are asking the 10 most recent messages.
          child: getItems(),
        ),
      ),
    );
  }
}

Widget getItems() {
  return StreamBuilder<QuerySnapshot>(
    stream: FirebaseFirestore.instance
        .collection('items')
        // .orderBy('timestamp', descending: true)
        .limit(10)
        .snapshots(),
    builder: (context, snapshot) {
      if (snapshot.hasError) {
        return Center(child: Text('$snapshot.error'));
      } else if (!snapshot.hasData) {
        return const Center(
          child: SizedBox(
            width: 50,
            height: 50,
            child: CircularProgressIndicator(),
          ),
        );
      }
      var docs = snapshot.data!.docs;
      return ListView.builder(
        itemCount: docs.length,
        itemBuilder: (context, i) {
          return ListTile(
            leading: Text('-- ${docs[i]['description']}'),
            title: Text('${docs[i]['unit_cost']}'),
          );
        },
      );
    },
  );
}
