import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:ilabs/iLabs/Utils/Preferences.dart';
import 'iLabs/home/routes.dart';
import 'package:in_app_purchase_android/in_app_purchase_android.dart';
import 'package:firebase_core/firebase_core.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'Flutter Demo',
      routes: appRoutes,
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      initialRoute: "/welcome",
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Hive.initFlutter();const
  await EasyLocalization.ensureInitialized();
  debugPrint("Starting2");
  if (defaultTargetPlatform != TargetPlatform.linux) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      InAppPurchaseAndroidPlatformAddition.enablePendingPurchases();
    }
    await Firebase.initializeApp();
  }
  await UserPreferences.init();
  runApp(EasyLocalization(
      supportedLocales: const [
        Locale('en', 'US'),
        Locale('ur', 'PK'),
        Locale('ar', 'DZ'),
        Locale('de', 'DE'),
      ],
      path: 'resources/langs',
      //  fallbackLocale: Locale('ar', 'DZ'),
      child: const MyApp()));
}
