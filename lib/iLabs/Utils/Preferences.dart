import 'package:shared_preferences/shared_preferences.dart';


class UserPreferences{
  static SharedPreferences? _preferences;
  static const _keyAuthToken = 'authToken';

  static Future init() async =>
    _preferences = await SharedPreferences.getInstance();

  static Future setAuthToken(String token) async{
    await _preferences?.setString(_keyAuthToken, token);
  }

  static Object? getAuthToken() => _preferences?.get(_keyAuthToken);

}