import 'dart:convert';
import 'dart:io';

import 'package:cached_map/cached_map.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io' as Io;
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import '../services/api_client.dart';
import '../home/app_constants.dart';
import 'dart:ui' as ui;

class Utils {
  static String imgPath = "https://invoicelabs.co/api/";
  // static String imgPath = ApiClient.instance!.mBaseUrl;

  // static imageURl(String relativePath) {
  //   return relativePath == ""
  //       ? 'https://invoicelabs.co/wp-content/uploads/2021/11/logo-2-1.png'
  //       : imgPath + relativePath;
  // }

  static pushReplacement(BuildContext context, Widget targetClass) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
      return targetClass;
    }));
  }

  static Future push(BuildContext context, Widget targetClass) async {
    return Navigator.push(context, MaterialPageRoute(builder: (context) {
      return targetClass;
    }));
  }

  static Future<String> encodeImageTObase64(PickedFile pickedFile) async {
    final bytes = await Io.File(pickedFile.path).readAsBytes();
    return base64.encode(bytes);
  }

  static Future<Image> decodeBase64ToImage(String base64String) async {
    var bytes = base64.decode(base64String);
    Image image = Image.memory(bytes);
    return image;
  }

  static Future<File> fileFromImageUrl(String url) async {
    final response = await http.get(Uri.parse(url));
    final documentDirectory = await getApplicationDocumentsDirectory();
    final file = File(join(documentDirectory.path, 'imagetest.png'));
    file.writeAsBytesSync(response.bodyBytes);
    return file;
  }

  // static Future<File> imageBytesToFile(ByteData bytes) async {
  //   String tempPath = (await getApplicationDocumentsDirectory()).path;
  //   File file = File('$tempPath/${DateTime.now().millisecondsSinceEpoch}.png');
  //   await file.writeAsBytes(
  //       bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
  //   return file;
  // }

  static Future<File> imageToFile(ui.Image image) async {
    ByteData? bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/${DateTime.now().millisecondsSinceEpoch}.png');
    await file.writeAsBytes(
        bytes!.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
    return file;
  }

  static Future<File> imageToFile2(String fileName, ui.Image image) async {
    ByteData? bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    // String tempPath = (await getApplicationDocumentsDirectory()).path;
    File file = File(fileName);
    await file.writeAsBytes(
        bytes!.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
    return file;
  }

  static void clearCache(){
    Mapped.deleteFileDirectly(cachedFileName: "items");
    Mapped.deleteFileDirectly(cachedFileName: "clients");
    clearAllPreferences();
  }

  static void clearAllPreferences() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
     await preferences.clear();
  }
}
