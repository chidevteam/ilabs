import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ilabs/iLabs/models/social_login/facebook_user_data.dart';

class FacebookAuthButton extends StatelessWidget {
  void Function(BuildContext context,FacebookUserData facebookUserData) onSuccess;
   FacebookAuthButton({Key? key,required this.onSuccess}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: (){
        fbLogin(context);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: SvgPicture.asset(
          'iLabsSreens/login_signup/facebook icon.svg',
        ),
      ),
    );
  }

  Future<void> fbLogin(BuildContext context) async {
    final LoginResult result = await FacebookAuth.instance
        .login(); // by default we request the email and the public profile
    if (result.status == LoginStatus.success) {
      AccessToken? accessToken = result.accessToken;
      final userDataJson = await FacebookAuth.instance.getUserData();
      FacebookUserData facebookUserData = FacebookUserData.fromJson(userDataJson); ///class defined in models
      Map<String, dynamic> req = {'face_book_token': result.accessToken!.token};
      facebookUserData.token = result.accessToken!.token;
      onSuccess(context,facebookUserData);
    } else {
      print(result.status);
      print(result.message);
    }
  }

}