import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../../login/google_sign_in_api.dart';

class GoogleAuthButton extends StatelessWidget {
  void Function(BuildContext context,GoogleSignInAccount googleUser) onSuccess;
  GoogleAuthButton({Key? key,required this.onSuccess}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: (){
        onGoogleSignUp(context);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: SvgPicture.asset(
          //todo: include google svg image here
          'iLabsSreens/login_signup/google_icon.svg',
        ),
      ),
    );
  }

  onGoogleSignUp(BuildContext context) async {
    GoogleSigninApi googleSigninApi = GoogleSigninApi();
    googleSigninApi.googleLogout();
    await googleSigninApi.googleLogin(); 
    if(googleSigninApi.user != null) {
      onSuccess(context, googleSigninApi.user!);
    }
  }
}