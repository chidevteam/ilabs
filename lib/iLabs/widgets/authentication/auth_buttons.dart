import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../Utils/utils.dart';
import '../../language/generated/locale_keys.g.dart';
import '../../login/forget_password/forgot_password_vu.dart';

TextButton forgotPasswordButton(BuildContext context) {
  return TextButton(
    onPressed: () {
      Utils.push(context, ForgetPasswordScreen(null));
    },
    child: Text(
      tr(LocaleKeys.login_view_forgot_password),
      style: const TextStyle(
          color: Color(0xfffd7848),
          //  color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
          fontSize: 14.0,
          fontWeight: FontWeight.w500),
    ),
    style: TextButton.styleFrom(
      minimumSize: Size.zero,
      padding: const EdgeInsets.only(top: 6.0),
      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    ),
  );
}

Widget myAuthButton(BuildContext context,
    //todo: set an appropriate name of this widget
    {required Color buttonColor,
    required Color textColor,
    required String label,
    required GestureTapCallback onPressed}) {
  return Padding(
    padding: const EdgeInsets.all(20.0),
    child: SizedBox(
      width: double.infinity,
      height: 45,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            elevation: 0,
            primary: buttonColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            )),
        onPressed: onPressed,
        child: Text(
          label,
          style: TextStyle(color: textColor, fontSize: 14),
        ),
      ),
    ),
  );
}

Widget googleFacebookButtons(
  BuildContext context, {
  required GestureTapCallback onPressGoogle,
  required GestureTapCallback onPressFaceBook,
}) {
  return Padding(
    padding: const EdgeInsets.only(top: 10.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            onPressFaceBook();
          },
          child: CircleAvatar(
            backgroundColor: Colors.grey,
            radius: 22,
            child: CircleAvatar(
              radius: 21.5,
              backgroundColor: Colors.white,
              child: SvgPicture.asset(
                //todo: include google svg image here
                'iLabsSreens/login_signup/facebook icon.svg',
                height: 26,
                width: 26,
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 10.0,
        ),
        GestureDetector(
          onTap: () {
            onPressGoogle();
          },
          child: CircleAvatar(
            backgroundColor: Colors.grey,
            radius: 22,
            child: CircleAvatar(
              radius: 21.5,
              backgroundColor: Colors.white,
              child: SvgPicture.asset(
                //todo: include google svg image here
                'iLabsSreens/login_signup/icons8-google.svg',
                height: 26,
                width: 26,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
