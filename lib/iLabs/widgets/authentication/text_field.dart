import 'package:get/get.dart';
import 'package:flutter/material.dart';

Widget myAuthField({
  required String label,
  required TextInputType textInputType,
  bool? obsecureField,
  required void Function(String?)? onSaved,
  required String? Function(String?)? validator,
}) {
  ///an Independant TextField that will manage its state
  RxBool passwordVisible =
      obsecureField == null ? false.obs : obsecureField.obs;
  return Obx(
    () => Padding(
      padding: const EdgeInsets.fromLTRB(18.0, 16, 18.0, 0),
      child: TextFormField(
        enableInteractiveSelection: false,
        cursorColor: Color(0xfffd7848),
        obscureText: passwordVisible.value,
        keyboardType: textInputType,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: const BorderSide(color: Color(0xfffd7848), width: 2.0),
          ),
          suffixIcon: obsecureField == null
              ? null
              : GestureDetector(
                  onTap: () {
                    passwordVisible.value = !passwordVisible.value;
                  },
                  child: Icon(
                    passwordVisible.value
                        ? Icons.visibility
                        : Icons.visibility_off,
                    color: Color(0xff707070),
                  )),
          hintText: label,
          hintStyle: TextStyle(
              // fontWeight: FontWeight.w400,
              fontSize: 14,
              color: Color(0xff707070)
              //   color: Colors.grey.shade500

              ),
        ),
        validator: validator,
        onSaved: onSaved,
      ),
    ),
  );
}
