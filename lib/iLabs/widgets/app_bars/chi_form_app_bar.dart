import 'package:flutter/material.dart';

AppBar chiFormAppBar(String title, BuildContext context,
    {dynamic obj, List<Widget>? actions}) {
  return AppBar(
      backgroundColor: Colors.white,
      title: Text(
        title,
        style: const TextStyle(
            color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400),
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context, obj);
        },
        icon: Icon(
          Icons.arrow_back,
          color: Colors.orange[800],
        ),
      ),
      actions: actions);
}
