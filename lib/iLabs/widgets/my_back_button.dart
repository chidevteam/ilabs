import 'package:flutter/material.dart';

class MyBackButton extends StatelessWidget {
  GestureTapCallback? onPressed;
  MyBackButton({Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, top: 15),
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Color(0xfff4f7fa),
        ),
        width: 31,
        height: 31,
        child: IconButton(
          onPressed: onPressed ??
              () {
                Navigator.pop(context);
              },
          icon: const Icon(
            Icons.arrow_back,
            color: Color(0xff3a3938),
            size: 16,
          ),
        ),
      ),
    );
  }
}
