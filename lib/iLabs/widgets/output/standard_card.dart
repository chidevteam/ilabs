import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget invoiceFormBulletOne(IconData icon, String title) {
  return Padding(
    padding: const EdgeInsets.only(right: 10.0, top: 9.5, left: 10.0),
    child: Text(
      title,
      style: GoogleFonts.poppins(
          fontSize: 12.0,
          fontWeight: FontWeight.w500,
          color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0)),
    ),
  );
}

Widget invoiceFormCardTitle(String title, Widget widget) {
  return Column(children: [
    Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 8.0, 8.0, 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
          ),
          widget
        ],
      ),
    ),
    const Divider(
      color: Colors.grey,
      indent: 10,
      endIndent: 10,
    )
  ]);
}

Widget invoiceFormBulletTwo(IconData icon, String date, String dueDate) {
  return Padding(
    padding: const EdgeInsets.only(left: 10.0, top: 9.5),
    child: Row(
      children: [
        Icon(
          icon,
          size: 16.5,
          color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
        ),
        const SizedBox(
          width: 3.6,
        ),
        Text(
          date,
          style: GoogleFonts.poppins(
            fontSize: 12.0,
            fontWeight: FontWeight.w500,
            color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
          ),
        ),
        // const Spacer(),
        // Text(
        //   dueDate,
        //   style: TextStyle(
        //       fontSize: 13.0,
        //       fontWeight: FontWeight.w400,
        //       color: Colors.grey[600]),
        // ),
      ],
    ),
  );
}

Widget invoiceFormBulletThree(IconData icon, String dueDate) {
  return Padding(
    padding: const EdgeInsets.only(right: 10.0, top: 5.5),
    child: Text(
      dueDate,
      style: GoogleFonts.poppins(
        fontSize: 14.0,
        fontWeight: FontWeight.w500,
        color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
      ),
    ),
  );
}
