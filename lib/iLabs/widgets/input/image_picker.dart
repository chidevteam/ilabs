import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';
import 'package:image/image.dart' as Im; //For image resizing DON'T DELETE

Widget chiImageField(
    {required BuildContext context,
    required String imageName,
    required void Function(String?) callback,
    ImageSource? imageSource,
    bool doCrop = false}) {
  return Container(
    decoration: BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.transparent.withOpacity(0.2),
          spreadRadius: 0.1,
          blurRadius: 3,
          offset: const Offset(2, 0),
        ),
      ],
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () async {
            ImageSource? iSrc = imageSource;
            iSrc ??= await showChoiceDialog(context);
            if (iSrc != null) {
              String? newFile = await onPickImage(
                  context: context, imgSrc: iSrc, doCrop: doCrop);
              callback(newFile);
            }
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                color: imageName == ''
                    ? Colors.orange.shade100
                    : Colors.grey.shade100,
              ),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(3),
                  child: imageName != ''
                      ? Image.file(File(imageName), fit: BoxFit.cover)
                      : imagePlaceHolder()
                  // imageNetwork(imageName),
                  ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget imagePlaceHolder() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: const [
      Icon(Icons.add, color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0)),
      Text(
        "Select Image",
        style: TextStyle(
            color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
            fontSize: 15.0,
            fontWeight: FontWeight.w600),
      )
    ],
  );
}

Future<ImageSource?> showChoiceDialog(BuildContext context) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text(
            "Choose option",
            style: TextStyle(color: Colors.grey),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                const Divider(
                  height: 1,
                  color: Colors.grey,
                ),
                ListTile(
                  onTap: () {
                    print("Galary");
                    Navigator.of(context).pop(ImageSource.gallery);
                    // return "Galary";
                    // _openGallery(context);
                  },
                  title: const Text("Gallery"),
                  leading: const Icon(
                    Icons.account_box,
                    color: Colors.grey,
                  ),
                ),
                const Divider(
                  height: 1,
                  color: Colors.grey,
                ),
                ListTile(
                  onTap: () {
                    print("Camera");
                    Navigator.of(context).pop(ImageSource.camera);
                    // _openCamera(context);
                  },
                  title: const Text("Camera"),
                  leading: const Icon(
                    Icons.camera,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
        );
      });
}

Future<String?> onPickImage(
    {required BuildContext context,
    ImageSource imgSrc = ImageSource.camera,
    bool doCrop = false}) async {
  final ImagePicker picker = ImagePicker();
  XFile? imagePath = await picker.pickImage(
    source: imgSrc,
  );
  if (imagePath == null) return null;

  String croppedImage = imagePath.path;
  if (doCrop) {
    croppedImage = (await cropImage(imagePath))!;
  }

  return croppedImage;
  // File inputImage = File(croppedImage);
  // printImageSize(inputImage, "Orig");
  // Im.Image? image = Im.decodeImage(inputImage.readAsBytesSync());
  // Im.Image smallerImage = Im.copyResize(image!,
  //     width: 200,
  //     height: 200); // choose the size here, it will maintain aspect ratio

  // File compressedImage = File(imagePath.path)
  //   ..writeAsBytesSync(Im.encodeJpg(smallerImage, quality: 85));
  // printImageSize(compressedImage, "Compressed");

  // return compressedImage.path;
}

void printImageSize(File image, String info) {
  final bytes = image.readAsBytesSync().lengthInBytes;
  final kb = bytes / 1024;
  // final mb = kb / 1024;
  print("???????????????   File size $info : $kb");
}

Future<String?> cropImage(XFile? imagePath) async {
  File? croppedImage = await ImageCropper().cropImage(
      sourcePath: imagePath!.path,
      aspectRatioPresets: Platform.isAndroid
          ? [
              CropAspectRatioPreset.square,
              CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.original,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio16x9
            ]
          : [
              CropAspectRatioPreset.original,
              CropAspectRatioPreset.square,
              CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio5x3,
              CropAspectRatioPreset.ratio5x4,
              CropAspectRatioPreset.ratio7x5,
              CropAspectRatioPreset.ratio16x9
            ],
      androidUiSettings: const AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.deepOrange,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      iosUiSettings: const IOSUiSettings(
        title: 'Cropper',
      ));
  if (croppedImage != null) {
    return croppedImage.path;
  }
  return null;
}
