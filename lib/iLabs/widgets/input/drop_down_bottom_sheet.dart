import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// import '../../../../language/generated/locale_keys.g.dart';

/*
Will always have selected index (may be -1)


*/

Widget dropDownBottomSheet(context,
    {required String title,
    required List<String> items,
    required Function onTap,
    int selectedIndex = -1}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const SizedBox(
        height: 10.0,
      ),
      Text(
        title,
        // textAlign: TextAlign.left,
        style: GoogleFonts.poppins(
            color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
            fontSize: 12.0,
            fontWeight: FontWeight.w400),
      ),
      const SizedBox(
        height: 10.0,
      ),
      Padding(
        padding: const EdgeInsets.only(bottom: 6.0),
        child: InkWell(
          onTap: () async {
            bottomSheetContent(context,
                title: title,
                items: items,
                onTap: onTap,
                selectedIndex: selectedIndex);
          },
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.fromLTRB(10, 12, 10, 12),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
              borderRadius: BorderRadius.circular(3),
              border: Border.all(
                color: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
                width: 1,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  selectedIndex == -1 ? "" : items[selectedIndex],
                  style: GoogleFonts.poppins(
                    fontSize: 14.0,
                    color: Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
                  ),
                ),
                _rotatedBoxIcon()
              ],
            ),
          ),
        ),
      ),
    ],
  );
}

Future<dynamic> bottomSheetContent(context,
    {required String title,
    required List<String> items,
    required Function onTap,
    int? selectedIndex}) {
  // String flag = selectedIndex
  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(children: [
          sheetHeader(title),
          Flexible(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {
                        onTap(index);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ListTile(
                            visualDensity: const VisualDensity(
                                horizontal: 0, vertical: -4),
                            title: Text(items[index]),
                            trailing: selectedIndex == index
                                ? const Icon(Icons.done)
                                : null),
                      ));
                }),
          )
        ]);
      });
}

Widget sheetHeader(String title) {
  return Container(
    decoration: BoxDecoration(
        color: const Color.fromARGB(255, 200, 200, 200),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: const Offset(0, 0), // changes position of shadow
          ),
        ]),
    width: double.infinity,
    padding: const EdgeInsets.fromLTRB(10, 12, 10, 12),
    // color: Colors.black12,
    child: Center(
      child: Text(
        title,
        style: const TextStyle(fontSize: 16.0),
      ),
    ),
  );
}

RotatedBox _rotatedBoxIcon() {
  return const RotatedBox(
    quarterTurns: 1,
    child: Icon(
      Icons.arrow_forward_ios_outlined,
      color: Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
      size: 18,
    ),
  );
}
