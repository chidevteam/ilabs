import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilabs/iLabs/widgets/input/chi_textfield.dart';

Text headingText(String title) {
  return Text(
    title,
    style: const TextStyle(
        color: Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
        fontSize: 12.0,
        fontWeight: FontWeight.w400),
  );
}
//not deleting this code for identifying bugs if occurred later

//
// chiFloatFormField({
//   required String heading,
//   required String hintText,
//     bool enabled = true,
//   required Function func,
//   FormFieldValidator<String>? validator,
//   List<TextInputFormatter>? inputFormat,
//   String initialValue = "",
//   TextInputType fieldType =
//       const TextInputType.numberWithOptions(signed: false, decimal: false),
// }) {
//   return _chiTextFormField2(
//       initialValue: initialValue,
//       fieldType: fieldType,
//       func: func,
//       enabled: enabled,
//       heading: heading,
//       hintText: hintText,
//       validator: validator,
//       inputFormat: inputFormat);
// }
//

chiNumberFormField({
  required String heading,
  required String hintText,
  required Function func,
  bool enabled = true,
  FormFieldValidator<String>? validator,
  List<TextInputFormatter>? inputFormat,
  String initialValue = "",
  TextInputType fieldType = TextInputType.number,
}) {
  return CHITextField(
      initialValue: initialValue,
      fieldType: fieldType,
      func: func,
      enabled: enabled,
      heading: heading,
      hintText: hintText,
      validator: validator,
      inputFormat: inputFormat);
}
//
// chiTextFormField(
//     {required String heading,
//     required String hintText,
//     required Function func,
//     bool  enabled= true,
//     List<TextInputFormatter>? inputFormat,
//     FormFieldValidator<String>? validator,
//     String? initialValue,
//     TextInputType fieldType = TextInputType.text,
//     int maxLines = 1,
//     ValueChanged<String>? onChangeFunc,
//     Widget? suffixIcon}) {
//   initialValue ??= "";
//   fieldType = (maxLines > 1) ? TextInputType.multiline : TextInputType.text;
//   return _chiTextFormField2(
//     inputFormat: inputFormat,
//       initialValue: initialValue,
//       fieldType: fieldType,
//       func: func,
//       hintText: hintText,
//       heading: heading,
//       validator: validator,
//       maxLines: maxLines,
//       enabled: enabled,
//       onChangeFunc: onChangeFunc,
//       suffixIcon: suffixIcon);
// }
//
// _chiTextFormField2(
//     {required String heading,
//     required String hintText,
//     required Function func,
//     int maxLines = 1,
//     FormFieldValidator<String>? validator,
//     List<TextInputFormatter>? inputFormat,
//     ValueChanged<String>? onChangeFunc,
//     String initialValue = "",
//     required enabled,
//     TextInputType fieldType = TextInputType.text,
//     Widget? suffixIcon}) {
//   return Column(
//     crossAxisAlignment: CrossAxisAlignment.start,
//     children: [
//       const SizedBox(
//         height: 16.0,
//       ),
//       headingText(heading),
//       const SizedBox(
//         height: 14.0,
//       ),
//       chiTextFormField3(hintText, func, validator, inputFormat, onChangeFunc,
//       enabled: enabled,
//           initialValue: initialValue,
//           fieldType: fieldType,
//           maxLines: maxLines,
//           suffixIcon: suffixIcon)
//     ],
//   );
// }
//
// Widget chiTextFormField3(
//     String hintText,
//     Function func,
//     FormFieldValidator<String>? validator,
//     List<TextInputFormatter>? inputFormat,
//     ValueChanged<String>? onChangeFunc,
//     {String initialValue = "",
//     TextInputType fieldType = TextInputType.text,
//     int maxLines = 1,
//     required enabled,
//     Widget? suffixIcon}) {
//   return TextFormField(
//     enabled: enabled,
//       key: Key(initialValue),
//       inputFormatters: inputFormat,
//       onChanged: onChangeFunc,
//       textCapitalization: TextCapitalization.sentences,
//       initialValue: initialValue,
//       keyboardType: fieldType,
//       validator: validator,
//       maxLines: maxLines,
//       // #f7f7f7
//       decoration: InputDecoration(
//         suffixIcon: suffixIcon,
//         contentPadding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
//         filled: true,
//         fillColor: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
//         hintText: hintText,
//         hintStyle: const TextStyle(
//             fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
//         focusedBorder: const OutlineInputBorder(
//             borderSide: BorderSide(
//           color: Colors.orange,
//         )),
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(
//             color: Colors.grey.shade300,
//             //Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
//           ),
//         ),
//         errorBorder: OutlineInputBorder(
//           borderSide: const BorderSide(color: Colors.red),
//           borderRadius: BorderRadius.circular(12.0),
//         ),
//         focusedErrorBorder: OutlineInputBorder(
//           borderSide: const BorderSide(color: Colors.red),
//           borderRadius: BorderRadius.circular(12.0),
//         ),
//       ),
//       onSaved: (String? value) {
//         func(value);
//       });
// }

// focusedBorder: const OutlineInputBorder(
//           borderRadius: BorderRadius.all(Radius.circular(8.0)),
//           borderSide: BorderSide(
//             color: Colors.orange,
//           )),
//       enabledBorder: OutlineInputBorder(
//         borderSide: BorderSide(color: Colors.grey.shade300),
//         borderRadius: const BorderRadius.all(
//           Radius.circular(8.0),
//         ),
//       ),
//       errorBorder: OutlineInputBorder(
//         borderSide: const BorderSide(color: Colors.red),
//         borderRadius: BorderRadius.circular(8.0),
//       ),
//       focusedErrorBorder: OutlineInputBorder(
//         borderSide: const BorderSide(color: Colors.red),
//         borderRadius: BorderRadius.circular(8.0),
//       ),

Widget chiSaveButton(String title, VoidCallback func) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 16.0),
    child: SizedBox(
      width: double.infinity,
      height: 45.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.orange[600],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            )),
        onPressed: func,
        child: Text(
          title,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
        ),
      ),
    ),
  );
}

AppBar chiAppBar(String title, BuildContext context,
    {dynamic obj, List<Widget>? actions}) {
  return AppBar(
      shadowColor: const Color.fromRGBO(0x29, 0x00, 0x00, 1.0).withOpacity(0.3),
      // elevation: 0.3,
      backgroundColor: Colors.white,
      title: Text(
        title,
        style: GoogleFonts.poppins(
          fontSize: 16.0,
          fontWeight: FontWeight.w600,
          color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
        ),
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context, obj);
        },
        icon: const Icon(
          Icons.arrow_back,
          color: Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
        ),
      ),
      actions: actions);
}

TextField searchField() {
  return const TextField(
    decoration: InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
      // fillColor: Colors.grey[200],
      prefixIcon: Icon(Icons.search),
      filled: true,
      hintText: "Search...",
      hintStyle: TextStyle(
          fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
          borderSide: BorderSide(
            color: Colors.orange,
          )),
      // enabledBorder: OutlineInputBorder(
      //   borderSide: BorderSide(color: Colors.grey.shade400),
      //   borderRadius: BorderRadius.all(
      //     Radius.circular(12.0),
      //   ),
      // ),
      // errorBorder: OutlineInputBorder(
      //   borderSide:  BorderSide(color: Colors.red),
      //   borderRadius: BorderRadius.circular(12.0),
      // ),
      // focusedErrorBorder: OutlineInputBorder(
      //   borderSide:  BorderSide(color: Colors.red),
      //   borderRadius: BorderRadius.circular(12.0),
      // ),
    ),
  );
}
