import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

Dialog imagePickerDialog(context, VoidCallback galleryPicker,
    VoidCallback cameraPicker, VoidCallback delete) {
  return Dialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)), //this right here
    child: SizedBox(
      height: MediaQuery.of(context).size.height / 4,
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 22.0, left: 24.0, right: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Add Picture',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
              const SizedBox(
                height: 20.0,
              ),
              GestureDetector(
                onTap: () async {
                  Navigator.of(context).pop();
                  cameraPicker();
                },
                child: const Text('Take a photo',
                    style:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16)),
              ),
              const SizedBox(
                height: 20.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  galleryPicker();
                },
                child: const Text('Choose from library',
                    style:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'CANCEL',
                        style: TextStyle(
                            color: Colors.orange.shade800, fontSize: 13),
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    ),
  );
}


// onUpdateImage(BuildContext context) {
//     final ImagePicker picker = ImagePicker();

//   showDialog(
//     context: context,
//     builder: (BuildContext dialogContext) =>
//         imagePickerDialog(dialogContext, () async {
//       try {
//         final pickedFile = await picker.pickImage(
//           source: ImageSource.gallery,
//         );
//         cropImage(pickedFile);
//       } catch (e) {
//         debugPrint('$e');
//       }
//     }, () async {
//       try {
//         final pickedFile = await picker.pickImage(
//           source: ImageSource.camera,
//         );
//         cropImage(pickedFile);
//       } catch (e) {
//         debugPrint('$e');
//       }
//     }, () async {
//       try {
//       } catch (e) {
//         print(e);
//       }
//     }),
//   );
// }
