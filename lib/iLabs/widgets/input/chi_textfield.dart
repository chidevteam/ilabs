import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class CHITextField extends StatelessWidget {
  String hintText;
  String heading;
  Function func;
  FocusNode? focusNode;
  TextInputAction? textInputAction;
  void Function()? onEditingComplete;
  FormFieldValidator<String>? validator;
  List<TextInputFormatter>? inputFormat;
  ValueChanged<String>? onChangeFunc;
  String? initialValue;
  TextInputType fieldType = TextInputType.text;
  int maxLines = 1;
  bool enabled;
  Widget? suffixIcon;
  CHITextField(
      {this.heading = "",
      this.hintText = "",
      required this.func,
      this.validator,
      this.inputFormat,
      this.onEditingComplete,
      this.focusNode,
      this.onChangeFunc,
      this.initialValue = "",
      this.fieldType = TextInputType.text,
      this.maxLines = 1,
      this.enabled = true,
      this.suffixIcon,
      this.textInputAction,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10.0,
        ),
        headingText(heading),
        const SizedBox(
          height: 10.0,
        ),
        TextFormField(
            textInputAction: textInputAction,
            focusNode: focusNode,
            autofocus: true,
            onEditingComplete: onEditingComplete,
            enableInteractiveSelection: false,
            cursorColor: const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
            enabled: enabled,
            key: Key(initialValue ?? ""),
            inputFormatters: inputFormat,
            onChanged: onChangeFunc,
            textCapitalization: TextCapitalization.sentences,
            initialValue: initialValue ?? "",
            keyboardType: fieldType,
            validator: validator,
            maxLines: maxLines,
            style: GoogleFonts.poppins(
              fontSize: 14.0,
              color: Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
            ),

            // #f7f7f7
            decoration: InputDecoration(
              suffixIcon: suffixIcon,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              filled: true,
              fillColor: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
              hintText: hintText,
              hintStyle: const TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 12,
                  color: Colors.grey),

// #fd7848
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
                ),
                borderRadius: BorderRadius.circular(3.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
                ),
                borderRadius: BorderRadius.circular(3.0),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.red),
                borderRadius: BorderRadius.circular(3.0),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.red),
                borderRadius: BorderRadius.circular(3.0),
              ),
            ),
            onSaved: (String? value) {
              func(value);
            }),
      ],
    );
  }

  Text headingText(String title) {
    return Text(
      title,
      style: GoogleFonts.poppins(
          color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
          fontSize: 12.0,
          fontWeight: FontWeight.w400),
    );
  }
}
