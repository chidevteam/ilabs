import 'package:flutter/material.dart';

class AddCircledButtonWithLabel extends StatelessWidget {
  GestureTapCallback onTap;
  String label;
   AddCircledButtonWithLabel({required this.label,required this.onTap, Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   GestureDetector(
      onTap: onTap,
      child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                Container(height: 18,width: 18,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xfffd7848)
                ),
                child: const Center(
                  child:  Icon(Icons.add,
                  color: Colors.white,
                  size: 16,),
                ),
                ),
                const SizedBox(width: 10,),
                 Text(label,
                style:const TextStyle(
                  color: Color(0xfffd7848),
                  fontWeight: FontWeight.w500,
                  fontSize: 15
                ),)
    
              ],),
    );
  }
}