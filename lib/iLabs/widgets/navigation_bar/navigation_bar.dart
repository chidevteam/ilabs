import 'package:flutter/material.dart';

Widget bottomNavigationBar(
    List<Map> items, int currIndex, void Function(int value) func) {
  List<BottomNavigationBarItem> wList = [];
  for (int i = 0; i < items.length; i++) {
    BottomNavigationBarItem w = BottomNavigationBarItem(
      icon: Padding(
        padding: EdgeInsets.all(8.0),
        child: Icon(
          items[i]['icon'],
          size: 18.0,
        ),
      ),
      label: items[i]['title'],
    );
    wList.add(w);
  }

  return BottomNavigationBar(
      currentIndex: currIndex,
      type: BottomNavigationBarType.fixed,
      selectedItemColor: Colors.orange,
      unselectedItemColor: Colors.grey,
      selectedFontSize: 12.0,
      unselectedFontSize: 12.0,
      items: wList,
      onTap: func);
}
