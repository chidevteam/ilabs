class BusinessDetail {
  int? id;
  String? businessName;
  String? businessOwnerName;
  String? businessLogoUrl;
  String? businessLogoFile;
  String? businessNumber;
  String? businessAddress;
  String? businessEmail;
  String? businessPhone;
  String? businessMobile;
  String? businessWebsite;

  BusinessDetail({
    this.id,
    this.businessName,
    this.businessOwnerName,
    this.businessLogoUrl,
    this.businessLogoFile,
    this.businessNumber,
    this.businessAddress,
    this.businessEmail,
    this.businessPhone,
    this.businessMobile,
    this.businessWebsite,
  });

  static BusinessDetail fromMap(Map<String, dynamic> resp) {
    return BusinessDetail(
      id: resp['id'],
      businessName: resp['business_name'],
      businessOwnerName: resp['business_owner_name'],
      businessLogoUrl: resp['business_logo_url'],
      businessLogoFile: resp['business_downloaded_logo_file'],
      businessNumber: resp['business_number'],
      businessAddress: resp['business_address'],
      businessEmail: resp['business_email'],
      businessPhone: resp['business_phone'],
      businessMobile: resp['business_mobile'],
      businessWebsite: resp['business_website'],
    );
  }

  Map<String, dynamic> toMap({Map<String, dynamic>? data}) {
    Map<String, dynamic>? data;
    data ??= {};

    if (id != null) {
      data['id'] = id;
    }
    if (businessName != null) {
      data['business_name'] = businessName;
    }
    if (businessOwnerName != null) {
      data['business_owner_name'] = businessOwnerName;
    }
    if (businessLogoUrl != null) {
      data['business_logo_url'] = businessLogoUrl;
    }
    if (businessLogoFile != null) {
      data['business_downloaded_logo_file'] = businessLogoFile;
    }

    if (businessNumber != null) {
      data['business_number'] = businessNumber;
    }
    if (businessAddress != null) {
      data['business_address'] = businessAddress;
    }
    if (businessEmail != null) {
      data['business_email'] = businessEmail;
    }
    if (businessPhone != null) {
      data['business_phone'] = businessPhone;
    }
    if (businessMobile != null) {
      data['business_mobile'] = businessMobile;
    }
    if (businessWebsite != null) {
      data['business_website'] = businessWebsite;
    }

    return data;
  }

  BusinessDetail copy() {
    return BusinessDetail.fromMap(toMap());
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "businessName:: $businessName\n";
    st += "businessOwnerName:: $businessOwnerName\n";
    st += "businessLogoUrl:: $businessLogoUrl\n";
    st += "downloadedLogoFilename:: $businessLogoFile\n";
    st += "businessNumber:: $businessNumber\n";
    st += "businessAddress:: $businessAddress\n";
    st += "businessEmail:: $businessEmail\n";
    st += "businessPhone:: $businessPhone\n";
    st += "businessMobile:: $businessMobile\n";
    st += "businessWebsite:: $businessWebsite\n";
    return st;
  }
}
