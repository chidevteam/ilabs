import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  print(resp);
  History pInst = await getApi();
}

Future<History> getApi() async {
  APIResponse resp = await ApiClient.get<History>(
      endPoint: "/invoices/emailhistory/22058/0/20/id/DESC",
      params: "",
      fromJson: History.fromMap);
  History iList = resp["data"];
  print(iList);
  return iList;
}
