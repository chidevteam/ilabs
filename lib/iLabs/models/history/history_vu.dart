import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../widgets/input/chi_text_field.dart';
import 'history_vm.dart';

class HistoryVU extends ViewModelBuilderWidget<HistoryVM> {
  const HistoryVU(this.tempInvoiceID, {Key? key}) : super(key: key);
  final int tempInvoiceID;

  @override
  Widget builder(BuildContext context, HistoryVM viewModel, Widget? child) {
    return Scaffold(
      body: Center(
          child: ListView.builder(
              itemCount: viewModel.data.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: Icon(Icons.email_sharp),
                  title: Text(
                      '${viewModel.data[index].status} - ${viewModel.data[index].email}'),
                  subtitle: Text(
                      '${viewModel.data[index].date} ${viewModel.data[index].datetime}'),
                );
              })
          // child: Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: <Widget>[
          //     Text(
          //       viewModel.timeTaken,
          //       style: const TextStyle(fontSize: 20),
          //     ),
          //     Column(
          //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //       children: [
          //         Row(
          //           children: [
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDF3(invoiceData);
          //                 },
          //                 child: const Text('design3')),
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDFSimpleInvoice();
          //                 },
          //                 child: const Text('Simple')),
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDFMultiPage();
          //                 },
          //                 child: const Text('Multi')),
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDFRealMultiPage();
          //                 },
          //                 child: const Text('Multi')),
          //           ],
          //         ),
          //         Row(
          //           children: [
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDF1(invoiceData);
          //                 },
          //                 child: const Text('design1')),
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDF2(invoiceData);
          //                 },
          //                 child: const Text('design2')),
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDF3(invoiceData);
          //                 },
          //                 child: const Text('design3')),
          //             ElevatedButton(
          //                 onPressed: () {
          //                   viewModel.createPDF4(invoiceData);
          //                 },
          //                 child: const Text('design4')),
          //           ],
          //         )
          //       ],
          //     ),
          //     Expanded(child: invoicePDFview(viewModel.fileName)),
          //   ],
          // ),
          ),
    );
  }

  @override
  HistoryVM viewModelBuilder(BuildContext context) {
    HistoryVM vm = HistoryVM(tempInvoiceID);
    return vm;
  }
}
