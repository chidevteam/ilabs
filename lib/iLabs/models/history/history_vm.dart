// import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../services/api_client.dart';
import 'model.dart';

class HistoryVM extends BaseViewModel {
  List<HistoryData> data = [];
  int invoiceID;

  HistoryVM(this.invoiceID) {
    getApi();
  }

  Future<History> getApi() async {
    APIResponse resp = await ApiClient.get<History>(
        endPoint: "/invoices/emailhistory/$invoiceID/0/20/id/DESC",
        params: "",
        fromJson: History.fromMap);
    History iList = resp["data"];
    data = iList.data;
    print(iList);
    notifyListeners();
    return iList;
  }
}
