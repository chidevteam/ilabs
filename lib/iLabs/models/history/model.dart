// Model Review Complete: Ahmad

class History {
  late int pages;
  late String count;
  List<HistoryData> data = [];

  History({this.pages = 0, this.count = "0", required this.data});

  History.fromMap(Map<String, dynamic> json) {
    pages = json['pages'];
    count = json['count'];
    for (var historyData in json['data']) {
      data.add(HistoryData.fromMap(historyData));
    }
    // if (json['data'] != null) {
    //   data = <HistoryData>[];
    //   json['data'].forEach((v) {
    //     data.add(HistoryData.fromMap(v));
    //   });
    // }
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = {};
    data['pages'] = pages;
    data['count'] = count;
    data['data'] = this.data.map((v) => v.toMap()).toList();

    return data;
  }

  History copy() {
    return History.fromMap(toMap());
  }

  @override
  String toString() {
    String st = '';
    st += "pages:: $pages\n";
    st += "count:: $count\n";
    st += "data:: $data\n";
    return st;
  }
}

class HistoryData {
  int? id;
  String? status;
  String? email;
  String? date2;
  String? date;
  String? datetime;

  HistoryData(
      {this.id, this.status, this.email, this.date2, this.date, this.datetime});

  HistoryData.fromMap(Map<String, dynamic> json) {
    id = json['id'];
    status = json['status'];
    email = json['email'];
    date2 = json['date2'];
    date = json['date'];
    datetime = json['datetime'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['status'] = status;
    data['email'] = email;
    data['date2'] = date2;
    data['date'] = date;
    data['datetime'] = datetime;
    return data;
  }

  HistoryData copy() {
    return HistoryData.fromMap(toMap());
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "status:: $status\n";
    st += "email:: $email\n";
    st += "date2:: $date2\n";
    st += "date:: $date\n";
    st += "datetime:: $datetime\n";
    return st;
  }
}

// class PaymentInstructions {
//   int? id;
//   late String paypalEmail;
//   late String checkPayableTo;
//   late String bankTransfer;

//   PaymentInstructions({
//     this.id,
//     this.paypalEmail = "",
//     this.checkPayableTo = "",
//     this.bankTransfer = "",
//   });

//   PaymentInstructions.fromMap(Map<String, dynamic> resp) {
//     id = resp['id'];
//     paypalEmail = resp['paypal_email'] ?? "";
//     checkPayableTo = resp['check_payable_to'] ?? "";
//     bankTransfer = resp['bank_transfer'] ?? "";
//   }

//   Map<String, dynamic> toMap() {
//     Map<String, dynamic> map = {};
    
//     map["id"] = id;
//     map["paypal_email"] = paypalEmail;
//     map["check_payable_to"] = checkPayableTo;
//     map["bank_transfer"] = bankTransfer;
//     return map;
//   }

//   PaymentInstructions copy() {
//     return PaymentInstructions.fromMap(toMap());
//   }

//   @override
//   String toString() {
//     String st = '';
//     st += "id:: $id\n";
//     st += "paypalEmail:: $paypalEmail\n";
//     st += "checkPayableTo:: $checkPayableTo\n";
//     st += "bankTransfer:: $bankTransfer\n";
//     return st;
//   }
// }
