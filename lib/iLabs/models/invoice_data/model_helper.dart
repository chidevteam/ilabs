// import 'model.dart';
// import '../business/model.dart';
// import '../client/model.dart';
// import '../payment_instructions/model.dart';

// // BusinessDetail getBusinessDetail(InvoiceData invoiceData) {
// //   return BusinessDetail(
// //     id: invoiceData.businessId!,
// //     businessName: invoiceData.businessName!,
// //     businessOwnerName: invoiceData.businessOwnerName!,
// //     businessLogoUrl: invoiceData.businessLogoUrl!,
// //     businessNumber: invoiceData.businessNumber!,
// //     businessAddress: invoiceData.businessAddress!,
// //     businessEmail: invoiceData.businessEmail!,
// //     businessPhone: invoiceData.businessPhone!,
// //     businessMobile: invoiceData.businessMobile!,
// //     businessWebsite: invoiceData.businessWebsite!,
// //   );
// // }

// // void setBusinessDetail(InvoiceData invoiceData, BusinessDetail bDetail) {
// //   invoiceData.businessId = bDetail.id as int;
// //   invoiceData.businessName = bDetail.businessName;
// //   invoiceData.businessOwnerName = bDetail.businessOwnerName;
// //   invoiceData.businessLogoUrl = bDetail.businessLogoUrl;
// //   invoiceData.businessNumber = bDetail.businessNumber;
// //   invoiceData.businessAddress = bDetail.businessAddress;
// //   invoiceData.businessEmail = bDetail.businessEmail;
// //   invoiceData.businessPhone = bDetail.businessPhone;
// //   invoiceData.businessMobile = bDetail.businessMobile;
// //   invoiceData.businessWebsite = bDetail.businessWebsite;
// // }

// // Client getClient(InvoiceData invoiceData) {
// //   return Client(
// //       name: invoiceData.clientName ?? "",
// //       email: invoiceData.clientEmail ?? "",
// //       fax: invoiceData.clientFax ?? "",
// //       clientId: invoiceData.clientId,
// //       phone: invoiceData.clientPhone ?? "",
// //       mobile: invoiceData.clientMobile ?? "",
// //       address: invoiceData.clientAddress ?? "");
// // }

// // setClient(InvoiceData invoiceData, Client? value) {
// //   invoiceData.clientName = value?.name;
// //   invoiceData.clientEmail = value?.email;
// //   invoiceData.amount = value?.amount;
// //   invoiceData.clientFax = value?.fax;
// //   invoiceData.clientId = value?.clientId;
// //   invoiceData.clientPhone = value?.phone;
// //   invoiceData.clientMobile = value?.mobile;
// //   invoiceData.clientAddress = value?.address;
// // }

// PaymentInstructions getPaymentInstructions(InvoiceData invoiceData) {
//   PaymentInstructions paymentInst = PaymentInstructions(
//       paypalEmail: invoiceData.paypalEmail,
//       checkPayableTo: invoiceData.checkPayableTo,
//       bankTransfer: invoiceData.bankTransfer);
//   return paymentInst;
// }

// setPaymentInstructions(
//     InvoiceData invoiceData, PaymentInstructions? paymentInst) {
//   invoiceData.paypalEmail = paymentInst?.paypalEmail;
//   invoiceData.checkPayableTo = paymentInst?.checkPayableTo;
//   invoiceData.bankTransfer = paymentInst?.bankTransfer;
// }
