import 'dart:io';
import '../business/model.dart';
import '../client/model.dart';
import '../payment_instructions/model.dart';
import '../tax/model.dart';
// import 'model_helper.dart' as model_helper;

class InvoiceData {
  int? id;
  late InvoiceInfo invoiceInfo;
  List<Photo> photos;
  String? signature;
  String? notes;
  bool paid;
  String? paidDate;
  String? signDate;
  late TaxModel taxModel;
  // String? taxType;
  // String? taxLabel;
  // String? taxRate;
  // bool? taxInclusive;
  String? discountType;
  num? discountAmount;
  PaymentInstructions paymentInstructions;
  // String? paypalEmail;
  // String? checkPayableTo;
  // String? bankTransfer;
  BusinessDetail businessDetail; ////
  Client client; ////
  int? colourId;
  List<Payments> payments;
  List<ItemData>? items;
  String? subTotal;
  String? discount;
  String? tax;
  String? amount;
  String? total;
  String? balanceDue;

  InvoiceData(
      {this.id,
      required this.invoiceInfo,
      required this.photos,
      this.signature,
      this.notes,
      this.paid = false,
      this.paidDate,
      this.signDate,
      // this.taxType,
      // this.taxLabel,
      // this.taxRate,
      // this.taxInclusive,
      required this.taxModel,
      this.discountType,
      this.discountAmount,
      required this.paymentInstructions,
      // this.paypalEmail,
      // this.checkPayableTo,
      // this.bankTransfer,
      required this.businessDetail,
      required this.client,
      this.colourId,
      this.payments= const [],
      this.items,
      this.subTotal,
      this.discount,
      this.tax,
      this.amount,
      this.total,
      this.balanceDue});

  static InvoiceData fromMap(Map<String, dynamic> resp) {
    List<Photo> localPhotos = [];
    if (resp['photos'] != null) {
      localPhotos =
          List<Photo>.from(resp['photos'].map((item) => Photo.fromMap(item)));
    }
    List<Payments> localPayments = [];
    if (resp['payments'] != null) {
      localPayments = List<Payments>.from(
          resp['payments'].map((item) => Payments.fromMap(item)));
    }
    List<ItemData> localItems = [];
    if (resp['items'] != null) {
      localItems = List<ItemData>.from(
          resp['items'].map((item) => ItemData.fromMap(item)));
    }
    return InvoiceData(
      id: resp['id'],
      invoiceInfo: InvoiceInfo(
        number: resp['number'],
        isInvoice: resp['is_invoice'],
        date: resp['date'],
        terms: resp['terms'],
        dueDate: resp['due_date'],
        poNumber: resp['po_number'],
      ),
      photos: localPhotos,
      signature: resp['signature'] ?? "",
      notes: resp['notes'],
      paid: resp['paid'],
      paidDate: resp['paid_date'],
      signDate: resp['sign_date'],
      taxModel: TaxModel(
          taxType: resp['tax_type'],
          taxLabel: resp['tax_label'],
          taxRate: resp['tax_rate'],
          taxInclusive: resp['tax_inclusive']),
      discountType: resp['discount_type'],
      discountAmount: resp['discount_amount'],
      paymentInstructions: PaymentInstructions(
        paypalEmail: resp['paypal_email'], //Done
        checkPayableTo: resp['check_payable_to'], //Done
        bankTransfer: resp['bank_transfer'], //Done
      ),
      businessDetail: BusinessDetail(
        id: resp['business_id'],
        businessName: resp['business_name'],
        businessOwnerName: resp['business_owner_name'],
        businessLogoUrl: resp['business_logo_url'],
        businessNumber: resp['business_number'],
        businessAddress: resp['business_address'],
        businessEmail: resp['business_email'],
        businessPhone: resp['business_phone'],
        businessMobile: resp['business_mobile'],
        businessWebsite: resp['business_website'],
      ),
      client: Client(
          clientId: resp['client_id'],
          name: resp['client_name'],
          email: resp['client_email'],
          fax: resp['client_fax'],
          phone: resp['client_phone'],
          mobile: resp['client_mobile'],
          address: resp['client_address']),
      colourId: resp['colour_id'],
      payments: localPayments,
      items: localItems,
      subTotal: resp['sub_total'],
      discount: resp['discount'],
      tax: resp['tax'].toString(),
      amount: resp['amount'].toString(),
      total: resp['total'].toString(),
      balanceDue: resp['balance_due'].toString(),
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = {};
    data['id'] = id;
    if (invoiceInfo.number != null) data['number'] = invoiceInfo.number;
    if (invoiceInfo.isInvoice != null) {
      data['is_invoice'] = invoiceInfo.isInvoice;
    }
    if (invoiceInfo.date != null) data['date'] = invoiceInfo.date;
    if (invoiceInfo.terms != null) data['terms'] = invoiceInfo.terms;
    if (invoiceInfo.dueDate != null) data['due_date'] = invoiceInfo.dueDate;
    if (invoiceInfo.poNumber != null) data['po_number'] = invoiceInfo.poNumber;

    data['photos'] = photos.map((v) => v.toMap()).toList();

    if (signature != null) data['signature'] = signature;
    if (notes != null) data['notes'] = notes;
    data['paid'] = paid;
    if (paidDate != null) data['paid_date'] = paidDate;
    if (signDate != null) data['sign_date'] = signDate;

    data['tax_type'] = taxModel.taxType;
    data['tax_label'] = taxModel.taxLabel;
    data['tax_rate'] = taxModel.taxRate;
    data['tax_inclusive'] = taxModel.taxInclusive;
    if (discountType != null) data['discount_type'] = discountType;
    if (discountAmount != null) data['discount_amount'] = discountAmount;

    if (paymentInstructions.paypalEmail != null)
      data['paypal_email'] = paymentInstructions.paypalEmail;
    if (paymentInstructions.checkPayableTo != null)
      data['check_payable_to'] = paymentInstructions.checkPayableTo;
    if (paymentInstructions.bankTransfer != null)
      data['bank_transfer'] = paymentInstructions.bankTransfer;
    // data['payment_notes'] = paymentNotes;

    // businessDetail.toMap(data: data);

    if (businessDetail.id != null) data['business_id'] = businessDetail.id;
    if (businessDetail.businessName != null)
      data['business_name'] = businessDetail.businessName;
    if (businessDetail.businessOwnerName != null)
      data['business_owner_name'] = businessDetail.businessOwnerName;
    if (businessDetail.businessLogoUrl != null)
      data['business_logo_url'] = businessDetail.businessLogoUrl;
    if (businessDetail.businessNumber != null)
      data['business_number'] = businessDetail.businessNumber;
    if (businessDetail.businessAddress != null)
      data['business_address'] = businessDetail.businessAddress;
    if (businessDetail.businessEmail != null)
      data['business_email'] = businessDetail.businessEmail;
    if (businessDetail.businessPhone != null)
      data['business_phone'] = businessDetail.businessPhone;
    if (businessDetail.businessMobile != null)
      data['business_mobile'] = businessDetail.businessMobile;
    if (businessDetail.businessWebsite != null)
      data['business_website'] = businessDetail.businessWebsite;

    if (client.clientId != null) data['client_id'] = client.clientId;
    if (client.name != null) data['client_name'] = client.name;
    if (client.email != null) data['client_email'] = client.email;
    if (client.phone != null) data['client_phone'] = client.phone;
    if (client.mobile != null) data['client_mobile'] = client.mobile;
    if (client.fax != null) data['client_fax'] = client.fax;
    if (client.address != null) data['client_address'] = client.address;
    if (colourId != null) data['colour_id'] = colourId;
    if (payments != null) {
      data['payments'] = payments.map((v) => v.toMap()).toList();
    } else {
      data['payments'] = [];
    }
    if (items != null) {
      data['items'] = items!.map((v) => v.toMap()).toList();
    } else {
      data['items'] = [];
    }
    if (subTotal != null) data['sub_total'] = subTotal ?? "0.00";
    if (discount != null) data['discount'] = discount ?? 0;
    if (tax != null) data['tax'] = tax;
    if (amount != null) data['amount'] = amount ?? "0.00";
    if (total != null) data['total'] = total ?? "0.00";
    if (balanceDue != null) data['balance_due'] = balanceDue ?? "0.00";
    return data;
  }

  // BusinessDetail getBusinessDetail() => businessDetail;
  //
  // void setBusinessDetail(BusinessDetail bDetail) {
  //   businessDetail = bDetail;
  // }

  Client getClient() => client;

  void setClient(Client? clientx) {
    if (clientx != null) {
      client = clientx;
    } else {
      client = Client();
    }
  }

  PaymentInstructions getPaymentInstructions() => paymentInstructions;

  void setPaymentInstructions(PaymentInstructions paymentInst) {
    paymentInstructions = paymentInst;
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "InvoiceInfo:: $invoiceInfo";
    st += "photos:: $photos\n";
    st += "signature:: $signature\n";
    st += "notes:: $notes\n";
    st += "paid:: $paid\n";
    st += "paidDate:: $paidDate\n";
    st += "signDate:: $signDate\n";
    // st += "taxType:: ${taxModel.taxType\n";
    // st += "taxLabel:: ${taxModel.taxLabel\n";
    // st += "taxRate:: ${taxModel.taxRate\n";
    // st += "taxInclusive:: $taxModel.taxInclusive\n";
    st += "discountType:: $discountType\n";
    st += "discountAmount:: $discountAmount\n";
    st += "===========================payment============\n";
    st += "paymentInstructions: $paymentInstructions";
    st += "===========================payment==end==========\n";
    // st += "paypalEmail:: $paypalEmail\n";
    // st += "checkPayableTo:: $checkPayableTo\n";
    // st += "bankTransfer:: $bankTransfer\n";
    st += "buinesssDetail:: $businessDetail";
    st += "client:: $client";
    st += "colourId:: $colourId\n";
    st += "payments:: $payments\n";
    st += "items:: $items\n";
    st += "subTotal:: $subTotal\n";
    st += "discount:: $discount\n";
    st += "tax:: $tax\n";
    st += "amount:: $amount\n";
    st += "total:: $total\n";
    st += "balanceDue:: $balanceDue\n";
    return st;
  }
}

class ItemData {
  num? id;
  late int quantity;
  late String discountType;
  late num discountAmount;
  num? itemId;
  late String description;
  late num unitCost;
  late bool taxable;
  late num taxValue;
  late String additionalDetail;
  late String? total;

  ItemData(
      {this.id,
      this.quantity = 1,
      this.discountType = "Percentage",
      this.discountAmount = 0,
      this.itemId,
      this.description = "",
      this.unitCost = 0,
      this.taxable = true,
      this.taxValue = 0,
      this.additionalDetail = "",
      this.total = ""});

  ItemData.fromMap(Map<String, dynamic> json) {
    id = json['id'];
    quantity = json['quantity'] ?? 1;
    discountType = json['discount_type'] ?? "Percentage";
    discountAmount = json['discount_amount'] ?? 0;
    itemId = json['item_id'];
    description = json['description'] ?? "";
    unitCost = json['unit_cost'] ?? 0;
    taxable = json['taxable'] ?? false;
    taxValue = json['tax_value'] ?? 0;
    additionalDetail = json['additional_detail'] ?? "";
    total = json['total'] ?? "";
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['quantity'] = this.quantity;
    data['discount_type'] = this.discountType;
    data['discount_amount'] = this.discountAmount;
    data['item_id'] = this.itemId;
    data['description'] = this.description;
    data['unit_cost'] = this.unitCost;
    data['taxable'] = this.taxable;
    data['tax_value'] = this.taxValue;
    data['additional_detail'] = this.additionalDetail;
    data['total'] = this.total;
    return data;
  }

  ItemData copy() {
    return ItemData.fromMap(toMap());
  }

  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "quantity:: $quantity\n";
    st += "discountType:: $discountType\n";
    st += "discountAmount:: $discountAmount\n";
    st += "itemId:: $itemId\n";
    st += "description:: $description\n";
    st += "unitCost:: $unitCost\n";
    st += "taxable:: $taxable\n";
    st += "taxValue:: $taxValue\n";
    st += "additionalDetail:: $additionalDetail\n";
    st += "total:: $total\n";
    return st;
  }
}

class Payments {
  num? id;
  num? amount;
  String? method;
  String? date;
  String? note;

  Payments(
    this.id,
    this.amount,
    this.method,
    this.date,
    this.note,
  );

  static Payments fromMap(Map<String, dynamic> resp) {
    return Payments(
      resp['id'] as num?,
      resp['amount'] as num?,
      resp['method'] as String?,
      resp['date'] as String?,
      resp['note'] as String?,
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = {};
    data['id'] = id;
    data['amount'] = amount;
    data['method'] = method;
    data['date'] = date;
    data['note'] = note;
    return data;
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "amount:: $amount\n";
    st += "method:: $method\n";
    st += "date:: $date\n";
    st += "note:: $note\n";
    return st;
  }
}

class Photo {
  num? id;
  String? photoUrl;
  String? description;
  String? additionalDetails;
  File? downloadedImage;
  // Image? image;
  String? imagePathX;

  Photo(this.id, this.photoUrl, this.description, this.additionalDetails,
      {this.imagePathX});

  static Photo fromMap(Map<String, dynamic> resp) {
    return Photo(resp['id'] as num?, resp['photo_url'] as String?,
        resp['description'] as String?, resp['additional_details'] as String?,
        // image: Image.network(Utils.imgPath + resp['photo_url'] as String));
        imagePathX: resp['photo_url']); // Utils.imgPath +
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = {};
    data['id'] = id;
    data['photo_url'] = photoUrl;
    data['description'] = description;
    data['additional_details'] = additionalDetails;
    return data;
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "photoUrl:: $photoUrl\n";
    st += "description:: $description\n";
    st += "additionalDetails:: $additionalDetails\n";
    return st;
  }
}

class InvoiceInfo {
  String? number;
  bool? isInvoice;
  String? date;
  String? terms;
  String? dueDate;
  String? dateFormat;
  String? poNumber;
  InvoiceInfo(
      {this.number,
      this.date,
      this.isInvoice,
      this.terms,
      this.dueDate,
      this.dateFormat, //String dateFormat = 'yyyy-MM-dd';
      this.poNumber});

  static InvoiceInfo fromMap(Map<String, dynamic> resp) {
    return InvoiceInfo(
      number: resp['number'],
      isInvoice: resp['is_invoice'],
      date: resp['date'],
      terms: resp['terms'],
      dueDate: resp['due_date'],
      poNumber: resp['po_number'],
      dateFormat: "dd-MM-yyyy",
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    map['number'] = number;
    map['is_invoice'] = isInvoice;
    map['date'] = date;
    map['terms'] = terms;
    map['due_date'] = dueDate;
    map['po_number'] = poNumber;
    // map['number'] = number;
    return map;
  }

  @override
  String toString() {
    String st = '';
    st += "number:: $number\n";
    st += "isInvoice:: $isInvoice\n";
    st += "date:: $date\n";
    st += "terms:: $terms\n";
    st += "duedate:: $dueDate\n";
    st += "PO NUmber:: $poNumber\n";
    return st;
  }
}

// class TaxModel {
//   int t;
//   bool isInclusiveToggle;
//   String selectedOption;
//   TaxModel(this.rate, this.isInclusiveToggle, this.selectedOption);
// }

// Model Review Complete: Ahmad

 
