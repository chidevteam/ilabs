//// Code automatically generated by FlutterGen

class ReportPaidList {
  // final List<String>? years;
  final String? year;
  final num? clients;
  final num? invoices;
  final num? amount;
  final List<ReportPaid>? data;

  ReportPaidList(
    // this.years,
    this.year,
    this.clients,
    this.invoices,
    this.amount,
    this.data,
  );

  static ReportPaidList fromMap(Map<String, dynamic> resp) {
    // List<String>? years;
    // if (resp['years'] != null){
    //   years = List<String>.from(
    //       resp['years'].map((item) => str.fromMap(item)));
    // }
    List<ReportPaid>? data;
    if (resp['data'] != null) {
      data = List<ReportPaid>.from(
          resp['data'].map((item) => ReportPaid.fromMap(item)));
    }

    return ReportPaidList(
      // resp['years'] == null ? null: years,
      resp['year'].toString() as String?,
      resp['clients'] as num?,
      resp['invoices'] as num?,
      resp['amount'] as num?,
      resp['data'] == null ? null : data,
    );
  }

  @override
  String toString() {
    String st = '';
    //  st += "years:: ${years}\n";
    st += "year:: ${year}\n";
    st += "clients:: ${clients}\n";
    st += "invoices:: ${invoices}\n";
    st += "amount:: ${amount}\n";
    st += "data:: ${data}\n";
    return st;
  }
}

class ReportPaid {
  final String? months;
  final String? clients;
  final String? invoices;
  final String? amount;

  ReportPaid(
    this.months,
    this.clients,
    this.invoices,
    this.amount,
  );

  static ReportPaid fromMap(Map<String, dynamic> resp) {
    return ReportPaid(
      resp['months'] as String?,
      resp['clients'] as String?,
      resp['invoices'] as String?,
      resp['amount'].toString() as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "months:: ${months}\n";
    st += "clients:: ${clients}\n";
    st += "invoices:: ${invoices}\n";
    st += "amount:: ${amount}\n";
    return st;
  }
}
