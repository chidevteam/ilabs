Map<String, dynamic> response = {
  "years": ["2021", "2022"],
  "year": "2021",
  "clients": 1,
  "invoices": 1,
  "amount": 0,
  "data": [
    {
      "months": "December",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "November",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "October",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "September",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "August",
      "clients": "1",
      "invoices": "1",
      "amount": 0
    },
    {
      "months": "July",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "June",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "May",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "April",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "March",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "February",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    },
    {
      "months": "January",
      "clients": "0",
      "invoices": "0",
      "amount": "0"
    }
  ]
}
;