// ignore_for_file: avoid_print
import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  String email = "vocalmatrix@gmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  print(resp);
  await testApi();
}

Future testApi() async {
  APIResponse resp = await ApiClient.get<ReportPaidList>(
      endPoint: "/reports",
      params: "/paid/2021",
      fromJson: ReportPaidList.fromMap);
  ReportPaidList iList = resp["data"];
  print(iList);
}
