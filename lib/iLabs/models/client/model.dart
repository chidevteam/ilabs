// Model Review Complete: Ahmad
import 'dart:io';

class ClientList {
  File? file;
  int? pages;
  int? currentPage;
  String? count;
  List<Client>? data;

  ClientList({
    this.pages,
    this.currentPage,
    this.count,
    this.data,
  });

  static ClientList fromMap(Map<String, dynamic> resp) {
    List<Client>? data;
    if (resp['data'] != null) {
      data =
          List<Client>.from(resp['data'].map((item) => Client.fromMap(item)));
    }

    return ClientList(
      pages: resp['pages'],
      count: resp['count'],
      currentPage: resp['currentPage'] ?? 0,
      data: resp['data'] == null ? null : data,
    );
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = {};
    data['pages'] = pages;
    data['count'] = count;
    data['currentPage'] = currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toMap()).toList();
    }
    return data;
  }

  @override
  String toString() {
    String st = '';
    st += "pages:: $pages\n";
    st += "count:: $count\n";
    st += "data:: $data\n";
    return st;
  }
}

class Client {
  int? clientId;
  String? name;
  String? email;
  String? phone;
  String? mobile;
  String? fax;
  String? address;
  String? invoicesCount;
  String? amount;

  Client({
    this.name,
    this.email,
    this.invoicesCount,
    this.amount,
    this.fax,
    this.address,
    this.phone,
    this.mobile,
    this.clientId,
  });

  static Client fromMap(Map<String, dynamic> resp) {
    return Client(
        clientId: resp['id'],
        name: resp['name'],
        email: resp['email'],
        invoicesCount: resp['invoices_count'],
        amount: resp['amount'] ?? "0",
        fax: resp['fax'],
        address: resp['address'],
        phone: resp['phone'],
        mobile: resp['mobile']);
  }

  Map toMap() {
    Map<String, dynamic> map = {};
    map["id"] = clientId;
    map["name"] = name;
    map['invoices_count'] = invoicesCount;
    map['amount'] = amount;
    map["email"] = email;
    map["phone"] = phone;
    map["mobile"] = mobile;
    map["fax"] = fax;
    map["address"] = address;
    return map;
  }

  @override
  String toString() {
    String st = '';
    st += "name:: $name\n";
    st += "email:: $email\n";
    st += "invoicesCount:: $invoicesCount\n";
    st += "amount:: $amount\n";
    st += "fax:: $fax\n";
    st += "address:: $address\n";
    st += "phone:: $phone\n";
    st += "mobile:: $mobile\n";
    return st;
  }
}
