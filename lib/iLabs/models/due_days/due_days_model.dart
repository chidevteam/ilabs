import 'package:intl/intl.dart';

class DueDaysModel {
  String dueDays;

  DueDaysModel({this.dueDays = "999 days"});

  static DueDaysModel fromMap(Map<String, dynamic> resp) {
    return DueDaysModel(
      dueDays: resp['due_date'],
    );
  }

  Map<String, dynamic> toMap() {
    return {'due_date': dueDays};
  }

  DueDaysModel copy() {
    return fromMap(toMap());
  }

  static List<String> dayTerms = [
    'None',
    'Due on receipt',
    'Next Day',
    '2 Days',
    '4 Days',
    '5 Days',
    '6 Days',
    '7 Days',
    '14 Days',
    '20 Days',
    '21 Days',
    '30 Days',
    '45 Days',
    '60 Days',
    '90 Days',
    '120 Days',
    '365 Days',
  ];

  // static String getTermDaysFromNumDays(int numDays) {
  //   switch (numDays) {
  //     case 0:
  //       return "Custom";
  //       break;
  //   }
  // }

  static int getSelectedIndex(String value) {
    // int selIndex = -1;
    for (int i = 0; i < dayTerms.length; i++) {
      if (value == dayTerms[i]) {
        return i;
      }
    }
    return -1;
  }

  static int? getNumDaysFromTermDays(String dterm) {
    print("============$dterm===");

    // print("hello $list");

    switch (dterm) {
      case 'None':
        return 0;
      case 'Next Day':
        return 1;
      case 'Due on receipt':
        return null;
    }
    List<String> list = dterm.split(" ");
    if (list.length == 2) {
      return int.parse(list[0]);
    } else {
      return null;
    }
  }


  static String? getTermFromNumberOfDays(int days) {

    switch (days) {
      case 0:
        return 'None';
      case 1:
        return 'Next Day';
      default:
        {
          if (days <= 365) {
           for(int i = 3; i<dayTerms.length; i++)
             {
               if(dayTerms[i].split(" ")[0]==days.toString()) {
                 return dayTerms[i];
               }
             }
           return 'Custom';
          }
        }
    }
  }




}

main1() {
  String dateFormat = 'dd-MMM-yyyy';
  DateTime now = DateTime.now();
  String d = DateFormat(dateFormat).format(now);

  DateTime dt = DateFormat(dateFormat).parse(d);
  int ts = dt.millisecondsSinceEpoch + 86400 * 1000 * 365;

  DateTime dateNew = DateTime.fromMillisecondsSinceEpoch(ts);

  print("hello worldd!! >$d< $dt $dateNew");
}

main2() {
  print("my hellos");
  for (int i = 0; i < DueDaysModel.dayTerms.length; i++) {
    String dTerm = DueDaysModel.dayTerms[i];
    int? k = DueDaysModel.getNumDaysFromTermDays(dTerm);
    print("$i  $k  ${DueDaysModel.dayTerms[i]}");
  }
}


// class DueDaysModal {
//   String? terms;
//   int? days;
//
//   DueDaysModal({this.terms, this.days});
//
//   DueDaysModal.fromJson(Map<String, dynamic> json) {
//     terms = json['terms'];
//     days = json['days'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['terms'] = this.terms;
//     data['days'] = this.days;
//     return data;
//   }
// }
