import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../widgets/input/chi_text_field.dart';
import 'due_days_vm.dart';
import 'due_days_model.dart';

class DueDaysVU extends ViewModelBuilderWidget<DueDaysVM> {
  const DueDaysVU(this.tempInputModel, {Key? key, this.callAPI = true})
      : super(key: key);

  final DueDaysModel tempInputModel;
  final bool callAPI;
  @override
  Widget builder(BuildContext context, DueDaysVM viewModel, Widget? child) {
    return Scaffold(
        appBar: chiAppBar('Due Date', context),
        body: ListView.builder(
            shrinkWrap: true,
            itemCount: viewModel.daysList.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  viewModel.onSave(index, callAPI);
                  Navigator.pop(context, viewModel.outputModel);
                },
                child: ListTile(
                  title: Row(
                    children: [
                      Text(viewModel.daysList[index]),
                      const Spacer(),
                      Visibility(
                          visible: tempInputModel.dueDays ==
                              viewModel.daysList[index],
                          child: const Icon(
                            Icons.check,
                            color: Colors.black,
                          ))
                    ],
                  ),
                ),
              );
            }));
  }

  @override
  DueDaysVM viewModelBuilder(BuildContext context) {
    return DueDaysVM(tempInputModel);
  }
}
