import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/models/userdefaults/model.dart';
import 'package:stacked/stacked.dart';
import '../../services/api_client.dart';
import 'due_days_model.dart';

class DueDaysVM extends BaseViewModel {
  List<String> daysList = DueDaysModel.dayTerms;
  DueDaysModel? inputModel;
  DueDaysModel outputModel = DueDaysModel();

  DueDaysVM(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  onSave(int index, bool callAPI) async {
    outputModel.dueDays = daysList[index];
    if (callAPI) {
      Map<String, dynamic> req = {"due_date": outputModel.dueDays};
      APIResponse? resp = await ApiClient.post<UserDefault>(
          request: req,
          endPoint: "/userdefaults",
          fromJson: UserDefault.fromMap);
      debugPrint("$resp['data']");
    }
    // notifyListeners();
  }
}
