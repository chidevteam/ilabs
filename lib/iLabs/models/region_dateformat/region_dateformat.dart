class DateFormatIL {
  final int? id;
  final String? name;
  final String? format;
  final String date;
  final String mask;
// [{id: 3, name: "mm-dd-yy (02-27-22)", format: "mm-dd-yy", date: "03-04-21", mask: "##/##/####"},…//   final String? body;
  const DateFormatIL(
      {required this.id,
      required this.name,
      required this.format,
      required this.date,
      required this.mask});

  // Post({
  //   @required this.userId,
  //   @required this.id,
  //   @required this.title,
  //   @required this.body,
  // });

  factory DateFormatIL.fromJson(Map<String, dynamic> json) {
    return DateFormatIL(
      id: json['id'] as int,
      name: json['name'] as String,
      format: json['format'] as String,
      date: json['date'] as String,
      mask: json['mask'] as String,
    );
  }
}
