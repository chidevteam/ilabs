Map<String, dynamic> response = {
  "result": [
    {
      "id": 11,
      "name": "A red first",
      "primaryCode": "#E54B4B",
      "secondaryCode": "#FDF1F1",
      "isDelete": false
    },
    {
      "id": 3,
      "name": "B orange second",
      "primaryCode": "#FD7E49",
      "secondaryCode": "#FEF1EB",
      "isDelete": false
    },
    {
      "id": 6,
      "name": "C yellow third",
      "primaryCode": "#EBBE4E",
      "secondaryCode": "#FEFBF3",
      "isDelete": false
    },
    {
      "id": 12,
      "name": "D Green fourth",
      "primaryCode": "#32B67A",
      "secondaryCode": "#EFFAF5",
      "isDelete": false
    },
    {
      "id": 13,
      "name": "E dark green fifth",
      "primaryCode": "#3b755f",
      "secondaryCode": "#eff4f2",
      "isDelete": false
    },
    {
      "id": 15,
      "name": "F blue sixth",
      "primaryCode": "#167c81",
      "secondaryCode": "#ecf5f5",
      "isDelete": false
    },
    {
      "id": 5,
      "name": "G blue seventh",
      "primaryCode": "#165c81",
      "secondaryCode": "#ECF2F5",
      "isDelete": false
    },
    {
      "id": 16,
      "name": "H blue eight",
      "primaryCode": "#384d9e",
      "secondaryCode": "#eff1f7",
      "isDelete": false
    },
    {
      "id": 4,
      "name": "I purple 9",
      "primaryCode": "#9457aa",
      "secondaryCode": "#f6f1f8",
      "isDelete": false
    },
    {
      "id": 7,
      "name": "K  Brown 10",
      "primaryCode": "#A78B73",
      "secondaryCode": "#F2EEEA",
      "isDelete": false
    },
    {
      "id": 10,
      "name": "Z Gray",
      "primaryCode": "#808080",
      "secondaryCode": "#E0E0E0",
      "isDelete": false
    },
    {
      "id": 2,
      "name": "m Philippine Brown ",
      "primaryCode": "#641E16",
      "secondaryCode": "#EEE7E6",
      "isDelete": false
    },
    {
      "id": 9,
      "name": "y Black",
      "primaryCode": "#000000",
      "secondaryCode": "#E4E4E4",
      "isDelete": false
    }
  ]
}
;