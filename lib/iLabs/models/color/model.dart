class ColorList {
  final List<TemplateColor>? colorList;
  Map<int, int> colorMapper = {};

  ColorList(
    this.colorList,
  );

  static ColorList fromMap(Map<String, dynamic> resp) {
    List<TemplateColor>? result;
    if (resp['result'] != null) {
      result = [];
      for (Map<String, dynamic> item in resp['result']) {
        TemplateColor t = TemplateColor.fromMap(item);
        result.add(t);
        // print("t");
      }
      // result = List<TemplateColor>.from(resp['result'].map((item) {
      //   return TemplateColor.fromMap(item);
      // }));
    }

    return ColorList(
      resp['result'] == null ? null : result,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "result:: $colorList\n";
    return st;
  }
}

class TemplateColor {
  final int id;
  final String name;
  final String primaryCode;
  final String secondaryCode;
  final bool isDelete;

  TemplateColor(
    this.id,
    this.name,
    this.primaryCode,
    this.secondaryCode,
    this.isDelete,
  );

  static TemplateColor fromMap(Map<String, dynamic> resp) {
    return TemplateColor(
      resp['id'] as int,
      resp['name'] as String,
      resp['primaryCode'] as String,
      resp['secondaryCode'] as String,
      resp['isDelete'] as bool,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "name:: $name\n";
    st += "primaryCode:: $primaryCode\n";
    st += "secondaryCode:: $secondaryCode\n";
    st += "isDelete:: $isDelete\n";
    return st;
  }
}
