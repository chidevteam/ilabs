class InvoiceItemList {
  int pages = 0;
  int count = 0;
  List<InvoiceItem> invoiceList = [];

  InvoiceItemList.fromJson(Map<String, dynamic> json) {
    pages = json["pages"];
    count = json["count"];
    for (Map<String, dynamic> invItem in json["data"]) {
      InvoiceItem iItem = InvoiceItem.fromJson(invItem);
      invoiceList.add(iItem);
    }
  }

  @override
  String toString() {
    return invoiceList.toString();
  }
}

class InvoiceItem {
  int? id;
  String? description;
  double? unitCost;
  bool? taxable;
  String? additionalDetail;

  InvoiceItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    unitCost = json['unit_cost'].toDouble();
    taxable = json['taxable'];
    additionalDetail = json['additional_detail'];
  }

  @override
  String toString() {
    String st = "\n=====================";
    st += "\n ID: $id";
    st += "\n description: $description";
    st += "\n unit_cost: $unitCost";
    return st;
  }
}
