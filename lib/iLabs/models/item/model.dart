class InvoiceItemList {
  int? pages;
  int? count;
  int? currentPage;
  List<InvoiceItem>? data;

  InvoiceItemList({this.currentPage, this.pages, this.count, this.data});

  InvoiceItemList.fromJson(Map<String, dynamic> json) {
    pages = json['pages'];
    count = json['count'];
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = <InvoiceItem>[];
      json['data'].forEach((v) {
        data!.add(InvoiceItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['pages'] = pages;
    data['count'] = count;
    data['current_page'] = currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InvoiceItem {
  int? id;
  String? description;
  double? unitCost;
  bool? taxable; //obs;
  String? additionalDetail;

  InvoiceItem(this.id, this.description, this.unitCost, this.taxable,
      this.additionalDetail);

  InvoiceItem.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id'].toString());
    description = json['description'];
    unitCost = double.parse(json['unit_cost'].toString());
    taxable = json['taxable'];
    additionalDetail = json['additional_detail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['description'] = description;
    data['unit_cost'] = unitCost;
    data['taxable'] = taxable;
    data['additional_detail'] = additionalDetail;
    return data;
  }
}
