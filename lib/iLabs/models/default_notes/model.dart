class DefaultNote {
  String invoiceNote;
  String estimateNote;

  DefaultNote({this.invoiceNote = "", this.estimateNote = ""});

  static DefaultNote fromMap(Map<String, dynamic> resp) {
    return DefaultNote(
      invoiceNote: resp['invoice_note'] ?? "",
      estimateNote: resp['estimate_note'] ?? "",
    );
  }

  Map<String, dynamic> toMap() {
    return {"invoice_note": invoiceNote, "estimate_note": estimateNote};
  }

  DefaultNote copy() {
    // return DefaultNote(invoiceNote: invoiceNote, estimateNote: estimateNote);
    return DefaultNote.fromMap(toMap());
  }

  @override
  String toString() {
    String st = "=== $runtimeType ===\n";
    Map map = toMap();
    for (String key in map.keys) {
      st += "$key :: ${map[key]}\n";
    }
    return st;
  }
}
