// ignore_for_file: avoid_print
import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  print(resp);
  DefaultNote pInst = await getApi();
  await postApi(pInst);
}

Future<DefaultNote> getApi() async {
  APIResponse resp = await ApiClient.get<DefaultNote>(
      endPoint: "/userdefaults", params: "", fromJson: DefaultNote.fromMap);
  DefaultNote iList = resp["data"];
  print(iList);
  return iList;
}

Future postApi(DefaultNote pInst) async {
  // pInst.paypalEmail = "ahmad@paypal.com";
  APIResponse resp = await ApiClient.post<DefaultNote>(
      request: pInst.toMap(),
      endPoint: "/userdefaults",
      fromJson: DefaultNote.fromMap);
  DefaultNote iList = resp["data"];
  print(iList);
}
