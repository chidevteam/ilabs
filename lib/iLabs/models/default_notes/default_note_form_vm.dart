import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'model.dart';
import '../../services/api_client.dart';

class DefaultNoteFormVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  DefaultNote? inputModel;
  DefaultNote outputModel = DefaultNote();

  DefaultNoteFormVM(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  onSave(bool callAPI) async {
    formKey.currentState!.save();
    if (callAPI) {
      APIResponse? resp = await ApiClient.post(
          request: outputModel.toMap(),
          endPoint: "/userdefaults",
          fromJson: DefaultNote.fromMap);
      debugPrint("$resp['data']}");
    }
    notifyListeners();
  }
}
