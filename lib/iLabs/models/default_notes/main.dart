import 'package:flutter/material.dart';
import 'default_notes_form_vu.dart';
import '../../services/api_client.dart';
import 'model.dart';

void main() async {
  debugPrint("Starting Item View");

  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  debugPrint("$resp");

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DefaultNote dNote = DefaultNote();
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.orange),
        home: DefaultNoteFormVU(dNote));
  }
}
