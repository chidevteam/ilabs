import 'package:flutter/material.dart';
import '../../widgets/input/chi_text_field.dart';
import '../../widgets/input/chi_textfield.dart';
import 'default_note_form_vm.dart';
import 'package:stacked/stacked.dart';

import 'model.dart';

class DefaultNoteFormVU extends ViewModelBuilderWidget<DefaultNoteFormVM> {
  // DefaultNote? defaultNoteX;
  const DefaultNoteFormVU(this.note, {Key? key, this.callAPI = true})
      : super(key: key);
  final DefaultNote? note;
  final bool callAPI;

  @override
  Widget builder(
      BuildContext context, DefaultNoteFormVM viewModel, Widget? child) {
    // print('==============${defaultNote!.invoicenote}');
    DefaultNote outModel = viewModel.outputModel;
    return Scaffold(
      appBar: chiAppBar('Default Notes', context),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CHITextField(
                    heading: 'Invoice',
                    hintText: 'Type Invoice Notes',
                    func: (value) => outModel.invoiceNote = value,
                    initialValue: outModel.invoiceNote,
                    validator: null),
                CHITextField(
                    heading: 'Estimates',
                    hintText: 'Type Estimates Notes',
                    func: (value) => outModel.estimateNote = value,
                    initialValue: outModel.estimateNote,
                    validator: null),
                chiSaveButton('SAVE AS DEFAULT', () {
                  viewModel.onSave(callAPI);
                  Navigator.pop(context, outModel);
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  DefaultNoteFormVM viewModelBuilder(BuildContext context) {
    return DefaultNoteFormVM(note);
  }
}
