class TaxModel {
  late String taxType;
  late String taxLabel;
  late String taxRate;
  late bool taxInclusive;

  TaxModel(
      {this.taxType = "",
      this.taxLabel = "",
      this.taxRate = "",
      this.taxInclusive = false});

  TaxModel.fromMap(Map<String, dynamic> resp) {
    taxType = resp['tax_type'] ?? "";
    taxLabel = resp['tax_label'] ?? "";
    taxRate = resp['tax_rate'] ?? "";
    taxInclusive = resp['tax_inclusive'] ?? false;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    // if (id != null)
    map["tax_type"] = taxType;
    // if (paypalEmail.trim() != "")
    map["tax_label"] = taxLabel;
    // if (checkPayableTo.trim() != "")
    map["tax_rate"] = taxRate;
    // if (bankTransfer.trim() != "")
    map["tax_inclusive"] = taxInclusive;
    return map;
  }

  TaxModel copy() {
    return TaxModel.fromMap(toMap());
  }

  @override
  String toString() {
    String st = '';
    st += "taxType:: $taxType\n";
    st += "taxLabel:: $taxLabel\n";
    st += "taxRate:: $taxRate\n";
    st += "taxInclusive:: $taxInclusive\n";
    return st;
  }
}
