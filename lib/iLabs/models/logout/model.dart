class LogOutModel{
  String message;

  LogOutModel(this.message);

  static LogOutModel fromMap(Map<String, dynamic> resp){
    return LogOutModel(
        resp['message'] as String
    );
  }
}