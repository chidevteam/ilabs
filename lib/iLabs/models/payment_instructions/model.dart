// Model Review Complete: Ahmad
class PaymentInstructions {
  int? id;
  late String paypalEmail;
  late String checkPayableTo;
  late String bankTransfer;

  PaymentInstructions({
    this.id,
    this.paypalEmail = "",
    this.checkPayableTo = "",
    this.bankTransfer = "",
  });

  PaymentInstructions.fromMap(Map<String, dynamic> resp) {
    id = resp['id'];
    paypalEmail = resp['paypal_email'] ?? "";
    checkPayableTo = resp['check_payable_to'] ?? "";
    bankTransfer = resp['bank_transfer'] ?? "";
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    // if (id != null)
    map["id"] = id;
    // if (paypalEmail.trim() != "")
    map["paypal_email"] = paypalEmail;
    // if (checkPayableTo.trim() != "")
    map["check_payable_to"] = checkPayableTo;
    // if (bankTransfer.trim() != "")
    map["bank_transfer"] = bankTransfer;
    return map;
  }

  PaymentInstructions copy() {
    return PaymentInstructions.fromMap(toMap());
  }

  @override
  String toString() {
    String st = '';
    st += "id:: $id\n";
    st += "paypalEmail:: $paypalEmail\n";
    st += "checkPayableTo:: $checkPayableTo\n";
    st += "bankTransfer:: $bankTransfer\n";
    return st;
  }
}
