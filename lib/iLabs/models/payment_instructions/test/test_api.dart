// ignore_for_file: avoid_print
import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  print(resp);
  PaymentInstructions pInst = await getApi();
  await postApi(pInst);
}

Future<PaymentInstructions> getApi() async {
  APIResponse resp = await ApiClient.get<PaymentInstructions>(
      endPoint: "/paymentinstructions",
      params: "",
      fromJson: PaymentInstructions.fromMap);
  PaymentInstructions iList = resp["data"];
  print(iList);
  return iList;
}

Future postApi(PaymentInstructions pInst) async {
  pInst.paypalEmail = "ahmad@paypal.com";
  APIResponse resp = await ApiClient.post<PaymentInstructions>(
      request: pInst.toMap(),
      endPoint: "/paymentinstructions",
      fromJson: PaymentInstructions.fromMap);
  PaymentInstructions iList = resp["data"];
  print(iList);
}
