import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/models/payment_instructions/model.dart';
import 'payment_instruction_form-vu.dart';
import '../../services/api_client.dart';

void main() async {
  debugPrint("Starting Item View");

  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  debugPrint("$resp");

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PaymentInstructions pInst = PaymentInstructions();
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.orange),
        home: const PaymentInstructionFormVU(null));
  }
}
