import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../services/api_client.dart';
import 'model.dart';

class PaymentInstructionFormVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  PaymentInstructions? inputModel;
  PaymentInstructions outputModel = PaymentInstructions();

  PaymentInstructionFormVM(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  savePaymentInstructions(bool callAPI) async {
    formKey.currentState!.save();

    if (callAPI) {
      APIResponse? resp = await ApiClient.post<PaymentInstructions>(
          request: outputModel.toMap(),
          endPoint: "/paymentinstructions",
          fromJson: null);
      setBusy(false);
    }
    // notifyListeners();
  }
}




  // getPaymentInstructions() async {
  //   APIResponse resp = await ApiClient.get<PaymentInstructions>(
  //       endPoint: "/paymentinstructions",
  //       params: "",
  //       fromJson: PaymentInstructions.fromMap);
  //   paymentInstuctions = resp["data"];
  //   notifyListeners();
  // }