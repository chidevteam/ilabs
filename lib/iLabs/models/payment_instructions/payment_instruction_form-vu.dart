import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../models/payment_instructions/model.dart';
import '../../widgets/input/chi_text_field.dart';
import '../../widgets/input/chi_textfield.dart';
import 'payment_instruction_form-vm.dart';

class PaymentInstructionFormVU
    extends ViewModelBuilderWidget<PaymentInstructionFormVM> {
  const PaymentInstructionFormVU(this.tempInputModel,
      {Key? key, this.callAPI = false})
      : super(key: key);

  final bool callAPI;
  final PaymentInstructions? tempInputModel;

  @override
  Widget builder(
      BuildContext context, PaymentInstructionFormVM viewModel, Widget? child) {
    return Scaffold(
      appBar: chiAppBar('Payment Instructions', context),
      body: Padding(
        padding: const EdgeInsets.only(top: 18.0, right: 14.0, left: 14.0),
        child: SingleChildScrollView(
          child: makeForm(viewModel, context),
        ),
      ),
    );
  }

  Form makeForm(PaymentInstructionFormVM viewModel, BuildContext context) {
    return Form(
      key: viewModel.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CHITextField(
              heading: 'Paypal Email',
              hintText: 'Type Paypal Email',
              func: (value) => viewModel.outputModel.paypalEmail = value,
              // maxline: 1,
              validator: null,
              initialValue: viewModel.outputModel.paypalEmail),
          CHITextField(
              heading: 'Make Checks Payable To',
              hintText: 'Type you or your business name ',
              func: (value) => viewModel.outputModel.checkPayableTo = value,
              validator: null,
              // maxline: 1,
              initialValue: viewModel.outputModel.checkPayableTo),
          CHITextField(
              heading: 'Bank Transfer',
              hintText: 'Enter bank account and other details here',
              func: (value) => viewModel.outputModel.bankTransfer = value,
              validator: null,
              // maxline: 5,
              initialValue: viewModel.outputModel.bankTransfer),
          chiSaveButton('Save', () {
            viewModel.savePaymentInstructions(callAPI);
            Navigator.pop(context, viewModel.outputModel);
          })
        ],
      ),
    );
  }

  @override
  PaymentInstructionFormVM viewModelBuilder(BuildContext context) {
    PaymentInstructionFormVM vm = PaymentInstructionFormVM(tempInputModel);
    return vm;
  }
}
