


import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleSigninApi{
  final googleSignIn = GoogleSignIn();

  GoogleSignInAccount? user;

  Future<GoogleSignInAccount?> googleLogin() async {
    final googleUser = await googleSignIn.signIn();
    if(googleUser == null) return null;
    user = googleUser;
    return user;
  }

  Future googleLogout() async{
    googleSignIn.disconnect();
    FirebaseAuth.instance.signOut();
  }
}