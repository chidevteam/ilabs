import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../Utils/utils.dart';
import '../language/generated/locale_keys.g.dart';
import '../login/login_controller.dart';
import '../login_or_register/login_or_register_vu.dart';
import '../widgets/authentication/auth_buttons.dart';
import '../widgets/authentication/text_field.dart';
import '../widgets/my_back_button.dart';

class LoginScreen extends StatelessWidget {
  LoginController controller = Get.put(LoginController());
  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          backgroundColor: Color(0xfffafafa),
          body: WillPopScope(
            onWillPop: () {
              Utils.pushReplacement(context, const LoginOrRegisterScreen());
              return Future.value(false);
            },
            child: Stack(
              children: [
                orientation == Orientation.portrait
                    ? portraitView(context)
                    : landscapeView(context),
                Obx(
                  () => (controller.isLoading.value)
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : const SizedBox(),
                )
              ],
            ),
          )),
    );
  }

  Widget portraitView(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 30,
      ),
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: controller.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  MyBackButton(
                    onPressed: () {
                      Utils.pushReplacement(
                          context, const LoginOrRegisterScreen());
                    },
                  )
                ],
              ),
              loginImage(),
              const SizedBox(
                height: 24.0,
              ),
              welcomeBackHeader(),
              const SizedBox(
                height: 8.0,
              ),
              informationBelowTxt(),
              emailField(),
              passwordField(),
              loginButton(context),
              forgotPasswordButton(context),
              SizedBox(
                height: 45,
              ),
              loginWithSocialMedia(),
              googleFacebookImage(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget landscapeView(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(40.0),
      child: Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      MyBackButton(
                        onPressed: () {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              '/loginOrReg', (Route<dynamic> route) => false);
                          //Navigator.pushNamedAndRemoveUntil(context, '/loginOrReg');
                        },
                      )
                    ],
                  ),
                  loginImage(),
                  const SizedBox(
                    height: 6.0,
                  ),
                  welcomeBackHeader(),
                  loginWithSocialMedia(),
                  googleFacebookImage(context)
                ],
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: controller.formKey,
                child: Column(
                  children: [
                    informationBelowTxt(),
                    emailField(),
                    passwordField(),
                    loginButton(context),
                    forgotPasswordButton(context)
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Padding informationBelowTxt() => Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: Text(
          tr(LocaleKeys.login_view_sub_title),
          style: TextStyle(
            color: Color(0xff707070),
            fontSize: 12.0,
            // fontWeight: FontWeight.w600
          ),
        ),
      );

  Text welcomeBackHeader() => Text(
        tr(LocaleKeys.login_view_title),
        style: const TextStyle(
            color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
      );

  Widget googleFacebookImage(
    BuildContext context,
  ) =>
      googleFacebookButtons(
        context,
        onPressGoogle: () {
          controller.onGoogleSignIn(context);
        },
        onPressFaceBook: () {
          controller.fbLogin(context);
        },
      );

  Widget loginImage() => SvgPicture.asset(
        'iLabsSreens/login_signup/login illustration.svg',
      );

  Widget emailField() => myAuthField(
      label: tr(LocaleKeys.login_view_email_hint),
      textInputType: TextInputType.emailAddress,
      onSaved: (String? value) {
        controller.onEmail(value);
      },
      validator: null
      //validator: controller.onEmailValidator,
      );

  Widget passwordField() => myAuthField(
      label: tr(LocaleKeys.login_view_password_hint),
      textInputType: TextInputType.visiblePassword,
      obsecureField: true,
      onSaved: (String? value) {
        controller.onPassword(value);
      },
      validator: null
      //  validator: controller.onPasswordValidator,
      );

  Widget loginWithSocialMedia() => Padding(
        padding: const EdgeInsets.fromLTRB(30, 12, 30, 12),
        child: Row(
          children: [
            Expanded(
              child: Divider(
                // color: Colors.grey[300],
                color: Color(0xff707070),
                thickness: 1.5,
              ),
            ),
            Text(
              "     ${tr(LocaleKeys.login_view_login_with_socialmedia)}     ",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              textAlign: TextAlign.justify,
              style: TextStyle(
                  color: Color(0xff707070),
                  //color: Colors.grey.shade600,
                  fontSize: 12,
                  fontWeight: FontWeight.w500),
            ),
            Expanded(
              child: Divider(
                //  color: Colors.grey[300],
                color: Color(0xff707070),
                thickness: 1.5,
              ),
            )
          ],
        ),
      );

  Widget loginButton(context) => myAuthButton(
        context,
        buttonColor: Color(0xfffd7848),
        //  buttonColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
        textColor: Colors.white,
        label: tr(LocaleKeys.login_view_login_button),
        onPressed: () {
          controller.onClickItem(context);
        },
      );
}
