import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/Utils/Preferences.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:ilabs/iLabs/home/home_vu.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import '../services/api_client.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'google_sign_in_api.dart';

String prettyPrint(Map json) {
  JsonEncoder encoder = const JsonEncoder.withIndent('  ');
  String pretty = encoder.convert(json);
  return pretty;
}

class LoginController extends GetxController {
  final formKey = GlobalKey<FormState>();
  var email = ''.obs;
  var password = ''.obs;
  RxBool isLoading = false.obs;
  AccessToken? accessToken;
  Map<String, dynamic>? mUserData;
  bool checking = false;
  onEmail(String? value) async {
    email.value = value!.trim();
  }

  String? onEmailValidator(value) {
    if (value == null || value.isEmpty
        // || !isValidEmail(value)
        ) {
      return 'email is required';
    }
    return null;
  }

  bool isValidEmail(String email) {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if (emailRegExp.hasMatch(email)) {
      return true;
    } else {
      return false;
    }
  }

  onPassword(String? value) async {
    password.value = value!.trim();
  }

  String? onPasswordValidator(value) {
    if (value == null || value.isEmpty) {
      return 'password is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    debugPrint('$email.value');
    debugPrint('$password');

    isLoading.value = true;
    // await ApiClient.login(email.value, password.value);
    await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
    // debugPrint("Token: ${ApiClient.sInstance!.mAuthToken}");
    isLoading.value = false;
    await UserPreferences.setAuthToken(ApiClient.instance!.mAuthToken);
    Utils.pushReplacement(context, HomeScreen());
  }

  fbLogin(BuildContext context) async {
    print('fb login button clicked');
    checking = true;
    await fblogin(context);
    //     Navigator.pop(context);
    // Navigator.pushReplacementNamed(context, '/myHomePage');
  }

  void printCredentials() {
    print(
      prettyPrint(accessToken!.toJson()),
    );
  }

  Future<void> fblogin(BuildContext context) async {
    final LoginResult result = await FacebookAuth.instance
        .login(); // by default we request the email and the public profile
    // loginBehavior is only supported for Android devices, for ios it will be ignored
    // final result = await FacebookAuth.instance.login(
    //   permissions: ['email', 'public_profile', 'user_birthday', 'user_friends', 'user_gender', 'user_link'],
    //   loginBehavior: LoginBehavior
    //       .DIALOG_ONLY, // (only android) show an authentication dialog instead of redirecting to facebook app
    // );
    if (result.status == LoginStatus.success) {
      accessToken = result.accessToken;
      print('access token : ---------------------------${result.accessToken}');
      printCredentials();
      // get the user data
      // by default we get the userId, email,name and picture
      final userData = await FacebookAuth.instance.getUserData();
      // final userData = await FacebookAuth.instance.getUserData(fields: "email,birthday,friends,gender,link");
      mUserData = userData;
      Map<String, dynamic> req = {'face_book_token': result.accessToken!.token};
      //
      print('-----------------fb api=--======================');
      APIResponse resp =
          await ApiClient.post(request: req, endPoint: '/auth/facebook');
      print(resp);
      ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
      print(ApiClient.instance!.mAuthToken);
      Navigator.pushReplacementNamed(context, '/myHomePage');
      //Navigator.pushNamed(context, '/myHomePage');
    } else {
      print(
          '---------------------Status: ${result.status}---------------------');
      //Navigator.pop(context);
      print(
          '------------------Message: ${result.message}-----------------------');
    }
    checking = false;
  }

  onGoogleSignIn(BuildContext context) async {
    GoogleSigninApi googleSigninApi = GoogleSigninApi();
    await googleSigninApi.googleLogin();
    //print('user for apiiiiiiiiiiiiiiii: ${googleSigninApi.user!.email}');
    Map<String, dynamic> req = {'email': googleSigninApi.user!.email};
    //
    print('-----------------google api=--======================');
    APIResponse resp =
        await ApiClient.post(request: req, endPoint: '/auth/google');
    print(resp);
    ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
    print(ApiClient.instance!.mAuthToken);
    Navigator.pushNamed(context, '/myHomePage');
  }
}
