// https://invoice___labs.co/api/auth/forgotpassword

// ignore_for_file: unnecessary_brace_in_string_interps

class ForgotPassword {
  ForgotPassword({
    this.message,
  });
  String? message;

  factory ForgotPassword.fromJson(Map<String, dynamic> json) => ForgotPassword(
        message: json["msg"] == null ? null : json["msg"],
      );

  @override
  String toString() {
    String st = '';
    st += "message:: ${message}\n";
    return st;
  }
}
