import 'package:flutter/material.dart';
import 'forgot_password_model.dart';
import '../../services/api_client.dart';
import 'package:stacked/stacked.dart';
// import '../services/api_client.dart';

class ForgetPasswordVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  ForgotPassword? forgotPassword;

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }

    Map<String, dynamic> req = {
      "message": 'message',
    };
    APIResponse? resp = await ApiClient.post(
        request: req, endPoint: "/auth/forgotpassword", fromJson: null);
    forgotPassword = resp["data"];

    notifyListeners();
  }
}
