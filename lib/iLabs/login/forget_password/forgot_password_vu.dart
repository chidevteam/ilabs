import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'forgot_password_model.dart';
import 'forgot_password_vm.dart';

class ForgetPasswordScreen extends ViewModelBuilderWidget<ForgetPasswordVM> {
  ForgotPassword? forgotPassword;
  ForgetPasswordScreen(this.forgotPassword, {Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, ForgetPasswordVM viewModel, Widget? child) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
          top: 30,
        ),
        child: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                iconButtonHeader(context),
                image(),
                const SizedBox(
                  height: 22.0,
                ),
                forgetPasswordTxt(),
                const SizedBox(
                  height: 8.0,
                ),
                restPasswordTxt(),
                email(viewModel),
                getEmailButton(viewModel, context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // ************loginWidget*********

  Row iconButtonHeader(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
            size: 18,
          ),
        ),
      ],
    );
  }

  Padding restPasswordTxt() {
    return Padding(
      padding: const EdgeInsets.only(top: 6.0),
      child: Text(
        'Enter Your Email Below to Reset Your Password',
        style: TextStyle(
            color: Colors.grey.shade700,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
      ),
    );
  }

  Text forgetPasswordTxt() {
    return const Text(
      'Forget Password!',
      style: TextStyle(
          color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
    );
  }

  Widget image() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: Image.network(
        'https://www.billdu.com/wp-content/uploads/2018/04/Billdu_How-to-avoid-double-invoicing.png',
        width: 112.0,
        height: 112.0,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget email(
    ForgetPasswordVM viewModel,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(18.0, 16, 18.0, 0),
      child: TextFormField(
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Get Email',
            labelStyle: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Colors.grey.shade500),
          ),
          initialValue: forgotPassword == null ? '' : forgotPassword!.message),
    );
  }

  Widget getEmailButton(ForgetPasswordVM viewModel, context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 28.0, 20.0, 12.0),
      child: SizedBox(
        width: double.infinity,
        height: 44.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              )),
          onPressed: () {
            viewModel.onClickItem(context);
          },
          child: const Text(
            'Login',
            style:
                TextStyle(color: Colors.white, fontSize: 14, letterSpacing: 1),
          ),
        ),
      ),
    );
  }

  @override
  ForgetPasswordVM viewModelBuilder(BuildContext context) {
    return ForgetPasswordVM();
  }
}
