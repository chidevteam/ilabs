// import 'package:flutter/material.dart';

List<int> listFromHex(String hexString) {
  List<int> color = [];
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));

  int v = int.parse(buffer.toString(), radix: 16);
  for (int i = 0; i < 4; i++) {
    int rem = v & 255;
    v = v ~/ 256;
    color.add(rem);
  }
  return color;

  // return Color(int.parse(buffer.toString(), radix: 16));
}

main() {
  List v = listFromHex("#F38010");
  print(v);
}
