// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader {
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String, dynamic> en_US = {
    "switch_eng": "Switch Eng",
    "switch_urdu": "Switch Urdu",
    "login_register_view": {
      "title": "Invoice Labs Invoicing Made Easy",
      "sub_title":
          "Invoice Labs makes invoicing simple and effective. We allows you to create professional invoices with our invoice generator and estimates within seconds, so that you can focus on your work",
      "login_button": "Login",
      "createaccount_button": "Create Account",
      "continue_button": "Continue as Guest"
    },
    "login_view": {
      "title": "Welcome Back!",
      "sub_title": "Enter information Below to Login your Account",
      "email_hint": "Email",
      "password_hint": "Password",
      "login_button": "Login",
      "forgot_password": "Forgot Password?",
      "login_with_socialmedia": "Or login with social media"
    },
    "welcome_view": {
      "title": "Welcome!",
      "sub_title": "Enter information Below to Create your Account",
      "email_hint": "Email",
      "password_hint": "Password",
      "confirm_password_hint": "Confirm Password",
      "create_button": "Create",
      "signup_with_socialmedia": "Or Signup with social media"
    },
    "continue_as_guest_view": {
      "title": "Business Information",
      "sub_title": "Enter information Below to Complete Account",
      "business_name_heading": "Business Name",
      "business_name_hint": "Type your business name",
      "business_email_heading": "Business Email",
      "business_email_hint": "Type your business email",
      "business_contact_heading": "Business Contact",
      "business_contact_hint": "Enter business contact",
      "continue_button": "Create",
      "ask_me_later_button": "Ask me Later"
    },
    "action_drawer_view": {
      "drawer_header": {
        "business_name": "CHI International",
        "business_email": "chi@chi.com",
        "edit": "Edit"
      },
      "choose_template": "Choose Template",
      "choose_template_screen": {
        "app_bar_title": "Choose Template",
        "save_button": "Save"
      },
      "region": "Region",
      "region_screen": {
        "app_bar_title": "Region",
        "set_language_heading": "Set Language",
        "currency_type_heading": "Currency Type",
        "date_formate_heading": "Date Formate",
        "save_button": "Save"
      },
      "reports": "Reports",
      "reports_screen": {
        "app_bar_title": "Reports",
        "tab_1": {
          "title": "Clients",
          "client_name": "Client Name",
          "invoices": "Invoices",
          "paid": "Paid"
        },
        "tab_2": {
          "title": "Invoices",
          "month": "Month",
          "clients": "Clients",
          "invoices": "Invoices",
          "paid": "Paid"
        }
      },
      "upgrade_Subscription": "Upgrade Subscription",
      "settigs": "Settings",
      "settigs_screen": {
        "app_bar_title": "Settings",
        "payment_inst": "Payment Instructions",
        "tax": "Tax",
        "default_notes": "Default Notes",
        "due_in_days": "Due in (days)",
        "dafault_sign": "Default Signature",
        "invoice_no": "Invoice & Estimate Numbar",
        "customize": "Customize",
        "dafault_email_msg": "Default Email Messages",
        "send_me_copy_of_emails": "Send me copy of emails"
      },
      "switch_account": "Switch Account",
      "contact_us": "Contact Us",
      "contact_us_screen": {
        "app_bar_title": "Contact Us",
        "app_bar_button": "Save",
        "business_heading": "Business Name",
        "business_hint": "Type business name",
        "email_heading": "Email",
        "email_hint": "Type email",
        "phone_heading": "Phone",
        "phone_hint": "Type phone",
        "subject_heading": "Subject",
        "subject_hint": "Type subject",
        "message_heading": "Message",
        "message_hint": "Type message"
      },
      "info": "Info",
      "info_screen": {
        "app_bar_title": "Information",
        "about": "About",
        "term_of_use": "Terms of Use",
        "privacy_policy": "Privacy Policy",
        "help": "Help"
      },
      "logout": "Log Out"
    },
    "main_home_view": {
      "invoices": "Invoices",
      "estimates": "Estimates",
      "clients": "Clients",
      "items": "Items"
    },
    "add_new_item": {
      "title": "Add new Item",
      "itemname": "Item Name*",
      "unitcost": "Unit Cost*",
      "textable": "Textable",
      "saveBtn": "Save"
    },
    "add_new_client": {
      "title": "Create New Client",
      "clientName": "Client Name",
      "clientEmail": "Email",
      "mobileNumber": "Mobile Number",
      "officePhone": "Office Phone",
      "fax": "Fax",
      "officeAddress": "Office Address",
      "addNowBtn": "ADD NOW"
    },
    "msg": "Hello {} in the {} world ",
    "msg_named": "{} are written in the {lang} language",
    "clickMe": "Click me",
    "profile": {
      "reset_password": {
        "label": "Reset Password",
        "username": "Username",
        "password": "password"
      }
    },
    "clicked": {
      "zero": "You clicked {} times!",
      "one": "You clicked {} time!",
      "two": "You clicked {} times!",
      "few": "You clicked {} times!",
      "many": "You clicked {} times!",
      "other": "You clicked {} times!"
    },
    "amount": {
      "zero": "Your amount : {} ",
      "one": "Your amount : {} ",
      "two": "Your amount : {} ",
      "few": "Your amount : {} ",
      "many": "Your amount : {} ",
      "other": "Your amount : {} "
    },
    "gender": {
      "male": "Hi man ;) ",
      "female": "Hello girl :)",
      "with_arg": {"male": "Hi man ;) {}", "female": "Hello girl :) {}"}
    },
    "reset_locale": "Reset Language"
  };
  static const Map<String, dynamic> ur_PK = {
    "switch_eng": "انگریزی سوئچ کریں",
    "switch_urdu": "اردو تبدیل کریں۔",
    "login_register_view": {
      "title": "انوائس لیبز انوائسنگ کو آسان بنا دیا گیا۔",
      "sub_title":
          "انوائس لیبز انوائس کو آسان اور موثر بناتی ہے۔ ہم آپ کو اپنے انوائس جنریٹر کے ساتھ پیشہ ورانہ رسیدیں بنانے اور سیکنڈوں میں تخمینہ لگانے کی اجازت دیتے ہیں، تاکہ آپ اپنے کام پر توجہ مرکوز کر سکیں",
      "login_button": "لاگ ان کریں",
      "createaccount_button": "اکاؤنٹ بنائیں",
      "continue_button": "بطور مہمان جاری رکھیں"
    },
    "login_view": {
      "title": "خوش آمدید!",
      "sub_title": "اپنا اکاؤنٹ لاگ ان کرنے کے لیے نیچے معلومات درج کریں۔",
      "email_hint": "ای میل",
      "password_hint": "پاس ورڈ",
      "login_button": "لاگ ان کریں",
      "forgot_password": "پاسورڈ بھول گے؟",
      "login_with_socialmedia": "یا سوشل میڈیا ورڈ کے ساتھ لاگ ان کریں؟"
    },
    "welcome_view": {
      "title": "خوش آمدید!",
      "sub_title": "اپنا اکاؤنٹ بنانے کے لیے نیچے معلومات درج کریں۔",
      "email_hint": "ای میل",
      "password_hint": "پاس ورڈ",
      "confirm_password_hint": "پاس ورڈ کی تصدیق کریں۔",
      "create_button": "بنانا",
      "signup_with_socialmedia": "یا سوشل میڈیا کے ساتھ سائن اپ کریں۔"
    },
    "continue_as_guest_view": {
      "title": "کاروباری معلومات",
      "sub_title": "اکاؤنٹ مکمل کرنے کے لیے نیچے معلومات درج کریں۔",
      "business_name_heading": "کاروبار کا نام",
      "business_name_hint": "اپنے کاروبار کا نام ٹائپ کریں۔",
      "business_email_heading": "بزنس ای میل",
      "business_email_hint": "اپنا کاروباری ای میل ٹائپ کریں۔",
      "business_contact_heading": "کاروباری رابطہ",
      "business_contact_hint": "کاروباری رابطہ درج کریں۔",
      "continue_button": "بنانا",
      "ask_me_later_button": "بعد میں مجھ سے پوچھیں"
    },
    "action_drawer_view": {
      "drawer_header": {
        "business_name": "CHI انٹرنیشنل",
        "business_email": "chi@chi.com",
        "edit": "ترمیم"
      },
      "choose_template": "ٹیمپلیٹ کا انتخاب کریں۔",
      "choose_template_screen": {
        "app_bar_title": "ٹیمپلیٹ کا انتخاب کریں۔",
        "save_button": "محفوظ کریں۔"
      },
      "region": "علاقہ",
      "region_screen": {
        "app_bar_title": "علاقہ",
        "set_language_heading": "زبان سیٹ کریں۔",
        "currency_type_heading": "کرنسی کی قسم",
        "date_formate_heading": "تاریخ کی شکل",
        "save_button": "محفوظ کریں۔"
      },
      "reports": "رپورٹس",
      "reports_screen": {
        "app_bar_title": "رپورٹس",
        "tab_1": {
          "title": "کلائنٹس",
          "client_name": "کلائنٹ کا نام",
          "invoices": "رسیدیں",
          "paid": "ادا کیا"
        },
        "tab_2": {
          "title": "رسیدیں",
          "month": "مہینہ",
          "clients": "کلائنٹس",
          "invoices": "رسیدیں",
          "paid": "ادا کیا"
        }
      },
      "upgrade_Subscription": "سبسکرپشن کو اپ گریڈ کریں۔",
      "settigs": "ترتیبات",
      "settigs_screen": {
        "app_bar_title": "ترتیبات",
        "payment_inst": "ادائیگی کی ہدایات",
        "tax": "ٹیکس",
        "default_notes": "پہلے سے طے شدہ نوٹس",
        "due_in_days": "(دن) میں واجب الادا",
        "dafault_sign": "ڈافلٹ دستخط",
        "invoice_no": "انوائس اور تخمینہ نمبر",
        "customize": "حسب ضرورت بنائیں",
        "dafault_email_msg": "ڈیفالٹ ای میل پیغامات",
        "send_me_copy_of_emails": "مجھے ای میلز کی کاپی بھیجیں۔"
      },
      "switch_account": "کھاتہ بدلیں",
      "contact_us": "ہم سے رابطہ کریں۔",
      "contact_us_screen": {
        "app_bar_title": "ہم سے رابطہ کریں۔",
        "app_bar_button": "محفوظ کریں۔",
        "business_heading": "کاروبار کا نام",
        "business_hint": "کاروباری نام ٹائپ کریں۔",
        "email_heading": "ای میل",
        "email_hint": "ای میل ٹائپ کریں۔",
        "phone_heading": "فون",
        "phone_hint": "فون ٹائپ کریں۔",
        "subject_heading": "مضمون",
        "subject_hint": "موضوع ٹائپ کریں۔",
        "message_heading": "پیغام",
        "message_hint": "پیغام ٹائپ کریں۔"
      },
      "info": "معلومات",
      "info_screen": {
        "app_bar_title": "معلومات",
        "about": "کے بارے میں",
        "term_of_use": "استعمال کرنے کی شرائط",
        "privacy_policy": "رازداری کی پالیسی",
        "help": "مدد"
      },
      "logout": "لاگ آوٹ"
    },
    "main_home_view": {
      "invoices": "رسیدیں",
      "estimates": "تخمینہ",
      "clients": "کلائنٹس",
      "items": "اشیاء"
    },
    "add_new_item": {
      "title": "عنوان",
      "itemname": "شے کا نام",
      "unitcost": "یونٹ لاگت",
      "textable": "ٹیکسٹ ایبل",
      "saveBtn": "بٹن محفوظ کریں"
    },
    "add_new_client": {
      "title": "نیا کلائنٹ بنائیں",
      "clientName": "کلائنٹ کا نام",
      "clientEmail": "ای میل",
      "mobileNumber": "موبائل فون کانمبر",
      "officePhone": "دفتر فون",
      "fax": "فیکس",
      "officeAddress": "دفتر کا پتہ",
      "addNowBtn": "ابھی شامل کریں۔"
    },
    "msg": "Hello {} in the {} world ",
    "msg_named": "{} are written in the {lang} language",
    "clickMe": "Click me",
    "profile": {
      "reset_password": {
        "label": "Reset Password",
        "username": "Username",
        "password": "password"
      }
    },
    "clicked": {
      "zero": "You clicked {} times!",
      "one": "You clicked {} time!",
      "two": "You clicked {} times!",
      "few": "You clicked {} times!",
      "many": "You clicked {} times!",
      "other": "You clicked {} times!"
    },
    "amount": {
      "zero": "Your amount : {} ",
      "one": "Your amount : {} ",
      "two": "Your amount : {} ",
      "few": "Your amount : {} ",
      "many": "Your amount : {} ",
      "other": "Your amount : {} "
    },
    "gender": {
      "male": "Hi man ;) ",
      "female": "Hello girl :)",
      "with_arg": {"male": "Hi man ;) {}", "female": "Hello girl :) {}"}
    },
    "reset_locale": "Reset Language"
  };
  static const Map<String, dynamic> ru = {
    "title": "Привет!",
    "msg": "Привет! {} добро пожаловать {} мир! ",
    "msg_named": "{} написан на языке {lang}",
    "clickMe": "Нажми на меня",
    "profile": {
      "reset_password": {
        "label": "Сбросить пароль",
        "username": "Логин",
        "password": "Пароль"
      }
    },
    "clicked": {
      "zero": "Ты кликнул {} раз!",
      "one": "Ты кликнул {} раз!",
      "two": "Ты кликнул {} раза!",
      "few": "Ты кликнул {} раз!",
      "many": "Ты кликнул {} раз!",
      "other": "Ты кликнул {} раз!"
    },
    "amount": {
      "zero": "Твой счет : {} ",
      "one": "Твой счет : {} ",
      "two": "Твой счет : {} ",
      "few": "Твой счет : {} ",
      "many": "Твой счет : {} ",
      "other": "Твой счет : {} "
    },
    "gender": {
      "male": "Привет мужык ;) ",
      "female": "Привет девчуля :)",
      "with_arg": {
        "male": "Привет мужык ;) {}",
        "female": "Привет девчуля :) {}"
      }
    },
    "reset_locale": "Сбросить язык"
  };
  static const Map<String, dynamic> en = {
    "title": "Hello",
    "msg": "Hello {} in the {} world ",
    "msg_named": "{} are written in the {lang} language",
    "clickMe": "Click me",
    "profile": {
      "reset_password": {
        "label": "Reset Password",
        "username": "Username",
        "password": "password"
      }
    },
    "clicked": {
      "zero": "You clicked {} times!",
      "one": "You clicked {} time!",
      "two": "You clicked {} times!",
      "few": "You clicked {} times!",
      "many": "You clicked {} times!",
      "other": "You clicked {} times!"
    },
    "amount": {
      "zero": "Your amount : {} ",
      "one": "Your amount : {} ",
      "two": "Your amount : {} ",
      "few": "Your amount : {} ",
      "many": "Your amount : {} ",
      "other": "Your amount : {} "
    },
    "gender": {
      "male": "Hi man ;) ",
      "female": "Hello girl :)",
      "with_arg": {"male": "Hi man ;) {}", "female": "Hello girl :) {}"}
    },
    "reset_locale": "Reset Language"
  };
  static const Map<String, dynamic> ar = {
    "title": "السلام",
    "msg": "السلام عليكم يا {} في عالم {}",
    "msg_named": "{} مكتوبة باللغة {lang}",
    "clickMe": "إضغط هنا",
    "profile": {
      "reset_password": {
        "label": "اعادة تعين كلمة السر",
        "username": "المستخدم",
        "password": "كلمة السر"
      }
    },
    "clicked": {
      "zero": "لم تنقر بعد!",
      "one": "لقد نقرت مرة واحدة!",
      "two": "لقد قمت بالنقر مرتين!",
      "few": " لقد قمت بالنقر {} مرات!",
      "many": "لقد قمت بالنقر {} مرة!",
      "other": "{} نقرة!"
    },
    "amount": {
      "zero": "المبلغ : {}",
      "one": " المبلغ : {}",
      "two": " المبلغ : {}",
      "few": " المبلغ : {}",
      "many": " المبلغ : {}",
      "other": " المبلغ : {}"
    },
    "gender": {
      "male": " مرحبا يا رجل",
      "female": " مرحبا بك يا فتاة",
      "with_arg": {"male": "{} مرحبا يا رجل", "female": "{} مرحبا بك يا فتاة"}
    },
    "reset_locale": "إعادة ضبط اللغة"
  };
  static const Map<String, dynamic> ar_DZ = {
    "login_register_view": {
      "title": "انوائس لیبز انوائسنگ کو آسان بنا دیا گیا۔",
      "sub_title":
          "انوائس لیبز انوائس کو آسان اور موثر بناتی ہے۔ ہم آپ کو اپنے انوائس جنریٹر کے ساتھ پیشہ ورانہ رسیدیں بنانے اور سیکنڈوں میں تخمینہ لگانے کی اجازت دیتے ہیں، تاکہ آپ اپنے کام پر توجہ مرکوز کر سکیں",
      "login_button": "لاگ ان کریں",
      "createaccount_button": "اکاؤنٹ بنائیں",
      "continue_button": "بطور مہمان جاری رکھیں"
    },
    "login_view": {
      "title": "خوش آمدید!",
      "sub_title": "اپنا اکاؤنٹ لاگ ان کرنے کے لیے نیچے معلومات درج کریں۔",
      "email_hint": "ای میل",
      "password_hint": "پاس ورڈ",
      "login_button": "لاگ ان کریں",
      "forgot_password": "پاسورڈ بھول گے؟",
      "login_with_socialmedia": "یا سوشل میڈیا ورڈ کے ساتھ لاگ ان کریں؟"
    },
    "profile": {
      "reset_password": {
        "label": "اعادة تعين كلمة السر",
        "username": "المستخدم",
        "password": "كلمة السر"
      }
    },
    "clicked": {
      "zero": "لم تنقر بعد!",
      "one": "لقد نقرت مرة واحدة!",
      "two": "لقد قمت بالنقر مرتين!",
      "few": " لقد قمت بالنقر {} مرات!",
      "many": "لقد قمت بالنقر {} مرة!",
      "other": "{} نقرة!"
    },
    "amount": {
      "zero": "المبلغ : {}",
      "one": " المبلغ : {}",
      "two": " المبلغ : {}",
      "few": " المبلغ : {}",
      "many": " المبلغ : {}",
      "other": " المبلغ : {}"
    },
    "gender": {
      "male": " مرحبا يا رجل",
      "female": " مرحبا بك يا فتاة",
      "with_arg": {"male": "{} مرحبا يا رجل", "female": "{} مرحبا بك يا فتاة"}
    },
    "reset_locale": "إعادة ضبط اللغة"
  };
  static const Map<String, dynamic> de = {
    "title": "Hallo",
    "msg": "Hallo {} in der {} welt ",
    "msg_named": "{} ist in {lang} geschrieben",
    "clickMe": "Click mich",
    "profile": {
      "reset_password": {
        "label": "Password zurücksetzten",
        "username": "Name",
        "password": "Password"
      }
    },
    "clicked": {
      "zero": "Du hast {} mal geklickt",
      "one": "Du hast {} mal geklickt",
      "two": "Du hast {} mal geklickt",
      "few": "Du hast {} mal geklickt",
      "many": "Du hast {} mal geklickt",
      "other": "Du hast {} mal geklickt"
    },
    "amount": {
      "zero": "Deine Klicks: {}",
      "one": "Deine Klicks: {}",
      "two": "Deine Klicks: {}",
      "few": "Deine Klicks: {}",
      "many": "Deine Klicks: {}",
      "other": "Deine Klicks: {}"
    },
    "gender": {
      "male": "Hi Mann ;) ",
      "female": "Hallo Frau :)",
      "with_arg": {"male": "Hi Mann ;) {}", "female": "Hallo Frau :) {}"}
    },
    "reset_locale": "Sprache zurücksetzen"
  };
  static const Map<String, dynamic> de_DE = {
    "title": "Hallo",
    "msg": "Hallo {} in der {} welt ",
    "msg_named": "{} ist in {lang} geschrieben",
    "clickMe": "Click mich",
    "profile": {
      "reset_password": {
        "label": "Password zurücksetzten",
        "username": "Name",
        "password": "Password"
      }
    },
    "clicked": {
      "zero": "Du hast {} mal geklickt",
      "one": "Du hast {} mal geklickt",
      "two": "Du hast {} mal geklickt",
      "few": "Du hast {} mal geklickt",
      "many": "Du hast {} mal geklickt",
      "other": "Du hast {} mal geklickt"
    },
    "amount": {
      "zero": "Deine Klicks: {}",
      "one": "Deine Klicks: {}",
      "two": "Deine Klicks: {}",
      "few": "Deine Klicks: {}",
      "many": "Deine Klicks: {}",
      "other": "Deine Klicks: {}"
    },
    "gender": {
      "male": "Hi Mann ;) ",
      "female": "Hallo Frau :)",
      "with_arg": {"male": "Hi Mann ;) {}", "female": "Hallo Frau :) {}"}
    },
    "reset_locale": "Sprache zurücksetzen"
  };
  static const Map<String, dynamic> ru_RU = {
    "title": "Привет!",
    "msg": "Привет! {} добро пожаловать {} мир! ",
    "msg_named": "{} написан на языке {lang}",
    "clickMe": "Нажми на меня",
    "profile": {
      "reset_password": {
        "label": "Сбросить пароль",
        "username": "Логин",
        "password": "Пароль"
      }
    },
    "clicked": {
      "zero": "Ты кликнул {} раз!",
      "one": "Ты кликнул {} раз!",
      "two": "Ты кликнул {} раза!",
      "few": "Ты кликнул {} раз!",
      "many": "Ты кликнул {} раз!",
      "other": "Ты кликнул {} раз!"
    },
    "amount": {
      "zero": "Твой счет : {} ",
      "one": "Твой счет : {} ",
      "two": "Твой счет : {} ",
      "few": "Твой счет : {} ",
      "many": "Твой счет : {} ",
      "other": "Твой счет : {} "
    },
    "gender": {
      "male": "Привет мужык ;) ",
      "female": "Привет девчуля :)",
      "with_arg": {
        "male": "Привет мужык ;) {}",
        "female": "Привет девчуля :) {}"
      }
    },
    "reset_locale": "Сбросить язык"
  };
  static const Map<String, Map<String, dynamic>> mapLocales = {
    "en_US": en_US,
    "ur_PK": ur_PK,
    "ru": ru,
    "en": en,
    "ar": ar,
    "ar_DZ": ar_DZ,
    "de": de,
    "de_DE": de_DE,
    "ru_RU": ru_RU
  };
}
