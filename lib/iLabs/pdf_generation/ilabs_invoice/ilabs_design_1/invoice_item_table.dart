import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

PdfLayoutResult drawItemTable(PdfPage page, PdfLayoutResult result) {
  final PdfGrid grid = getGrid();

  PdfLayoutFormat format = PdfLayoutFormat();
  final Size size = page.getClientSize();
  format.paginateBounds = Rect.fromLTWH(0, 0, size.width, size.height - 15);
  //Draw grid into PDF page
  PdfLayoutResult? gridResult = grid.draw(
      page: page,
      bounds: Rect.fromLTWH(
          0, result.bounds.bottom + 15, size.width, size.height - 15));

  final PdfStandardFont h0Font =
      PdfStandardFont(PdfFontFamily.helvetica, 15, style: PdfFontStyle.bold);

  final PdfTextElement element =
      PdfTextElement(text: 'Grand Total: 14740.84', font: h0Font);
  result = element.draw(
      page: gridResult!.page,
      bounds: Rect.fromLTWH(
          0, gridResult.bounds.bottom + 15, size.width, size.height - 15),
      format: PdfLayoutFormat(
          paginateBounds: Rect.fromLTWH(0, 15, size.width, size.height - 15)))!;
  return result;
}

//Create PDF grid and return
PdfGrid getGrid() {
  //Create a PDF grid
  final PdfGrid grid = PdfGrid();
  //Secify the columns count to the grid.
  grid.columns.add(count: 5);
  //Create the header row of the grid.
  final PdfGridRow headerRow = grid.headers.add(1)[0];
  //Set style
  headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(68, 114, 196));
  headerRow.style.textBrush = PdfBrushes.white;
  headerRow.cells[0].value = 'S.No.';
  headerRow.cells[0].stringFormat.alignment = PdfTextAlignment.center;
  headerRow.cells[1].value = 'Product Name';
  headerRow.cells[2].value = 'Price';
  headerRow.cells[3].value = 'Quantity';
  headerRow.cells[4].value = 'Total';
  // headerRow.cells[4].stringFormat.alignment = PdfTextAlignment.center;
  //Add rows
  for (int i = 1; i < 6; i++) {
    addProducts('$i', 'AWC Logo Cap',
        'This cap is really good for most things ', 8.99, 2, 17.98, grid);
  }
  //Apply the table built-in style
  grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable3Accent5);
  //Set gird columns width
  grid.columns[0].width = 50;
  grid.columns[1].width = 250;
  for (int i = 0; i < headerRow.cells.count; i++) {
    headerRow.cells[i].style.cellPadding =
        PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
  }
  for (int i = 0; i < grid.rows.count; i++) {
    final PdfGridRow row = grid.rows[i];
    for (int j = 0; j < row.cells.count; j++) {
      final PdfGridCell cell = row.cells[j];
      if (j == 0 || j == 3) {
        cell.stringFormat.alignment = PdfTextAlignment.center;
      }
      cell.style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }
  }
  return grid;
}

//Create and row for the grid.
void addProducts(String productId, String productName, String productDetail,
    double price, int quantity, double total, PdfGrid grid) {
  final PdfGridRow row = grid.rows.add();
  row.cells[0].value = productId;
  row.cells[1].value = "" + productName + "" + "\n" + productDetail;
  row.cells[2].value = price.toString();
  row.cells[3].value = quantity.toString();
  row.cells[4].value = total.toString();
}
