import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

PdfLayoutResult drawMiniTable1(PdfPage page, PdfLayoutResult result) {
  final PdfGrid grid = getGrid();

  PdfLayoutFormat format = PdfLayoutFormat();
  final Size size = page.getClientSize();
  format.paginateBounds = Rect.fromLTWH(0, 0, size.width, size.height - 15);
  //Draw grid into PDF page
  result = grid.draw(
      page: page,
      bounds: Rect.fromLTWH(
          0, result.bounds.bottom + 15, size.width, size.height - 15))!;

  final PdfStandardFont h0Font =
      PdfStandardFont(PdfFontFamily.helvetica, 15, style: PdfFontStyle.bold);

  final PdfTextElement element =
      PdfTextElement(text: 'Grand Total: 14740.84', font: h0Font);
  result = element.draw(
      page: result.page,
      bounds: Rect.fromLTWH(
          0, result.bounds.bottom + 15, size.width, size.height - 15),
      format: PdfLayoutFormat(
          paginateBounds: Rect.fromLTWH(0, 15, size.width, size.height - 15)))!;
  return result;
}

//Create PDF grid and return
PdfGrid getGrid() {
  final PdfGrid grid = PdfGrid();
  grid.columns.add(count: 5);
  grid.headers.add(1);
  PdfGridRow headerRow = grid.headers[0];

  // headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(0, 200, 200));
  headerRow.style.textBrush = PdfBrushes.red;
  headerRow.cells[0].value = 'S1.No.';
  headerRow.cells[0].stringFormat.alignment = PdfTextAlignment.center;
  headerRow.cells[1].value = 'Product Name';
  headerRow.cells[2].value = 'Price';
  headerRow.cells[3].value = 'Quantity';
  headerRow.cells[4].value = 'Total';
  // headerRow.cells[4].stringFormat.alignment = PdfTextAlignment.center;
  //Add rows
  for (int i = 1; i < 4; i++) {
    addProducts('$i', 'AWC Logo Cap',
        'This cap is really good for most things ', 8.99, 2, 17.98, grid);
  }
  //Apply the table built-in style
  grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable5DarkAccent2);
  //Set gird columns width
  grid.columns[0].width = 50;
  grid.columns[1].width = 250;
  for (int i = 0; i < headerRow.cells.count; i++) {
    headerRow.cells[i].style.cellPadding =
        PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
  }
  for (int i = 0; i < grid.rows.count; i++) {
    final PdfGridRow row = grid.rows[i];
    for (int j = 0; j < row.cells.count; j++) {
      final PdfGridCell cell = row.cells[j];
      if (j == 0 || j == 3) {
        cell.stringFormat.alignment = PdfTextAlignment.center;
      }
      cell.style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }
  }
  return grid;
}

//Create and row for the grid.
void addProducts(String productId, String productName, String productDetail,
    double price, int quantity, double total, PdfGrid grid) {
  final PdfGridRow row = grid.rows.add();
  row.cells[0].value = productId;
  row.cells[1].value = "" + productName + "" + "\n" + productDetail;
  row.cells[2].value = price.toString();
  row.cells[3].value = quantity.toString();
  row.cells[4].value = total.toString();
}

PdfLayoutResult drawMiniTable2(PdfPage page, PdfLayoutResult result) {
//Create a new PDF document
  // PdfDocument document = PdfDocument();
  // page = document.pages.add();
//Create a PdfGrid class
  PdfGrid grid = PdfGrid();

//Add the columns to the grid
  grid.columns.add(count: 3);

//Add header to the grid
  grid.headers.add(1);

//Add the rows to the grid
  PdfGridRow header = grid.headers[0];
  header.cells[0].value = 'Employee ID';
  header.cells[1].value = 'Employee Name';
  header.cells[2].value = 'Salary';

//Add rows to grid
  PdfGridRow row = grid.rows.add();
  row.cells[0].value = 'E01';
  row.cells[1].value = 'Clay';
  row.cells[2].value = '\$10,000';

  row = grid.rows.add();
  row.cells[0].value = 'E02';
  row.cells[1].value = 'Simon';
  row.cells[2].value = '\$12,000';

//Set the grid style
  grid.style = PdfGridStyle(
      cellPadding: PdfPaddings(left: 2, right: 3, top: 4, bottom: 5),
      backgroundBrush: PdfBrushes.blue,
      textBrush: PdfBrushes.white,
      font: PdfStandardFont(PdfFontFamily.timesRoman, 25));
  final Size size = page.getClientSize();
//Draw the grid
  return grid.draw(
      page: page,
      bounds: Rect.fromLTWH(
          0, result.bounds.bottom + 15, size.width, size.height - 15))!;
}

PdfLayoutResult drawMiniTable3(PdfPage page, PdfLayoutResult result) {
  //Create a new PDF document
  // PdfDocument document = PdfDocument();
  // page = document.pages.add();
//Create a PdfGrid class
  PdfGrid grid = PdfGrid();

//Add the columns to the grid
  grid.columns.add(count: 3);

//Add header to the grid
  grid.headers.add(1);

  grid.columns[0].width = 100;
  grid.columns[1].width = 100;
  grid.columns[2].width = 100;
//Add the rows to the grid
  PdfGridRow header = grid.headers[0];
  header.cells[0].value = 'Employee ID';
  header.cells[1].value = 'Employee Name';
  header.cells[2].value = 'Salary';

//Add the styles to specific cell
  header.cells[0].style.stringFormat = PdfStringFormat(
    alignment: PdfTextAlignment.center,
    lineAlignment: PdfVerticalAlignment.middle,
    // wordSpacing: 10
  );
  header.cells[1].style.textPen = PdfPens.mediumVioletRed;
  header.cells[2].style.backgroundBrush = PdfBrushes.yellow;
  header.cells[2].style.textBrush = PdfBrushes.darkOrange;

//Add rows to grid
  PdfGridRow row1 = grid.rows.add();
  row1.cells[0].value = 'E01';
  row1.cells[1].value = 'Clay';
  row1.cells[2].value = '\$10,000';

  PdfGridRow row2 = grid.rows.add();
  row2.cells[0].value = 'E02';
  row2.cells[1].value = 'Simon';
  row2.cells[2].value = '\$12,000';

  grid.style = PdfGridStyle(
    cellPadding: PdfPaddings(left: 2, right: 3, top: 4, bottom: 5),
    // backgroundBrush: PdfBrushes.green,
    // textBrush: PdfBrushes.red,
    // font: PdfStandardFont(PdfFontFamily.timesRoman, 25)
  );

  row1.cells[0].style = PdfGridCellStyle(
    backgroundBrush: PdfBrushes.lightYellow,
    cellPadding: PdfPaddings(left: 2, right: 3, top: 4, bottom: 5),
    font: PdfStandardFont(PdfFontFamily.timesRoman, 12),
    // textBrush: PdfBrushes.orange,
    textPen: PdfPens.orange,
  );

  //Add the style to specific cell
  row2.cells[2].style.borders = PdfBorders(
    // left: PdfPen(PdfColor(240, 0, 0), width: 1),
    // top: PdfPen(PdfColor(0, 240, 0), width: 1),
    bottom: PdfPen(PdfColor(0, 0, 240), width: 1),
    // right: PdfPen(PdfColor(240, 100, 240), width: 1)
  );

  final Size size = page.getClientSize();

  // for (int i = 0; i < grid.rows.count; i++) {
  //   for (int j = 0; j < grid.rows[i].cells.count; j++) {
  //     grid.rows[i].cells[j].style.borders = PdfBorders(
  //         left: PdfPens.transparent,
  //         top: PdfPens.transparent,
  //         // bottom: PdfPens.transparent,
  //         right: PdfPens.transparent);
  //     //PdfPen(PdfColor(240, 100, 240), width: 1)
  //   }
  // }

  return grid.draw(
      page: page,
      bounds: Rect.fromLTWH(
          150, result.bounds.bottom + 15, size.width, size.height - 15))!;
}
