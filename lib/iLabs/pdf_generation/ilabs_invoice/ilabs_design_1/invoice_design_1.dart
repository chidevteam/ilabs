import 'package:flutter/material.dart';
// import 'package:ilabs/iLabs/models/business/model.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
import 'package:ilabs/iLabs/pdf_generation/ilabs_invoice/ilabs_design_1/table5.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../../file_write/save_file_mobile.dart';
import 'letterhead1.dart';
import '../../pdf_utils/pdf_image.dart';
import '../../../models/invoice_data/model.dart';
import 'invoice_top_section.dart';

class InvoiceDesign1 extends Letterhead1 {
  InvoiceDesign1(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero})
      : super(
            pageSize: PdfPageSize.a4,
            pMargins: const Rect.fromLTRB(0, 40, 0, 10),
            letterheadMargins: const Rect.fromLTRB(50, 80, 50, 20),
            indentSize: const Size(50, 50)) {
    debug = false;
  }

  Future<String> createPDF(InvoiceData invData) async {
    await addHeader(document.template.top!);
    addFooter(document.template.bottom!);

    PdfPage page = document.pages.add();

    Size size = page.getClientSize();

    PdfLayoutResult result = newParagraph("Invoice to:", page,
        rect: Rect.fromLTWH(0, 50, size.width * .4, 30), font: contentFont);

    addDateSection(page: result.page, contentFont: contentFont);
    for (int i = 0; i < 1; i++) {
      result = addClientInfo(
        result,
        size.width * .4,
        invData.client,
      );
    }

    // PdfColor bgColor = PdfColor(0, 0, 128);

    // result = drawItemTable(result.page, result);
    // result = drawMiniTable1(result.page, result);
    // result = drawMiniTable2(result.page, result);
    // // result = drawMiniTable3(result.page, result);
    // await drawSignature(result.page, result);
    // result = drawMiniTable4(result.page, result);
      result = drawMiniTable5(result.page, result);



    var bytes = document.save();
    document.dispose();
    return await showPDF(bytes, 'Invoice.pdf');
  }

  PdfLayoutResult addClientInfo(
      PdfLayoutResult result, double width, Client client) {
    result =
        addParagraph("${client.name},", result.page, result, font: h1bFont);

    String text = "";

    text =
        'United States, California, San Mateo, 9920 BridgePointe Parkway, 9365550136';
    result = addParagraph(text, result.page, result, width: width);

    text = 'Date: Monday 04, January 2021';
    result = addParagraph(text, result.page, result, width: width);
    return result;
  }

  Future drawSignature(PdfPage page, PdfLayoutResult result) async {
    PdfBitmap image = await readImageData('assets/signature.png');

    double pageWidth = contentRect.width;
    double imHeight = 70;
    double topOfImage = result.bounds.bottom;
    double imWidth = image.width * imHeight / image.height;

    debugPrint("${image.width} ${image.height}");
    Rect imRect =
        Rect.fromLTWH(pageWidth - imWidth, topOfImage, imWidth, imHeight);
    print("BRECT:  $imRect");

    drawImageRounded(page, image, imRect, 0, fillType: 'fit', debug: debug);
  }
}


// String primaryCode = "#E54B4B";
// String secondaryCode = "#FDF1F1";
// addColors(page, primaryCode, secondaryCode);


// PdfLayoutResult addBusinessInfo(PdfPage page) {}

// rgb(161,75,140)  #a14b8c
// rgb(180,155,177)  #b49bb1
// rgb(207,187,203)  #cfbbcb

// Rect rect = Rect.fromLTWH(0, result.bounds.bottom, size.width, 20);

// page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);
// Rect rb = result.bounds;
// Rect bounds = Rect.fromLTRB(rb.left, rb.top, rb.right, rb.bottom);

// PdfLayoutResult2 result2 = PdfLayoutResult2(result.page, bounds);
