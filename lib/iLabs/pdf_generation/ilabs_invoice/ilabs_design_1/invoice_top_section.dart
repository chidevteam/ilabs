import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

addDateSection({required PdfPage page, required PdfFont contentFont}) {
  int r = 163;
  int g = 73;
  int b = 141;
  int dr = ((255 - 163) ~/ 3);
  int dg = ((255 - 73) ~/ 3);
  int db = ((255 - 141) ~/ 3);
  Size size = page.getClientSize();

  Rect rect = Rect.fromLTWH(size.width * .4, 0, size.width * .2, 60);
  drawPairRect(
      page: page,
      rect: rect,
      contentFont: contentFont,
      title: "Due Date",
      detail: "22-03-2021",
      bgColor: PdfColor(r + 0 * dr, g + 0 * dg, b + 0 * db),
      align: PdfTextAlignment.left);

  rect = Rect.fromLTWH(size.width * .6, 0, size.width * .2, 60);

  drawPairRect(
      page: page,
      rect: rect,
      contentFont: contentFont,
      title: "Due Date",
      detail: "22-03-2022",
      bgColor: PdfColor(r + 1 * dr, g + 1 * dg, b + 1 * db),
      align: PdfTextAlignment.center);

  rect = Rect.fromLTWH(size.width * .8, 0, size.width * .2, 60);

  drawPairRect(
      page: page,
      rect: rect,
      contentFont: contentFont,
      title: "Due Date",
      detail: "22-03-2022",
      bgColor: PdfColor(r + 2 * dr, g + 2 * dg, b + 2 * db),
      align: PdfTextAlignment.right);
}

drawPairRect(
    {required PdfPage page,
    required PdfFont contentFont,
    required Rect rect,
    required String title,
    required String detail,
    required PdfColor bgColor,
    PdfTextAlignment align = PdfTextAlignment.center}) {
  page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);

  page.graphics.drawString(title, contentFont,
      brush: PdfSolidBrush(PdfColor(255, 255, 255)),
      bounds: Rect.fromLTWH(
          rect.left + 10, rect.top + 10, rect.width - 20, rect.height),
      format: PdfStringFormat(
          // lineAlignment: PdfVerticalAlignment.middle,
          alignment: align));

  final PdfStandardFont myFont =
      PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.bold);

  page.graphics.drawString(detail, myFont,
      brush: PdfSolidBrush(PdfColor(0, 0, 0)),
      bounds: Rect.fromLTWH(
          rect.left + 10, rect.top + 23, rect.width - 20, rect.height),
      format: PdfStringFormat(
          // lineAlignment: PdfVerticalAlignment.middle,
          alignment: align));
}
