import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

PdfLayoutResult drawMiniTable4(PdfPage page, PdfLayoutResult result) {
  PdfGrid grid = PdfGrid();
  grid.columns.add(count: 7);
  grid.headers.add(1);

  grid.columns[0].width = 26;
  grid.columns[1].width = 64;
  grid.columns[2].width = 64;
  grid.columns[3].width = 64;
  grid.columns[4].width = 64;
  grid.columns[5].width = 64;
  grid.columns[6].width = 64;

  PdfGridRow header = grid.headers[0];
  header.cells[0].value = '';
  header.cells[1].value = 'Item';
  header.cells[2].value = 'Description';
  header.cells[3].value = 'Quantity';
  header.cells[4].value = 'Unit Price';
  header.cells[5].value = 'Discount';
  header.cells[6].value = 'Total';

  header.cells[0].style.stringFormat = PdfStringFormat(
    alignment: PdfTextAlignment.center,
    lineAlignment: PdfVerticalAlignment.middle,
  );
  header.cells[0].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.bold);
  header.cells[1].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.center ,PdfFontStyle.bold);
  header.cells[2].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.center ,PdfFontStyle.bold);
  header.cells[3].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.center ,PdfFontStyle.bold);
  header.cells[4].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.center ,PdfFontStyle.bold);
  header.cells[5].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.center ,PdfFontStyle.bold);
  header.cells[6].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.center ,PdfFontStyle.bold);


  PdfGridRow row1 = grid.rows.add();
  row1.cells[0].value = '1';
  row1.cells[1].value = 'Item 1';
  row1.cells[2].value = '';
  row1.cells[3].value = '5';
  row1.cells[4].value = '\$10';
  row1.cells[5].value = '\$1';
  row1.cells[6].value = '\$49';

  row1.cells[0].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.left,PdfFontStyle.bold);
  row1.cells[1].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row1.cells[2].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row1.cells[3].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row1.cells[4].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row1.cells[5].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row1.cells[6].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);


  PdfGridRow row2 = grid.rows.add();
  row2.cells[0].value = '2';
  row2.cells[1].value = 'Item 2';
  row2.cells[2].value = '';
  row2.cells[3].value = '6';
  row2.cells[4].value = '\$5';
  row2.cells[5].value = '\$3';
  row2.cells[6].value = '\$27';

  row2.cells[0].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.left,PdfFontStyle.bold);
  row2.cells[1].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row2.cells[2].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row2.cells[3].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row2.cells[4].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row2.cells[5].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row2.cells[6].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);


  PdfGridRow row3 = grid.rows.add();
  row3.cells[0].value = '3';
  row3.cells[1].value = 'Item 3';
  row3.cells[2].value = '';
  row3.cells[3].value = '4';
  row3.cells[4].value = '\$22';
  row3.cells[5].value = '\$4';
  row3.cells[6].value = '\$84';
  row3.cells[0].style = headerStyle(PdfBrushes.pink,PdfTextAlignment.left,PdfFontStyle.bold);
  row3.cells[1].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row3.cells[2].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row3.cells[3].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row3.cells[4].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row3.cells[5].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);
  row3.cells[6].style = headerStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular);



  final Size size = page.getClientSize();

  // for (int i = 0; i < grid.rows.count; i++) {
  //   for (int j = 0; j < grid.rows[i].cells.count; j++) {
  //     grid.rows[i].cells[j].style.borders = PdfBorders(
  //         left: PdfPens.transparent,
  //         top: PdfPens.transparent,
  //         // bottom: PdfPens.transparent,
  //         right: PdfPens.transparent);
  //     //PdfPen(PdfColor(240, 100, 240), width: 1)
  //   }
  // }

  return grid.draw(
      page: page,
      bounds: Rect.fromLTWH(
          40, result.bounds.bottom + 15, size.width, size.height))!;
}

PdfGridCellStyle headerStyle(PdfBrush bgColor, PdfTextAlignment alignText, PdfFontStyle fontStyle) {
  return PdfGridCellStyle(
  font: PdfStandardFont(PdfFontFamily.timesRoman, 10, style: fontStyle ),
    cellPadding: PdfPaddings(
      top: 2.0,
      bottom: 2.0,
      left: 6,
      right: 6,
       ),
  backgroundBrush: bgColor ,
  format: PdfStringFormat(
  alignment: alignText ,
  lineAlignment: PdfVerticalAlignment.middle,
),
  borders: PdfBorders(
    top: PdfPen(PdfColor(255, 255, 255)),
    bottom: PdfPen(PdfColor(255, 255, 255)),
    right: PdfPen(PdfColor(255, 255, 255)),
    left: PdfPen(PdfColor(255, 255, 255)),
  )

);
}