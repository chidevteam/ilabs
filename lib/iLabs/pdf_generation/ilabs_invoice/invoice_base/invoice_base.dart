import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class InvoiceBase {
  late PdfDocument document; // = PdfDocument();
  late double px0, px1, py0, py1;
  late Rect pageRect; //active area page size
  late Rect contentRect; //active area content size
  late Rect leftRect, topRect, rightRect, bottomRect;
  bool debug = false;
  Size indentSize;

   final PdfStandardFont h1Font =
      PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.regular);
  final PdfStandardFont h1bFont =
      PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.bold);
  final PdfStandardFont contentFont =
      PdfStandardFont(PdfFontFamily.helvetica, 11, style: PdfFontStyle.regular);
  final PdfStandardFont boldFont =
        PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.bold);
  final PdfStandardFont listItemFont =
      PdfStandardFont(PdfFontFamily.helvetica, 6, style: PdfFontStyle.regular);
  final PdfStandardFont heading =
      PdfStandardFont(PdfFontFamily.helvetica, 16, style: PdfFontStyle.bold);
  final PdfStandardFont myFont =
        PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.bold);
  final PdfStandardFont lastFont =
        PdfStandardFont(PdfFontFamily.helvetica, 11, style: PdfFontStyle.bold);

  InvoiceBase(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero,
      required this.indentSize}) {
    document = PdfDocument();
    document.pageSettings.size = pageSize;
    document.pageSettings.margins.left = pMargins.left;
    document.pageSettings.margins.top = pMargins.top;
    document.pageSettings.margins.right = pMargins.right;
    document.pageSettings.margins.bottom = pMargins.bottom;

    Rect cMargins = letterheadMargins;

    double pWidth = document.pageSettings.size.width;
    double pHeight = document.pageSettings.size.height;

    px0 = document.pageSettings.margins.left;
    px1 = pWidth - document.pageSettings.margins.right - px0;
    py0 = document.pageSettings.margins.top;
    py1 = pHeight - document.pageSettings.margins.bottom - py0;

    pageRect = Rect.fromLTRB(0, 0, px1, py1);

    contentRect = Rect.fromLTRB(cMargins.left, cMargins.top,
        pageRect.right - cMargins.right, pageRect.bottom - cMargins.bottom);

// rgb(161,75,140)  #a14b8c
// rgb(180,155,177)  #b49bb1
// rgb(207,187,203)  #cfbbcb

    setLetterHead(cMargins);
  }

  Rect inset0(Rect r, double d) {
    return Rect.fromLTWH(d, d, r.width - 2 * d, r.height - 2 * d);
  }

  Rect inset(Rect r, double d) {
    return Rect.fromLTWH(
        r.left + d, r.top + d, r.right - 2 * d, r.bottom - 2 * d);
  }

  debugLetterHeadElement(PdfPageTemplateElement el, PdfColor color) {
    if (debug == false) return;

    el.graphics.drawRectangle(
        pen: PdfPen(PdfColor(255, 0, 0, 255), width: 1),
        bounds: inset0(el.bounds, 0));

    Rect r = inset0(el.bounds, 2);
    el.graphics.drawRectangle(brush: PdfSolidBrush(color), bounds: r);
  }

  setLetterHead(Rect cMargins) async {
    //SetRectangles of letterhead
    topRect = Rect.fromLTRB(0, 0, pageRect.width, cMargins.top);
    leftRect = Rect.fromLTRB(
        0, cMargins.top, cMargins.left, pageRect.height - cMargins.bottom);

    bottomRect = Rect.fromLTRB(
        0, pageRect.height - cMargins.bottom, pageRect.width, pageRect.height);

    rightRect = Rect.fromLTRB(contentRect.right, contentRect.top,
        pageRect.width, pageRect.height - cMargins.bottom);

    //Add the 4 elements
    PdfPageTemplateElement topElement = PdfPageTemplateElement(topRect);
    document.template.top = topElement;
    debugLetterHeadElement(topElement, MyPDFColor.red);

    PdfPageTemplateElement leftElement = PdfPageTemplateElement(leftRect);
    document.template.left = leftElement;
    debugLetterHeadElement(leftElement, MyPDFColor.green);

    PdfPageTemplateElement rightElement = PdfPageTemplateElement(rightRect);
    document.template.right = rightElement;
    debugLetterHeadElement(rightElement, MyPDFColor.blue);

    PdfPageTemplateElement bottomElement = PdfPageTemplateElement(bottomRect);
    document.template.bottom = bottomElement;
    debugLetterHeadElement(bottomElement, MyPDFColor.purple);
  }

  PdfLayoutResult addParagraph(
      String text, PdfPage page, PdfLayoutResult result,
      {double? width, PdfFont? font, PdfBrush? color}) {
    font ??= contentFont;
    Size size = page.getClientSize();
    Rect rect;
    if (width == null) {
      rect = Rect.fromLTWH(0, result.bounds.bottom, size.width, size.height);
    } else {
      rect = Rect.fromLTWH(0, result.bounds.bottom, width, size.height);
    }

    PdfLayoutResult result2 = PdfTextElement(
      text: text,
      font: font,
      brush: color,
    ).draw(
      page: page,
      bounds: rect,
    )!;
    return result2;
  }

  PdfColor rgbFromArray(List arr) {
    return PdfColor(arr[2], arr[1], arr[0]);
  }

  PdfLayoutResult newParagraph(String text, PdfPage page,
      {Rect? rect, PdfFont? font, PdfBrush? color}) {
    font ??= contentFont;
    Size size = page.getClientSize();
    rect ??= Rect.fromLTWH(0, 0, size.width, size.height);

    PdfLayoutResult result = PdfTextElement(
            text: text,
            font: font,
            brush: PdfBrushes.black,
            format: PdfStringFormat(alignment: PdfTextAlignment.left))
        .draw(page: page, bounds: rect)!;
    // format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate))!;
    return result;
  }
}

class MyPDFColor extends PdfColor {
  MyPDFColor(int red, int green, int blue) : super(red, green, blue);
  static PdfColor white = PdfColor(255, 255, 255);
  static PdfColor black1 = PdfColor(0, 0, 0);
  static PdfColor grey = PdfColor(0, 0, 0);
  static PdfColor red = PdfColor(255, 0, 0);
  static PdfColor green = PdfColor(0, 255, 0);
  static PdfColor blue = PdfColor(0, 0, 255);
  static PdfColor purple = PdfColor(255, 0, 255);
  static PdfColor yellow = PdfColor(255, 255, 0);
  static PdfColor cyan = PdfColor(0, 255, 255);
  static PdfColor dblue = PdfColor(91, 126, 215);
  static PdfColor lblue = PdfColor(142, 170, 219);
}





    // image = await readImageData('assets/photo.jpg');
    // imageRect = const Rect.fromLTWH(10, 0, 150, 150);
    // drawImageRounded(page, image, imageRect, 25);

    // image = await readImageData('assets/images/choose_template/invoice.png');
    // imageRect = const Rect.fromLTWH(120, 210, 200, 200);
    // drawImageRounded(page, image, imageRect, 50);

    // image = await readImageData('assets/invoice_labs.png');
    // imageRect = const Rect.fromLTWH(240, 10, 100, 100);
    // drawImageRounded(page, image, imageRect, 0);

    // addPara(page);

    // return document;
    // imageRect = const Rect.fromLTWH(200, 350, 50, 50);
    // drawImageRounded(page, image, imageRect, 5);


    // headerElement.graphics
    //     .drawLine(PdfPens.gray, const Offset(0, 49), const Offset(515, 49));

    // PdfBitmap image = await readImageData('assets/invoice_labs.png');
    // Rect imageRect;

    // // debugPrint("${image.width} ${image.height}");
    // // imageRect = ;
    // drawImageRounded(
    //     headerElement, image, const Rect.fromLTWH(0, 0, 50, 50), 5);

    // result = PdfTextElement(text: 'Abraham Swearegin,', font: h1Font).draw(
    //     page: page,
    //     bounds: Rect.fromLTWH(0, result!.bounds.bottom, result.bounds.width,
    //         result.bounds.height - result.bounds.bottom));



// void addPara(PdfPage page) {
//   String text = ' Newton was a fellow of Trinity College '
//       'and the second Lucasian Professor of Mathematics '
//       'at the University of Cambridge. He was a devout but '
//       'unorthodox Christian who privately rejected the doctrine '
//       'of the Trinity. but most of his work'
//       'in those areas remained unpublished until long after his '
//       'death. Politically and personally tied to the Whig party,'
//       'Newton served two brief terms as Member of Parliament '
//       'for the University of Cambridge, in 1689 1690 and'
//       '1701-1702. He was knighted by Queen Anne in 1705 and spent'
//       'the last three decades of his life in London, serving as Warden'
//       '(1696-1699) and Master (1699- 1727) of the Royal Mint, as well as'
//       'president of the Royal Society (1703-1727).';

//   PdfFont font = PdfStandardFont(PdfFontFamily.helvetica, 13);

//   PdfTextElement element = PdfTextElement(text: text, font: font);
//   PdfStringFormat format = PdfStringFormat();
//   element.stringFormat = format;
//   Rect bounds = const Rect.fromLTWH(200, 400, 300, 3000);
//   PdfLayoutResult result = element.draw(page: page, bounds: bounds)!;
//   debugPrint("$result");
// }
