import 'package:syncfusion_flutter_pdf/pdf.dart';

drawGridCell(PdfBrush bgClr,String borderClr, PdfGrid grid, String itemName,String price, String itemDescription, String quantity, String unitPrice, String discount, String total){

  PdfGridRow row1 = grid.rows.add();
  row1.cells[0].value = itemName;
  row1.cells[1].value = price;
  row1.cells[1].rowSpan = 2;
  row1.cells[2].value = quantity;
  row1.cells[2].rowSpan = 2;
  row1.cells[3].value = discount;
  row1.cells[3].rowSpan = 2;
  row1.cells[4].value = total;
  row1.cells[4].rowSpan = 2;
  row1.cells[0].style = cellStyle(bgClr, PdfTextAlignment.left,  PdfFontStyle.bold,    borderClr, PdfBrushes.black, false);
  row1.cells[1].style = cellStyle(bgClr, PdfTextAlignment.center,PdfFontStyle.regular, borderClr, PdfBrushes.black, false);
  row1.cells[2].style = cellStyle(bgClr, PdfTextAlignment.center,PdfFontStyle.regular, borderClr, PdfBrushes.black, false);
  row1.cells[3].style = cellStyle(bgClr, PdfTextAlignment.center,PdfFontStyle.regular, borderClr, PdfBrushes.black, false);
  row1.cells[4].style = cellStyle(bgClr, PdfTextAlignment.right,PdfFontStyle.regular,  borderClr, PdfBrushes.black, false);
  PdfGridRow row2 = grid.rows.add();
  row2.cells[0].value = itemDescription;
  row2.cells[0].style = cellStyle(bgClr, PdfTextAlignment.left,PdfFontStyle.regular ,borderClr, PdfBrushes.black, false);
  row2.cells[1].style = cellStyle(bgClr,PdfTextAlignment.center,PdfFontStyle.regular,borderClr, PdfBrushes.black, false);
  row2.cells[2].style = cellStyle(bgClr,PdfTextAlignment.center,PdfFontStyle.regular,borderClr, PdfBrushes.black, false);
  row2.cells[3].style = cellStyle(bgClr,PdfTextAlignment.right,PdfFontStyle.regular,borderClr,  PdfBrushes.black, false);
  PdfGridRow row3 = grid.rows.add();
  row3.cells[0].value = '';
  row3.cells[1].value = '';
  row3.cells[2].value = '';
  row3.cells[3].value = '';
  row3.cells[0].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, 'white',PdfBrushes.black, false);
  row3.cells[1].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, 'white',PdfBrushes.black, false);
  row3.cells[2].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, 'white',PdfBrushes.black, false);
  row3.cells[3].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, 'white',PdfBrushes.black, false);
  row3.cells[4].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, 'white',PdfBrushes.black, false);
  }

  PdfGridCellStyle cellStyle(PdfBrush bgColor,
  PdfTextAlignment alignText,
  PdfFontStyle fontStyle,
  String borderClr,
  PdfBrush textColor,
  bool isHeader) {
  return PdfGridCellStyle(
  font: PdfStandardFont(PdfFontFamily.helvetica, 10, style: fontStyle ),
  backgroundBrush: bgColor ,
  textBrush:  textColor,
  cellPadding: isHeader ? PdfPaddings(top: 4, bottom: 4): null,
  format: PdfStringFormat(
  alignment: alignText ,
  lineAlignment: PdfVerticalAlignment.middle,
),
  borders: borderClr == 'white' ? PdfBorders(
    top:    PdfPen(PdfColor(255, 255, 255)),
    bottom: PdfPen(PdfColor(255, 255, 255)),
    right:  PdfPen(PdfColor(255, 255, 255)),
    left:   PdfPen(PdfColor(255, 255, 255)),
  ) : borderClr == 'pink' ?
  PdfBorders(
    top: PdfPen(PdfColor   (255,182,193)),
    bottom: PdfPen(PdfColor(255,182,193)),
    right: PdfPen(PdfColor (255,182,193)),
    left: PdfPen(PdfColor  (255,182,193)),
  ) :borderClr == 'grey' ? PdfBorders(
    top: PdfPen(PdfColor   (211,211,211)),
    bottom: PdfPen(PdfColor(211,211,211)),
    right: PdfPen(PdfColor (211,211,211)),
    left: PdfPen(PdfColor  (211,211,211)),)
    :borderClr == 'customize'? PdfBorders(
    top:   PdfPen(PdfColor (255,182,193)),
    bottom: PdfPen(PdfColor(255,182,193)),
    right: PdfPen(PdfColor (255, 255, 255)),
    left: PdfPen(PdfColor  (255, 255, 255)),)
    :PdfBorders(
    top:   PdfPen(PdfColor (255, 255, 255)),
    bottom: PdfPen(PdfColor(255,182,193)),
    right: PdfPen(PdfColor (255, 255, 255)),
    left: PdfPen(PdfColor  (255, 255, 255)),)
);
}

