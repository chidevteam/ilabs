import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

addDateSection({required PdfPage page, required PdfFont contentFont}) {
  int r = 163;
  int g = 73;
  int b = 141;
  int dr = ((255 - 163) ~/ 3);
  int dg = ((255 - 73) ~/ 3);
  int db = ((255 - 141) ~/ 3);
  Size size = page.getClientSize();

  Rect rect = Rect.fromLTWH(size.width * .4, 10, size.width * .2, 60);
  drawPairRect(
      page: page,
      rect: rect,
      contentFont: contentFont,
      title: "Due Date",
      detail: "22-03-2021",
      bgColor: PdfColor(255,255,255),
      align: PdfTextAlignment.left);

  rect = Rect.fromLTWH(size.width * .6, 10, size.width * .2, 60);

  drawPairRect(
      page: page,
      rect: rect,
      contentFont: contentFont,
      title: "Due Date",
      detail: "22-03-2022",
      bgColor: PdfColor(255,255,255),
      align: PdfTextAlignment.center);

  rect = Rect.fromLTWH(size.width * .8, 10, size.width * .2, 60);

  drawPairRect(
      page: page,
      rect: rect,
      contentFont: contentFont,
      title: "Due Date",
      detail: "22-03-2022",
      bgColor: PdfColor(255,255,255),
      align: PdfTextAlignment.right);
}

drawPairRect(
    {required PdfPage page,
    required PdfFont contentFont,
    required Rect rect,
    required String title,
    required String detail,
    required PdfColor bgColor,
    PdfTextAlignment align = PdfTextAlignment.center}) {
        final PdfStandardFont myFont =
      PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.bold);
  page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);
  page.graphics.drawString(title, contentFont,
      brush: PdfBrushes.gray,
      bounds: Rect.fromLTWH(
          rect.left + 10, rect.top + 10, rect.width - 20, rect.height),
      format: PdfStringFormat(
          alignment: align));
  page.graphics.drawString(detail, myFont,
      brush: PdfSolidBrush(PdfColor(0, 0, 0)),
      bounds: Rect.fromLTWH(
          rect.left + 10, rect.top + 28, rect.width - 20, rect.height),
      format: PdfStringFormat(
          alignment: align));
}
