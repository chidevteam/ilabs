import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'dart:async';
import '../../../../../iLabs/material_helpers/color_helper.dart';
import '../invoice_base/invoice_base.dart';
import '../../pdf_utils/pdf_image.dart';

class Letterhead5 extends InvoiceBase {
  Letterhead5(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero,
      required Size indentSize})
      : super(
            pageSize: pageSize,
            pMargins: pMargins,
            letterheadMargins: letterheadMargins,
            indentSize: indentSize);

  Future addHeader(PdfPageTemplateElement pdfElement) async {
    //Draw rectangle
    Rect bRect = pdfElement.bounds;

    PdfBitmap image =
        await readImageData('assets/sample_logos/amazon/grey.png');

    double heightRatio = 1.0;
    double imHeight = heightRatio * bRect.height;
    double topOfImage = 0 * (1 - heightRatio) / 2 * bRect.height;
    double imWidth = image.width * imHeight / image.height;

    debugPrint("${image.width} ${image.height}");
    Rect imRect = Rect.fromLTWH(topOfImage, topOfImage, imWidth, imHeight);

    drawImageRounded(pdfElement, image, imRect, 0,
        fillType: 'fit', debug: debug);

    PdfFont font30 =
        PdfStandardFont(PdfFontFamily.helvetica, 40, style: PdfFontStyle.bold);

    String companyName = '';
    pdfElement.graphics.drawString(companyName, font30,
        brush: PdfSolidBrush(PdfColor(150, 0, 150)),
        bounds: Rect.fromLTWH(imRect.right, -10, bRect.width, bRect.height - 3),
        format: PdfStringFormat(
            alignment: PdfTextAlignment.left));

    PdfFont font50 = PdfStandardFont(PdfFontFamily.helvetica, 50);


    String invoice = 'INVOICE';
    PdfSolidBrush iTextColor = PdfSolidBrush(PdfColor(80, 80, 80));
    PdfSolidBrush uLineColor = PdfSolidBrush(PdfColor(200, 200, 200));
    pdfElement.graphics.drawString(invoice, font50,
        brush: iTextColor,
        bounds:
            Rect.fromLTWH(bRect.left, bRect.top, bRect.width, bRect.height - 3),
        format: PdfStringFormat(
            lineAlignment: PdfVerticalAlignment.bottom,
            alignment: PdfTextAlignment.right));

    pdfElement.graphics.drawRectangle(
        bounds: Rect.fromLTRB((bRect.left + bRect.right) * .4, bRect.bottom - 7,
            bRect.right, bRect.bottom),
        brush: uLineColor);
  }

  addFooter(PdfPageTemplateElement footerElement) {
    Rect bRect = footerElement.bounds;
    footerElement.graphics.drawString(
      'This is page footer',
      PdfStandardFont(PdfFontFamily.helvetica, 10),
      bounds: bRect,
    );
    footerElement.graphics.setTransparency(0.6);
    PdfCompositeField(text: 'Page {0} of {1}', fields: <PdfAutomaticField>[
      PdfPageNumberField(brush: PdfBrushes.red),
      PdfPageCountField(brush: PdfBrushes.blue),
    ]).draw(footerElement.graphics, const Offset(450, 0));
  }

  addLeft(PdfDocument document) {
    double pageWidth = document.pageSettings.width;
    double pageHeight = document.pageSettings.height;

    //Create a header template and draw image/text.
    final PdfPageTemplateElement leftElement =
        PdfPageTemplateElement(Rect.fromLTWH(0, 0, pageWidth / 5, pageHeight));

    leftElement.graphics.drawRectangle(
        brush: PdfSolidBrush(PdfColor(255, 128, 128, 255)),
        bounds: Rect.fromLTWH(0, 10, pageWidth / 5, pageHeight));
    document.template.left = leftElement;
  }

  void addColors(PdfPage page, String primaryColor, String secondaryColor) {
    PdfGraphics graphics = page.graphics;
    PdfColor pdfColorP = rgbFromArray(listFromHex(primaryColor));
    PdfColor pdfColorS = rgbFromArray(listFromHex(secondaryColor));

    graphics.drawRectangle(
        brush: PdfSolidBrush(pdfColorP),
        bounds: const Rect.fromLTWH(10, 60, 100, 100));

    graphics.drawRectangle(
        brush: PdfSolidBrush(pdfColorS),
        bounds: const Rect.fromLTWH(200, 60, 100, 100));
  }
}

