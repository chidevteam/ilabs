import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../ilabs_design_5/table.dart';
import '../../pdf_utils/pdf_image.dart';
import '../ilabs_design_5/functions.dart';
import '../ilabs_design_5/tableTotal.dart';
import '../invoice_base/invoice_base.dart';
import '../ilabs_design_5/letterhead5.dart';
import '../../file_write/save_file_mobile.dart';
import '../../../models/invoice_data/model.dart';
import 'date_section.dart';

class InvoiceDesign5 extends Letterhead5 {
  double xOffset = 0;
  double xOffset2 = 0; //for second column
  double yOffset = 0;
  PdfPage? currentPage;
  double lineSpacing = 5;
  // double fontHeight = 10;
  InvoiceDesign5(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero})
      : super(
            pageSize: PdfPageSize.a4,
            pMargins: const Rect.fromLTRB(40, 40, 40, 10),
            letterheadMargins: const Rect.fromLTRB(0, 80, 0, 20),
            indentSize: const Size(50, 50)) {
    debug = false;
  }

  Future<String> createPDF(InvoiceData invData) async {
    PdfPage page = document.pages.add();
    Size size = page.getClientSize();

    await addHeader(document.template.top!);
    addFooter(document.template.bottom!);
    PdfLayoutResult result = newParagraph("Invoice to:", page,
        rect: Rect.fromLTWH(0, 16, size.width * .4, 30), font: contentFont);
    // result = addClientInfo(result,size.width * .4,invData.client,size);
    addDateSection(contentFont: contentFont, page: result.page, );
    result = drawItemsTable(result.page, result, invData);
    result = drawTotalTable(result.page, result, invData);
    // result = addPaymentMethod(result, size.width * 4, invData.client, size);
    await drawSignature( result.page, result);
    var bytes = document.save();
    document.dispose();
    return await showPDF(bytes, 'Invoice.pdf');
  }


  drawCellThankYou(
      {required PdfPage page,
      PdfTextAlignment align = PdfTextAlignment.left,
      required Size size}) {
    page.graphics.drawString('Thank you for your Business', lastFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(size.width / 3, size.height * .970,
            size.width * .4, size.height * 0.033),
        format: PdfStringFormat());
  }
  Future drawSignature(PdfPage page, PdfLayoutResult result) async {
    PdfBitmap image = await readImageData('assets/signature.png');

    double pageWidth = contentRect.width;
    // double heightRatio = 1.0;
    double imHeight = 70;
    double topOfImage = result.bounds.bottom;
    double imWidth = image.width * imHeight / image.height;

    debugPrint("${image.width} ${image.height}");
    Rect imRect =
        Rect.fromLTWH(pageWidth - imWidth, topOfImage, imWidth, imHeight);
    print("BRECT:  $imRect");

    drawImageRounded(page, image, imRect, 0, fillType: 'fit', debug: debug);
  }
}
