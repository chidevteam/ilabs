import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../ilabs_design_5/tableCell.dart';
import '../../../models/invoice_data/model.dart';


PdfLayoutResult drawTotalTable(PdfPage page, PdfLayoutResult result, InvoiceData invoiceData) {
  PdfGrid grid = PdfGrid();
  grid.columns.add(count: 2);

  grid.columns[0].width = 100;
  grid.columns[1].width = 74;
  for (int i=0; i<6; i++){
      drawTotalCell(i!=5 ?  PdfBrushes.white : PdfBrushes.magenta, i==0 ? 'null':'customize', grid, 'sub Total',i==0 ? '\$40': '\$40000', i!=5 ?  PdfBrushes.black : PdfBrushes.white );
  } 
  final Size size = page.getClientSize();
  return grid.draw(
      page: page,
      bounds: Rect.fromLTWH(size.width *.65, result.bounds.bottom, size.width*.35, size.height))!;
  }

drawTotalCell(PdfBrush bgClr,String borderClr, PdfGrid grid, String title, String value, PdfBrush textClr){
  PdfGridRow row1 = grid.rows.add();
  row1.cells[0].value = title;
  row1.cells[1].value = value;
  row1.cells[0].style = cellStyle(bgClr, PdfTextAlignment.center,  PdfFontStyle.regular, borderClr, textClr, false);
  row1.cells[1].style = cellStyle(bgClr, PdfTextAlignment.right,   PdfFontStyle.bold,    borderClr, textClr, false);
  }