import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/sec07_payment_instructions/payment_instruction.dart';
import 'package:ilabs/iLabs/models/payment_instructions/model.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../../file_write/save_file_mobile.dart';
// import '../../pdf_utils/pdf_image.dart';
import 'letterhead3.dart';
import '../../pdf_utils/pdf_image.dart';
import '../invoice_base/invoice_base.dart';

import 'package:ilabs/iLabs/models/business/model.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
import '../../../models/invoice_data/model.dart';

class InvoiceDesign3 extends Letterhead3 {
  InvoiceDesign3(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero})
      : super(
            pageSize: PdfPageSize.a4,
            pMargins: const Rect.fromLTRB(40, 40, 40, 10),
            letterheadMargins: const Rect.fromLTRB(0, 80, 0, 20),
            indentSize: const Size(50, 50)) {
    debug = false;
  }

  Future<String> createPDF(InvoiceData invData) async {
    await addHeader(document.template.top!);
    addFooter(document.template.bottom!);

    PdfPage page = document.pages.add();

    // String primaryCode = "#E54B4B";
    // String secondaryCode = "#FDF1F1";

    // addColors(page, primaryCode, secondaryCode);

    Size size = page.getClientSize();

    PdfLayoutResult result = newParagraph("Invoice to:", page,
        rect: Rect.fromLTWH(0, 50, size.width * .4, 30), font: contentFont);

    for (int i = 0; i < 1; i++) {
      result = addClientInfo(result, size.width * .4, invData.client);
    }

    int r = 163;
    int g = 73;
    int b = 141;
    int dr = ((255 - 163) ~/ 3);
    int dg = ((255 - 73) ~/ 3);
    int db = ((255 - 141) ~/ 3);

    Rect rect = Rect.fromLTWH(size.width * .4, 0, size.width * .2, 60);

    drawPairRect(
      invoiceInfo: invData.invoiceInfo,
        page: result.page,
        rect: rect,
        title: "Start Date",
        detail: invData.invoiceInfo.date.toString(),
        bgColor: PdfColor(r + 0 * dr, g + 0 * dg, b + 0 * db),
        align: PdfTextAlignment.left);

    rect = Rect.fromLTWH(size.width * .6, 0, size.width * .2, 60);

    drawPairRect(
      invoiceInfo: invData.invoiceInfo,
        page: result.page,
        rect: rect,
        title: "Last Date",
        detail: invData.invoiceInfo.dueDate.toString(),
        bgColor: PdfColor(r + 1 * dr, g + 1 * dg, b + 1 * db),
        align: PdfTextAlignment.center);

    rect = Rect.fromLTWH(size.width * .8, 0, size.width * .2, 60);

    drawPairRect(
      invoiceInfo: invData.invoiceInfo,
        page: result.page,
        rect: rect,
        title: "Due Days",
        detail: invData.invoiceInfo.terms.toString(),
        bgColor: PdfColor(r + 2 * dr, g + 2 * dg, b + 2 * db),
        align: PdfTextAlignment.right);

    final PdfGrid grid = getGrid();
    result = drawGrid(result.page, grid, result);

    await drawSignature(result.page, result);

    double height = size.height - (size.height * .8);
    drawPaymentInfo(
        page: height < 120 ? document.pages.add() : result.page, size: size, paymentInstructions: invData.paymentInstructions);
    drawSectionTotal(
      size: size,
      page: height < 120 ? document.pages.add() : result.page,
    );

    drawPersonalNote(
        page: height < 120 ? document.pages.add() : result.page, size: size, invData: invData);
    drawCellThankYou(
        page: height < 40 ? document.pages.add() : result.page, size: size);

    page = document.pages.add();
    Rect rectInvoiceBy = Rect.fromLTWH(0, size.height * .25, size.width * .48, 120);
    await drawSectionInvoiceDetails(invoiceData: invData, title: 'Invoice By:',rectInvoiceBy: rectInvoiceBy, page: page, size: size, name: invData.businessDetail.businessOwnerName!,
     mail: invData.businessDetail.businessEmail!, address: invData.businessDetail.businessAddress!, phone: invData.businessDetail.businessPhone!, mobile: invData.businessDetail.businessMobile!,
     mailIcon: 'assets/email.png', phoneIcon: 'assets/phone.png', mobileIcon: 'assets/phone.png', addressIcon: 'assets/location.png');
    rectInvoiceBy = Rect.fromLTWH(size.width* .5, size.height * .25, size.width * .5, 120);
    await drawSectionInvoiceDetails(invoiceData: invData,title: 'Invoice To:',rectInvoiceBy: rectInvoiceBy, page: page, size: size, name: invData.client.name!,
     mail: invData.client.email!, address: invData.client.address!, phone: invData.client.phone!, mobile: invData.client.mobile!,
     mailIcon: 'assets/email.png', phoneIcon: 'assets/phone.png', mobileIcon: 'assets/phone.png', addressIcon: 'assets/location.png');
    // drawInvoiceDetailsD2(page: page, size: size, align: PdfTextAlignment.left);
    // drawInvoiceToD2(page: page, size: size, align: PdfTextAlignment.left);

    var bytes = document.save();
    document.dispose();
    return await showPDF(bytes, 'Invoice.pdf');
  }

  drawPairRect(
      {required PdfPage page,
      required Rect rect,
      required String title,
      required String detail,
      required PdfColor bgColor,
      PdfTextAlignment align = PdfTextAlignment.center,
      required InvoiceInfo invoiceInfo}) {
    page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);

    page.graphics.drawString(title, contentFont,
        brush: PdfSolidBrush(MyPDFColor.white),
        bounds: Rect.fromLTWH(
            rect.left + 10, rect.top + 10, rect.width - 20, rect.height),
        format: PdfStringFormat(
            // lineAlignment: PdfVerticalAlignment.middle,
            alignment: align));

    page.graphics.drawString(detail, myFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            rect.left + 10, rect.top + 23, rect.width - 20, rect.height),
        format: PdfStringFormat(
            // lineAlignment: PdfVerticalAlignment.middle,
            alignment: align));
  }

  drawPersonalNote(
      {required PdfPage page,
      PdfTextAlignment align = PdfTextAlignment.left,
      required Size size,
      required InvoiceData invData}) {
    page.graphics.drawString('Personal Note', heading,
        brush: PdfSolidBrush(MyPDFColor.red),
        bounds: Rect.fromLTWH(
            0, size.height * .920, size.width * .4, size.height * 0.033),
        format: PdfStringFormat(alignment: align));
    page.graphics.drawString(invData.notes!, contentFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            0, size.height * .950, size.width * .4, size.height * 0.033),
        format: PdfStringFormat(alignment: align));
  }

  drawCellThankYou(
      {required PdfPage page,
      PdfTextAlignment align = PdfTextAlignment.left,
      required Size size}) {
    page.graphics.drawString('Thank you for your Business', lastFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(size.width / 3, size.height * .970,
            size.width * .4, size.height * 0.033),
        format: PdfStringFormat());
  }

  drawInvoiceToD2(
      {required PdfPage page,
      PdfTextAlignment align = PdfTextAlignment.left,
      required Size size}) {
    page.graphics.drawString('To:', boldFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        //  Rect rectInvoiceInfo = Rect.fromLTWH(0, size.height * .08, size.width * .4, size.height * 0.025);
        bounds: Rect.fromLTWH(size.width * .7, size.height * .08,
            size.width * .4, size.height * 0.025),
        format: PdfStringFormat(alignment: align));
    page.graphics.drawString('Mr Ali Hassan', boldFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(size.width * .7, size.height * .103,
            size.width * .4, size.height * 0.025),
        format: PdfStringFormat(alignment: align));
    page.graphics.drawString('Managing Director', contentFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(size.width * .7, size.height * .126,
            size.width * .4, size.height * 0.025),
        format: PdfStringFormat(alignment: align));
    page.graphics.drawString('Cognitive Healthcare Intl.', contentFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(size.width * .7, size.height * .152,
            size.width * .4, size.height * 0.025),
        format: PdfStringFormat(alignment: align));
  }

  drawCellTotalRect(
      {required PdfPage page,
      required Rect rect,
      required String title,
      required String detail,
      PdfTextAlignment align = PdfTextAlignment.left,
      PdfTextAlignment alignright = PdfTextAlignment.right}) {
    final PdfStandardFont boldFont =
        PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.bold);

    page.graphics.drawString(title, boldFont,
        brush: PdfSolidBrush(MyPDFColor.grey),
        bounds: Rect.fromLTWH(
            rect.left + 10, rect.top + 5, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: align));

    page.graphics.drawString(detail, contentFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            rect.left, rect.top + 5, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: alignright));
  }

  drawCellInvoiceBy(
      {required PdfPage page,
      required Rect rect,
      required String icon,
      required String detail,
      PdfTextAlignment align = PdfTextAlignment.left,
      PdfTextAlignment alignright = PdfTextAlignment.left}) async {
    Rect rectIcon =
        Rect.fromLTWH(rect.left + 16, rect.top + 2, 16, rect.height);
    await drawIcon(page, rectIcon, icon);

    // page.graphics.drawString(icon, boldFont,
    //     brush: PdfSolidBrush(MyPDFColor.grey),
    //     bounds: Rect.fromLTWH(
    //         rect.left+16, rect.top + 5, rect.width - 20, rect.height),
    //     format: PdfStringFormat(alignment: align));

    page.graphics.drawString(detail, contentFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            rect.left + 48, rect.top + 5, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: alignright));
  }

  drawCellPaymentInfo({
    required PdfPage page,
    required Rect rect,
    required String title,
    required String detail,
    required PdfColor bgColor,
    PdfTextAlignment align = PdfTextAlignment.left,
  }) {
    page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);
    final PdfStandardFont boldFont =
        PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.bold);

    page.graphics.drawString(title, boldFont,
        brush: PdfSolidBrush(MyPDFColor.grey),
        bounds: Rect.fromLTWH(0, rect.top + 5, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: align));

    page.graphics.drawString(detail, contentFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            rect.left + 90, rect.top + 5, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: align));
  }

  drawSectionTotal({
    required PdfPage page,
    required Size size,
  }) {
    Rect rectCellTotal = Rect.fromLTWH(
        size.width * .6, size.height * .8, size.width * .4, size.height * .16);
    page.graphics.drawRectangle(
        brush: PdfSolidBrush(PdfColor(230, 230, 230)), bounds: rectCellTotal);
    rectCellTotal = Rect.fromLTWH(
        size.width * .6, size.height * .8, size.width * .4, size.height * .033);
    drawCellTotalRect(
        page: page,
        rect: rectCellTotal,
        title: "Sub Total",
        detail: "Kz384718.84",
        align: PdfTextAlignment.left);

    rectCellTotal = Rect.fromLTWH(size.width * .6, size.height * .825,
        size.width * .4, size.height * .033);
    drawCellTotalRect(
        page: page,
        rect: rectCellTotal,
        title: "Tax",
        detail: "Kz0.00",
        align: PdfTextAlignment.left);
    rectCellTotal = Rect.fromLTWH(size.width * .6, size.height * .85,
        size.width * .4, size.height * .033);
    drawCellTotalRect(
        page: page,
        rect: rectCellTotal,
        title: "Discount",
        detail: "Kz0.00",
        align: PdfTextAlignment.left);
    rectCellTotal = Rect.fromLTWH(size.width * .6, size.height * .875,
        size.width * .4, size.height * .033);
    drawCellTotalRect(
        page: page,
        rect: rectCellTotal,
        title: "Total",
        detail: "Kz384718.84",
        align: PdfTextAlignment.left);
    rectCellTotal = Rect.fromLTWH(
        size.width * .6, size.height * .9, size.width * .4, size.height * .033);
    drawCellTotalRect(
        page: page,
        rect: rectCellTotal,
        title: "Paid",
        detail: "Kz0.00",
        align: PdfTextAlignment.left);
    rectCellTotal = Rect.fromLTWH(size.width * .6, size.height * .925,
        size.width * .4, size.height * .033);
    drawCellTotalRect(
        page: page,
        rect: rectCellTotal,
        title: "Total Due",
        detail: "Kz384718.84",
        alignright: PdfTextAlignment.right);
  }

  drawPaymentInfo(
      {required PdfPage page,
      PdfTextAlignment align = PdfTextAlignment.left,
      required Size size,
      required PaymentInstructions paymentInstructions}) {
    page.graphics.drawString('Payment Info', heading,
        brush: PdfSolidBrush(MyPDFColor.red),
        bounds: Rect.fromLTWH(
            0, size.height * .8, size.width * .4, size.height * 0.033),
        format: PdfStringFormat(alignment: align));

    Rect rectPaymentInfoCell = Rect.fromLTWH(
        0, size.height * .830, size.width * .4, size.height * 0.033);
    drawCellPaymentInfo(
        page: page,
        rect: rectPaymentInfoCell,
        title: "Checkpayable To:",
        detail: paymentInstructions.checkPayableTo,
        bgColor: PdfColor(255, 255, 255),
        align: PdfTextAlignment.left);

    rectPaymentInfoCell = Rect.fromLTWH(
        0, size.height * .860, size.width * .4, size.height * 0.033);
    drawCellPaymentInfo(
        page: page,
        rect: rectPaymentInfoCell,
        title: "Paypal Email:",
        detail: paymentInstructions.paypalEmail,
        bgColor: PdfColor(255, 255, 255),
        align: PdfTextAlignment.left);
    rectPaymentInfoCell = Rect.fromLTWH(
        0, size.height * .890, size.width * .4, size.height * 0.033);
    drawCellPaymentInfo(
        page: page,
        rect: rectPaymentInfoCell,
        title: "Bank Info:",
        detail: paymentInstructions.bankTransfer,
        bgColor: PdfColor(255, 255, 255),
        align: PdfTextAlignment.left);
  }

  drawInvoiceDetailsD2(
      {required PdfPage page,
      PdfTextAlignment align = PdfTextAlignment.left,
      required Size size}) {
    Rect rectInvoiceInfo = Rect.fromLTWH(
        0, size.height * .08, size.width * .4, size.height * 0.025);
    drawCellPaymentInfo(
        page: page,
        rect: rectInvoiceInfo,
        title: "Issue Date:",
        detail: "31 March 2022",
        bgColor: PdfColor(255, 255, 255),
        align: PdfTextAlignment.left);
    rectInvoiceInfo = Rect.fromLTWH(
        0, size.height * .103, size.width * .3, size.height * 0.025);
    drawCellPaymentInfo(
        page: page,
        rect: rectInvoiceInfo,
        title: "Account NO:",
        detail: "31032022",
        bgColor: PdfColor(255, 255, 255),
        align: PdfTextAlignment.left);
    rectInvoiceInfo = Rect.fromLTWH(
        0, size.height * .126, size.width * .3, size.height * 0.025);
    drawCellPaymentInfo(
        page: page,
        rect: rectInvoiceInfo,
        title: "Invoice NO:",
        detail: "INV#2022",
        bgColor: PdfColor(255, 255, 255),
        align: PdfTextAlignment.left);
  }

  drawSectionInvoiceDetails({
    required String title,
    required Rect rectInvoiceBy,
    required PdfPage page,
    required Size size,
    required String name,
    required String mail,
    required String mailIcon,
    required String address,
    required String addressIcon,
    required String mobile,
    required String mobileIcon,
    required String phone,
    required String phoneIcon,
    required InvoiceData invoiceData
  }) async {
    Rect rectInvoiceByCell = Rect.fromLTWH(
        title == 'Invoice By:' ? 8 : size.width * .51,
        size.height * .256,
        size.width * .4,
        size.height * 0.033);
    page.graphics.drawRectangle(
        brush: PdfSolidBrush(PdfColor(223, 193, 217)), bounds: rectInvoiceBy);
    page.graphics.drawString(title, heading,
        brush: PdfSolidBrush(MyPDFColor.red),
        bounds: rectInvoiceByCell,
        format: PdfStringFormat());
    rectInvoiceByCell = Rect.fromLTWH(title == 'Invoice By:' ? 16 : size.width * .53,
        size.height * .29, size.width * .4, size.height * 0.033);
    page.graphics.drawString(name, heading,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: rectInvoiceByCell,
        format: PdfStringFormat());
    rectInvoiceBy = Rect.fromLTWH(title == 'Invoice By:' ? 0 : size.width * .5,
        size.height * .32, size.width * .4, size.height * 0.033);
    await drawCellInvoiceBy(
        page: page,
        rect: rectInvoiceBy,
        icon: mailIcon,
        detail: mail,
        align: PdfTextAlignment.left);
    rectInvoiceBy = Rect.fromLTWH(title == 'Invoice By:' ? 0 : size.width * .5,
        size.height * .345, size.width * .4, size.height * 0.033);
    await drawCellInvoiceBy(
        page: page,
        rect: rectInvoiceBy,
        icon: addressIcon,
        detail: address,
        align: PdfTextAlignment.left);
    rectInvoiceBy = Rect.fromLTWH(title == 'Invoice By:' ? 0 : size.width * .5,
        size.height * .37, size.width * .4, size.height * 0.033);
    await drawCellInvoiceBy(
        page: page,
        rect: rectInvoiceBy,
        icon: mobileIcon,
        detail: mobile,
        align: PdfTextAlignment.left);
    rectInvoiceBy = Rect.fromLTWH(
        title == 'Invoice By:' ? size.width * .23 : size.width * .75,
        size.height * .37,
        size.width * .4,
        size.height * 0.033);
    await drawCellInvoiceBy(
        page: page,
        rect: rectInvoiceBy,
        icon: phoneIcon,
        detail: phone,
        align: PdfTextAlignment.left);
  }

  drawListCell(
      {required PdfPage page,
      required Rect rect,
      required String srNum,
      required String productName,
      required String price,
      required String quantity,
      required String total,
      required PdfColor bgColor,
      PdfTextAlignment align = PdfTextAlignment.left}) {
    page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);

    page.graphics.drawString(productName, listItemFont,
        brush: PdfSolidBrush(MyPDFColor.white),
        bounds: Rect.fromLTWH(0, rect.top + 10, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: align));

    final PdfStandardFont myFont =
        PdfStandardFont(PdfFontFamily.helvetica, 6, style: PdfFontStyle.bold);

    page.graphics.drawString(price, myFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            rect.left + 10, rect.top + 23, rect.width - 20, rect.height),
        format: PdfStringFormat(alignment: align));
  }

  PdfLayoutResult addClientInfo(
      PdfLayoutResult result, double width, Client client) {
    result =
        addParagraph("${client.name},", result.page, result, font: h1bFont);

    String text = "";

    text =
        client.address!;
    result = addParagraph(text, result.page, result, width: width);

    text = client.email!;
    result = addParagraph(text, result.page, result, width: width);
    return result;
  }

  Future drawSignature(PdfPage page, PdfLayoutResult result) async {
    PdfBitmap image = await readImageData('assets/signature.png');

    double pageWidth = contentRect.width;
    // double heightRatio = 1.0;
    double imHeight = 70;
    double topOfImage = result.bounds.bottom;
    double imWidth = image.width * imHeight / image.height;

    debugPrint("${image.width} ${image.height}");
    Rect imRect =
        Rect.fromLTWH(pageWidth - imWidth, topOfImage, imWidth, imHeight);
    debugPrint("BRECT:  $imRect");

    drawImageRounded(page, image, imRect, 0, fillType: 'fit', debug: debug);
  }

  Future drawIcon(PdfPage page, Rect rect, String icon) async {
    PdfBitmap image = await readImageData(icon);
    Rect imRect = rect;
    drawImageRounded(page, image, imRect, 0, fillType: 'fit', debug: debug);
  }

  PdfLayoutResult drawGrid(PdfPage page, PdfGrid grid, PdfLayoutResult result) {
    PdfLayoutFormat format = PdfLayoutFormat();
    final Size size = page.getClientSize();
    format.paginateBounds = Rect.fromLTWH(0, 0, size.width, size.height - 15);
    //Draw grid into PDF page
    PdfLayoutResult? gridResult = grid.draw(
        page: page,
        bounds: Rect.fromLTWH(
            0, result.bounds.bottom + 15, size.width, size.height - 15));

    final PdfStandardFont h0Font =
        PdfStandardFont(PdfFontFamily.helvetica, 15, style: PdfFontStyle.bold);

    final PdfTextElement element =
        PdfTextElement(text: 'Grand Total: 14740.84', font: h0Font);
    result = element.draw(
        page: gridResult!.page,
        bounds: Rect.fromLTWH(
            0, gridResult.bounds.bottom + 15, size.width, size.height - 15),
        format: PdfLayoutFormat(
            paginateBounds:
                Rect.fromLTWH(0, 15, size.width, size.height - 15)))!;
    return result;
  }

  //Create PDF grid and return
  PdfGrid getGrid() {
    //Create a PDF grid
    final PdfGrid grid = PdfGrid();
    //Secify the columns count to the grid.
    grid.columns.add(count: 5);
    //Create the header row of the grid.
    final PdfGridRow headerRow = grid.headers.add(1)[0];
    //Set style
    headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(68, 114, 196));
    headerRow.style.textBrush = PdfBrushes.white;
    headerRow.cells[0].value = 'S.No.';
    headerRow.cells[0].stringFormat.alignment = PdfTextAlignment.center;
    headerRow.cells[1].value = 'Product Name';
    headerRow.cells[2].value = 'Price';
    headerRow.cells[3].value = 'Quantity';
    headerRow.cells[4].value = 'Total';
    // headerRow.cells[4].stringFormat.alignment = PdfTextAlignment.center;
    //Add rows
    for (int i = 1; i < 10; i++) {
      addProducts('$i', 'AWC Logo Cap',
          'This cap is really good for most things ', 8.99, 2, 17.98, grid);
    }
    //Apply the table built-in style
    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    //Set gird columns width
    grid.columns[0].width = 50;
    grid.columns[1].width = 250;
    for (int i = 0; i < headerRow.cells.count; i++) {
      headerRow.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }
    for (int i = 0; i < grid.rows.count; i++) {
      final PdfGridRow row = grid.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final PdfGridCell cell = row.cells[j];
        if (j == 0 || j == 3) {
          cell.stringFormat.alignment = PdfTextAlignment.center;
        }
        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }
    return grid;
  }

  //Create and row for the grid.
  void addProducts(String productId, String productName, String productDetail,
      double price, int quantity, double total, PdfGrid grid) {
    final PdfGridRow row = grid.rows.add();
    row.cells[0].value = productId;
    row.cells[1].value = "" + productName + "" + "\n" + productDetail;
    row.cells[2].value = price.toString();
    row.cells[3].value = quantity.toString();
    row.cells[4].value = total.toString();
  }
}
