 import 'package:flutter/material.dart';
import '../../../models/client/model.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import 'invoice_design_4.dart';

 final PdfStandardFont h1bFont =
       PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.bold);

PdfLayoutResult addPaymentMethod(
      PdfLayoutResult result, double width, Client client, Size size) {
       
    result = addParagraph("${client.name}", result.page, result,        0 ,result.bounds.bottom+40, font: h1bFont);
    result = addParagraph("PayPal", result.page, result,  0 ,result.bounds.bottom+5, width: width, color: PdfBrushes.blue);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom+10,  width: width);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom,  width: width);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom,  width: width);
    result = addParagraph("HSBC",result.page, result,0 ,result.bounds.bottom+16,  width: width, font: h1bFont);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom+16,  width: width);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom,  width: width);
    result = addParagraph("Terms & Conditions", result.page, result,0 ,result.bounds.bottom+20,  width: width, font: h1bFont);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom,  width: width);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom,  width: width);
    result = addParagraph("${client.address}", result.page, result,0 ,result.bounds.bottom,  width: width);
    return result;
  }

PdfLayoutResult addClientInfo(
      PdfLayoutResult result, double width, Client client, Size size) {
    result = addParagraph("${client.name}",    result.page, result, size.width * .75,result.bounds.bottom, font: h1bFont,);
    result = addParagraph("${client.email}",   result.page, result, size.width * .75,result.bounds.bottom, width: width);
    result = addParagraph("${client.address}", result.page, result, size.width * .75,result.bounds.bottom, width: width);
    result = addParagraph("${client.address}", result.page, result, size.width * .75,result.bounds.bottom, width: width);
    return result;
  }

PdfLayoutResult invoiceDetails(Size size,
      PdfLayoutResult result,  Client client, PdfPage page) {
    Rect rect =  Rect.fromLTWH(0, 70, size.width * .16, 30);
    page.graphics.drawString('Issue date: ',  PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.bold), bounds: rect);
     rect =  Rect.fromLTWH(size.width * .16, 70, size.width * .4, 30);
    page.graphics.drawString('6 April 2022',  PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.regular), bounds: rect);
     rect =  Rect.fromLTWH(0, 90, size.width * .18, 30);
    page.graphics.drawString('Account NO: ',  PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.bold), bounds: rect);
     rect =  Rect.fromLTWH(size.width * .18, 90, size.width * .3, 30);
    page.graphics.drawString('328643545',  PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.regular), bounds: rect);
     rect =  Rect.fromLTWH(0, 110, size.width *.18, 30);
    page.graphics.drawString('Invoice NO: ',  PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.bold), bounds: rect);
     rect =  Rect.fromLTWH(size.width * .18, 110, size.width * .3, 30);
    page.graphics.drawString('INV#198',  PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.regular), bounds: rect);
    return result;
  }




PdfLayoutResult addParagraph(
      String text, PdfPage page, PdfLayoutResult result, double leftPad, double bottomBounds,
      {double? width, PdfFont? font, PdfBrush? color}) {
    font ??= PdfStandardFont(PdfFontFamily.helvetica, 14, style: PdfFontStyle.regular);
    Size size = page.getClientSize();
    Rect rect;
    if (width == null) {
      rect = Rect.fromLTWH(leftPad, bottomBounds, size.width, size.height);
    } else {
      rect = Rect.fromLTWH(leftPad, bottomBounds, width, size.height);
    }

    PdfLayoutResult result2 = PdfTextElement(
      text: text,
      font: font,
      brush: color,
    ).draw(
      page: page,
      bounds: rect,
    )!;
    return result2;
  }