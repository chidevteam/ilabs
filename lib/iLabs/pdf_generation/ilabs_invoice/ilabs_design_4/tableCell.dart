import 'package:syncfusion_flutter_pdf/pdf.dart';

drawGridCell(PdfGrid grid, String itemName,String price, String itemDescription, String quantity, String unitPrice, String discount, String total){

  PdfGridRow row1 = grid.rows.add();
  row1.cells[0].value = itemName;
  row1.cells[1].value = price;
  row1.cells[1].rowSpan = 2;
  row1.cells[2].value = quantity;
  row1.cells[2].rowSpan = 2;
  row1.cells[3].value = discount;
  row1.cells[3].rowSpan = 2;
  row1.cells[4].value = total;
  row1.cells[4].rowSpan = 2;
  row1.cells[0].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold    , true, false);
  row1.cells[1].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular, true, false);
  row1.cells[2].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular, true, false);
  row1.cells[3].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular, true, false);
  row1.cells[4].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular, true, false);
  PdfGridRow row2 = grid.rows.add();
  row2.cells[0].value = itemDescription;
  row2.cells[0].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.regular ,true, false);
  row2.cells[1].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular,true, false);
  row2.cells[2].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular,true, false);
  row2.cells[3].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular,true, false);
  PdfGridRow row3 = grid.rows.add();
  row3.cells[0].value = '';
  row3.cells[1].value = '';
  row3.cells[2].value = '';
  row3.cells[3].value = '';
  row3.cells[0].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, true, false);
  row3.cells[1].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, true, false);
  row3.cells[2].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, true, false);
  row3.cells[3].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, true, false);
  row3.cells[4].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold, true, false);
  }

  PdfGridCellStyle cellStyle(PdfBrush bgColor,
  PdfTextAlignment alignText,
  PdfFontStyle fontStyle,
  bool isBorder,
  bool isHeader) {
  return PdfGridCellStyle(
  font: PdfStandardFont(PdfFontFamily.helvetica, 10, style: fontStyle ),
  backgroundBrush: bgColor ,
  textBrush: isHeader ? PdfBrushes.white : PdfBrushes.black,
  cellPadding: isHeader ? PdfPaddings(top: 4, bottom: 4): null,
  format: PdfStringFormat(
  alignment: alignText ,
  lineAlignment: PdfVerticalAlignment.middle,
),
  borders: isBorder ? PdfBorders(
    top:    PdfPen(PdfColor(255, 255, 255)),
    bottom: PdfPen(PdfColor(255, 255, 255)),
    right:  PdfPen(PdfColor(255, 255, 255)),
    left:   PdfPen(PdfColor(255, 255, 255)),
  ) :
  PdfBorders(
    top: PdfPen(PdfColor(0, 0, 0)),
    bottom: PdfPen(PdfColor(0, 0, 0)),
    right: PdfPen(PdfColor(0, 0, 0)),
    left: PdfPen(PdfColor(0, 0, 0)),
  ) 
);
}

