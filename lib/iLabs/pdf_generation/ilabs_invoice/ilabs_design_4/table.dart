import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import 'tableCell.dart';

PdfLayoutResult drawItemsTable(PdfPage page, PdfLayoutResult result, InvoiceData invoiceData) {
  PdfGrid grid = PdfGrid();
  grid.columns.add(count: 5);
  grid.headers.add(1);

  grid.columns[0].width = 210;
  grid.columns[1].width = 75;
  grid.columns[2].width = 75;
  grid.columns[3].width = 75;
  grid.columns[4].width = 75;

  PdfGridRow header = grid.headers[0];
  header.cells[0].value = 'Description';
  header.cells[1].value = 'Quantity';
  header.cells[2].value = 'Unit Price';
  header.cells[3].value = 'Discount';
  header.cells[4].value = 'Total';

  header.cells[0].style = cellStyle(PdfBrushes.black,PdfTextAlignment.left  ,PdfFontStyle.bold,false, true);
  header.cells[1].style = cellStyle(PdfBrushes.black,PdfTextAlignment.center,PdfFontStyle.bold,false, true);
  header.cells[2].style = cellStyle(PdfBrushes.black,PdfTextAlignment.center,PdfFontStyle.bold,false, true);
  header.cells[3].style = cellStyle(PdfBrushes.black,PdfTextAlignment.center,PdfFontStyle.bold,false, true);
  header.cells[4].style = cellStyle(PdfBrushes.black,PdfTextAlignment.center,PdfFontStyle.bold,false, true);
  for (int i=0; i<3; i++){
      drawGridCell(grid, 'Laptop', '40', 'This is a macbook', '4','12000', '0', '48000');
  } 
  final Size size = page.getClientSize();
  return grid.draw(
      page: page,
      bounds: Rect.fromLTWH(0, result.bounds.bottom + 15, size.width, size.height))!;
  }


PdfLayoutResult drawRectTotal(PdfPage page, PdfLayoutResult result, InvoiceData invoiceData) {
  final Size size = page.getClientSize();
  page.graphics.drawRectangle(bounds: Rect.fromLTWH(size.width * 0.75, result.bounds.bottom, size.width * 0.3, size.height * .06), brush: PdfBrushes.black);
  page.graphics.drawString('Total : ', PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.regular), brush: PdfBrushes.white, bounds: Rect.fromLTWH(size.width * 0.8, result.bounds.bottom+15, size.width * 0.3, size.height * .08));
    page.graphics.drawString('\$ 300', PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.regular), brush: PdfBrushes.white, bounds: Rect.fromLTWH(size.width * 0.9, result.bounds.bottom+15, size.width * 0.3, size.height * .08));
  return result;
  }

PdfLayoutResult drawTableInvoiceDetails(PdfPage page, PdfLayoutResult result, InvoiceData invoiceData) {
  PdfGrid grid = PdfGrid();
  grid.columns.add(count: 2);
  grid.columns[0].width = 70;
  grid.columns[1].width = 120;
  for (int i=0; i<3; i++){
      drawInvoiceDetailsCell (grid, 'Account NO: ','58735845');
  } 
  final Size size = page.getClientSize();
  return grid.draw(
      page: page,
      bounds: Rect.fromLTWH(0, result.bounds.bottom, size.width, size.height))!;
  }
  drawInvoiceDetailsCell(PdfGrid grid, String heading,String details,){
  PdfGridRow row1 = grid.rows.add();
  row1.cells[0].value = heading;
  row1.cells[1].value = details;
  row1.cells[0].style = cellStyle(PdfBrushes.white, PdfTextAlignment.left,PdfFontStyle.bold    , true, false);
  row1.cells[1].style = cellStyle(PdfBrushes.white,PdfTextAlignment.center,PdfFontStyle.regular, true, false);

  }