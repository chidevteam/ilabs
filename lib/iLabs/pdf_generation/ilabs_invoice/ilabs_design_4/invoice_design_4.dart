import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/pdf_generation/ilabs_invoice/ilabs_design_4/table.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'letterhead4.dart';
import '../../file_write/save_file_mobile.dart';
import '../../pdf_utils/pdf_image.dart';
import '../../../models/invoice_data/model.dart';
import 'functions.dart';

class InvoiceDesign4 extends Letterhead4 {
  InvoiceDesign4(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero})
      : super(
            pageSize: PdfPageSize.a4,
            pMargins: const Rect.fromLTRB(0, 0, 0, 10),
            letterheadMargins: const Rect.fromLTRB(50, 80, 50, 20),
            indentSize: const Size(50, 50)) {
    debug = false;
  }

  Future<String> createPDF(InvoiceData invData) async {
    await addHeader(document.template.top!);
    addFooter(document.template.bottom!);
    PdfPage page = document.pages.add();
    Size size = page.getClientSize();

    PdfLayoutResult result = newParagraph("To:", page,rect: Rect.fromLTWH(size.width * .75, 50, size.width * .4, 30), font: h1bFont);
    result = addClientInfo(result,size.width * .4,invData.client,size);
    // result = drawTableInvoiceDetails(result.page, result, invData);
    result = invoiceDetails( size, result, invData.client, page );
    result = drawItemsTable(result.page, result, invData);
    result = drawRectTotal(result.page, result, invData);
    result = addPaymentMethod(result, size.width * .4, invData.client, size);
    await drawSignature(result.page, result);

    var bytes = document.save();
    document.dispose();
    return await showPDF(bytes, 'Invoice.pdf');
  }

  Future drawSignature(PdfPage page, PdfLayoutResult result) async {
    PdfBitmap image = await readImageData('assets/signature.png');

    double pageWidth = contentRect.width;
    double imHeight = 70;
    double topOfImage = result.bounds.bottom;
    double imWidth = image.width * imHeight / image.height;

    debugPrint("${image.width} ${image.height}");
    Rect imRect =
        Rect.fromLTWH(pageWidth - imWidth, topOfImage, imWidth, imHeight);
    debugPrint("BRECT:  $imRect");

    drawImageRounded(page, image, imRect, 0, fillType: 'fit', debug: debug);
  }
  }