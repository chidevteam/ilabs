import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:pdf_viewer_plugin/pdf_viewer_plugin.dart';
import 'dart:io';
import 'pdf_invoice_vm.dart';
import '../models/invoice_data/model.dart';

class PDFInvoiceVU extends ViewModelBuilderWidget<PdfInvoiceVM> {
  PDFInvoiceVU(this.tempRedrawIndex, this.invoiceData, {Key? key})
      : super(key: key) {
    debugPrint("PDF VU called");
  }

  final int tempRedrawIndex;
  final InvoiceData invoiceData;
  @override
  Widget builder(BuildContext context, PdfInvoiceVM viewModel, Widget? child) {
    // iLabs1.createPDF();
    // if (tempRedrawIndex != viewModel.reDrawIndex) {
    //   viewModel.createPDF1(invoiceData);
    //   viewModel.reDrawIndex = tempRedrawIndex;
    // }

    return Scaffold(
      appBar: null,
      // AppBar(
      //   title: const Text('Create PDF document'),
      // ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              viewModel.timeTaken,
              style: const TextStyle(fontSize: 20),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDF5(invoiceData);
                        },
                        child: const Text('design5')),
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDFSimpleInvoice();
                        },
                        child: const Text('Simple')),
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDFMultiPage();
                        },
                        child: const Text('Multi')),
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDFRealMultiPage();
                        },
                        child: const Text('Multi')),
                  ],
                ),
                Row(
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDF1(invoiceData);
                        },
                        child: const Text('design1')),
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDF2(invoiceData);
                        },
                        child: const Text('design2')),
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDF3(invoiceData);
                        },
                        child: const Text('design3')),
                    ElevatedButton(
                        onPressed: () {
                          viewModel.createPDF4(invoiceData);
                        },
                        child: const Text('design4')),
                  ],
                )
              ],
            ),
            Expanded(child: invoicePDFview(viewModel.fileName)),
          ],
        ),
      ),
    );
  }

  Widget invoicePDFview(String? fileName) {
    Widget pdfWidget = const Text("Empty PDF");
    if (fileName != null) {
      if (Platform.isLinux) {
        pdfWidget = Text("$fileName: PDFview on LInux not supported");
      } else {
        pdfWidget = PdfView(path: fileName);
      }
    }
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
        child: SizedBox(
            width: double.infinity,
            child: Container(
                color: Colors.green,
                child: Container(
                  color: Colors.blue,
                  child: pdfWidget,
                ))));
  }
  // ? PdfView(path: file.path)

  @override
  PdfInvoiceVM viewModelBuilder(BuildContext context) {
    PdfInvoiceVM viewModel = PdfInvoiceVM(tempRedrawIndex);

    return viewModel;
  }
}
