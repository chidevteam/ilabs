import 'package:stacked/stacked.dart';
import 'ilabs_invoice/ilabs_design_1/invoice_design_1.dart';
import 'ilabs_invoice/ilabs_design_2/invoice_design_2.dart';
import 'ilabs_invoice/ilabs_design_3/invoice_design_3.dart';

import '../models/invoice_data/model.dart';
import 'ilabs_invoice/ilabs_design_4/invoice_design_4.dart';
import 'ilabs_invoice/ilabs_design_5/invoice_design_5.dart';
import 'simple_invoice/simple_invoice.dart';
import 'multi_page/multi_page.dart';
import 'real_multi_page/real_multi_page_doc.dart';

class PdfInvoiceVM extends BaseViewModel {
  String? fileName;
  int reDrawIndex;
  String timeTaken = "";

  PdfInvoiceVM(this.reDrawIndex) {
    // createPDF1();
  }
  createPDFSimpleInvoice() async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    SimpleInvoice doc = SimpleInvoice();
    fileName = null;
    notifyListeners();
    fileName = await doc.createPDF();
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }

  createPDF1(InvoiceData invoiceData) async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    InvoiceDesign1 design1 = InvoiceDesign1();
    fileName = null;
    notifyListeners();
    fileName = await design1.createPDF(invoiceData);
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }

  createPDF2(InvoiceData invoiceData) async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    InvoiceDesign2 design2 = InvoiceDesign2();
    fileName = null;
    notifyListeners();
    fileName = await design2.createPDF(invoiceData);
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }

  createPDF3(InvoiceData invoiceData) async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    InvoiceDesign3 doc = InvoiceDesign3();
    fileName = null;
    notifyListeners();
    fileName = await doc.createPDF(invoiceData);
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }

  createPDF4(InvoiceData invoiceData) async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    InvoiceDesign4 design1 = InvoiceDesign4();
    fileName = null;
    notifyListeners();
    fileName = await design1.createPDF(invoiceData);
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }
    createPDF5(InvoiceData invoiceData) async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    InvoiceDesign5 design5 = InvoiceDesign5();
    fileName = null;
    notifyListeners();
    fileName = await design5.createPDF(invoiceData);
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }

  createPDFMultiPage() async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    MultiPageInvoice doc = MultiPageInvoice();
    fileName = null;
    notifyListeners();
    fileName = await doc.createPDF();
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }

  createPDFRealMultiPage() async {
    double startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    RealMultiPageDoc doc = RealMultiPageDoc();
    fileName = null;
    notifyListeners();
    fileName = await doc.createPDF();
    double endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    timeTaken = (endTime - startTime).toString();
    notifyListeners();
  }
}
