import 'package:flutter/material.dart';
// import 'pdf_tests_vu.dart';
import 'pdf_invoice_vu.dart';
import '../models/invoice_data/test/response.dart';
import '../models/invoice_data/model.dart';

void main() {
  runApp(CreatePdfWidget());
}

/// Represents the PDF widget class.
class CreatePdfWidget extends StatelessWidget {
  CreatePdfWidget({Key? key}) : super(key: key);

  final InvoiceData invData = InvoiceData.fromMap(response);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // home: PDFTestsVU(),
        home: SafeArea(child: Scaffold(body: PDFInvoiceVU(1, invData))));
  }
}

/*
- unclear planning.

- Draw the address space and then the table below that
- Keep testing for multipage.

*/ 