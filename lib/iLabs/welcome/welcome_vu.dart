import 'dart:async';
import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/Preferences.dart';
import '../login_or_register/login_or_register_vu.dart';
import '../services/api_client.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 2), () {
      Object token = UserPreferences.getAuthToken() ?? '';
      print(token);
      if (token == '') {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => const LoginOrRegisterScreen()));
      } else {
        // Navigator.pushReplacement(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => const LoginOrRegisterScreen()));
        ApiClient.instance!.mAuthToken = token.toString();
        Navigator.pushNamed(context, '/myHomePage');
      }
    });
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Stack(
          children: [
            // Positioned(
            //     top: 0,
            //     left: 0,
            //     child: Container(
            //       width: 40,
            //       height: 100,
            //       decoration: BoxDecoration(
            //         color: Colors.grey[200],
            //         borderRadius: const BorderRadius.horizontal(
            //           right: Radius.elliptical(150, 250),
            //           left: Radius.elliptical(5, 5),
            //         ),
            //       ),
            //     )),
            // Positioned(
            //     top: 50,
            //     left: 80,
            //     child: Container(
            //       width: 50,
            //       height: 56,
            //       decoration: BoxDecoration(
            //         color: Colors.grey[200],
            //         borderRadius: BorderRadius.circular(100),
            //       ),
            //     )),
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('iLabsSreens/splash/splash bg.png'),
                    fit: BoxFit.cover),
              ),
            ),

            Center(
                child: Image.asset(
              'iLabsSreens/splash/splash icon.png',
              scale: 1.8,
            ))

            // Image.asset('iLabsSreens/splash/splash bg.png')

            // Positioned(
            //     bottom: 0.0,
            //     left: 40,
            //     child: Container(
            //       width: 350,
            //       height: 80,
            //       decoration: BoxDecoration(
            //         color: Colors.grey[200],
            //         borderRadius: const BorderRadius.vertical(
            //           top: Radius.elliptical(150, 60),
            //           bottom: Radius.elliptical(0, 0),
            //         ),
            //       ),
            //     )),
          ],
        ),
      ),
    );
  }
}
