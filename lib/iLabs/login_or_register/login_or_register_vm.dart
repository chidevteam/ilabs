import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class LoginRegisteViewModel extends BaseViewModel {
  String selectedOptionLanguage = "English(US)";

  List<String> optionLanguage = [
    'English(US)',
    'Urdu(PK)',
    'Arabic',
    'English(US)',
    'Urdu(PK)',
    'Arabic',
  ];

  updateLanguageOption(int index, BuildContext context) {
    selectedOptionLanguage = optionLanguage[index];
    Locale locale = context.supportedLocales[index];
    context.setLocale(locale);
    notifyListeners();
    Navigator.pop(context);
  }
}
