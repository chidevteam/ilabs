import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../Utils/utils.dart';
import '../language/generated/locale_keys.g.dart';
import 'package:stacked/stacked.dart';
import '../login/login_vu.dart';
import '../register/register_vu.dart';
import '../widgets/authentication/auth_buttons.dart';
import 'continue_guest/continues_guest_vu.dart';
import 'login_or_register_vm.dart';

class LoginOrRegisterScreen
    extends ViewModelBuilderWidget<LoginRegisteViewModel> {
  const LoginOrRegisterScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, LoginRegisteViewModel viewModel, Widget? child) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
        body: orientation == Orientation.portrait
            ? portraitView(viewModel, context)
            : landscapeView(viewModel, context));
  }
  Widget portraitView(LoginRegisteViewModel viewModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 80, 0, 25),
      child: Column(
        children: [
          invoiceMadeImage(Orientation.portrait),
          const SizedBox(height: 34.0,),
          invoiceLabText(),
          const SizedBox(height: 16.0,),
          invoicLabText2(Orientation.portrait),
          const SizedBox(height: 16.0,),
          const Spacer(),
          loginButton(context),
          createAccountButton(context),
          continueAsGuestButton(context)],),
    );
  }

  Widget landscapeView(LoginRegisteViewModel viewModel, BuildContext context) {
    return Row(children: [
        Expanded(child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              const  Spacer(),
              invoiceMadeImage(Orientation.landscape),
              const  Spacer(),
              invoiceLabText(),
              const SizedBox(height: 10,),
              invoicLabText2(Orientation.landscape),
              const  Spacer(),],),
        )),
        Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                loginButton(context),
               createAccountButton(context),
                continueAsGuestButton(context)
              ],
            ))
      ],
    );
  }

  // *******loginRegisterWidget*******
  Widget loginButton(BuildContext context)=> myAuthButton(context, buttonColor:
  const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
    textColor: Colors.white,
    label: tr(LocaleKeys.login_register_view_login_button),
    onPressed:  () {Utils.push(context, LoginScreen());},);

  Widget createAccountButton(BuildContext context)=> myAuthButton(context,
      buttonColor: const Color(0xfffee3d9), textColor:const Color(0xfffd7848),
      label: tr(LocaleKeys.login_register_view_createaccount_button),
      onPressed: (){Utils.push(context,const  RegisterScreen());});

  Widget continueAsGuestButton(BuildContext context)=> myAuthButton(context, buttonColor: const Color(0xfff4f7fa),
      textColor: Colors.black,
      label: tr(LocaleKeys.login_register_view_continue_button),
      onPressed: (){Utils.push(context,const ContinueGuestVU(null));});
      Widget invoiceMadeImage(Orientation orientation) =>
      Image.asset('iLabsSreens/login_signup/Telecommuting-pana.png', height: 130,);

  Text invoiceLabText()=> Text(
    tr(LocaleKeys.login_register_view_title),
    textAlign: TextAlign.center,
    style: const TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold),);

  Text invoicLabText2(Orientation orientation)=> Text(
      tr(LocaleKeys.login_register_view_sub_title),
      textAlign: TextAlign.center,
      style:const  TextStyle(color: Color(0xff949da5), fontSize: 15.0, fontWeight: FontWeight.w400),);

  @override
  LoginRegisteViewModel viewModelBuilder(BuildContext context) {
    return LoginRegisteViewModel();
  }
}
