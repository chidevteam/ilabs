import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../language/generated/locale_keys.g.dart';
import '../../widgets/input/chi_text_field.dart';
import '../../models/business/model.dart';
import 'package:stacked/stacked.dart';

import '../../widgets/input/chi_textfield.dart';
import 'continue_guest_vm.dart';

class ContinueGuestVU extends ViewModelBuilderWidget<ContinueGuestVM> {
  final BusinessDetail? businessDetails;
  const ContinueGuestVU(this.businessDetails, {Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, ContinueGuestVM viewModel, Widget? child) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: viewModel.formKey,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 10.0, 18.0, 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                businessInformationTxt(),
                informationBelowTxt(),
                headerImg(),
                const SizedBox(
                  height: 24.0,
                ),
                CHITextField(
                    heading: tr(LocaleKeys
                        .continue_as_guest_view_business_name_heading),
                    hintText: tr(
                        LocaleKeys.continue_as_guest_view_business_name_hint),
                    func: (value) {
                      //print(value);
                      viewModel.businessDetails.businessName = value;
                      viewModel.notifyListeners();
                    },
                    validator: null,
                    initialValue: viewModel.businessDetails.businessName),
                CHITextField(
                    heading: tr(LocaleKeys
                        .continue_as_guest_view_business_email_heading),
                    hintText: tr(
                        LocaleKeys.continue_as_guest_view_business_email_hint),
                    validator: null,
                    func: (value) {
                      viewModel.businessDetails.businessEmail = value;
                      viewModel.notifyListeners();
                    },
                    initialValue: viewModel.businessDetails.businessEmail),
                CHITextField(
                    heading: tr(LocaleKeys
                        .continue_as_guest_view_business_contact_heading),
                    hintText: tr(LocaleKeys
                        .continue_as_guest_view_business_contact_hint),
                    func: (value) =>
                        viewModel.businessDetails.businessNumber = value,
                    initialValue: viewModel.businessDetails.businessName),
                chiSaveButton(
                    tr(LocaleKeys.continue_as_guest_view_continue_button), () {
                  viewModel.onSaveItem(
                    context,
                  );
                }),
                askMeTxtButton(viewModel, context)
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  ContinueGuestVM viewModelBuilder(BuildContext context) {
    return ContinueGuestVM();
  }
}

// ******************BusinessInformationImage****************

Widget headerImg() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      ClipRRect(
        child: Padding(
          padding: const EdgeInsets.only(top: 32.0),
          child: Container(
            width: 120.0,
            height: 120.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey.shade200,
                border: Border.all(width: 3, color: Colors.grey.shade400)),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Image.network(
                'https://www.reciphergroup.com/assets-new/images/automated-invoicing.png',
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

// ******************BusinessInformationTxt****************

Padding informationBelowTxt() {
  return Padding(
    padding: const EdgeInsets.only(top: 6.0, left: 12),
    child: Center(
      child: Text(
        tr(LocaleKeys.continue_as_guest_view_sub_title),
        style: TextStyle(
            color: Colors.grey.shade700,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
      ),
    ),
  );
}

Widget businessInformationTxt() {
  return Padding(
    padding: const EdgeInsets.only(top: 60.0, left: 12, bottom: 10.0),
    child: Center(
      child: Text(
        tr(LocaleKeys.continue_as_guest_view_title),
        style: const TextStyle(
            color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
      ),
    ),
  );
}

Widget askMeTxtButton(ContinueGuestVM viewModel, BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      TextButton(
          onPressed: () {
            viewModel.onAskMeLater(context);
          },
          child: Text(
            tr(LocaleKeys.continue_as_guest_view_ask_me_later_button),
            style: TextStyle(color: Colors.orange.shade800),
          )),
    ],
  );
}
