import 'package:flutter/material.dart';
import 'dart:async';
import 'package:ilabs/iLabs/models/business/model.dart';
import '../../services/api_client.dart';

import 'package:stacked/stacked.dart';

class ContinueGuestVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  BusinessDetail businessDetails = BusinessDetail();

  ContinueGuestVM() {
    initialize();
    //getBussinessDetails();
  }

  initialize() async {
    await guestSignUp();
  }

  onSaveItem(BuildContext context) async {
    formKey.currentState?.save();

    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    onPostBusinessDetails(context);
  }

  onAskMeLater(BuildContext context) {
    Navigator.pushNamed(context, '/myHomePage');
  }

  onPostBusinessDetails(BuildContext context) async {
    Map<String, dynamic> req = businessDetails.toMap();
    // {
    //   "id": businessDetails.id,
    //   "business_name": businessDetails.businessName,
    //   "business_email": businessDetails.businessEmail,
    //   "business_phone": businessDetails.businessPhone,
    //   "business_website": ' ',
    //   "business_mobile": ' ',
    //   "business_number": ' ',
    //   "business_owner_name": ' ',
    //   "business_address": ' ',
    //   "business_address2": ' ',
    //   "business_address3": ' ',
    //   "business_logo_url": ' ',
    // };
    APIResponse resp = await ApiClient.post(
        request: req, endPoint: "/business", fromJson: null);
    debugPrint("$resp");

    notifyListeners();
    Navigator.pushNamed(context, '/myHomePage');
  }

  Future getBussinessDetails() async {
    APIResponse resp = await ApiClient.get<BusinessDetail>(
        endPoint: "/business", params: "", fromJson: BusinessDetail.fromMap);
    businessDetails = resp["data"];
    debugPrint("$resp");

    notifyListeners();
  }

  guestSignUp() async {
    APIResponse resp = await ApiClient.post(
        request: {"fcm_token": ''}, endPoint: '/auth/register-guest');
    ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
    ApiClient.instance!.guestToken = resp['data']['accessToken'];
    businessDetails.id = resp['data']['id'];
    ApiClient.instance!.isGuestToken = true;
    print(resp);
  }
}
