import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ilabs/iLabs/home/onboarding/onboarding_vu.dart';
import 'package:ilabs/iLabs/login/google_sign_in_api.dart';
import 'package:ilabs/iLabs/models/social_login/facebook_user_data.dart';
import 'package:stacked/stacked.dart';
import '../services/api_client.dart';

String prettyPrint(Map json) {
  JsonEncoder encoder = const JsonEncoder.withIndent('  ');
  String pretty = encoder.convert(json);
  return pretty;
}

class RegisterViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  String confirmPassword = '';
  AccessToken? accessToken;
  Map<String, dynamic>? mUserData;
  bool checking = true;
  String message = '';

  onEmail(String? value) async {
    email = value!.trim();

    notifyListeners();
  }

  String? onEmailValidator(value) {
    if (value == null || value.isEmpty || !isValidEmail(value)) {
      return 'email is invalid';
    }
    return null;
  }

  bool isValidEmail(String email) {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if (emailRegExp.hasMatch(email)) {
      return true;
    } else {
      return false;
    }
  }

  onPassword(String? value) async {
    password = value!.trim();

    notifyListeners();
  }

  onConfirmPassword(String? value) async {
    confirmPassword = value!.trim();

    notifyListeners();
  }

  String? onPasswordValidator(value) {
    if (value == null || value.isEmpty) {
      return 'password is required';
    }
    return null;
  }

  String? onconfirmPasswordValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Confirm password is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    debugPrint('$email');
    debugPrint('$password');
    debugPrint('$confirmPassword');

    Map<String, dynamic> json = await ApiClient.register(email, password);
    //Navigator.pushNamed(context, '/myHomePage');
    print('guest Token-${ApiClient.instance!.guestToken} =-======');
    if (json.containsKey('message')) {
      message = json['message'];
      print(message);
    } else {
      message = '';
      if (ApiClient.instance!.guestToken == '') {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return OnBoardingScreen();
        }));
      } else {
        ApiClient.instance!.guestToken = '';
        Navigator.pushNamed(context, '/myHomePage');
      }
    }
    notifyListeners();
  }

  void printCredentials() {
    print(
      prettyPrint(accessToken!.toJson()),
    );
  }

  fbSignIn(BuildContext context) async {
    print('fb login button clicked');
    await fbSignUp(context);
    //setBusy(false);
    //notifyListeners();
    if (ApiClient.instance!.guestToken == '') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return OnBoardingScreen();
      }));
    } else {
      ApiClient.instance!.guestToken = '';
      Navigator.pushNamed(context, '/myHomePage');
    }
  }

  googleSignIn(BuildContext context) async {
    await onGoogleSignUp(context);
    // if(ApiClient.instance.guestToken == ''){
    //   Navigator.push(context, MaterialPageRoute(builder: (context){
    //     return OnBoardingScreen();
    //   }));
    // }
    // else{
    //   ApiClient.instance.guestToken = '';
    //   Navigator.pushNamed(context, '/myHomePage');
    // }
  }

  Future<void> fbSignUp(BuildContext context) async {
    setBusy(true);
    final LoginResult result = await FacebookAuth.instance
        .login(); // by default we request the email and the public profile
    // loginBehavior is only supported for Android devices, for ios it will be ignored
    // final result = await FacebookAuth.instance.login(
    //   permissions: ['email', 'public_profile', 'user_birthday', 'user_friends', 'user_gender', 'user_link'],
    //   loginBehavior: LoginBehavior
    //       .DIALOG_ONLY, // (only android) show an authentication dialog instead of redirecting to facebook app
    // );
    if (result.status == LoginStatus.success) {
      accessToken = result.accessToken;
      print('access token : ---------------------------${result.accessToken}');
      printCredentials();
      // get the user data
      // by default we get the userId, email,name and picture
      final userData = await FacebookAuth.instance.getUserData();
      // final userData = await FacebookAuth.instance.getUserData(fields: "email,birthday,friends,gender,link");
      mUserData = userData;
      Map<String, dynamic> req = {'face_book_token': result.accessToken!.token};

      print('-----------------fb api=--======================');
      APIResponse resp =
          await ApiClient.post(request: req, endPoint: '/auth/facebook');
      print(resp);
      ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
      print(ApiClient.instance!.mAuthToken);
      //Navigator.pushNamed(context, '/myHomePage');

    } else {
      print(result.status);
      print(result.message);
    }
    checking = false;
  }

  onGoogleSignUp(BuildContext context) async {
    GoogleSigninApi googleSigninApi = GoogleSigninApi();
    googleSigninApi.googleLogout();
    await googleSigninApi.googleLogin();
    //print('user for apiiiiiiiiiiiiiiii: ${googleSigninApi.user!.email}');
    Map<String, dynamic> req = {'email': googleSigninApi.user!.email};

    print('-----------------google api=--======================');
    APIResponse resp =
        await ApiClient.post(request: req, endPoint: '/auth/google');
    print(resp);
    ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
    print(ApiClient.instance!.mAuthToken);
    //Navigator.pushNamed(context, '/myHomePage');
    if (ApiClient.instance!.guestToken == '') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return OnBoardingScreen();
      }));
    } else {
      ApiClient.instance!.guestToken = '';
      Navigator.pushNamed(context, '/myHomePage');
    }

    checking = false;
  }



  ///new social auth code


  onFacebookAuthSuccess(BuildContext context,FacebookUserData facebookUserData) async {
    Map<String, dynamic> req = {'face_book_token': facebookUserData.token};
    APIResponse resp =
        await ApiClient.post(request: req, endPoint: '/auth/facebook');
    print(resp);
    ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
    if (ApiClient.instance!.guestToken == '') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return OnBoardingScreen();
      }));
    } else {
      ApiClient.instance!.guestToken = '';
      Navigator.pushNamed(context, '/myHomePage');
    }
  }

  onGoogleAuthSuccess(BuildContext context,GoogleSignInAccount googleUser) async{

    Map<String, dynamic> req = {'email': googleUser.email};
    APIResponse resp =
        await ApiClient.post(request: req, endPoint: '/auth/google');
    print(resp);
    ApiClient.instance!.mAuthToken = resp['data']['accessToken'];
    print(ApiClient.instance!.mAuthToken);
    //Navigator.pushNamed(context, '/myHomePage');
    if (ApiClient.instance!.guestToken == '') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return OnBoardingScreen();
      }));
    } else {
      ApiClient.instance!.guestToken = '';
      Navigator.pushNamed(context, '/myHomePage');
    }
  }

}
