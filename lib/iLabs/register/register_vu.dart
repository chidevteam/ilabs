import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ilabs/iLabs/widgets/authentication/google_auth_button.dart';
import '../language/generated/locale_keys.g.dart';
import 'package:stacked/stacked.dart';
import '../widgets/authentication/auth_buttons.dart';
import '../widgets/authentication/facebook_auth_button.dart';
import '../widgets/authentication/text_field.dart';
import 'register_vm.dart';

class RegisterScreen extends ViewModelBuilderWidget<RegisterViewModel> {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      body: viewModel.isBusy
          ? const Text('Signing In')
          : Padding(
              padding: const EdgeInsets.only(top: 30,),
              child: SingleChildScrollView(
                child: Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: viewModel.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {Navigator.pop(context);},
                            icon: const Icon(
                              Icons.arrow_back,
                              color: Colors.grey,
                              size: 18,
                            ),),],),
                      const SizedBox(height: 50.0,),
                      registerImage(),
                      const SizedBox(height: 24.0,),
                      Text(tr(LocaleKeys.welcome_view_title),
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(height: 8.0,),
                      Text(tr(LocaleKeys.welcome_view_sub_title),
                        style: const TextStyle(
                            color: Colors.grey,
                            fontSize: 13.0,
                            fontWeight: FontWeight.w500),),
                       emailField(viewModel),
                      passwordField(viewModel),
                      confirmPasswordField(viewModel),
                      registerButton(viewModel, context),
                      Visibility(visible: viewModel.message == ''? false : true,
                          child: Text(viewModel.message)),
                      registerWithSocialMedia(),
                      // googleFacebookButtons(context,
                      //   onPressGoogle: () { viewModel.onGoogleSignUp(context);},
                      //   onPressFaceBook: () {viewModel.fbSignIn(context);},)

                      //todo: test new code
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GoogleAuthButton(onSuccess: viewModel.onGoogleAuthSuccess),
                          const SizedBox(
                            width: 20.0,
                          ),
                          FacebookAuthButton(onSuccess: viewModel.onFacebookAuthSuccess),
                        ],
                      )

                    ],),),),),
    );}

  Widget emailField(RegisterViewModel viewModel)=>
      myAuthField(label:  tr(LocaleKeys.login_view_email_hint),
  textInputType: TextInputType.emailAddress,
  onSaved:  (String? value) {
  viewModel.onEmail(value);},
  validator:  viewModel.onEmailValidator,);

  Widget passwordField(RegisterViewModel viewModel)=>
      myAuthField(label: tr(LocaleKeys.login_view_password_hint),
    textInputType: TextInputType.visiblePassword,
    obsecureField: true,
    onSaved: (String? value) {viewModel.onPassword(value);},
    validator: viewModel.onPasswordValidator,);

  Widget confirmPasswordField(RegisterViewModel viewModel)=> myAuthField(
    label: tr(LocaleKeys.welcome_view_confirm_password_hint),
    textInputType: TextInputType.visiblePassword,
    obsecureField: true,
      onSaved: (String? value) {viewModel.onConfirmPassword(value);},
    validator: viewModel.onconfirmPasswordValidator,);

  SvgPicture registerImage()=>SvgPicture.asset('iLabsSreens/login_signup/signup illustration.svg',);

  Widget registerButton(RegisterViewModel viewModel, context) =>
     myAuthButton(context,
        buttonColor:  const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
        textColor: Colors.white,
        label:  tr(LocaleKeys.welcome_view_create_button),
        onPressed:  (){viewModel.onClickItem(context);});

  Widget registerWithSocialMedia() =>Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 12.0),
      child: Row(
        children: [
          Expanded(flex: 3, child: Divider(
              color: Colors.grey[300],
              thickness: 1.0,),),
          Expanded(flex: 6,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(2, 7, 2, 10),
              child: Text(
                tr(LocaleKeys.welcome_view_signup_with_socialmedia),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey.shade500,
                    fontSize: 12,
                    fontWeight: FontWeight.w600),),),),
          Expanded(flex: 3, child: Divider(
              color: Colors.grey[300],
              thickness: 1.0,),)
        ],),);

  @override
  RegisterViewModel viewModelBuilder(BuildContext context) => RegisterViewModel();
}
