import '../models/region_dateformat/region_dateformat.dart';

// ignore: constant_identifier_names
const String SERVER_URL = "https://invoicelabs.co/api";
const String SERVER_URL2 = "https://invoicelabs.co/api/";
String SIGNATURE_FILE_NAME = "signature.png";
String BUSINESS_FILE_NAME = "business_logo.png";

class AppConstants {
  static List<String> languageOptions = ['English(US)'];

  static List<DateFormatIL> dayFormatOptions = <DateFormatIL>[
    const DateFormatIL(
        id: 0,
        name: "yyyy/MM/dd (2022/03/24)",
        format: "yyyy/MM/dd",
        date: "(2022/03/24)",
        mask: "####/##/##"),
    const DateFormatIL(
        id: 1,
        name: "yyyy-MM-dd (2022-03-24)",
        format: "yyyy-MM-dd",
        date: "(2022-03-24)",
        mask:  "####-##-##"),
    const DateFormatIL(
        id: 2,
        name: "MM/dd/yyyy (03/24/2022)",
        format: "MM/dd/yyyy",
        date: "(03/24/2022)",
        mask: "##/##/####"),
    const DateFormatIL(
        id: 3,
        name: "MM-dd-yyyy (03-24-2022)",
        format: "MM-dd-yyyy",
        date: "(03-24-2022)",
        mask: "##-##-####"),

    // '',
    // '',
    // '',
    // '',
  ];
}
