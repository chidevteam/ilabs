// import 'package:flutter/material.dart';
// import '../../../models/invoice/model.dart';
// import '../../home_controller.dart';
// // import '../add_invoice/add_invoice_vu.dart';

// Widget estimateListCell(Invoice item, HomeController homeVM,
//     GestureTapCallback onTap, BuildContext context) {
//   return GestureDetector(
//     onTap: onTap,
//     child: Padding(
//       padding: const EdgeInsets.fromLTRB(8.0, 2, 8.0, 2),
//       child: Card(
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(8.0),
//         ),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Padding(
//               padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
//               child: Row(
//                 children: [
//                   Icon(
//                     Icons.date_range_outlined,
//                     color: Colors.grey.shade500,
//                     size: 30,
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(left: 5),
//                     child: Text(
//                       item.dueDate.toString(),
//                       style: const TextStyle(color: Colors.grey, fontSize: 12),
//                     ),
//                   ),
//                   const Spacer(),
//                   Text(
//                     item.number.toString(),
//                     style: const TextStyle(color: Colors.grey, fontSize: 12),
//                   ),
//                 ],
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
//               child: Row(
//                 children: [
//                   Text(
//                     item.name!,
//                     style: const TextStyle(color: Colors.black87, fontSize: 15),
//                   ),
//                   const Spacer(),
//                   Text(
//                     "\$" + item.amount.toString(),
//                     style: const TextStyle(
//                       color: Colors.black87,
//                       fontSize: 14,
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
//               child: Row(
//                 children: [
//                   Text(
//                     item.read!,
//                     style: const TextStyle(color: Colors.green, fontSize: 14),
//                   ),
//                   const Spacer(),
//                   Text(
//                     (item.paid == true ? "Paid" : "Unpaid") +
//                         " - " +
//                         item.id.toString(),
//                     style: const TextStyle(color: Colors.grey, fontSize: 14),
//                   ),
//                 ],
//               ),
//             )
//           ],
//         ),
//       ),
//     ),
//   );
// }
