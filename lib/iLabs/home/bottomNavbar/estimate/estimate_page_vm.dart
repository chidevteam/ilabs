import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../models/invoice/model.dart';
import '../../../services/api_client.dart';

class EstimatePageVM extends BaseViewModel {
  Map<String, dynamic> data = {
    "all": <Invoice>[],
    "open": <Invoice>[],
    "closed": <Invoice>[],
  };

  EstimatePageVM() {
    debugPrint("INvoice Page Builder called");
    refreshData("all");
    refreshData("open");
    refreshData("closed");
  }

  refreshData(String tabCase) async {
    List<Invoice> items = data[tabCase];
    APIResponse resp = await ApiClient.get<InvoiceList>(
        endPoint: "/estimates",
        params: "/0/50/id/DESC/" + tabCase,
        fromJson: InvoiceList.fromMap);
    items.clear();
    items.addAll(resp["data"].data);
    debugPrint("nO OF Estimates: ${items.length}");
    notifyListeners();
  }
}
