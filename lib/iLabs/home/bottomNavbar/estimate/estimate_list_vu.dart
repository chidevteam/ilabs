import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/home/home_controller.dart';
// import 'package:stacked/stacked.dart';
import '../invoice/invoice_form/add_invoice_vu.dart';
import 'estimate_cell_vu.dart';
import 'estimate_page_vm.dart';
// import '../../../home/home_vm.dart';
import '../../../models/invoice/model.dart';
import '../invoice/invoice_list/invoice_cell_vu.dart';

class EstimateListScreen extends StatelessWidget {
  EstimateListScreen(this.invoicePageVM, this.invoiceFilter, this.homeVM,
      {Key? key})
      : super(key: key) {
    // debugPrint("Invoice Constructor Called");
  }
  final EstimatePageVM invoicePageVM;
  final String invoiceFilter;
  final HomeController homeVM;

  @override
  Widget build(BuildContext context) {
    // debugPrint("InvoicePageBuilder ----------------");
    List<Invoice> items = invoicePageVM.data[invoiceFilter];
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          await invoicePageVM.refreshData(invoiceFilter);
        },
        child: invoicePageVM.isBusy
            ? const CircularProgressIndicator()
            : Column(
                children: [
                  Expanded(
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: items.length,
                        itemBuilder: (context, index) {
                          return invoiceListCell(items[index], homeVM, () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddInvoiceScreen(
                                          homeVM,
                                          invoice: items[index],
                                          isInvoice: false,
                                        )));
                          }, context);
                        }),
                  ),
                ],
              ),
      ),
    );
  }
}
