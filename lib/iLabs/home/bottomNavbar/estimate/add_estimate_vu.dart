// import 'package:flutter/material.dart';

// import 'package:stacked/stacked.dart';

// import 'add_estimate_vm.dart';

// class AddEstimateScreen extends ViewModelBuilderWidget<AddEstimateViewModel> {
//   const AddEstimateScreen({Key? key}) : super(key: key);

//   @override
//   Widget builder(
//       BuildContext context, AddEstimateViewModel viewModel, Widget? child) {
//     return Scaffold(
//       body: Padding(
//         padding: const EdgeInsets.only(top: 128.0, left: 16.0, right: 16.0),
//         child: Expanded(
//           flex: 4,
//           child: Column(
//             children: [
//               estimateInvoiceImage(),
//               const SizedBox(
//                 height: 34.0,
//               ),
//               const Text(
//                 'Convert estimate onto invoice',
//                 textAlign: TextAlign.center,
//                 style: TextStyle(
//                     color: Colors.black,
//                     fontSize: 18.0,
//                     fontWeight: FontWeight.w500),
//               ),
//               const SizedBox(
//                 height: 16.0,
//               ),
//               Text(
//                 'Create proffissional invoices from any where within seconds and share with your customers through email and text.',
//                 textAlign: TextAlign.center,
//                 style: TextStyle(
//                     color: Colors.grey[500],
//                     fontSize: 14.0,
//                     fontWeight: FontWeight.w500),
//               ),
//               const Expanded(flex: 6, child: Text('')),
//               Expanded(
//                 flex: 1,
//                 child: estimateButton(),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Image estimateInvoiceImage() {
//     return Image.network(
//       'https://www.sender.net/wp-content/uploads/2019/08/email_invoicing_5_excellent_ways_to_professional_email_billing_2.png',
//       width: 140,
//       height: 140,
//       fit: BoxFit.cover,
//     );
//   }

//   Row estimateButton() {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         TextButton(
//           onPressed: () {},
//           child: const Text(
//             'Skip',
//             style: TextStyle(
//                 color: Colors.grey,
//                 fontSize: 16.0,
//                 fontWeight: FontWeight.w500),
//           ),
//           style: TextButton.styleFrom(
//             minimumSize: Size.zero,
//             padding: EdgeInsets.zero,
//             tapTargetSize: MaterialTapTargetSize.shrinkWrap,
//           ),
//         ),
//         TextButton(
//           onPressed: () {},
//           child: Text(
//             'Next',
//             style: TextStyle(
//                 color: Colors.orange[800],
//                 fontSize: 16.0,
//                 fontWeight: FontWeight.w500),
//           ),
//           style: TextButton.styleFrom(
//             minimumSize: Size.zero,
//             padding: EdgeInsets.zero,
//             tapTargetSize: MaterialTapTargetSize.shrinkWrap,
//           ),
//         )
//       ],
//     );
//   }

//   @override
//   AddEstimateViewModel viewModelBuilder(BuildContext context) {
//     return AddEstimateViewModel();
//   }
// }
