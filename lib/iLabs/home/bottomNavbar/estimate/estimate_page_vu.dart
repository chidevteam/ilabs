import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../home_controller.dart';
import '../invoice/invoice_form/add_invoice_vu.dart';

import '../estimate/estimate_list_vu.dart';
import 'estimate_page_vm.dart';

// ignore: must_be_immutable
class EstimatePageView extends ViewModelBuilderWidget<EstimatePageVM> {
  const EstimatePageView(this.homeVM, {Key? key}) : super(key: key);
  final HomeController homeVM;

  @override
  Widget builder(
      BuildContext context, EstimatePageVM viewModel, Widget? child) {
    return Scaffold(
      // appBar: tabBarItems(["All", "Outstanding", "Paid"]),
      body: TabBarView(
        children: [
          EstimateListScreen(viewModel, "all", homeVM),
          EstimateListScreen(viewModel, "open", homeVM),
          EstimateListScreen(viewModel, "closed", homeVM),
        ],
      ),
      floatingActionButton: floatingButton(context, homeVM),
    );
  }

  FloatingActionButton floatingButton(
      BuildContext context, HomeController homeVM) {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddInvoiceScreen(
                homeVM,
                invoice: null,
                isInvoice: false,
              ),
            ));
      },
      backgroundColor:
          const Color.fromRGBO(0xff, 0x78, 0x48, 1.0).withOpacity(0.8),
      elevation: 0.0,
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
      heroTag: "estimte_page_vu",
    );
  }

  @override
  EstimatePageVM viewModelBuilder(BuildContext context) {
    var vm = EstimatePageVM();
    // vm.refreshData(invoiceFilter);
    return vm;
  }
}
