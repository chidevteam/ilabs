import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../../models/invoice/model.dart';
import '../../../home_controller.dart';
// import '../add_invoice/add_invoice_vu.dart';

Widget invoiceListCell(Invoice item, HomeController homeVM,
    GestureTapCallback onTap, BuildContext context) {
  return GestureDetector(
    onTap: onTap,
    child: Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color:
                  const Color.fromRGBO(0x29, 0x00, 0x00, 1.0).withOpacity(0.2),
              spreadRadius: 0.1,
              blurRadius: 3,
              offset: const Offset(2, 0),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 11.0),
              child: Row(
                children: [
                  const Icon(
                    Icons.date_range_outlined,
                    color: Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                    size: 16.5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(
                      item.dueDate.toString(),
                      style: GoogleFonts.poppins(
                          color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                          fontSize: 12),
                    ),
                  ),
                  const Spacer(),
                  Text(
                    item.number.toString(),
                    style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                        fontSize: 12),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10.0, 5.0),
              child: Row(
                children: [
// #193145
                  Text(
                    item.name!,
                    style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                  const Spacer(),
                  Obx(
                    ()=> Text(
                      "${homeVM.userDefault.rXregionCurrency.value} " + item.amount.toString(),
                      style: GoogleFonts.poppins(
                          color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 13.0),
              child: Row(
                children: [
                  Text(
                    item.read!,
                    style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(0xfb, 0xbc, 0x2c, 1.0),
                        fontSize: 12),
                  ),
                  const Spacer(),
                  Text(
                    (item.paid == true ? "Paid" : "Unpaid") +
                        " - " +
                        item.id.toString(),
                    style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(0xac, 0xac, 0xac, 1.0),
                        fontSize: 12),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ),
  );
}
