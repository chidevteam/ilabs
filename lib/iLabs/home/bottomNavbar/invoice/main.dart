import 'package:flutter/material.dart';
import '../../../services/api_client.dart';
// import 'package:stacked/stacked.dart';
import '../../home_controller.dart';
import "invoice_page_vu.dart";

void main() async {
  //
  await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
  debugPrint("Starting Item View");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  HomeController controller = HomeController();

  @override
  Widget build(BuildContext context) {
    debugPrint("MyApp build again !!!");
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.orange),
        home: DefaultTabController(
          length: 3,
          child: Scaffold(
              appBar: AppBar(
                elevation: 1.0,
                title: const Text(
                  "Invoices",
                  style: TextStyle(color: Colors.black54),
                ),
                backgroundColor: Colors.white,
                bottom: tabBarItems(["All", "Outstanding", "Paid"]),
              ),
              body: InvoicePageView(controller)),
        ));
  }
}

TabBar tabBarItems(List<String> sList) {
  List<Tab> tabList = [];
  TextStyle tStyle = const TextStyle(fontSize: 12, fontWeight: FontWeight.w400);
  for (String item in sList) {
    Text text = Text(item, style: tStyle);
    Tab tab = Tab(child: text);
    tabList.add(tab);
  }

  return TabBar(
      unselectedLabelColor: Colors.black45,
      indicatorWeight: 3,
      // indicatorColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
      labelColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
      labelPadding: EdgeInsets.zero,
      tabs: tabList);
}
