import 'package:flutter/material.dart';
import 'invoice_page_vm.dart';
import 'package:stacked/stacked.dart';
import '../../home_controller.dart';
import 'invoice_list/invoice_list_vu.dart';
import 'invoice_form/add_invoice_vu.dart';

// class InvoicePageView extends ViewModelBuilder<InvoicePageViewModel> {
//   const InvoicePageView(this.homeVM, {Key? key}) : super(key: key);

class InvoicePageView extends ViewModelBuilderWidget<InvoicePageViewModel> {
  const InvoicePageView(this.homeVM, {Key? key}) : super(key: key);
  final HomeController homeVM;

  @override
  Widget builder(
      BuildContext context, InvoicePageViewModel viewModel, Widget? child) {
    return Scaffold(
      // appBar: tabBarItems(["All", "Outstanding", "Paid"]),
      body: TabBarView(
        children: [
          InvoiceListScreen(viewModel, "all", homeVM),
          InvoiceListScreen(viewModel, "outstanding", homeVM),
          InvoiceListScreen(viewModel, "paid", homeVM)
        ],
      ),
      floatingActionButton: floatingButton(context, homeVM, viewModel),
    );
  }

  FloatingActionButton floatingButton(BuildContext context,
      HomeController homeVM, InvoicePageViewModel viewModel) {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddInvoiceScreen(
                homeVM,
                invoice: null,
                isInvoice: true,
              ),
            )).then((value) {
          if (value ?? false) {
            viewModel.refreshData("all");
          }
        });
      },
      backgroundColor:
          const Color.fromRGBO(0xff, 0x78, 0x48, 1.0).withOpacity(0.8),
      elevation: 0.0,
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
      heroTag: "invoice_page_vu",
    );
  }

  @override
  InvoicePageViewModel viewModelBuilder(BuildContext context) {
    var vm = InvoicePageViewModel();
    // vm.refreshData(invoiceFilter);
    return vm;
  }
}
