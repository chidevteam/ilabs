import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../models/invoice/model.dart';
import '../../../services/api_client.dart';

class InvoicePageViewModel extends BaseViewModel {
  Map<String, dynamic> data = {
    "all": <Invoice>[],
    "outstanding": <Invoice>[],
    "paid": <Invoice>[],
  };

  InvoicePageViewModel() {
    debugPrint("INvoice Page Builder called");
    refreshData("all");
    refreshData("outstanding");
    refreshData("paid");
  }

invoiceDeleted(int index,String tabCase){
List<Invoice> items = data[tabCase];
items.removeAt(index);
}

  refreshData(String tabCase) async {
    List<Invoice> items = data[tabCase];
    APIResponse resp = await ApiClient.get<InvoiceList>(
        endPoint: "/invoices",
        params: "/0/50/id/DESC/" + tabCase,
        fromJson: InvoiceList.fromMap);
    items.clear();
    items.addAll(resp["data"].data);
    debugPrint("nO OF INVOICES: ${items.length}");
    notifyListeners();
  }
}
