import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:get/get.dart';
// import 'package:get/get_state_manager/src/rx_flutter/rx_notifier.dart';
import 'package:ilabs/iLabs/home/app_constants.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/client/client_list/client_list_controller.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/sec05_payment_details/payment_detail_forms/discount_form/model.dart';
import 'package:ilabs/iLabs/models/userdefaults/model.dart';
import 'package:intl/intl.dart';
import 'package:open_filex/open_filex.dart';
import 'package:share_plus/share_plus.dart';
import 'package:stacked/stacked.dart';
import '../../../home_controller.dart';
import '../../../../models/invoice_data/model.dart';
import '../../../../models/tax/model.dart';
import '../../../../models/client/model.dart';
import '../../../../models/invoice/model.dart';
import '../../../../services/api_client.dart';

class AddInvoiceViewModel extends BaseViewModel {
  InvoiceData? invoiceData;
  bool isInvoice;
  Invoice? invoice;
  String fileName = 'example.pdf';
  List<Map> tabItems = [
    {"title": "Edit", "icon": Icons.edit_outlined},
    {"title": "Preview", "icon": Icons.cloud_circle},
    {"title": "History", "icon": Icons.wallet_giftcard},
    {"title": "PDF VU", "icon": Icons.wallet_giftcard},
    {"title": "Test PDF", "icon": Icons.wallet_giftcard},
  ];
  List<String> items = ['Delete', 'Open In', 'Share', 'Print', 'Duplicate'];
  int currentindex = 0;
  HomeController homecontroller;
  String endPointPrefix = "/invoices";

  int redrawIndex = 0;

  AddInvoiceViewModel(Invoice? invoice, this.homecontroller, this.isInvoice) {
    invoice = invoice;
    if (isInvoice == false) endPointPrefix = "/estimates";
    initializeInvoiceData();
    if (invoice != null) {
      if (invoice.invoiceData != null) {
        // invoiceData = invoice.invoiceData;
        //todo: uncomment after fixing date format issue
      }
      loadInvoiceData(invoice.id!).then((value) {
        if (invoiceData != null) {
          invoice!.invoiceData = invoiceData;
        }
      });
    }
  }
  initializeInvoiceVariablesToInvoiceData() {}
  initializeInvoiceData() {
    invoiceData = InvoiceData(
        invoiceInfo: InvoiceInfo(
            isInvoice: isInvoice,
            number: homecontroller.userDefault.invoiceNumber,
            terms: homecontroller.userDefault.dueDate,
            dateFormat: homecontroller.userDefault.regionDateFormat),
        photos: [],
        items: [],
        taxModel: TaxModel(
            taxType: homecontroller.userDefault.taxType ?? "",
            taxLabel: homecontroller.userDefault.taxLabel ?? "",
            taxRate: homecontroller.userDefault.taxRate ?? "",
            taxInclusive: homecontroller.userDefault.taxInclusive!),
        paymentInstructions: homecontroller.paymentInst!.copy(),
        businessDetail: homecontroller.businessDetails.copy(),
        client: Client(),
        discountAmount: 0,
        tax: '0',
        total: '0',
        balanceDue: '0',
        notes: isInvoice
            ? homecontroller.userDefault.invoiceNote
            : homecontroller.userDefault.estimateNote);
  }

  Future loadInvoiceData(int id) async {
    APIResponse resp = await ApiClient.get<InvoiceData>(
        endPoint: endPointPrefix,
        params: "/$id",
        fromJson: InvoiceData.fromMap);
    invoiceData = resp['data'];
    downloadBusinessLogoFile();
    notifyListeners();
  }

  downloadBusinessLogoFile() async {
    String fileURL = SERVER_URL2 + invoiceData!.businessDetail.businessLogoUrl!;
    String dt = DateTime.now().millisecondsSinceEpoch.toString();
    String filePath = homecontroller.applicationDir + "/$dt.jpg}";
    File? file =
        await ApiClient.downloadFile(fileURL: fileURL, filePath: filePath);
    if (file != null) {
      invoiceData!.businessDetail.businessLogoFile = file.path;
      notifyListeners();
    }
  }

  onAddImage(Photo? photo) {
    if (photo != null) {
      invoiceData!.photos.insert(0, photo);
      notifyListeners();
      invoicePatch(invoiceData!.id);
    }
  }

  onUpdatePhoto(Photo? photo) {
    if (photo != null) {
      int index =
          invoiceData!.photos.indexWhere((element) => element.id == photo.id);
      if (index != -1) {
        invoiceData!.photos[index] = photo;
        notifyListeners();
        invoicePatch(invoiceData!.id);
      }
    }
  }

  onDeleteImage(Photo photo) async {
    if (await deleteImageFromServer(photo.id.toString())) {
      invoiceData!.photos.removeWhere((element) => element.id == photo.id);
    }
    notifyListeners();
    invoicePatch(invoiceData!.id);
  }

  Future<bool> deleteImageFromServer(String photoID) async {
    // delete photos end point is same in both cases
    var response = await ApiClient.delete(endPoint: "/invoicephotos/$photoID");
    if (response['status'] == 'success') {
      return true;
    } else {
      return false;
    }
  }

  onCurrent(int index) {
    currentindex = index;
    print("curretn index is $index");
    if (index == 2) redrawIndex += 1;
    notifyListeners();
  }

  Future<InvoiceData?> invoicePatch(num? invoiceID,
      {Map<String, dynamic>? request}) async {
    bool shouldIncrementInvoiceNumber = invoiceData!.id == null;
    String endPoint = "$endPointPrefix/$invoiceID";
    APIResponse respx = await ApiClient.patch<InvoiceData>(
        request: request ?? invoiceData!.toMap(),
        endPoint: endPoint,
        fromJson: InvoiceData.fromMap);
    debugPrint("$respx");
    invoiceData = respx['data'];
    downloadBusinessLogoFile();
    if (shouldIncrementInvoiceNumber) {
      homecontroller.updateinvoiceNumberinUserDefualts();
    }
    debugPrint("$invoiceData");
    if (invoiceData != null) {
      // invoice!.invoiceData = invoiceData;
    }
    return invoiceData;
  }

  num calculateSubTotal() {
    //todo: work here
    num subTotal = 0;
    for (var element in invoiceData!.items!) {
      subTotal += calculateItemTotal(element);
    }
    debugPrint("sub total $subTotal");
    invoiceData!.subTotal = subTotal.toString();
    return subTotal;
  }

  Future markPaid() async {
    print("mark paid called with id ${invoiceData!.id}");
    if (invoiceData!.id != null) {
      //invoice patch will update the invoiceData and will notify listeners
      await invoicePatch(invoiceData!.id, request: {
        "paid": !invoiceData!.paid,
      });
    } else {
      invoiceData!.paid = !invoiceData!.paid;
      await invoicePatch(invoiceData!.id);
    }
  }

//convert estimate to invoice
  Future makeInvoice() async {
    await ApiClient.patch(
        request: {},
        endPoint: "/estimates/markclosed/${invoiceData!.id}",
        fromJson: null);
  }

  /////////////////////////////// client functionality ///////////////////////

  addClient(Client? newClient) {
    if (newClient != null) {
      invoiceData!.client = newClient;
      notifyListeners();
      invoicePatch(invoiceData!.id);
    }
  }

  updateClient(Client? updatedClient) {
    if (updatedClient != null) {
      invoiceData!.client = updatedClient;
      notifyListeners();
      int index = Get.find<ClientListController>()
          .clientList
          .indexWhere((element) => element.clientId == updatedClient.clientId);
      if (index != -1) {
        Get.find<ClientListController>().clientList[index] = updatedClient;
      }
    }
  }

  Future deleteClient() async {
    invoiceData!.setClient(Client(
        clientId: 0,
        name: "",
        address: "",
        amount: "",
        email: "",
        fax: "",
        mobile: "",
        phone: ""));
    if (invoiceData!.id != null) {
      print("calling delete client");
      await invoicePatch(invoiceData!.id);
    }
    notifyListeners();
  }

//====================================== Item functionality /////////////////////
  



  ///this method calculates the item total amount after applying discount (Percentage discount) or (Flat) along with quantity 
  num calculateItemTotal(ItemData element) {
    ///todo move this method to addinvoiceitemformvm
    num subTotal = 0;
    if (element.discountType == 'Percentage') {
      subTotal += (element.unitCost -
              (element.unitCost * element.discountAmount / 100)) *
          element.quantity;
    } else {
      subTotal +=
          (element.unitCost * element.quantity) - element.discountAmount;
    }
    return subTotal;
  }

  addNewItem(ItemData? item) {
    if (item != null) {
      item.total = calculateItemTotal(item).toString();
      invoiceData!.items!.insert(0, item);
      notifyListeners();
      calculateSubTotal();
      applyDiscountAndTax();
      if (invoiceData!.paid) {
        invoiceData!.paid = !invoiceData!.paid;
      }
      invoicePatch(invoiceData!.id);
    }
  }

  updateItem(int index, ItemData? item) {
    {
      if (item != null) invoiceData!.items![index] = item;
      notifyListeners();
      calculateSubTotal();
      applyDiscountAndTax();
      invoicePatch(invoiceData!.id);
    }
  }

  deleteItem(int index) async {
    bool isDeleted = await deleteInvoiceItemFromAPI(
        invoiceData!.items![index].id.toString());

    if (isDeleted) {
      invoiceData!.items!.removeAt(index);
      calculateSubTotal();
      applyDiscountAndTax();
      invoicePatch(invoiceData!.id);
      notifyListeners();
    }
  }

  Future<bool> deleteInvoiceItemFromAPI(String invoiceItemID) async {
    APIResponse resp = await ApiClient.delete(
        endPoint: "/invoiceitems/$invoiceItemID", fromJson: null);
    if (resp['status'] == 'success') {
      return true;
    } else {
      return false;
    }
  }

  onSignToggle() {
    if (invoiceData!.signature == "" || invoiceData!.signDate == null) {
      addSignatureFromUserDefaults();
    } else {
      invoiceData!.signDate = "";
      invoiceData!.signature = "";
      notifyListeners();
      invoicePatch(invoiceData!.id);
    }
  }

  addSignatureFromUserDefaults() {
    invoiceData!.signature = homecontroller.userDefault.signature;
    invoiceData!.signDate =
        DateFormat(homecontroller.userDefault.regionDateFormat)
            .format(DateTime.now());
    notifyListeners();
    invoicePatch(invoiceData!.id);
  }

  Future<void> uploadSignature(String imgPath) async {
    homecontroller.signatureFilePath = imgPath;
    // signaturePath = imgPath;
    notifyListeners();
    Request addReq = {};

    var res = await ApiClient.postMultiPart(
        request: addReq,
        endPoint: "/userdefaults",
        fromJson: UserDefault.fromMap,
        filePath: imgPath,
        imageKey: 'signature');

    homecontroller.userDefault.signature = res['data'].signature!;
    addSignatureFromUserDefaults();
  }

  ///======================================= all about payment instructions =======================================
  // https://invoicelabs.bit.ai/docs/view/qj6Ck7Y8vZDOu7E8

  onDiscountEdited(Discount? discount) {
    if (discount != null) {
      invoiceData!.discountType = discount.discountType;
      invoiceData!.discountAmount = discount.amount;
      applyDiscountAndTax();
      invoicePatch(invoiceData!.id);
    }
  }

  onTaxEdited(TaxModel? taxModel) {
    if (taxModel != null) {
      invoiceData!.taxModel = taxModel;
      applyDiscountAndTax();
      invoicePatch(invoiceData!.id);
    }
  }

  num applyDiscountAndTax() {
    TaxModel taxModel = invoiceData!.taxModel;

    ///taxable Subtotal
    num tsubTotal = 0;

    ///non Taxable Subtotal
    num ntsubTotal = 0;
    num totalTax = 0;

    if (invoiceData!.taxModel.taxType == 'Per Item') {
      num allItemsTax = 0;
      num subTotal = 0;

      ///calculating tax
      invoiceData!.items!.forEach((element) {
        if (element.taxable) {
          allItemsTax += calculateTaxWithFormula(
              inclusive: taxModel.taxInclusive,
              subTotal: calculateItemTotal(element),

              //along with discount
              taxRate: element.taxValue);
        }
      });

      subTotal = calculateSubTotal();

      ///applying discount
      if (invoiceData!.discountType == 'Percentage') {
        subTotal = subTotal - (subTotal * (invoiceData!.discountAmount! / 100));
        invoiceData!.discount =
            ((subTotal) * (invoiceData!.discountAmount! / 100)).toString();
      }
      if (invoiceData!.discountType == 'Flat Amount') {
        subTotal = subTotal - (invoiceData!.discountAmount!);
        invoiceData!.discount = invoiceData!.discountAmount!.toString();
      }

      ///applying calculated tax
      invoiceData!.tax = allItemsTax.toString();
      invoiceData!.total =
          (subTotal + (taxModel.taxInclusive ? 0 : allItemsTax)).toString();
      calculateBalanceDue();
    } else {
      ///calculating taxable and nontaxable subtotals
      invoiceData!.items!.forEach((element) {
        element.taxable
            ? tsubTotal += calculateItemTotal(element)
            : ntsubTotal += calculateItemTotal(element);
      });

      ///aplying discount
      if (invoiceData!.discountType == 'Percentage') {
        tsubTotal =
            tsubTotal - (tsubTotal * (invoiceData!.discountAmount! / 100));
        ntsubTotal =
            ntsubTotal - (ntsubTotal * (invoiceData!.discountAmount! / 100));
        invoiceData!.discount =
            ((tsubTotal + ntsubTotal) * (invoiceData!.discountAmount! / 100))
                .toString();
      }
      if (invoiceData!.discountType == 'Flat Amount') {
        num total = tsubTotal + ntsubTotal;
        tsubTotal =
            tsubTotal - (tsubTotal / total * invoiceData!.discountAmount!);
        ntsubTotal =
            ntsubTotal - (ntsubTotal / total * invoiceData!.discountAmount!);
        invoiceData!.discount = invoiceData!.discountAmount!.toString();
      }

      ///applying tax
      if (taxModel.taxType == 'On The Total') {
        totalTax = calculateTaxWithFormula(
            inclusive: taxModel.taxInclusive,
            subTotal: tsubTotal,
            taxRate: num.parse(taxModel.taxRate));
        invoiceData!.total =
            (tsubTotal + ntsubTotal + (taxModel.taxInclusive ? 0 : totalTax))
                .toString();
      }

      if (invoiceData!.taxModel.taxType == 'Deducted') {
        totalTax = calculateTaxWithFormula(
            inclusive: false,
            subTotal: tsubTotal,
            taxRate: num.parse(taxModel.taxRate));
        invoiceData!.total = (tsubTotal + ntsubTotal - totalTax).toString();
      }
      invoiceData!.tax = totalTax.toStringAsFixed(2);
      calculateBalanceDue();
    }

    return totalTax;
  }

  num calculateTaxWithFormula(
      {required bool inclusive, required num subTotal, required num taxRate}) {
    num totalTax = 0;
    if (inclusive && taxRate != 0) {
      totalTax = subTotal - (subTotal / (1 + (taxRate / 100)));
    } else if(!inclusive && taxRate != 0)
    {
      totalTax = (subTotal * (taxRate / 100));
    }
    return totalTax;
  }

  calculateBalanceDue() {
    num totalPayment = 0;
    invoiceData!.payments.forEach((element) {
      totalPayment += element.amount!;
    });
    invoiceData!.balanceDue =
        ((num.parse(invoiceData!.total!) - totalPayment).toString());
  }




///this code will be useful in the second phase of App 
// sendInvoiceViaEmail() async {
//   final Email email = Email(
//   body: 'invoice link here',
//   subject: '${invoiceData!.businessDetail.businessName} has sent you an ${isInvoice?'invoice': 'estimate'}',
//   cc: ['cc@example.com'],
//   bcc: ['bcc@example.com'],
// );
// await FlutterEmailSender.send(email);
// }
//======================================Invoice Pop up menu options//////////////////////////////


sendInvoiceViaEmail(){
  print("hwllo");
}

shareInvoice() async {
  setBusy(true);
  print("share invoice");
  APIResponse resp = await  ApiClient.get(endPoint: '/invoices/uniquepdf/${invoiceData!.id}');
  setBusy(false);
await Share.share('Sharing test invoice link! \n${SERVER_URL2}${resp['data']['pdf_url']}', subject: 'Sharing test invoice link!',);
}

Future<void>  deleteInvoice( )async{
APIResponse resp = await ApiClient.delete(endPoint: '/invoices/${invoiceData!.id}');
}

openInApps(){
OpenFilex.open(fileName);
}

Future<void> duplicateInvoice()async{
 await homecontroller.updateinvoiceNumberinUserDefualts();
 invoiceData!.invoiceInfo.number = homecontroller.userDefault.invoiceNumber ;
APIResponse  resp = await ApiClient.post(request: invoiceData!.toMap(), endPoint: '/invoices',fromJson: InvoiceData.fromMap);
if(resp['data']!=null){
invoiceData = resp['data'];
 homecontroller.updateinvoiceNumberinUserDefualts();
}
}

}

// onPopUpMenu(value) {
//   value = !value;
//   notifyListeners();
// }

// onSignToggle() {
//   isSignOnToggle = !isSignOnToggle;
//   notifyListeners();
// }

// num calculateSubTotal() {
//   num amount = 0;
//   for (var element in invoiceData!.items!) {
//     amount += element.unitCost! * element.quantity;
//   }
//   debugPrint("sub total $amount");
//   return amount;
// }

// discount = calculateDiscountAmount();
// tax = calculateTaxAmount();
// total = calculateTotal();

// num calculateTotal() {
//   num value = calculateTaxAmount() - calculateAmountBeforeTax();
//   debugPrint("discount amount $value");
//   return value;
// }

// calculateDiscountAmount() {
//   num value = 0;
//   for (var element in itemsList) {
//     if (element.discountType == "Percentage") {
//       value += (element.unitCost! * (element.discountAmount! / 100)) *
//           (element.quantity);
//     } else if (element.discountType == "Flat Amount") {
//       value += (element.unitCost! * (element.discountAmount! / 100)) *
//           (element.quantity);
//     }
//   }
//   log("discount amount $value");
//   return value + 0.0;
// }

// num calculateAmountBeforeTax() {
//   num value = 0;
//   for (var element in itemsList) {
//     value += element.quantity * (element.unitCost! - element.discountAmount!);
//   }
//   debugPrint(" amount before tax $value");
//   return value;
// }

// calculateTaxAmount() {
//   num value = 0;
//   for (var element in itemsList) {
//     value += element.quantity *
//         ((element.unitCost! - element.discountAmount!) *
//             (element.taxValue! / element.unitCost!));
//   }
//   debugPrint("tax amount $value");
//   return value;
// }
