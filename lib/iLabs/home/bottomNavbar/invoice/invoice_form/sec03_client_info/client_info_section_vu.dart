import 'package:flutter/material.dart';
// import 'package:ilabs/iLabs/models/invoice_data/model.dart';

// import 'add_client_to_invoice/select_client_for_invoice_vm.dart';
import '../../../../../Utils/utils.dart';
import '../../../../../widgets/buttons/add_circled_button_with_label.dart';
import '../../../client/add_client/add_client_vu.dart';
import '../add_invoice_vm.dart';
// import 'add_client_to_invoice/select_client_for_invoice_vm.dart';
import 'add_client_to_invoice/select_client_for_invoice_vu.dart';
import '../../../../../models/client/model.dart';

class ClientInfoSectionVU extends StatelessWidget {
  ClientInfoSectionVU({required this.viewModel, Key? key}) : super(key: key);
  AddInvoiceViewModel viewModel;
  // void Function() onTapAddClient;
  // void Function() deleteClient;
  // void Function() onPressedClientName;
  // Client? client;


  @override
  Widget build(BuildContext context) {
    return (viewModel.invoiceData!.client.clientId ?? 0) == 0
        ? AddCircledButtonWithLabel(
            label: "Add Client Details",
            onTap: () {
              Utils.push(context, const SelectClientForInvoiceScreen())
                  .then((newClient) {
                viewModel.addClient(newClient);
              });
            },
          )
        : Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextButton(
                    onPressed: () async {
                      Client? tempClient = await Utils.push(context,
                          NewClientScreen(viewModel.invoiceData!.client));
                      viewModel.updateClient(tempClient);
                    },
                    child: Text(
                      viewModel.invoiceData!.client.name ?? "no client",
                      style: const TextStyle(color: Colors.black),
                    )),
                IconButton(
                    onPressed: viewModel.deleteClient,
                    icon: const Icon(
                      Icons.delete,
                      color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
                    ))
              ],
            ),
        );
  }
}


///old design 

                            // class ClientInfoSectionVU extends StatelessWidget {
//   final AddInvoiceViewModel viewModel;

//   const ClientInfoSectionVU(this.viewModel, {Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     Client? invoiceClient = viewModel.invoiceData!.client;
//     debugPrint("invoice Client = $invoiceClient");
//     return Padding(
//       padding: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0),
//       child: Card(
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(8.0),
//           ),
//           elevation: 0.7,
//           child: Padding(
//             padding: const EdgeInsets.fromLTRB(10.0, 12.0, 8.0, 12.0),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     const Text(
//                       'Client info',
//                       style: TextStyle(
//                           fontSize: 15.0, fontWeight: FontWeight.w500),
//                     ),
//                     IconButton(
//                         padding: EdgeInsets.zero,
//                         constraints: const BoxConstraints(),
//                         onPressed: () {
//                           Navigator.push(context,
//                               MaterialPageRoute(builder: ((context) {
//                             return const SelectClientForInvoiceScreen();
//                           }))).then((tempClient) {
//                             if (tempClient != null) {
//                               invoiceClient = tempClient as Client;
//                               viewModel.invoiceData!.client = tempClient;
//                               viewModel.notifyListeners();
//                             }
//                           });
//                         },
//                         icon: const Icon(
//                           Icons.edit_outlined,
//                           size: 22.0,
//                           color: Colors.orange,
//                         ))
//                   ],
//                 ),
//                 const SizedBox(
//                   height: 8,
//                 ),
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     _showClientname(invoiceClient),
//                     const SizedBox(
//                       height: 6,
//                     ),
//                     _showClientEmail(invoiceClient),
//                     const SizedBox(
//                       height: 6,
//                     ),
//                     _showClientMobile(invoiceClient),
//                   ],
//                 ),
//               ],
//             ),
//           )),
//     );
//   }
// }

// Widget _showClientname(Client? client) {
//   String name = (client == null) ? "" : (client.name ?? "");

//   return Row(
//     children: [
//       const Icon(
//         Icons.person,
//         color: Colors.blueAccent,
//         size: 16,
//       ),
//       const SizedBox(
//         width: 8,
//       ),
//       Text(name, style: TextStyle(color: Colors.grey[700])),
//       const Spacer(),
//     ],
//   );
// }

// Widget _showClientEmail(Client? client) {
//   String email = (client == null) ? "" : (client.email ?? "");

//   return Row(
//     children: [
//       const Icon(
//         Icons.mail,
//         color: Colors.blueAccent,
//         size: 16,
//       ),
//       const SizedBox(
//         width: 8,
//       ),
//       Text(
//         email,
//         style: TextStyle(color: Colors.grey[700]),
//       ),
//     ],
//   );
// }

// Widget _showClientMobile(Client? client) {
//   String mobile = (client == null) ? "" : client.mobile.toString();

//   return Row(
//     children: [
//       const Icon(
//         Icons.phone,
//         color: Colors.blueAccent,
//         size: 16,
//       ),
//       const SizedBox(
//         width: 8,
//       ),
//       Text(mobile, style: TextStyle(color: Colors.grey[700])),
//     ],
//   );
// }

