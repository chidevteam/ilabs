import 'package:flutter/material.dart';
import '../../../../../../models/client/model.dart';

Widget clientCell(
    Client item, GestureTapCallback? tapFunc, BuildContext context) {
  return GestureDetector(
    onTap: tapFunc ??
        () {
          Navigator.pop(context, item);
          // print('item addddddddddddddddedressssssss${item.name}');
        },
    child: Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 8),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.name ?? "",
                    style: const TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    item.email.toString(),
                    style: TextStyle(fontSize: 12.0, color: Colors.grey[600]),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
