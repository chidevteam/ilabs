import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/client/client_list/client_list_controller.dart';
import 'package:stacked/stacked.dart';
import '../../../../../../models/client/model.dart';
import '../../../../../../services/api_client.dart';

class SelectClientForInvoiceViewModel extends BaseViewModel {
  ClientListController clientListController = ClientListController();
  List<Client> clients = Get.find<ClientListController>().clientList.value;

  onSearchFieldValueChanges(String val) {
    if (val.isEmpty) {
      clients = clientListController.clientList.value;
    } else {
      clients = clientListController
          .clientList
          .where((element) =>
      element.name!.toLowerCase().contains(val.toLowerCase()) ||
          element.email!
              .toLowerCase()
              .contains(val.toLowerCase()) ||
          element.mobile.toString().toLowerCase().contains(val.toLowerCase()))
          .toList();
    }
    notifyListeners();
  }

  addNewClientInCLients(Client? client){
      if (client != null) {
          Get.find<ClientListController>().clientList.insert(0, client);
          }
  }

}
