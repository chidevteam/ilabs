import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/client/add_client/add_client_controller.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/client/add_client/add_client_vu.dart';
import 'package:stacked/stacked.dart';
import 'select_client_for_invoice_vm.dart';
import '../../../../../../models/client/model.dart';
import 'select_client_cell_vu.dart';

class SelectClientForInvoiceScreen
    extends ViewModelBuilderWidget<SelectClientForInvoiceViewModel> {
  const SelectClientForInvoiceScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context,
      SelectClientForInvoiceViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Select Client',
          style: TextStyle(
              color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black54,
            size: 18.0,
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
              elevation: 0.2,
              margin: const EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
              child: itemSearchbox(context,viewModel)),
          Expanded(
            flex: 8,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: viewModel.clients.length,
                itemBuilder: (context, index) {
                  Client newClient = viewModel.clients[index];
                  return clientCell(newClient, () {
                    debugPrint("Client Tapped ${newClient.name}");
                    Navigator.pop(context, newClient);
                  }, context);
                }),
          )
        ],
      ),
      floatingActionButton: floatAddClientButton(context, viewModel),
    );
  }

  FloatingActionButton floatAddClientButton(
      BuildContext context, SelectClientForInvoiceViewModel vm) {
    return FloatingActionButton(
      onPressed: () async {
       Utils.push(context, NewClientScreen(null)).then((client) {
        vm.addNewClientInCLients(client);
        Navigator.pop(context,client);
       });
      },
      backgroundColor: Colors.orange,
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  popOut(BuildContext context, item) {}

  Widget itemSearchbox(BuildContext context,SelectClientForInvoiceViewModel viewModel) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 8.0),
      child: TextField(
        style: Theme.of(context).textTheme.headline6,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(top: 6.0, left: 6.0),
          hintText: 'Search',
          prefixIcon: IconButton(
            icon: const Icon(
              Icons.search,
              size: 16.0,
            ),
            color: Colors.grey[400],
            onPressed: () {},
          ),
          hintStyle: TextStyle(
              color: Colors.grey[500],
              fontWeight: FontWeight.normal,
              fontSize: 14.0),
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(6.0)),
          ),
          fillColor: Colors.grey[100],
          filled: true,
          focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
              borderSide: BorderSide(
                color: Colors.grey.shade200,
              )),
        ),
          onChanged: viewModel.onSearchFieldValueChanges
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    SelectClientForInvoiceViewModel vm = SelectClientForInvoiceViewModel();
    return vm;
  }
}


// frequentlyUsed(),
// const Expanded(child: Text('')),
// const Padding(
//   padding: EdgeInsets.only(left: 10.0, bottom: 8.0),
//   // child: DecoratedBox(
//   //   col
//   //   decoration: BoxDecoration(color: Colors.blue),
//   //   child: Text('Some text...'),
//   // ),
//   child: Text(
//     'Client List',
//     style: TextStyle(
//       fontSize: 14.0,
//       fontWeight: FontWeight.w500,
//     ),
//   ),
// ),

  // Row addNewItem() {
  //   return Row(
  //     children: [
  //       Expanded(
  //         child: Card(
  //           elevation: 1,
  //           color: Colors.orange,
  //           margin: const EdgeInsets.fromLTRB(0, 8.0, 0, 4.0),
  //           child: TextButton(
  //             onPressed: () {},
  //             child: const Text(
  //               '+ Add New Item',
  //               style: TextStyle(color: Colors.white, fontSize: 14.0),
  //             ),
  //           ),
  //         ),
  //       ),
  //     ],
  //   );
  // }


  // Widget frequentlyUsed() {
  //   return Expanded(
  //     flex: 8,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         const Padding(
  //           padding: EdgeInsets.only(left: 10, bottom: 8.0, top: 8.0),
  //           child: Text(
  //             'Frequently Used',
  //             style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500),
  //           ),
  //         ),
  //         Row(
  //           children: const [
  //             Expanded(
  //               child: Card(
  //                 elevation: 0.2,
  //                 margin: EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
  //                 child: Padding(
  //                   padding: EdgeInsets.fromLTRB(16.0, 8.0, 0, 8.0),
  //                   child: Text(
  //                     'Shifa International',
  //                     style: TextStyle(
  //                         color: Colors.black,
  //                         fontSize: 14.0,
  //                         fontWeight: FontWeight.w500),
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }