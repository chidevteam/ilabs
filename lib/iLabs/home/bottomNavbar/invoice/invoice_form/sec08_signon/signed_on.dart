import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:ilabs/iLabs/widgets/buttons/add_circled_button_with_label.dart';

import '../../../../action_drawer/leading_drawer/d005_settings/signature/signature_vu.dart';
import '../add_invoice_vm.dart';

class SignedOn extends StatelessWidget {
  SignedOn({required this.viewModel, Key? key}) : super(key: key);
  AddInvoiceViewModel viewModel;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 0.7,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Signed on',
                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500),
                ),
                viewModel.homecontroller.userDefault.signature == null ||
                        viewModel.homecontroller.userDefault.signature == ""
                    ? Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: AddCircledButtonWithLabel(
                            label: "Add",
                            onTap: () {
                              Utils.push(
                                      context,
                                      DefaultSignatureScreen('fromInvoice',
                                          viewModel.invoiceData!))
                                  .then((value) async {
                                if (value != null) {
                                  // viewModel.image = value;
                                  viewModel.uploadSignature(value);
                                }
                                viewModel.notifyListeners();
                              });
                              // Utils.push(context, )
                            }),
                      )
                    : Transform.scale(
                        scale: 0.8,
                        child: Switch(
                          value: viewModel.invoiceData!.signature != "" &&
                              viewModel.invoiceData!.signature != null,
                          activeColor: Colors.orange[700],
                          onChanged: (value) {
                            viewModel.onSignToggle();
                          },
                        ),
                      )
              ],
            ),
          )),
    );
  }
}

Widget signedOn(AddInvoiceViewModel viewModel) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
    child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Signed on',
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500),
              ),
              viewModel.invoiceData!.signature == null
                  ? AddCircledButtonWithLabel(
                      label: "Add",
                      onTap: () {
                        // Utils.push(context, )
                      })
                  : Transform.scale(
                      scale: 0.8,
                      child: Switch(
                        value: viewModel.invoiceData!.signature == null,
                        activeColor: Colors.orange[700],
                        onChanged: (value) {
                          // viewModel.onSignToggle();
                        },
                      ),
                    )
            ],
          ),
        )),
  );
}
