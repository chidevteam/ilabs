import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../../models/due_days/due_days_model.dart';
import '../../../../../models/invoice_data/model.dart';
// import '../../../../../services/api_client.dart';

class InvoiceInfoCardVM extends BaseViewModel {
  late InvoiceInfo invoiceInfo;
  int? invoiceID;

  InvoiceInfoCardVM(this.invoiceInfo, this.invoiceID) {
    setInitialDates();
  }

  setInitialDates({int? days}) {
    // if (invoiceInfo.date == null || invoiceInfo.dueDate == null) {

      invoiceInfo.date =
          DateFormat(invoiceInfo.dateFormat).format(invoiceInfo.date==null?DateTime.now():DateTime.parse(invoiceInfo.date!));
      int? days = DueDaysModel.getNumDaysFromTermDays(invoiceInfo.terms!);
      if (days != null) { 
        invoiceInfo.dueDate = DateFormat(invoiceInfo.dateFormat)
            .format((invoiceInfo.dueDate==null?DateTime.now():DateTime.parse(invoiceInfo.dueDate!)).add(Duration(days: days)));
      }
      notifyListeners();
    // }
  }
}
