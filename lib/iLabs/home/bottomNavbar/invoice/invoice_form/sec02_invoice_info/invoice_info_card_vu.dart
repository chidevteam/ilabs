import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../../../widgets/output/standard_card.dart';
import '../../../../../Utils/utils.dart';
import 'invoice_info_card_vm.dart';
import 'invoice_info_form/invoice_info_form_vu.dart';
import '../../../../../models/invoice_data/model.dart';

// ignore: must_be_immutable
class InvoiceInfoCardVU extends ViewModelBuilderWidget<InvoiceInfoCardVM> {
  InvoiceInfoCardVU(this.invoiceInfo, this.invoiceID,
      {required this.onEdited, Key? key})
      : super(key: key);
  InvoiceInfo invoiceInfo;
  int? invoiceID; // = viewModel.invoiceData!.invoiceInfo;
  void Function(InvoiceInfo? invoiceInfo) onEdited;
  // final int invoiceId;

  @override
  Widget builder(
      BuildContext context, InvoiceInfoCardVM viewModel, Widget? child) {
    return InkWell(
      onTap: () {
        Utils.push(context, InvoiceInfoFormVU(viewModel.invoiceInfo, invoiceID))
            .then((value) {
          if (value != null) {
            // viewModel.onSave(value, invoiceID);
            viewModel.invoiceInfo = value;
          }
          // print(invoiceInfo);
          // invoiceInfo = value;
          onEdited(value);
          //todo: a very serious discussion needs to be done here as well
          viewModel.notifyListeners();
        });
      },
      child:  Container(
          color: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // invoiceFormCardTitle(
              //     'Invoice info',
              //     const Icon(
              //       Icons.edit_outlined,
              //       size: 22.0,
              //       color: Colors.orange,
              //     )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  invoiceFormBulletTwo(
                      Icons.calendar_today,
                      viewModel.invoiceInfo.date ?? "",
                      viewModel.invoiceInfo.isInvoice!
                          ? viewModel.invoiceInfo.dueDate ?? ""
                          : ""),
                  const Spacer(),
                  invoiceFormBulletOne(
                      Icons.add_chart_outlined, viewModel.invoiceInfo.number!),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: Row(
                  children: [
                    Visibility(
                      visible: viewModel.invoiceInfo.isInvoice!,
                      child: invoiceFormBulletOne(
                          Icons.calendar_today, viewModel.invoiceInfo.terms!),
                    ),
                    const Spacer(),
                    invoiceFormBulletThree(
                        Icons.calendar_today,
                        viewModel.invoiceInfo.isInvoice!
                            ? viewModel.invoiceInfo.dueDate ?? ""
                            : ""),
                  ],
                ),
              )
            ],
          )),
    );
  }

  @override
  InvoiceInfoCardVM viewModelBuilder(BuildContext context) {
    return InvoiceInfoCardVM(invoiceInfo, invoiceID);
  }
}
