import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import 'package:ilabs/iLabs/widgets/buttons/add_circled_button_with_label.dart';
// import 'update_item_in_invoice/update_item_detail_vu.dart';
import 'add_item_in_invoice/select_item_for_invoice_vu.dart';
import 'add_item_in_invoice/invoice_item_form_vu.dart';

import '../add_invoice_vm.dart';

class ItemSectionVU extends StatelessWidget {
  final AddInvoiceViewModel viewModel;
  const ItemSectionVU({required this.viewModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<ItemData> itemsList = viewModel.invoiceData!.items!;

    ///need to discuss it with Sir ahmed and modify it
    ///extremely meaning less place for calculations
    List<Widget> itemsInForm = [];
    for (int i = 0; i < itemsList.length; i++) {
      String name = itemsList[i].description;
      num cost = itemsList[i].unitCost;
      num itemCount = itemsList[i].quantity;
      Widget itemCell =
          getItemCell(viewModel, name, cost, i, itemCount, context);
      // viewModel.subTotal += cost;s
      itemsInForm.add(itemCell);
    }
    itemsInForm.add(Padding(
      padding: const EdgeInsets.only(
        top: 10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Sub Total',
            style: TextStyle(
                fontSize: 13.0,
                fontWeight: FontWeight.w500,
                color: Colors.blue[600]),
          ),
          Text(
            "${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${viewModel.calculateSubTotal().toStringAsFixed(2)}",
            style: TextStyle(
                fontSize: 13.0,
                fontWeight: FontWeight.w500,
                color: Colors.blue[600]),
          )
        ],
      ),
    ));
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0.0),
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 0.7,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 10.0, 8.0, 6.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'items',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                ),
                const Divider(
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                  child: Column(
                    children: itemsInForm,
                  ),
                ),
                const Divider(
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                  child: Align(
                    child: AddCircledButtonWithLabel(
                        label: "Add Item",
                        onTap: () {
                          Utils.push(
                                  context,
                                  SelectItemForInvoiceScreen(
                                      viewModel.invoiceData))
                              .then((item) {
                            viewModel.addNewItem(item);
                          });
                        }),
                  ),
                )
              ],
            ),
          )),
    );
  }
}

Widget getItemCell(AddInvoiceViewModel viewModel, String text, num cost,
    int index, num count, BuildContext context) {
  List<ItemData> itemsList = viewModel.invoiceData!.items!;
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 4.0),
    child: InkWell(
      onTap: () {
        Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => InvoiceItemFormVU(itemsList[index])))
            .then((value) {
          viewModel.updateItem(index, value);
        });
      },
      child: Column(children: [
        topRowItem(viewModel, text, cost, index),
        bottomRowItem(viewModel, text, cost, count),
        const Divider(
          color: Colors.grey,
        ),
        Text(
            "${itemsList[index].taxable ? 'taxable' : 'non taxable'}    &    discount:  ${itemsList[index].discountType} ${itemsList[index].discountAmount}")
      ]),
    ),
  );
}

Widget topRowItem(
    AddInvoiceViewModel viewModel, String text, num cost, int index) {
  int myIndex = index;
  return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
    Text(
      text,
      style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w500),
    ),
    IconButton(
      padding: EdgeInsets.zero,
      constraints: const BoxConstraints(),
      onPressed: () {
        viewModel.deleteItem(myIndex);
      },
      icon: const Icon(
        Icons.delete,
        size: 22.0,
        color: Colors.orange,
      ),
    )
  ]);
}

Widget bottomRowItem(
    AddInvoiceViewModel viewModel, String text, num cost, num count) {
  return Row(mainAxisAlignment: MainAxisAlignment.start, children: [
    const Icon(
      Icons.book,
      size: 22.0,
      color: Colors.blue,
    ),
    Text('$count x $cost'),
    const Spacer(),
    Text(
      '${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${cost * count} ',
      style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w500),
    ),
  ]);
}
