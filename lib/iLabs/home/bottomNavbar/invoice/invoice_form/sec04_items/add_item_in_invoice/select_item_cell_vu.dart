import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import '../../../../../../models/item/model.dart';

Widget itemCell(InvoiceItem item, void Function() tapFunc) {
  return GestureDetector(
    onTap: tapFunc,
    child: Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: IntrinsicHeight(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12.0, 14.0, 12.0, 14.0),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.description!,
                      style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      item.unitCost.toString(),
                      style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                const SizedBox(
                  width: 14.0,
                ),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}
