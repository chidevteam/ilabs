import 'package:flutter/material.dart';
import '../../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import '../../../../../../widgets/input/chi_textfield.dart';
import 'invoice_item_form_vm.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';

class InvoiceItemFormVU extends ViewModelBuilderWidget<InvoiceItemFormVM> {
  final ItemData? itemData;

  const InvoiceItemFormVU(this.itemData, {Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Add New Item',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () async {
                 await  viewModel.onSaveForm();
                Navigator.pop(context, viewModel.itemData);
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: SingleChildScrollView(
        child: Form(
          key: viewModel.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                margin: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                elevation: 0.7,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 24.0, left: 14.0, right: 14.0, bottom: 18.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CHITextField(
                        heading: 'Item Name',
                        hintText: 'Type Item Name',
                        func: (value) {
                          viewModel.onSaveItemName(value);
                        },
                        initialValue: viewModel.itemData.description,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: CHITextField(
                                heading: 'Unit Cost',
                                hintText: '0.00',
                                func: viewModel.onSaveUnitCost,
                                // onChangeFunc: viewModel.onSaveUnitCost,
                                fieldType: TextInputType.number,
                                initialValue: viewModel.itemData.unitCost.toString()),
                          ),
                          const SizedBox(width: 18.0),
                          Expanded(
                              flex: 1,
                              child: CHITextField(
                                  heading: 'Quantity',
                                  hintText: '',
                                  func: viewModel.onSaveQuantity,
                                  // onChangeFunc: viewModel.onSaveQuantity,
                                  fieldType: TextInputType.number,
                                  initialValue: viewModel.itemData.quantity
                                      .toString())),
                          const SizedBox(width: 8.0),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 12.0,
              ),
              taxable(viewModel),
              const SizedBox(
                height: 12.0,
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                margin: const EdgeInsets.fromLTRB(12.0, 2.0, 12.0, 6.0),
                elevation: 0.7,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(12, 6.0, 12, 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Discount',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 13.0,
                            fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 12.0,
                      ),
                      percentageDropDown(viewModel),
                      CHITextField(
                          heading: '',
                          hintText: "0%",
                          func: viewModel.onSaveDiscountAmount,
                          fieldType: TextInputType.number,
                          // onChangeFunc: viewModel.onSaveDiscountAmount,
                          initialValue: viewModel.itemData.discountAmount.toString()),
                      const SizedBox(
                        height: 12.0,
                      ),
                      CHITextField(
                          heading: 'Tax',
                          hintText: 'Type Tax Amount',
                          fieldType: TextInputType.number,
                          func: viewModel.onSaveTax,
                          // onChangeFunc: viewModel.onSaveTax,
                          initialValue: viewModel.itemData.taxValue.toString()),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget percentageDropDown(InvoiceItemFormVM viewModel) {
    return DropdownButtonFormField(
        decoration: InputDecoration(
          fillColor: Colors.grey[200],
          filled: true,
          contentPadding: const EdgeInsets.fromLTRB(6.0, 8, 6.0, 8),
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(
              color: Colors.grey.shade400,
              width: 1.0,
            ),
          ),
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
          ),
        ),
        icon: const RotatedBox(
          quarterTurns: 1,
          child: Icon(
            Icons.arrow_forward_ios_outlined,
            color: Colors.black54,
            size: 12,
          ),
        ),
        items: viewModel.discountTypes.map((e) {
          return DropdownMenuItem<String>(
            child: Text(
              e,
              style: const TextStyle(
                  color: Colors.black54,
                  fontSize: 12,
                  fontWeight: FontWeight.w500),
            ),
            value: e,
          );
        }).toList(),
        value: viewModel.discountTypes[0],
        onChanged: (String? v) {
          viewModel.onDiscountType(v!);
        });
  }

  Widget taxable(InvoiceItemFormVM viewModel) {
    return Card(
        margin: const EdgeInsets.only(left: 12.0, right: 12.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Taxable',
                style: TextStyle(
                    fontSize: 13.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.orange[700]),
              ),
              Transform.scale(
                scale: 0.8,
                child: Switch(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: viewModel.itemData.taxable,
                  activeColor: Colors.orange[700],
                  onChanged: (value) {
                    viewModel.onTaxableToggle();
                  },
                ),
              )
            ],
          ),
        ));
  }

  Widget discountAmount(InvoiceItemFormVM viewModel, context) {
    return TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.fromLTRB(12.0, 8, 6.0, 8),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: '0%',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
        ),
        onSaved: (String? value) {
          viewModel.onSaveDiscountAmount(value);
        });
  }

  @override
  InvoiceItemFormVM viewModelBuilder(BuildContext context) {
    return InvoiceItemFormVM(itemData ??
        ItemData());
  }
}