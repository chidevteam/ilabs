import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stacked/stacked.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import '../../../../../../models/item/model.dart';
import '../../../../../../services/api_client.dart';
import '../../../../item/item_list/item_list_controller.dart';

class InvoiceItemFormVM extends BaseViewModel {
  InvoiceItemFormVM(this.itemData);

  final formKey = GlobalKey<FormState>();
  ItemData itemData;

  List<String> discountTypes = ["Percentage", "Flat Amount"];

  void onDiscountType(String type) {
    itemData.discountType = type;
  }

  onTaxableToggle() {
    itemData.taxable = !itemData.taxable;
    notifyListeners();
  }

  onSaveItemName(String value) {
    itemData.description = value.trim();
    notifyListeners();
  }

  onSaveUnitCost(value) {
    itemData.unitCost = num.parse(value == "" ? "0" : value.toString());
    notifyListeners();
  }

  onSaveQuantity(value) {
    itemData.quantity = int.parse(value == "" ? "0" : value.toString());
    // notifyListeners();
  }

  onSaveTax(value) {
    itemData.taxValue = num.parse(value == "" ? "0" : value.toString());
    notifyListeners();
  }

  onSaveDiscountAmount(value) {
    itemData.discountAmount =
        num.parse(value == "" ? "0" : value.toString());
    notifyListeners();
  }

  onSaveForm() async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }

    print(itemData.itemId);
  if(itemData.itemId==null) { Request addItemReq = {
      "taxable": itemData.taxable,
      "description": itemData.description,
      "additional_detail": "This unit",
      "unit_cost": itemData.unitCost
    };
    APIResponse? resp;

    resp = await ApiClient.post(
        request: addItemReq,
        endPoint: "/items",
        fromJson: InvoiceItem.fromJson);
        InvoiceItem invoiceItem = resp['data'];
      itemData.itemId = invoiceItem.id;
    saveItemToItemGetXController(invoiceItem);
    notifyListeners();
    return;}
  }

  saveItemToItemGetXController(InvoiceItem generalItem) {
    Get.find<ItemListController>().itemList.insert(0, generalItem);
    // print(generalItem.toJson().toString()+"added into item list");
  }
}
