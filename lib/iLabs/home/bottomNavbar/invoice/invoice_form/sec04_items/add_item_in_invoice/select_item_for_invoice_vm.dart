// import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/item/item_list/item_list_controller.dart';
import '../../../../../../models/invoice_data/model.dart';
import '../../../../../../models/item/model.dart';

class SelectItemForInvoiceViewModel extends BaseViewModel {
  ItemListController itemController = Get.find<ItemListController>();
  List<InvoiceItem> items = Get.find<ItemListController>().itemList.value;
  // ItemData? itemData;

  ItemData getItemData(int index) {
    InvoiceItem item = items[index];
    ItemData itemData = ItemData(
        itemId: item.id,
        description: item.description??"",
        unitCost: item.unitCost??0,
        taxable: item.taxable!,
        additionalDetail: item.additionalDetail??"",
        );
    return itemData;
  }

  SelectItemForInvoiceViewModel() {
    listenToItemsListFromItemListController();
  }

  listenToItemsListFromItemListController() {
    itemController.itemList.listen((p0) {
      items = itemController.itemList.value;
      notifyListeners();
    });
  }

  onSearchFieldValueChanges(String val) {
    if (val.isEmpty) {
      items = Get.find<ItemListController>().itemList.value;
    } else {
      items = Get.find<ItemListController>()
          .itemList
          .where((element) =>
              element.description!.toLowerCase().contains(val.toLowerCase()) ||
              element.additionalDetail!
                  .toLowerCase()
                  .contains(val.toLowerCase()) ||
              element.unitCost.toString().contains(val))
          .toList();
    }
    notifyListeners();
  }
}
