import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../../../models/invoice_data/model.dart';
import 'invoice_item_form_vu.dart';
import 'select_item_for_invoice_vm.dart';
import '../../../../../../models/item/model.dart';
import 'select_item_cell_vu.dart';

class SelectItemForInvoiceScreen
    extends ViewModelBuilderWidget<SelectItemForInvoiceViewModel> {
  final InvoiceData? invoiceData;
  const SelectItemForInvoiceScreen(this.invoiceData, {Key? key})
      : super(key: key);

  @override
  Widget builder(BuildContext context, SelectItemForInvoiceViewModel viewModel,
      Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Select Item',
          style: TextStyle(
              color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black54,
            size: 18.0,
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
              elevation: 0.2,
              margin: const EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
              child: itemSearchbox(context, viewModel)),
          Expanded(
            flex: 8,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: viewModel.items.length,
                itemBuilder: (context, index) {
                  InvoiceItem it = viewModel.items[index];
                  return itemCell(it, () {
                    if (invoiceData != null) {
                      if (invoiceData!.items!.any((element) =>
                          element.itemId == viewModel.items[index].id)) { //it will pick item only if item is not already exists in incvoice
                     //todo: show something to user
                        debugPrint(
                            "Choose Another Item Item already exists in invoice");
                      } else {
                        debugPrint("${viewModel.items[index].id}");
                        debugPrint("Item Tapped ${it.description!}");
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return InvoiceItemFormVU( viewModel.getItemData(index));
                        })).then((value) {
                          // if (value != null) {
                          Navigator.pop(context, value);
                          // }
                        });
                      }
                    }
                  });
                }),
          )
        ],
      ),
      floatingActionButton: floatAddItem(
        context,
        viewModel,
      ),
    );
  }

  FloatingActionButton floatAddItem(
    BuildContext context,
    SelectItemForInvoiceViewModel viewModel,
  ) {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
          return  InvoiceItemFormVU(ItemData());
        })).then((value) {
          Navigator.pop(context, value);
        });
        // vm.refreshData();
      },
      backgroundColor: Colors.orange,
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  popOut(BuildContext context, item) {}

  Widget itemSearchbox(
      BuildContext context, SelectItemForInvoiceViewModel viewModel) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 8.0),
      child: TextField(
          style: Theme.of(context).textTheme.headline6,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.only(top: 6.0, left: 6.0),
            hintText: 'Search',
            prefixIcon: IconButton(
              icon: const Icon(
                Icons.search,
                size: 16.0,
              ),
              color: Colors.grey[400],
              onPressed: () {},
            ),
            hintStyle: TextStyle(
                color: Colors.grey[500],
                fontWeight: FontWeight.normal,
                fontSize: 14.0),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(6.0)),
            ),
            fillColor: Colors.grey[100],
            filled: true,
            focusedBorder: OutlineInputBorder(
                borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                borderSide: BorderSide(
                  color: Colors.grey.shade200,
                )),
          ),
          onChanged: viewModel.onSearchFieldValueChanges),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    SelectItemForInvoiceViewModel vm = SelectItemForInvoiceViewModel();
    return vm;
  }
}

// frequentlyUsed(),
// const Expanded(child: Text('')),
// const Padding(
//   padding: EdgeInsets.only(left: 10.0, bottom: 8.0),
//   // child: DecoratedBox(
//   //   col
//   //   decoration: BoxDecoration(color: Colors.blue),
//   //   child: Text('Some text...'),
//   // ),
//   child: Text(
//     'Client List',
//     style: TextStyle(
//       fontSize: 14.0,
//       fontWeight: FontWeight.w500,
//     ),
//   ),
// ),

// Row addNewItem() {
//   return Row(
//     children: [
//       Expanded(
//         child: Card(
//           elevation: 1,
//           color: Colors.orange,
//           margin: const EdgeInsets.fromLTRB(0, 8.0, 0, 4.0),
//           child: TextButton(
//             onPressed: () {},
//             child: const Text(
//               '+ Add New Item',
//               style: TextStyle(color: Colors.white, fontSize: 14.0),
//             ),
//           ),
//         ),
//       ),
//     ],
//   );
// }

// Widget frequentlyUsed() {
//   return Expanded(
//     flex: 8,
//     child: Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         const Padding(
//           padding: EdgeInsets.only(left: 10, bottom: 8.0, top: 8.0),
//           child: Text(
//             'Frequently Used',
//             style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500),
//           ),
//         ),
//         Row(
//           children: const [
//             Expanded(
//               child: Card(
//                 elevation: 0.2,
//                 margin: EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
//                 child: Padding(
//                   padding: EdgeInsets.fromLTRB(16.0, 8.0, 0, 8.0),
//                   child: Text(
//                     'Shifa International',
//                     style: TextStyle(
//                         color: Colors.black,
//                         fontSize: 14.0,
//                         fontWeight: FontWeight.w500),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         )
//       ],
//     ),
//   );
// }
