import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import '../../../../../models/payment_instructions/model.dart';
import '../../../../../models/payment_instructions/payment_instruction_form-vu.dart';
import '../../../../action_drawer/leading_drawer/d005_settings/notes/notes_vu.dart';
import '../add_invoice_vm.dart';

class PaymentInstruction extends StatelessWidget {
  final AddInvoiceViewModel viewModel;

  const PaymentInstruction(this.viewModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // String paypalEmail = viewModel.invoiceData!.paypalEmail ?? "";
    // String checkPayableTo = viewModel.invoiceData!.checkPayableTo ?? "";
    // String bankTransfer = viewModel.invoiceData!.bankTransfer ?? "";
    PaymentInstructions payInst = viewModel.invoiceData!.paymentInstructions;

    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 0.7,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 8.0, 8.0, 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Payment Instructions and Notes',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                ),
                const Divider(
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Payment Instructions',
                        style: TextStyle(
                          fontSize: 13.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      IconButton(
                        padding: EdgeInsets.zero,
                        constraints: const BoxConstraints(),
                        onPressed: () {
                          PaymentInstructions pInst =
                              viewModel.invoiceData!.getPaymentInstructions();
                          Utils.push(context, PaymentInstructionFormVU(pInst))
                              .then((value) {
                            if (value != null) {
                              viewModel.invoiceData!
                                  .setPaymentInstructions(value);
                                  viewModel.notifyListeners();
                                  viewModel.invoicePatch(viewModel.invoiceData!.id);
                            }
                          });
                        },
                        icon: const Icon(
                          Icons.edit_outlined,
                          size: 13,
                          color: Colors.orange,
                        ),
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Paypal Email:           ${payInst.paypalEmail}'),
                    Text('Check Payable To:      ${payInst.checkPayableTo}'),
                    Text('Bank Transfer:             ${payInst.bankTransfer}')
                  ],
                ),
                Row(
                  children: [
                    const Text(
                      'Notes',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      padding: EdgeInsets.zero,
                      constraints: const BoxConstraints(),
                      onPressed: () {
                        Utils.push(context, NotesScreen(viewModel.invoiceData!.notes))
                            .then((value) {
                          if (value != null) {
                            viewModel.invoiceData!.notes = value;
                            viewModel.invoicePatch(viewModel.invoiceData!.id);
                          }
                          // print('note${viewModel.invoiceData!.paymentNotes}');
                          viewModel.notifyListeners();
                        });
                      },
                      icon: const Icon(
                        Icons.edit_outlined,
                        size: 13,
                        color: Colors.orange,
                      ),
                    )
                  ],
                ),
                viewModel.invoiceData == null ||
                        viewModel.invoiceData!.notes != null
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(viewModel.invoiceData == null
                            ? ""
                            : '${viewModel.invoiceData!.notes}'),
                      )
                    : const Text('')
              ],
            ),
          )),
    );
  }
}
