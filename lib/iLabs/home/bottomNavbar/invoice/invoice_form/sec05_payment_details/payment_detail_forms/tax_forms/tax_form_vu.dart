import 'package:flutter/material.dart';
import 'tax_form_vm.dart';
import 'package:stacked/stacked.dart';

// import 'discount_form_vm.dart';

class AddTaxScreen extends ViewModelBuilderWidget<AddTaxViewModel> {
  const AddTaxScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, AddTaxViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Discount',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                viewModel.onSave();
                // print(viewModel.rate);
                Navigator.pop(context, viewModel.rate);
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Discount',
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 13.0,
                          fontWeight: FontWeight.w400),
                    ),
                    const Spacer(),
                    TextButton(
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Wrap(
                                children: [
                                  ListTile(
                                    title: TextButton(
                                      onPressed: () {
                                        //viewModel.selectedOption = viewModel.options[0];
                                        viewModel.updateOptions(0, context);
                                        // print(viewModel.selectedOption);
                                      },
                                      child: Text(viewModel.options[0]),
                                    ),
                                  ),
                                  ListTile(
                                    //leading: Icon(Icons.copy),
                                    title: TextButton(
                                      onPressed: () {
                                        viewModel.updateOptions(1, context);
                                        // print(viewModel.selectedOption);
                                      },
                                      child: Text(viewModel.options[1]),
                                    ),
                                  ),
                                  ListTile(
                                    //leading: Icon(Icons.edit),
                                    title: TextButton(
                                      onPressed: () {
                                        viewModel.updateOptions(2, context);
                                        // print(viewModel.selectedOption);
                                      },
                                      child: Text(viewModel.options[2]),
                                    ),
                                  ),
                                ],
                              );
                            });
                      },
                      child: Text(
                        viewModel.selectedOption,
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 13.0,
                            fontWeight: FontWeight.w400),
                      ),
                    )
                  ],
                ),
                Visibility(
                    visible: viewModel.selectedOption != viewModel.options[0]
                        ? true
                        : false,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 2, child: Text(viewModel.selectedOption)),
                        Expanded(
                            flex: 1,
                            child: TextFormField(
                              controller: viewModel.userInput,
                              autofocus: true,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                //labelText: viewModel.percentage.toString(),
                                hintText: viewModel.rate.toString(),
                              ),
                              onSaved: (String? value) {
                                viewModel.onPercentage(value);
                                // print(viewModel.rate);
                              },
                            ))
                      ],
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  AddTaxViewModel viewModelBuilder(BuildContext context) {
    return AddTaxViewModel();
  }
}
