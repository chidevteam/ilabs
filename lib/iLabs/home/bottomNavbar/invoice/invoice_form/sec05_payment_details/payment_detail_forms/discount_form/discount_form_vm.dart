import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'model.dart';



class AddDiscountViewModel extends BaseViewModel{
  AddDiscountViewModel({required this.inputModel}){
    outPutModel = Discount(inputModel.discountType!, inputModel.amount!);
  }
  final formKey = GlobalKey<FormState>();
  Discount inputModel  = Discount('No Discount', 0);
  Discount outPutModel = Discount('No Discount', 0);
  TextEditingController userInput = TextEditingController();

  List<String> options = ['No Discount', 'Percentage', 'Flat Amount'];

  updateOptions(int index, BuildContext context){
    outPutModel.discountType = options[index];
    notifyListeners();
    Navigator.pop(context);
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onPercentage(String? value) async {
    outPutModel.amount = num.parse(value!.trim());
    notifyListeners();

  }

  onSave(){
    formKey.currentState!.save();
    notifyListeners();
  }



}