import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class AddTaxViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  num rate = 0;
  num amount = 0;
  TextEditingController userInput = TextEditingController();

  List<String> options = ['None', 'On The Total', 'Deducted', 'Per Item'];
  String selectedOption = 'None';

  updateOptions(int index, BuildContext context) {
    selectedOption = options[index];
    notifyListeners();
    Navigator.pop(context);
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onPercentage(String? value) async {
    rate = num.parse(value!.trim());
    notifyListeners();
  }

  onSave() {
    formKey.currentState!.save();
    notifyListeners();
  }
}
