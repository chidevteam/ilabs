class Discount{
  String? discountType;
  num? amount;

  Discount(this.discountType, this.amount);
}
