import 'package:flutter/material.dart';
import 'discount_form_vm.dart';
import 'package:stacked/stacked.dart';

import 'discount_form_vm.dart';
import 'model.dart';

class AddDiscountScreen extends ViewModelBuilderWidget<AddDiscountViewModel> {
 late Discount discount;
 AddDiscountScreen({required this.discount});

  @override
  Widget builder(
      BuildContext context, AddDiscountViewModel viewModel, Widget? child) {
  
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Discount',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                viewModel.onSave();
                Navigator.pop(context, viewModel.outPutModel);
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Discount',
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 13.0,
                          fontWeight: FontWeight.w400),
                    ),
                    const Spacer(),
                    TextButton(
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Wrap(
                                children: [
                                  ListTile(
                                    title: TextButton(
                                      onPressed: () {
                                        //viewModel.selectedOption = viewModel.options[0];
                                        viewModel.updateOptions(0, context);
                                        // print(viewModel.selectedOption);
                                      },
                                      child: Text(viewModel.options[0]),
                                    ),
                                  ),
                                  ListTile(
                                    //leading: Icon(Icons.copy),
                                    title: TextButton(
                                      onPressed: () {
                                        viewModel.updateOptions(1, context);
                                        // print(viewModel.selectedOption);
                                      },
                                      child: Text(viewModel.options[1]),
                                    ),
                                  ),
                                  ListTile(
                                    //leading: Icon(Icons.edit),
                                    title: TextButton(
                                      onPressed: () {
                                        viewModel.updateOptions(2, context);
                                        // print(viewModel.selectedOption);
                                      },
                                      child: Text(viewModel.options[2]),
                                    ),
                                  ),
                                ],
                              );
                            });
                      },
                      child: Text(
                        viewModel.outPutModel.discountType!,
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 13.0,
                            fontWeight: FontWeight.w400),
                      ),
                    )
                  ],
                ),
                Visibility(
                    visible: viewModel.outPutModel.discountType != viewModel.options[0]
                        ? true
                        : false,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 2, child: Text(viewModel.outPutModel.discountType!)),
                        Expanded(
                            flex: 1,
                            child: TextFormField(
                              // controller: viewModel.userInput,
                              initialValue: viewModel.outPutModel.amount.toString(),
                              autofocus: true,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.end,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                //labelText: viewModel.percentage.toString(),
                                hintText: viewModel.outPutModel.amount.toString(),
                              ),
                              onSaved: (String? value) {
                                viewModel.onPercentage(value);
                                // print(viewModel.percentage);
                              },
                            ))
                      ],
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget percentage(AddDiscountViewModel viewModel, context)
  // (AddItemViewModel viewModel, context)
  {
    return TextFormField(
        initialValue: viewModel.outPutModel.amount.toString(),
        keyboardType: const TextInputType.numberWithOptions(
            signed: false, decimal: false),
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          fillColor: Colors.grey[200],
          filled: true,
          hintText: 'unit code',
          hintStyle: const TextStyle(
              fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              borderSide: BorderSide(
                color: Colors.orange,
              )),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
            borderRadius: const BorderRadius.all(
              Radius.circular(12.0),
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(12.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(12.0),
          ),
        ),
        validator: viewModel.onItemUnitCodeValidator,
        onSaved: (String? value) {
          viewModel.onPercentage(value);
        });
  }

  @override
  AddDiscountViewModel viewModelBuilder(BuildContext context) {
    return AddDiscountViewModel(inputModel: Discount(discount.discountType??'No Discount',discount.amount?? 0));

}
}
