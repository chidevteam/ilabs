import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:ilabs/iLabs/home/action_drawer/leading_drawer/d005_settings/tax/new_tax/new_tax_vm.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/sec05_payment_details/client_payment/client_payment_vu.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/sec05_payment_details/payment_detail_forms/discount_form/model.dart';
import '../add_invoice_vm.dart';
import 'payment_detail_forms/discount_form/discount_from_vu.dart';
import 'payment_detail_forms/tax_forms/tax_form_vu.dart';
import '../../../../../models/invoice/model.dart';
import '../../../../../home/action_drawer/leading_drawer/d005_settings/tax/new_tax/new_tax_vu.dart';

class PaymentDetail extends StatelessWidget {
  Invoice? invoice;
  PaymentDetail(this.viewModel, this.invoice, {Key? key}) : super(key: key);
  AddInvoiceViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return paymentDetail(context, viewModel, invoice);
  }
}

Widget paymentDetail(
    BuildContext context, AddInvoiceViewModel viewModel, Invoice? invoice) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
    child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 8.0, 8.0, 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Payment Details',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
              ),
              const Divider(
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: [
                    Text(
                      'Discount(${viewModel.invoiceData!.discountType ?? 'No Discount'})',
                      style: const TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {
                        Utils.push(
                            context,
                            AddDiscountScreen(
                                discount: Discount(
                              viewModel.invoiceData!.discountType,
                              viewModel.invoiceData!.discountAmount,
                            ))).then((value) {
                          viewModel.onDiscountEdited(value);
                          viewModel.notifyListeners();
                        });
                      },
                      icon: const Icon(
                        Icons.edit_outlined,
                        size: 13,
                        color: Colors.orange,
                      ),
                    ),
                    const SizedBox(
                      width: 2.0,
                    ),
                    Text(
                      '${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${viewModel.invoiceData!.discountAmount}',
                     
                      style: const TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: [
                    Text(
                      'Tax (${viewModel.invoiceData!.taxModel.taxType})  ${viewModel.invoiceData!.taxModel.taxInclusive ? 'inc' : 'exc'}  rate:${viewModel.invoiceData!.taxModel.taxRate}',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Spacer(),
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NewTaxVU(invoice == null
                                    ? viewModel.homecontroller.taxModel
                                    : viewModel.invoiceData!.taxModel))).then(
                            (value) {
                          viewModel.onTaxEdited(value);
                          viewModel.notifyListeners();
                        });
                      },
                      icon: const Icon(
                        Icons.edit_outlined,
                        size: 13,
                        color: Colors.orange,
                      ),
                    ),
                    const SizedBox(
                      width: 2.0,
                    ),
                    Text(
                      "${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${viewModel.invoiceData!.tax??""}",
                      style: const TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                child: Row(
                  children: [
                    const Text(
                      'Total',
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const Spacer(),
                    Text(
                      "${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${viewModel.invoiceData!.total}",
                      style: const TextStyle(
                        fontSize: 13.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: viewModel.invoiceData!.invoiceInfo.isInvoice ?? true,
                child: Padding(
                  padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>  ClientPaymentVU(viewModel: viewModel)));
                    },
                    child: Row(
                      children: [
                        const Text(
                          'Client Payment',
                          style: TextStyle(
                            fontSize: 13.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const Spacer(),
                        const Icon(
                          Icons.edit_outlined,
                          size: 13,
                          color: Colors.orange,
                        ),
                        const SizedBox(
                          width: 2.0,
                        ),
                        Text(
                          "${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${viewModel.invoiceData!.payments}",
                          style: const TextStyle(
                            fontSize: 13.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: viewModel.invoiceData!.invoiceInfo.isInvoice ?? true,
                child: Padding(
                  padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: Row(
                    children: [
                      const Text(
                        'Balance Due',
                        style: TextStyle(
                            fontSize: 13.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.blue),
                      ),
                      const Spacer(),
                      Text(
                        '${viewModel.homecontroller.userDefault.rXregionCurrency.value} ${viewModel.invoiceData!.balanceDue!}' ,
                        style: const TextStyle(
                          fontSize: 13.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        )),
  );
}
