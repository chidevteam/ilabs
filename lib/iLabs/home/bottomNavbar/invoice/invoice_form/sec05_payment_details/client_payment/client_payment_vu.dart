import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/add_invoice_vm.dart';
import '../../../../../../widgets/buttons/add_circled_button_with_label.dart';
import '../../../../../../widgets/input/chi_text_field.dart';
import 'package../../client_payment_vm.dart';
import 'package:stacked/stacked.dart';

class ClientPaymentVU extends ViewModelBuilderWidget<ClientPaymentVM> {
  ClientPaymentVU({required this.viewModel,Key? key}) : super(key: key);
  AddInvoiceViewModel viewModel;

  @override
  Widget builder(
      BuildContext context, ClientPaymentVM viewModel, Widget? child) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
      appBar: chiAppBar(
        "Client Pay",
        context,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: topSectionCellVU(),
          ),
          const Spacer(),
          bottomSectionCellVU()
        ],
      ),
    );
  }

  //.........Two Cell.......................................

  Container bottomSectionCellVU() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.transparent.withOpacity(0.2),
            spreadRadius: 0.1,
            blurRadius: 3,
            offset: const Offset(2, 0),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "Paid",
                  style: GoogleFonts.poppins(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(0x1a, 0x1a, 0x1a, 1.0),
                  ),
                ),
                const Spacer(),
                Text(
                  "${viewModel.homecontroller.userDefault.rXregionCurrency.value} 27.00",
                  style: GoogleFonts.poppins(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.black54),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6.0),
              child: Row(
                children: [
                  Text(
                    "Balance Due",
                    style: GoogleFonts.poppins(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black87),
                  ),
                  const Spacer(),
                  Text(
                    "${viewModel.homecontroller.userDefault.rXregionCurrency.value} 27.00",
                    style: GoogleFonts.poppins(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black87),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container topSectionCellVU() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.transparent.withOpacity(0.2),
            spreadRadius: 0.1,
            blurRadius: 3,
            offset: const Offset(2, 0),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "Sub Total",
                  style: GoogleFonts.poppins(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(0x1a, 0x1a, 0x1a, 1.0),
                  ),
                ),
                const Spacer(),
                Text(
                  "${viewModel.homecontroller.userDefault.rXregionCurrency.value} 150.00",
                  style: GoogleFonts.poppins(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(0x1a, 0x1a, 0x1a, 1.0),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6.0),
              child: Row(
                children: [
                  Text(
                    "Date",
                    style: GoogleFonts.poppins(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
                    ),
                  ),
                  const Spacer(),
                  Text(
                    "${viewModel.homecontroller.userDefault.rXregionCurrency.value} 150.00",
                    style: GoogleFonts.poppins(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6.0),
              child: Row(
                children: [
                  Text(
                    "2011-11-11",
                    style: GoogleFonts.poppins(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                    ),
                  ),
                  const Spacer(),
                  Text(
                    "BankTransfer",
                    style: GoogleFonts.poppins(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Align(
                child: AddCircledButtonWithLabel(
                    label: "Add New Payment", onTap: () {}),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  ClientPaymentVM viewModelBuilder(BuildContext context) {
    return ClientPaymentVM();
  }
}
