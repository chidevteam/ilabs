import 'package:flutter/material.dart';
import '../../../../../models/invoice_data/model.dart';
import '../add_invoice_vm.dart';
import '../../../../../Utils/utils.dart';
import 'add_image_form/add_image_form_vu.dart';

class ImagesScreen extends StatelessWidget {
  final AddInvoiceViewModel viewModel;
  const ImagesScreen({required this.viewModel,Key? key})
      : super(key: key);

  @override
  Widget build(
    BuildContext context,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 0),
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 0.7,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 14.0, 8.0, 14.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Images',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.w500),
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size.zero,
                          padding: EdgeInsets.zero,
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return AddImageFormVU(
                               addInvoiceVM: viewModel,photo:  Photo(null, null, "", ""));
                          })).then((photo) {
                              viewModel.onAddImage(photo);
                          });
                        },
                        child: Text(
                          '+Add Image',
                          style: TextStyle(
                              color: Colors.orange[800], fontSize: 13.0),
                        ),
                      )
                    ],
                  ),
                ),
                viewModel.invoiceData == null
                    ? Container()
                    : Column(
                        children: viewModel.invoiceData!.photos.map((photo) {
                          return imgCell(photo, context);
                        }).toList(),
                      )
              ],
            ),
          )),
    );
  }

  Widget imgCell(Photo photo, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return AddImageFormVU(addInvoiceVM: viewModel,photo:Photo.fromMap(photo.toMap()));
        })).then((photo) {
            viewModel.onUpdatePhoto(photo);
        });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            Image.network(
              Utils.imgPath + (photo.photoUrl ?? ""),
              height: 40,
              width: 40,
            ),
            const SizedBox(
              width: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  photo.description!,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(photo.additionalDetails!)
              ],
            ),
            // Image.file(img, height: 40,width: 40,),
            const Spacer(),
            InkWell(
              onTap: () {
                viewModel.onDeleteImage(photo);
              },
              child: Container(
                height: 20,
                width: 20,
                child: const Icon(
                  Icons.close_outlined,
                  color: Colors.white,
                  size: 16,
                ),
                decoration: BoxDecoration(
                    color: Colors.red, borderRadius: BorderRadius.circular(50)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
