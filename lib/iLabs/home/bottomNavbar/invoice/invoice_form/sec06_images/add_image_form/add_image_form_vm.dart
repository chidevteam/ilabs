import 'package:flutter/material.dart';
import 'dart:developer';
import 'dart:io';

import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/add_invoice_vm.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:stacked/stacked.dart';

import '../../../../../../models/invoice_data/model.dart';
import '../../../../../../services/api_client.dart';
import '../../../../../app_constants.dart';

class AddImageFormViewModel extends BaseViewModel {
  AddImageFormViewModel({required this.photo,required this.addInvoiceVM}) {
    debugPrint("$photo");
  }
  bool isSharing = false;
  AddInvoiceViewModel addInvoiceVM ;
  File? image;
  Photo photo;

  Future<bool> checkPermission() async {
    if (!await Permission.storage.isGranted) {
      PermissionStatus status = await Permission.storage.request();
      if (status != PermissionStatus.granted &&
          status != PermissionStatus.permanentlyDenied) {
        return false;
      }
    }
    return true;
  }

  Future<void> pickImage() async {
    final tempImage = await ImagePicker().pickImage(source: ImageSource.camera);
    if (tempImage == null) {
      return;
    }
    image = File(tempImage.path);
    // mImg = tempImage;
    notifyListeners();
  }

  Future<Photo?> uploadImage() async {
    if (image != null) {
      setBusy(true);
      image ??= await Utils.fileFromImageUrl(SERVER_URL2 + photo.photoUrl!);

      Request req = {
        "description": photo.description ?? "",
        "additional_details": photo.additionalDetails ?? ""
      };

      ///we have required either invoice ID or photoID in order to upload photo
      ///in case of sending photoID to this API ... it will update existing photo data
      ///in case of sending invoiceID to this API ... it will add new photo data to this invoice
      ///in order to deal with this situation we must call null invoice patch

      if(addInvoiceVM.invoiceData!.id==null){
       await  addInvoiceVM.invoicePatch(null);
      }
      APIResponse response = await ApiClient.postMultiPart<Photo>(
        request: req,
        endPoint: '/invoicephotos/${photo.id ?? addInvoiceVM.invoiceData!.id}',
        filePath: image!.path,
        imageKey: "image",
        requestType: photo.id == null ? 'POST' : 'PATCH',
        fromJson: Photo.fromMap,
      );
      Photo? newPhotoModal = response['data'];

      setBusy(false);
      if (newPhotoModal != null) {
        photo = newPhotoModal;
        return photo;
      } else {
        return null;
      }
    }
    debugPrint('imagepath $image');
    return null;
  }

  onTitleChanged(String val) {
    photo.description = val;
    log("title Changed:  ${photo.description}");
  }

  onDescriptionChanged(String val) {
    photo.additionalDetails = val;
    debugPrint("title Changed:  ${photo.additionalDetails}");
  }
}
