import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/invoice/invoice_form/add_invoice_vm.dart';
import 'package:ilabs/iLabs/services/api_client.dart';
import 'package:stacked/stacked.dart';
import '../../../../../../models/invoice_data/model.dart';
import '../../../../../../widgets/input/chi_textfield.dart';
import '../../../../../app_constants.dart';
import 'add_image_form_vm.dart';

class AddImageFormVU extends ViewModelBuilderWidget<AddImageFormViewModel> {
  final AddInvoiceViewModel addInvoiceVM;
  final Photo photo;
  const AddImageFormVU({required this.addInvoiceVM,required this.photo, Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Invoice Image'),
          actions: [
            Row(
              children: [
                InkWell(
                    onTap: () {
                      if(viewModel.photo.photoUrl!=null || viewModel.image!=null){
                        viewModel.uploadImage().then((photo) =>
                            Navigator.pop(context, photo ?? viewModel.photo));
                      }
                      else{
                        debugPrint("handle user experience");
                      }
                    },
                    child: const Text(
                      'Save',
                      style: TextStyle(fontSize: 16),
                    )),
                const SizedBox(
                  width: 12,
                ),
              ],
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  viewModel.pickImage();
                },
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Container(
                        color: Colors.grey,
                        child: viewModel.image != null
                            ? Image.file(viewModel.image!)
                            : viewModel.photo.photoUrl != null
                                ? Image.network(SERVER_URL2 +
                                    photo.photoUrl!)
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Icon(Icons.add),
                                      SizedBox(
                                        height: 6,
                                      ),
                                      Text('Add Image')
                                    ],
                                  ),
                        height: MediaQuery.of(context).size.height / 2.5,
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
                  ],
                ),
              ),
              CHITextField(
                heading: "Image Title",
                hintText: 'Type Image Title',
                fieldType: TextInputType.text,
                func: (value) {},
                onChangeFunc: (value) {
                  viewModel.onTitleChanged(value);
                },

                initialValue: viewModel.photo.description == null
                    ? ""
                    : viewModel.photo.description!,
                // validator: viewModel.onItemNameValidator,
              ),
              CHITextField(
                heading: "Image Description",
                hintText: 'Type Image Description',
                fieldType: TextInputType.text,
                onChangeFunc: (value) {
                  viewModel.onDescriptionChanged(value);
                },
                func: (value) {},
                initialValue: viewModel.photo.additionalDetails == null
                    ? ""
                    : viewModel.photo.additionalDetails!,
                // validator: viewModel.onItemNameValidator,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    final vm = AddImageFormViewModel(addInvoiceVM: addInvoiceVM,photo: photo);
    return vm;
  }
}
