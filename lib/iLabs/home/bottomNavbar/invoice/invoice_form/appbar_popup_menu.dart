import 'dart:developer';

import 'package:flutter/material.dart';

import 'add_invoice_vm.dart';

Widget appbarPopUpMenu(AddInvoiceViewModel viewModel) {
  return Builder(builder: (context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxHeight: 500.0,
        maxWidth: 500.0,
      ),
      child: PopupMenuButton(
          icon: Icon(Icons.more_vert, color: Colors.orange[800]),
          elevation: 20,
          enabled: true,
          onSelected: (value) {
            log(value.toString());
            if(value == 'Delete'){
              viewModel.deleteInvoice().then((value) => Navigator.pop(context,true));
            }
               if(value == 'Open In'){
              viewModel.openInApps();
            }
           
            if(value == 'Share'){
              viewModel.shareInvoice();
            }
              if(value == 'Print'){
              // viewModel.printInvoice();
            }
             if(value == 'Duplicate')
            {
              viewModel.duplicateInvoice();
            }
          },
          itemBuilder: (context) {
            return viewModel.items.map((String item) {
              return PopupMenuItem(
                value: item,
                child: SizedBox(width: 136.14, child: Text(item)),
              );
            }).toList();
          }),
    );
  });
}
