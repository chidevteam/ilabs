import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:stacked/stacked.dart';
import '../../../../action_drawer/leading_drawer/d000_business_details/business_details_form_vu.dart';
import '../add_invoice_vm.dart';
import '../../../../../models/business/model.dart';
import 'business_info_card_vm.dart';

// ignore: must_be_immutable
class BusinessInfoCardVU extends ViewModelBuilderWidget<BusinessInfoCardVM> {
  BusinessInfoCardVU(this.businessDetail, this.invoiceVM,
      {Key? key, this.endPoint})
      : super(key: key);
  final AddInvoiceViewModel invoiceVM;
  BusinessDetail? businessDetail;
  String? endPoint;

  @override
  Widget builder(
      BuildContext context, BusinessInfoCardVM viewModel, Widget? child) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BusinessDetailsFormVU(businessDetail,
                    endPoint: endPoint,
                    invoicePatchFunction: invoiceVM.invoicePatch,
                    requestType: 'PATCH'))).then((value) async {
          if (value != null) {
            businessDetail = value;
            viewModel.notifyListeners();
            // invoiceVM.invoiceData!.setBusinessDetail(value);
            // invoiceVM.notifyListeners();

            // await invoiceVM.invoicePatch(invoiceVM.invoiceData!.id);
            invoiceVM.invoiceData!.businessDetail = value;
            // invoiceVM.notifyListeners();
          }
        });
      },
      child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(left: 10, top: 12, bottom: 11.0),
            child: Row(
              children: [
                businessDetail!.businessLogoFile == null
                    ? const Text("No Image")
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(3.0),
                        child: Image.file(
                          File(businessDetail!.businessLogoFile!),
                          width: 40,
                          height: 40,
                          fit: BoxFit.cover,
                        ),
                      ),
                const SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      businessDetail!.businessName ?? "",
                      style: GoogleFonts.poppins(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                        color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                      ),
                    ),
                    const SizedBox(
                      height: 3.0,
                    ),
                    Text(
                      businessDetail!.businessEmail ?? "",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  @override
  BusinessInfoCardVM viewModelBuilder(BuildContext context) {
    return BusinessInfoCardVM();
  }
}
