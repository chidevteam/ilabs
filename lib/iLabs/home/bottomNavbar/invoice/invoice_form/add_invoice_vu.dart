// import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:stacked/stacked.dart';
import 'package:ilabs/iLabs/models/history/history_vu.dart';
import 'package:ilabs/iLabs/widgets/input/chi_text_field.dart';
import '../../../../Utils/utils.dart';
import '../../../../models/invoice/model.dart';
import 'appbar_popup_menu.dart';
import 'sec01_business_info/business_info_card_vu.dart';
import 'sec02_invoice_info/invoice_info_card_vu.dart';
import 'sec03_client_info/client_info_section_vu.dart';
import 'sec04_items/item_section_vu.dart';
import 'sec05_payment_details/payment_detail.dart';
import 'sec06_images/image.dart';
import 'sec07_payment_instructions/payment_instruction.dart';
import 'sec08_signon/signed_on.dart';
import 'sec09_markpaid/mark_paid.dart';
import 'add_invoice_vm.dart';
import '../../../home_controller.dart';
import '../../../../widgets/navigation_bar/navigation_bar.dart';
import '../../../../pdf_generation/pdf_tests_vu.dart';
import '../../../../pdf_generation/pdf_invoice_vu.dart';

class AddInvoiceScreen extends ViewModelBuilderWidget<AddInvoiceViewModel> {
  const AddInvoiceScreen(this.homeController,
      {Key? key, required this.invoice, required this.isInvoice})
      : super(key: key);
  final HomeController? homeController; // = Get.find();
  final Invoice? invoice;
  final bool isInvoice;

  @override
  Widget builder(
      BuildContext context, AddInvoiceViewModel viewModel, Widget? child) {
    // log(homeController!.userDefault.toString());
    // List<Widget> widgetOptions = <Widget>[
    //   editBottomNavigation(context, viewModel),
    //   previewBottomNavigation(context, viewModel),
    //   // historyBottomNavigation(context, viewModel)
    //   PDFTestsVU()
    // ];

    String invoiceID = invoice == null ? "--New--" : invoice!.id.toString();

    return Scaffold(
      appBar: appBar(context, viewModel, invoiceID),
      bottomNavigationBar: bottomNavigationBar(
          viewModel.tabItems, viewModel.currentindex, viewModel.onCurrent),
      body: IndexedStack(
        index: viewModel.currentindex,
        children: [
          editBottomNavigation(context, viewModel, invoice),
          previewBottomNavigation(context, viewModel),
          // historyBottomNavigation(context, viewModel),
          // invoicePDFview(viewModel.fileName),
          viewModel.invoiceData!.id == null
              ? const Text("NULL ID")
              : HistoryVU(viewModel.invoiceData!.id!),
          PDFTestsVU(),
          PDFInvoiceVU(viewModel.redrawIndex, viewModel.invoiceData!),
        ],
      ),
      floatingActionButton: sendbutton(context, viewModel),
    );
  }

  // Widget invoicePDFview(String? fileName) {
  //   Widget pdfWidget = const Text("Empty PDF");
  //   if (fileName != null) {
  //     if (Platform.isLinux) {
  //       pdfWidget = Text("$fileName: PDFview on LInux not supported");
  //     } else {
  //       pdfWidget = PdfView(path: fileName);
  //     }
  //   }
  //   return Padding(
  //       padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
  //       child: SizedBox(
  //           width: double.infinity,
  //           child: Container(
  //               color: Colors.green,
  //               child: Container(
  //                 color: Colors.blue,
  //                 child: pdfWidget,
  //               ))));
  // }

  Scaffold editBottomNavigation(
      BuildContext context, AddInvoiceViewModel viewModel, Invoice? invoice) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Container(
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.transparent.withOpacity(0.2),
                      spreadRadius: 0.1,
                      blurRadius: 3,
                      offset: const Offset(2, 0),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    //todo: bug fix here ,,,, notify listener isnt working
                    InvoiceInfoCardVU(
                      viewModel.invoiceData!.invoiceInfo,
                      viewModel.invoiceData!.id,
                      onEdited: (invoiceInfo) {
                        if (invoiceInfo != null) //in case of navigator.pop()
                        {
                          viewModel.invoiceData!.invoiceInfo = invoiceInfo;
                          viewModel.invoicePatch(viewModel.invoiceData!.id);
                        }
                        //todo: a very serious discussion needs to be done here
                      },
                    ),
                    BusinessInfoCardVU(
                      viewModel.invoiceData!.businessDetail,
                      viewModel,
                      endPoint: viewModel.endPointPrefix +
                          "/invoicebusiness/${viewModel.invoiceData!.id}",
                    ),
                  ],
                ),
              ),
            ),
            // ClientInfoSectionVU(viewModel),
            const Divider(),
            ClientInfoSectionVU(viewModel: viewModel),
            const Divider(),
            ItemSectionVU(viewModel: viewModel),
            // TaxInfoCardVU(
            //   viewModel.invoiceData!.taxModel,
            //   onEdited: (invoiceInfo) {
            //     if (invoiceInfo != null) //in case of navigator.pop()
            //     {
            //       // viewModel.invoiceData!.invoiceInfo = invoiceInfo;
            //       // viewModel.invoicePatch(viewModel.invoiceData!.id);
            //     }
            //   },
            // ),

            PaymentDetail(viewModel, invoice),
            ImagesScreen(viewModel: viewModel),
            PaymentInstruction(viewModel),
            SignedOn(viewModel: viewModel),
            isInvoice
                ? markPaid(viewModel)
                : Visibility(
                    visible: viewModel.invoiceData!.id != null,
                    child: chiSaveButton(
                      "Make Invoice",
                      viewModel.makeInvoice,
                    )),
            const SizedBox(
              height: 50,
            )
            // const Spacer()
          ],
        ),
      ),
    );
  }

  Scaffold previewBottomNavigation(
    BuildContext context,
    AddInvoiceViewModel viewModel,
  ) {
    return Scaffold(
        body: viewModel.invoiceData != null
            // ? SfPdfViewer.file(viewModel.file!)
            ? Column(
                children: [
                  ...viewModel.invoiceData!.photos
                      .map((photo) => SizedBox(
                          height: 30,
                          width: 30,
                          child:
                              Image.network(Utils.imgPath + photo.imagePathX!)))
                      .toList(),
                  SizedBox(
                      height: 300,
                      child: SingleChildScrollView(
                          child: Text(viewModel.invoiceData.toString())))
                ],
              )
            : const Text(''));
  }

  Scaffold historyBottomNavigation(
    BuildContext context,
    viewModel,
  ) {
    return const Scaffold(body: Center(child: Text("text3")));
  }

  AppBar appBar(BuildContext context, viewModel, String invoiceNo) {
    return AppBar(
      backgroundColor: Colors.white,
      title: Text(
        'Invoice' + invoiceNo,
        style: const TextStyle(
            color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context, false);
        },
        icon: Icon(
          Icons.arrow_back,
          color: Colors.orange[800],
        ),
      ),
      actions: [appbarPopUpMenu(viewModel)],
    );
  }

  Widget sendbutton(
      BuildContext context, AddInvoiceViewModel viewModel) {
        return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      // this is ignored if animatedIcon is non null
      // child: Icon(Icons.add),
      visible: true,
      curve: Curves.bounceIn,
      overlayColor: Colors.transparent,
      overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      tooltip: 'Send',
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
      elevation: 8.0,
      child:const Icon(
        Icons.send,
        color: Colors.white,
      ),
      shape: const CircleBorder(),
      children: [
        SpeedDialChild(
          child: Icon(Icons.email,),
          backgroundColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
          label: 'Send email',
          labelStyle: TextStyle(fontSize: 18.0),
          onTap: () => viewModel.sendInvoiceViaEmail,
        ),
        SpeedDialChild(
          child: Icon(Icons.share),
          backgroundColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
          label: 'Share invoice',
          labelStyle: TextStyle(fontSize: 18.0),
          onTap: (){
            viewModel.shareInvoice();
          },
        ),
      ],
    );

    // return FloatingActionButton(
    //   onPressed: () async {
    //     debugPrint("Send Button Pressed");
    //     // await Navigator.pushNamed(context, route);
    //     // vm.refreshData()
    //     vm.invoicePatch(invoice == null ? null : invoice!.id).then((value) {
    //       Navigator.pop(context, true);
    //     });
    //   },
    //   backgroundColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
    //   child: const Icon(
    //     Icons.send,
    //     color: Colors.white,
    //   ),
    //   heroTag: "Send button",
    // );
  }

  @override
  AddInvoiceViewModel viewModelBuilder(BuildContext context) {
    return AddInvoiceViewModel(invoice, homeController!, isInvoice);
  }
}
