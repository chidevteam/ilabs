import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../../../widgets/output/standard_card.dart';
import '../../../../../Utils/utils.dart';
import 'tax_info_card_vm.dart';
import 'tax_info_form/tax_info_form_vu.dart';
import '../../../../../models/invoice_data/model.dart';
import '../../../../../models/tax/model.dart';

// ignore: must_be_immutable
class TaxInfoCardVU extends ViewModelBuilderWidget<TaxInfoCardVM> {
  TaxInfoCardVU(this.taxModel, {required this.onEdited, Key? key})
      : super(key: key);
  TaxModel taxModel;
  int? invoiceID; // = viewModel.invoiceData!.invoiceInfo;
  void Function(InvoiceInfo? invoiceInfo) onEdited;
  // final int invoiceId;

  @override
  Widget builder(BuildContext context, TaxInfoCardVM viewModel, Widget? child) {
    return InkWell(
      onTap: () {},
      // Utils.push(context, TaxInfoFormVU(viewModel.vmTaxModel, invoiceID))
      //     .then((value) {
      //   if (value != null) {
      //     // viewModel.onSave(value, invoiceID);
      //     viewModel.vmTaxModel = value;
      //   }
      //   // print(invoiceInfo);
      //   // invoiceInfo = value;
      //   onEdited(value);
      //   //todo: a very serious discussion needs to be done here as well
      //   viewModel.notifyListeners();
      //   });
      // },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            elevation: 0.7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                invoiceFormCardTitle(
                    'Tax info',
                    const Icon(
                      Icons.edit_outlined,
                      size: 22.0,
                      color: Colors.orange,
                    )),
                invoiceFormBulletOne(
                    Icons.add_chart_outlined, viewModel.vmTaxModel.taxType),
                invoiceFormBulletOne(
                    Icons.calendar_today, viewModel.vmTaxModel.taxLabel),
                invoiceFormBulletTwo(
                    Icons.calendar_today,
                    viewModel.vmTaxModel.taxRate,
                    viewModel.vmTaxModel.taxInclusive.toString()),
              ],
            )),
      ),
    );
  }

  @override
  TaxInfoCardVM viewModelBuilder(BuildContext context) {
    return TaxInfoCardVM(taxModel);
  }
}
