import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../../../models/invoice_data/model.dart';
import '../../../../../../widgets/input/chi_text_field.dart';
import '../../../../../../widgets/input/chi_textfield.dart';
import '../../../../../../widgets/input/drop_down_bottom_sheet.dart';
import 'tax_info_form_vm.dart';

class TaxInfoFormVU extends ViewModelBuilderWidget<InvoiceInfoFormViewModel> {
  final InvoiceInfo? invoiceInfo1;
  int? invoiceID;
  TaxInfoFormVU(this.invoiceInfo1, this.invoiceID, {Key? key})
      : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    // debugPrint("===========  ${viewModel.issueDate}");
    return Scaffold(
      appBar: chiAppBar('Invoice Number', context),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CHITextField(
                    heading: 'Invoice Number',
                    hintText: '',
                    func: viewModel.onInvoiceNumber,
                    fieldType: TextInputType.text,
                    initialValue: viewModel.invoiceInfo!.number),
                CHITextField(
                    // enabled: false,
                    heading: 'Issue Date',
                    hintText: '',
                    func: viewModel.onIssueDate,
                    fieldType: TextInputType.datetime,
                    initialValue: viewModel.invoiceInfo!.date,
                    suffixIcon: IconButton(
                        onPressed: () {
                          viewModel.selectDate(context, "ISSUE_DATE");
                        },
                        icon: const Icon(
                          Icons.date_range_rounded,
                          color: Colors.orange,
                        ))),
                CHITextField(
                    // enabled: false,
                    heading: 'Due Date',
                    hintText: '',
                    func: viewModel.onDueDate,
                    fieldType: TextInputType.number,
                    initialValue: viewModel.invoiceInfo!.dueDate,
                    suffixIcon: IconButton(
                        onPressed: () {
                          viewModel.selectDate(context, "DUE_DATE");
                        },
                        icon: const Icon(
                          Icons.date_range_rounded,
                          color: Colors.orange,
                        ))),
                dropDownBottomSheet(context,
                    selectedIndex: viewModel.termsIndex,
                    items: viewModel.terms2,
                    title: "Select Terms", onTap: (index) {
                  viewModel.onTerms(index);
                  Navigator.pop(context);
                }),
                CHITextField(
                    heading: 'P.O. Number',
                    hintText: '',
                    func: viewModel.onPoNumber,
                    fieldType: TextInputType.number,
                    initialValue: viewModel.invoiceInfo!.poNumber),
                chiSaveButton("Save", () async {
                  await viewModel.onSaveButton();
                  Navigator.pop(context, viewModel.invoiceInfo);
                })
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  InvoiceInfoFormViewModel viewModelBuilder(BuildContext context) {
    return InvoiceInfoFormViewModel(
        InvoiceInfo.fromMap(invoiceInfo1!.toMap()), invoiceID);
  }
}

// DO NOT DELETE: See how  DropdownMenuItem<Terms>(
// are used to do the desired stuff so that we can make widgets with 
// similar IO

// Widget termsDropDown(InvoiceNumberViewModel viewModel) {
//   return DropdownButtonFormField(
//     decoration: InputDecoration(
//       contentPadding: const EdgeInsets.fromLTRB(8.0, 4, 8.0, 2.0),
//       enabledBorder: OutlineInputBorder(
//         borderRadius: const BorderRadius.all(Radius.circular(8.0)),
//         borderSide: BorderSide(
//           color: Colors.grey.shade300,
//           width: 1.0,
//         ),
//       ),
//       border: const OutlineInputBorder(
//         borderRadius: BorderRadius.all(Radius.circular(8.0)),
//       ),
//     ),
//     icon: const RotatedBox(
//       quarterTurns: 1,
//       child: Icon(
//         Icons.arrow_forward_ios_outlined,
//         color: Colors.black54,
//         size: 12,
//       ),
//     ),
//     items: viewModel.items.map((e) {
//       return DropdownMenuItem<Terms>(
//         child: Text(
//           e.term,
//           style: const TextStyle(
//               color: Colors.black54, fontSize: 12, fontWeight: FontWeight.w500),
//         ),
//         value: e,
//       );
//     }).toList(),
//     value: viewModel.items[0],
//     onChanged: viewModel.onTermChange,
//   );
// }
