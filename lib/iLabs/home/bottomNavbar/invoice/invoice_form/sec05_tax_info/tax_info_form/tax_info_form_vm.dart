import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:intl/intl.dart';
import '../../../../../../models/invoice_data/model.dart';
import '../../../../../../models/due_days/due_days_model.dart';
import '../../../../../../services/api_client.dart';

class InvoiceInfoFormViewModel extends BaseViewModel {
  // HomeController hCtrl = Get.find<HomeController>();
  final formKey = GlobalKey<FormState>();

  InvoiceInfo? invoiceInfo; //copy of invoice info recieved from vu
  List<String> terms2 = [...DueDaysModel.dayTerms, 'Custom'];
  int? invoiceID;
  int termsIndex = -1;

  InvoiceInfoFormViewModel(this.invoiceInfo, this.invoiceID) {
    termsIndex = DueDaysModel.getSelectedIndex(invoiceInfo!.terms!);
  }

  Future<void> selectDate(BuildContext context, String dateType) async {
    DateTime? dateTime = await showDatePicker(
        context: context,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101),
        initialDate: dateType == "ISSUE_DATE"
            ? DateFormat(invoiceInfo!.dateFormat).parse(invoiceInfo!.date!)
            : DateFormat(invoiceInfo!.dateFormat).parse(invoiceInfo!.dueDate!));

    dateTime ??= DateTime.now();
    if (dateType == "ISSUE_DATE") {
      invoiceInfo!.date = DateFormat(invoiceInfo!.dateFormat).format(dateTime);
    } else if (dateType == "DUE_DATE") {
      debugPrint(DateTime.now().toString());
      invoiceInfo!.dueDate =
          DateFormat(invoiceInfo!.dateFormat).format(dateTime);
    } else {
      debugPrint("Should never come here");
    }
    setTermOnDateSelection();
    notifyListeners();
  }

  onInvoiceNumber(String? value) {
    invoiceInfo!.number = value!.trim();
  }

  onIssueDate(String? value) {
    // invoiceInfo!.date = value!.trim();
    // setTermOnDateSelection();
  }

  onDueDate(String? value) {
    // invoiceInfo!.dueDate = value!.trim();
    // setTermOnDateSelection();
  }

  onTerms(int index) {
    termsIndex = index;
    if (termsIndex == -1) {
      invoiceInfo!.terms = 'Custom';
    } else {
      invoiceInfo!.terms = terms2[index];
      setCalendersOnTermsSelection(
          DueDaysModel.getNumDaysFromTermDays(terms2[index]));
    }
    notifyListeners();
  }

  onPoNumber(String? value) {
    invoiceInfo!.poNumber = value!.trim();
  }

  Future<void> onSaveButton() async {
    formKey.currentState!.save();

    if (formKey.currentState == null && invoiceID != null) {
      setBusy(true);
      Request req = invoiceInfo!.toMap();
      String endPointPrefix = "/invoices";
      if (invoiceInfo!.isInvoice == false) endPointPrefix = "/estimates";
      String endPoint = "$endPointPrefix/$invoiceID";
      APIResponse respx = await ApiClient.patch<InvoiceInfo>(
          request: req, endPoint: endPoint, fromJson: InvoiceInfo.fromMap);
      debugPrint("$respx");
      InvoiceInfo temp = respx['data'];
      debugPrint("InvoiceInfo:: \n $temp");
      notifyListeners();
      setBusy(false);
    }
  }

  setCalendersOnTermsSelection(int? days) {
    if (days != null) {
      invoiceInfo!.date =
          DateFormat(invoiceInfo!.dateFormat).format(DateTime.now());
      invoiceInfo!.dueDate = DateFormat(invoiceInfo!.dateFormat)
          .format(DateTime.now().add(Duration(days: days)));
      notifyListeners();
    }
  }

  setTermOnDateSelection() {
    DateTime dueDate =
        DateFormat(invoiceInfo!.dateFormat).parse(invoiceInfo!.dueDate!);
    DateTime issueDate =
        DateFormat(invoiceInfo!.dateFormat).parse(invoiceInfo!.date!);
    invoiceInfo!.terms = DueDaysModel.getTermFromNumberOfDays(
        dueDate.difference(issueDate).inDays);
    if (invoiceInfo!.terms == 'Custom') {
      termsIndex = terms2.length - 1;

      ///last index
    } else {
      termsIndex = DueDaysModel.getSelectedIndex(invoiceInfo!.terms!);
    }
  }
}
