import 'package:flutter/material.dart';
// import 'package:stacked/stacked.dart';
import '../../../../services/api_client.dart';
import 'add_invoice_vu.dart';
import '../../../home_controller.dart';

void main() async {
  //
  await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
  debugPrint("Starting Item View");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  HomeController controller = HomeController();
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext buildContext) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: const AddInvoiceScreen(
          null,
          invoice: null,
          isInvoice: true,
        ));
  }

  @override
  viewModelBuilder(BuildContext context) {
    var vm = HomeController();
    return vm;
  }
}
