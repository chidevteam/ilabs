import 'package:flutter/material.dart';
import '../add_invoice_vm.dart';

Widget markPaid(AddInvoiceViewModel viewModel) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 22.0),
    child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Mark Paid',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
              ),
              Transform.scale(
                scale: 0.8,
                child: Switch(
                  value: viewModel.invoiceData!.paid,
                  activeColor: Colors.orange[700],
                  onChanged: (value) async {
                    // viewModel.invoiceData!.paid = !viewModel.invoiceData!.paid;
                    // viewModel.notifyListeners();
                    await viewModel.markPaid();
                    viewModel.notifyListeners();
                  },
                ),
              )
            ],
          ),
        )),
  );
}
