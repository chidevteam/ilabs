import 'package:flutter/material.dart';

TabBar tabBarItems(List<String> sList) {
  List<Tab> tabList = [];
  TextStyle tStyle = const TextStyle(fontSize: 12, fontWeight: FontWeight.w400);
  for (String item in sList) {
    Text text = Text(item, style: tStyle);
    Tab tab = Tab(child: text);
    tabList.add(tab);
  }

  return TabBar(
      unselectedLabelColor: Colors.black45,
      indicatorWeight: 3,
      // indicatorColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
      labelColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
      labelPadding: EdgeInsets.zero,
      tabs: tabList);
}
