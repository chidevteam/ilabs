import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../language/generated/locale_keys.g.dart';
import '../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import '../../../../widgets/input/chi_textfield.dart';
import 'add_item_vm.dart';
import '../../../../models/item/model.dart';

class NewItemScreen extends ViewModelBuilderWidget<AddItemVM> {
  final InvoiceItem? item;

  const NewItemScreen(this.item, {Key? key}) : super(key: key);
  @override
  Widget builder(BuildContext context, AddItemVM viewModel, Widget? child) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(0xf7, 0xf7, 0xf7, 1.0),
      appBar: chiAppBar("Add Item", context, actions: [
        TextButton(
          onPressed: () async {
            InvoiceItem? retVal = await viewModel.onSave();
            if (retVal != null) Navigator.pop(context, retVal);
          },
          child: Text(
            viewModel.item!.id == null ? "Save" : "Update",
            style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
          ),
        ),
      ]),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: SingleChildScrollView(
              child: Form(
                key: viewModel.formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.transparent.withOpacity(0.1),
                              spreadRadius: 0.1,
                              blurRadius: 1,
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(12.0, 0, 12, 10),
                          child: Column(
                            children: [
                              CHITextField(
                                heading:
                                    (LocaleKeys.add_new_item_itemname).tr(),
                                hintText: 'Type Item Name',
                                fieldType: TextInputType.text,
                                func: (value) {
                                  viewModel.onItemName(value);
                                },
                                initialValue: viewModel.item == null
                                    ? ""
                                    : viewModel.item!.description,
                                validator: viewModel.onItemNameValidator,
                              ),
                              CHITextField(
                                heading: "Item Description", //TODO: translate
                                // (LocleKeys.add_new_item_itemname).tr(),
                                hintText: 'Type Item Description',
                                fieldType: TextInputType.text,
                                func: (value) {
                                  viewModel.onItemAdditionalDetail(value);
                                },
                                initialValue: viewModel.item == null
                                    ? ""
                                    : viewModel.item!.additionalDetail,
                                // validator: viewModel.onItemNameValidator,
                              ),
                              CHITextField(
                                fieldType: TextInputType.phone,
                                heading:
                                    (LocaleKeys.add_new_item_unitcost).tr(),
                                hintText: '0.00',
                                func: (value) {
                                  viewModel.onItemUnitCost(value);
                                },
                                initialValue: viewModel.item == null
                                    ? ""
                                    : viewModel.item!.unitCost.toString(),
                                validator: viewModel.onItemUnitCostValidator,
                                inputFormat: [
                                  LengthLimitingTextInputFormatter(18),
                                  FilteringTextInputFormatter(
                                      RegExp(r'^\d+\.?\d{0,2}'),
                                      allow: true),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    taxable(viewModel),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: viewModel.isBusy,
            child: const SizedBox(
              height: double.maxFinite,
              width: double.maxFinite,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget taxable(AddItemVM viewModel) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.transparent.withOpacity(0.1),
            spreadRadius: 0.0,
            blurRadius: 1,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 12,
        ),
        child: Row(
          children: [
            const Text(
              "Taxable",
              style: TextStyle(
                  color: Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                  fontSize: 12.0,
                  fontWeight: FontWeight.w400),
            ),
            const Spacer(),
            Transform.scale(
              scale: 0.7,
              child: Switch(
                  value: viewModel.item!.taxable!,
                  onChanged: (value) {
                    viewModel.onToggleTaxable(value);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  @override
  AddItemVM viewModelBuilder(BuildContext context) {
    return AddItemVM(item);
  }
}
