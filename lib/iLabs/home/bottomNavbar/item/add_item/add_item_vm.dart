import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../models/item/model.dart';
import '../../../../services/api_client.dart';

class AddItemVM extends BaseViewModel {
  // void changeLanguage(BuildContext context) {}

  final formKey = GlobalKey<FormState>();

  InvoiceItem? item;

  AddItemVM(this.item) {
    item ??= InvoiceItem(null, "", 0.00, true, "");
  }

  onToggleTaxable(bool value) {
    item!.taxable = value;
    notifyListeners();
  }

  onItemName(String? value) async {
    item!.description = value!.trim();
  }

  onItemAdditionalDetail(String? value) async {
    // print
    item!.additionalDetail = value!.trim();
  }

  String? onItemNameValidator(value) {
    debugPrint("yes it is being called");
    if (value == null || value.isEmpty) {
      debugPrint('costvalue null called.');
      return 'field is required';
    }
    return null;
  }

  onItemUnitCost(String? value) {
    item!.unitCost = double.parse(value!.trim());
  }

  String? onItemUnitCostValidator(value) {
    if (value == null || value.isEmpty) {
      return 'field is required';
    }
    return null;
  }

  Future<InvoiceItem?> onSave() async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return null;
    }

    if (!formKey.currentState!.validate()) {
      return null;
    }

    Request addItemReq = {
      "taxable": item!.taxable,
      "description": item!.description,
      "additional_detail": item!.additionalDetail,
      "unit_cost": item!.unitCost
    };
    APIResponse resp;
    setBusy(true);
    if (item!.id != null) {
      addItemReq["taxable"] = item!.taxable;
      addItemReq["id"] = item!.id;
      resp = await ApiClient.patch<InvoiceItem>(
          request: addItemReq,
          endPoint: "/items/" + item!.id.toString(),
          fromJson: InvoiceItem.fromJson);
    } else {
      resp = await ApiClient.post<InvoiceItem>(
          request: addItemReq,
          endPoint: "/items",
          fromJson: InvoiceItem.fromJson);
    }
    setBusy(false);
    return resp["data"];
  }
}
