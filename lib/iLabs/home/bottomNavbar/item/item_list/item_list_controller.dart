import 'dart:async';

import 'package:cached_map/cached_map.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:ilabs/iLabs/home/home_controller.dart';
import '../../../../models/item/model.dart';
import '../../../../services/api_client.dart';

class ItemListController extends GetxController {
  ScrollController listScrollController = ScrollController();///only needed for pagination for now
  HomeController homeController = Get.find();
  RxList<InvoiceItem> itemList = <InvoiceItem>[].obs;
  RxInt ccc = 0.obs;
  RxBool isLoading = false.obs;
  RxBool loadingMore = false.obs;
  int pages = 0;
  int count = 50;
  int currentPage = 0;
 late  StreamSubscription connectivitySubscription ;


  ItemListController() {
    onLoading();
    // enablePagination();
  }

  ///if we plan to implement pagination again in future .. this will be used
  // enablePagination() {
  //   listScrollController.addListener(() {
  //     if (listScrollController.offset >=
  //             listScrollController.position.maxScrollExtent - 1000 &&
  //         !loadingMore.value) {
  //       // debugPrint("currentPage: ${currentPage + 1}");
  //       loadMore(currentPage + 1);
  //     }
  //   });
  // }

  onLoading() async {
    isLoading.value = true;
    await getFromCache();
    await checkConnectivityAndLoadItems();
    isLoading.value = false;
  }

  Future<void> refreshData(String search) async {
    APIResponse resp = await ApiClient.get<InvoiceItemList>(
        endPoint: "/items",
        params: search.isEmpty ? "/0/$count/id/DESC" : "/search/$search",
        fromJson: InvoiceItemList.fromJson);
    // itemList = resp["data"].data;
    itemList.clear();
    itemList.addAll(resp["data"].data);
    pages = resp["data"].pages;
    count = resp["data"].count;
    currentPage = 0;
    cacheCurrentList();
    debugPrint("Number of Items: ${itemList.length}");
    if(count>itemList.length)
      {
        refreshData('');
      }
  }

  //todo: pending work
  onUpdate(InvoiceItem item, int index) {
    itemList[index] = item;
    cacheCurrentList();
  }

  delete(int index) async {
    APIResponse resp = await ApiClient.delete<InvoiceItemList>(
        endPoint: "/items", params: "/${itemList[index].id}");

    itemList.removeAt(index);
    cacheCurrentList();
    // debugPrint("$itemList");
    // debugPrint("Delete Response: $resp");
  }

  loadMore(int pageNo) async {
    if (itemList.length < count) {
      loadingMore.value = true;
      debugPrint('Load more function called ');
      APIResponse resp = await ApiClient.get<InvoiceItemList>(
          endPoint: "/items",
          params: "/$pageNo/500/id/DESC",
          fromJson: InvoiceItemList.fromJson);
      // itemList = resp["data"].data;
      itemList.addAll(resp["data"].data);
      pages = resp["data"].pages;
      count = resp["data"].count;
      currentPage++;
      loadingMore.value = false;
      cacheCurrentList();
      // debugPrint("Number of Items: ${itemList.length}");
    }
  }

  cacheCurrentList() {
    Mapped.saveFileDirectly(
        file: InvoiceItemList(
                currentPage: currentPage, data: itemList, pages: count)
            .toJson(),
        cachedFileName: "items");
  }

  getFromCache() async {
    var json = await Mapped.loadFileDirectly(cachedFileName: "items");
    if (json  != null) {
      InvoiceItemList modal = InvoiceItemList.fromJson(json);
      itemList.clear();
      itemList.addAll(modal.data!);
      isLoading.value = false;
      currentPage = modal.currentPage ?? 0;
      count = modal.pages!;
    }
  }

  checkConnectivityAndLoadItems() {
    ///each time the connection is turned back on .. it will refresh the data itself tranparently.
    connectivitySubscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      print(result.toString());
      if(result == ConnectivityResult.wifi || result == ConnectivityResult.mobile)
      {
        refreshData('');
      }
    });
  }


  @override
  void onClose() {
    connectivitySubscription.cancel();
     itemList.close();
     ccc.close();
     isLoading.close();
     loadingMore.close();
    super.onClose();
  }

}
