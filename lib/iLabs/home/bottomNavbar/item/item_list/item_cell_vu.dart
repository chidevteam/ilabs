import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:get/get.dart';

import 'item_list_controller.dart';
import '../../../../models/item/model.dart';
import 'package:get/get.dart';
import '../add_item/add_item_vu.dart';

Widget itemCell(BuildContext context, ItemListController controller,
    InvoiceItem item, int index) {
  // RxString str = controller.homeController.userDefault.rXregionCurrency;
  return InkWell(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NewItemScreen(item),
          )).then((v) {
        // print("back from add update form");
        if (v != null) {
          controller.itemList[index] = v;
          // print("done");
        }
      });
    },
    child: Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color:
                  const Color.fromRGBO(0x29, 0x00, 0x00, 1.0).withOpacity(0.2),
              spreadRadius: 0.1,
              blurRadius: 3,
              offset: const Offset(2, 0),
            ),
          ],
        ),
        child: IntrinsicHeight(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(11.5, 10.0, 12.0, 10.0),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        item.description!,
                        // overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: GoogleFonts.poppins(
                          color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        item.additionalDetail == null
                            ? ""
                            : item.additionalDetail!,
                        // overflow: const TextOverflow.ellipsis,
                        maxLines: 1,
                        style: GoogleFonts.poppins(
                          fontSize: 12.0,
                          color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ),
                const VerticalDivider(
                  thickness: 1,
                  color: Color.fromRGBO(0xd5, 0xd5, 0xd5, 1.0),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Obx(() => Text(
                            '${controller.homeController.userDefault.rXregionCurrency.value} ${item.unitCost.toString()}',
                            // overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: GoogleFonts.poppins(
                              fontSize: 16.0,
                              color:
                                  const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                              fontWeight: FontWeight.w500,
                            ),
                          )),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        item.taxable == true ? "Taxable" : "Non Taxable",
                        // item.additionalDetail!,
                        // overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: GoogleFonts.poppins(
                          fontSize: 12.0,
                          color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ),
  );
}
