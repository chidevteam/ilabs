import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../widgets/loading/loading.dart';
import '../../../home_controller.dart';
import 'item_list_controller.dart';
import 'item_cell_vu.dart';
import '../add_item/add_item_vu.dart';
import '../../../../models/item/model.dart';

class InvoiceItemListScreen extends StatelessWidget {
  InvoiceItemListScreen({Key? key}) : super(key: key);
  final ItemListController controller = Get.find();
  final HomeController homeController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(0xfc, 0xfc, 0xfc, 1.0),
      body: RefreshIndicator(
        onRefresh: () async {
          await controller.refreshData('');
        },
        child: Stack(children: [
          Column(
            children: [
              Expanded(
                child: Obx(
                  () => ListView.builder(
                      controller: controller.listScrollController,
                      shrinkWrap: true,
                      itemCount: controller.itemList.length,
                      itemBuilder: (context, index) {
                        // debugPrint("Creating item at : $index");
                        InvoiceItem item = controller.itemList[index];
                        controller.ccc.value++;
                        return Dismissible(
                            key: Key("${controller.ccc.value}"),
                            onDismissed: (direction) {
                              controller.delete(index);
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                      backgroundColor: Colors.green,
                                      duration: const Duration(
                                          seconds: 0, milliseconds: 750),
                                      content: Text('$item dismissed')));
                            },
                            background: Container(color: Colors.red),
                            child: Column(
                              children: [
                                Obx(() {
                                  return itemCell(context, controller,
                                      controller.itemList[index], index);
                                }),
                                Obx(() {
                                  // print(controller.itemList.length);
                                  // print("<");
                                  // print((controller.count));
                                  return Visibility(
                                      visible: controller.itemList.length - 1 ==
                                              index &&
                                          controller.itemList.length <
                                              controller.count,
                                      child: const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Center(
                                          child: CircularProgressIndicator(),
                                        ),
                                      ));
                                })
                              ],
                            ));
                      }),
                ),
              )
            ],
          ),
          Obx(
            () => Positioned(
                child: controller.isLoading.value
                    ? const GettingRecords('Loading Items')
                    : const Text('')),
          )
        ]),
      ),
      floatingActionButton: floatAddItem(context, controller),
    );
  }

  FloatingActionButton floatAddItem(
      BuildContext context, ItemListController controller) {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => NewItemScreen(null),
            )).then((invoiceItem) {
          if (invoiceItem != null) {
            var copyList = [];
            for (var element in controller.itemList) {
              copyList.add(element);
            }
            controller.itemList.clear();
            controller.itemList.addAll([invoiceItem, ...copyList]);
            // print("done");
          }
        });
      },
      backgroundColor:
          const Color.fromRGBO(0xff, 0x78, 0x48, 1.0).withOpacity(0.8),
      elevation: 0.0,
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
      heroTag: "itemListView",
    );
  }
}
