import 'package:flutter/material.dart';
import '../../../services/api_client.dart';
import 'item_list/item_list_vu.dart';

void main() async {
  //
  await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
  debugPrint("Starting Item View");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.orange),
      home: InvoiceItemListScreen(),
    );
  }
}
