import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
import 'add_client/add_client_vu.dart';
import 'client_list/client_list_controller.dart';

class CustomSearchDelegate extends SearchDelegate<Client> {
  final controller = Get.find<ClientListController>();
  int selectedIndex = 0;
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, Client());
        },
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation));
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestionList = controller.clientList.value
        .where((p) =>
            p.name.toString().toLowerCase().contains(query.toLowerCase()))
        .toList();
    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
              onTap: () {
                print(controller.clientList[index].name);
                //   showResults(context);
                // selectedIndex = index;
                // query = suggestionList[index].name.toString();
                // debugPrint("$query -- ${suggestionList[index]}");
              },
              //    leading: const Icon(Icons.location_city),
              title: Text(suggestionList.elementAt(index).name.toString()));
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = controller.clientList.value
        .where((p) =>
            p.name.toString().toLowerCase().contains(query.toLowerCase()))
        .toList();
    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
              onTap: () {
                print(suggestionList[index]);
                // Iterable<Client> client = controller.clientList.value.where(
                //     (element) => element.name == suggestionList[index].name);
                // print(client);

                int n = controller.clientList.indexOf(suggestionList[index]);
                // print(n);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NewClientScreen(
                          suggestionList[index] == null
                              ? null
                              : suggestionList[index]),
                    )).then((v) {
                  // print("back from add update form");
                  if (v != null) {
                    // print('value returned ...............$v');
                    // print('object');
                    controller.clientList[n] = v;
                    //    controller.tempList[index] = v;

                    print("done");
                  }
                });
                //   showResults(context);
                // selectedIndex = index;
                // query = suggestionList[index].name.toString();
                // debugPrint("$query -- ${suggestionList[index]}");
              },
              //    leading: const Icon(Icons.location_city),
              title: Text(suggestionList.elementAt(index).name.toString()));
        });
  }
}
