import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import '../../../../widgets/input/chi_textfield.dart';
import '../client_list/client_list_controller.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
import '../../../../widgets/input/chi_text_field.dart';
import 'add_client_controller.dart';

class NewClientScreen extends StatelessWidget {
  AddClientController controller = AddClientController();
  ClientListController clientListController = ClientListController();
  NewClientScreen(Client? client, {Key? key}) : super(key: key) {
    debugPrint("const called");
    controller.client = client;
    //print('$item');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Add new Client',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(
                context,
              );
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                controller.onAddClient(context);
                //viewModel.onSave();
                //Navigator.pop(context, viewModel.taxModel);
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: SingleChildScrollView(
              child: Form(
                key: controller.formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 0.6,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10.0, 0, 10, 10),
                        child: Column(
                          children: [
                            CHITextField(
                              heading: ('Client Name*'),
                              hintText: 'Type Item Name',
                              fieldType: TextInputType.name,
                              func: (value) {
                                controller.onClientName(value);
                              },
                              initialValue: controller.client == null ||
                                      controller.client!.name == null
                                  ? ""
                                  : controller.client!.name,
                              validator: controller.onClientNameValidator,
                            ),
                            CHITextField(
                                heading: ('Email*'),
                                hintText: 'Type your Email',
                                fieldType: TextInputType.emailAddress,
                                func: (value) {
                                  controller.onClientEmail(value);
                                },
                                initialValue: controller.client == null ||
                                        controller.client!.email == null
                                    ? ""
                                    : controller.client!.email.toString(),
                                //   validator: null
                                validator: controller.validateEmail),
                            chiNumberFormField(
                                fieldType: TextInputType.phone,
                                inputFormat: <TextInputFormatter>[
                                  FilteringTextInputFormatter(
                                      RegExp(r'^\d+\d{0,100}'),
                                      allow: true),
                                ],
                                heading: ('Phone'),
                                hintText: 'Enter Phone',
                                func: (value) {
                                  controller.onClientMobile(value);
                                },
                                initialValue: controller.client == null ||
                                        controller.client!.mobile == null
                                    ? ""
                                    : controller.client!.mobile.toString(),
                                validator: null),
                            const SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                                onTap: () {}, child: Obx(() => getContainer())),
                            Obx(
                              () => Opacity(
                                  opacity:
                                      controller.isVisible.value ? 1.0 : 0.0,
                                  child: Column(
                                    children: [
                                      chiNumberFormField(
                                          fieldType: TextInputType.phone,
                                          inputFormat: <TextInputFormatter>[
                                            FilteringTextInputFormatter(
                                                RegExp(r'^\d+\d{0,100}'),
                                                allow: true),
                                          ],
                                          heading: ('Office Phone'),
                                          hintText: 'Enter Phone',
                                          func: (value) {
                                            controller.onClientOffice(value);
                                          },
                                          initialValue: controller.client ==
                                                      null ||
                                                  controller.client!.phone == null
                                              ? ""
                                              : controller.client!.phone
                                                  .toString(),
                                          validator: null),
                                      CHITextField(
                                          heading: ('Fax'),
                                          hintText: 'Type your Email',
                                          fieldType: TextInputType.text,
                                          func: (value) {
                                            controller.onClientFax(value);
                                          },
                                          initialValue: controller.client ==
                                                      null ||
                                                  controller.client!.fax == null
                                              ? ""
                                              : controller.client!.fax,
                                          validator: null),
                                      CHITextField(
                                          heading: ('Office Address'),
                                          hintText: 'Enter Address',
                                          fieldType: TextInputType.text,
                                          func: (value) {
                                            controller
                                                .onClientOfficeAddress(value);
                                          },
                                          initialValue: controller.client ==
                                                      null ||
                                                  controller.client!.address ==
                                                      null
                                              ? ""
                                              : controller.client!.address,
                                          validator: null),
                                    ],
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ),
                    // Card(
                    //   elevation: 0.6,
                    //   child: Padding(
                    //     padding: const EdgeInsets.only(left: 12),
                    //     child: Row(
                    //       children: [
                    //         Text(
                    //           (LocaleKeys.add_new_item_textable).tr(),
                    //           style: TextStyle(
                    //               color: Colors.grey[600],
                    //               fontWeight: FontWeight.w300),
                    //         ),
                    //         const Spacer(),
                    //         // Obx(
                    //         //   () => Switch(
                    //         //       value: controller.item == null
                    //         //           ? controller.taxable.value
                    //         //           : controller.item!.taxable.value,
                    //         //       onChanged: (value) {
                    //         //         if (controller.item == null) {
                    //         //           controller.onToggleTaxable();
                    //         //         } else {
                    //         //           controller.item!.taxable.value =
                    //         //               !controller.item!.taxable.value;
                    //         //         }
                    //         //       }),
                    //         // ),
                    //       ],
                    //     ),
                    //   ),
                    // ),

                    // chiSaveButton(controller.item == null ? "Save" : "Update",
                    //     () {
                    //   controller.onAddItem(context);
                    // }),
                  ],
                ),
              ),
            ),
          ),
          Obx(
            () => Visibility(
              visible: controller.isLoading.value,
              child: Container(
                height: double.maxFinite,
                width: double.maxFinite,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getContainer() {
    return !controller.isVisible.value
        ? Container(
            width: 360,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    decoration: BoxDecoration(
                        color: Colors.orange[800], shape: BoxShape.circle),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 14,
                    )),
                TextButton(
                  onPressed: () {
                    controller.moreDatails();
                    //viewModel.onSave();
                    //Navigator.pop(context, viewModel.taxModel);
                  },
                  child: Text(
                    'Add More Details',
                    style: TextStyle(color: Colors.orange[800], fontSize: 14.0),
                  ),
                ),
              ],
            ),
          )
        : Container(
            width: 360,
            height: 35,
            color: Colors.black12,
            child: IconButton(
              icon: Icon(
                Icons.expand_less_outlined,
                size: 24,
              ),
              onPressed: () {
                controller.moreDatails();
              },
            ));
  }
}
