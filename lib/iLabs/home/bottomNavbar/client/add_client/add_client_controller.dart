import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
// import '../../../../../../models/item/model.dart';
import '../../../../models/item/model.dart';
// import '../../../../../../services/api_client.dart' as client;
import '../../../../services/api_client.dart';

class AddClientController extends GetxController {
  void changeLanguage(BuildContext context) {}

  RxBool isVisible = false.obs;

  final formKey = GlobalKey<FormState>();
  // RxString itemName = "".obs;
  // RxString unitCost = ''.obs;
  // String? checkText = '';
  // RxBool taxable = false.obs;
  RxString name = ''.obs;
  RxString email = ''.obs;
  RxString mobile = ''.obs;
  RxString phone = ''.obs;
  RxString fax = ''.obs;
  RxString address = "".obs;

  Client? client;
  RxBool isLoading = false.obs;

  onClientName(String? value) async {
    name.value = value!.trim();
  }

  // onToggleTaxable() {
  //   taxable.value = !taxable.value;
  // }

  String? onClientNameValidator(value) {
    debugPrint("yes it is being called");
    if (value == null || value.isEmpty) {
      debugPrint('costvalue null called.');
      return 'Name is required';
    }
    return null;
  }

  onClientEmail(String? value) async {
    email.value = value!.trim();
  }

  String? onClientEmailValidator(value) {
    if (value == null || value.isEmpty) {
      return 'field is required';
    }
    return null;
  }

  onClientMobile(String? value) async {
    value == null || value.isEmpty
        ? mobile.value = ''
        : mobile.value = value.trim();
  }

  String? onClientMobileValidator(value) {
    if (value == null || value.isEmpty) {
      return 'field is required';
    }
    return null;
  }

  onClientOffice(String? value) async {
    value == null || value.isEmpty
        ? phone.value = ''
        : phone.value = value.trim();
  }

  String? onClientOfficeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'field is required';
    }
    return null;
  }

  onClientFax(String? value) async {
    value == null || value.isEmpty ? fax.value = '' : fax.value = value.trim();
  }

  String? onClientFaxValidator(value) {
    if (value == null || value.isEmpty) {
      return 'field is required';
    }
    return null;
  }

  onClientOfficeAddress(String? value) async {
    value == null || value.isEmpty
        ? address.value = ''
        : address.value = value.trim();
  }

  String? onClientOfficeAddressValidator(value) {
    if (value == null || value.isEmpty) {
      return 'field is required';
    }
    return null;
  }

  // onSaveCheck(value) {
  //   checkText = value;
  // }

  // String? onCheckValidator(value) {
  //   if (value == null || value.isEmpty) {
  //     return 'email is required';
  //   }
  //   return null;
  // }

  String? validateEmail(String? value) {
    if (value != null) {
      if (value.length > 5 && value.contains('@') && value.endsWith('.com')) {
        return null;
      }
      return 'Enter a Valid Email Address';
    }
  }

  onAddClient(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    Request req = {
      "name": name.value,
      "email": email.value,
      "phone": phone.value,
      "mobile": mobile.value,
      "fax": fax.value,
      "address": address.value,
      "contact": '',
      "invoicesCount": '',
      "amount": '',
      "address2": '',
      "address3": '',
      "clientId": '',
      "clientEmail": '',
      "clientPhone": '',
      "clientMobile": '',
      "clientFax": '',
      "clientContact": '',
      "clientAddress": '',
      "clientAddress2": '',
      "clientAddress3": '',
    };
    APIResponse? resp;
    if (client == null) {
      resp = await ApiClient.post(
          request: req, endPoint: "/clients", fromJson: Client.fromMap);
      Navigator.pop(context, resp['data']);
      return resp;
    } else {
      req["id"] = client!.clientId;
      resp = await ApiClient.patch<Client>(
          request: req,
          endPoint: "/clients/" + client!.clientId.toString(),
          fromJson: Client.fromMap);
      Navigator.pop(context, resp['data']);
    }
    // notifyListeners();
  }

  moreDatails() {
    isVisible.value = !isVisible.value;
  }
}
