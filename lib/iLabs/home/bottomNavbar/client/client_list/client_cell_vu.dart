import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
import 'client_list_controller.dart';
// import '../../../../../../models/item/model.dart';
import '../../../../models/item/model.dart';
import '../add_client/add_client_vu.dart';

Widget itemCell(BuildContext context, ClientListController controller,
    GestureTapCallback onTap, Client item, int index) {
  return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(top: 10.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: const Color.fromRGBO(0x29, 0x00, 0x00, 1.0)
                    .withOpacity(0.2),
                spreadRadius: 0.1,
                blurRadius: 3,
                offset: const Offset(2, 0),
              ),
            ],
          ),
          child: IntrinsicHeight(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(11.5, 10.0, 12.0, 10.0),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item.name ?? "",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          // #193145
                          style: GoogleFonts.poppins(
                            fontSize: 16.0,
                            color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          item.email.toString(),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: GoogleFonts.poppins(
                              fontSize: 12.0,
                              color:
                                  const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ),
                  const VerticalDivider(
                    thickness: 0.8,
                    color: Color.fromRGBO(0xd5, 0xd5, 0xd5, 1.0),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          'Rs ${item.email.toString()}',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: GoogleFonts.poppins(
                            fontSize: 16.0,
                            color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          item.phone ?? "",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: GoogleFonts.poppins(
                              fontSize: 12.0,
                              color:
                                  const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                              fontWeight: FontWeight.w400),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ));
}
