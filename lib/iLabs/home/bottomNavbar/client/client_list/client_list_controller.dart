import 'dart:async';

import 'package:cached_map/cached_map.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:stacked/stacked.dart';
// import '../../../../../../models/client/model.dart';
import '../../../../models/client/model.dart';
// import '../../../../../../services/api_client.dart' as client;
import '../../../../services/api_client.dart' as client;

class ClientListController extends GetxController {
  ScrollController listScrollController = ScrollController();
  RxList<Client> clientList = <Client>[].obs;
  RxList<Client> tempList = <Client>[].obs;
  RxInt ccc = 0.obs;
  RxInt itemsUpdated = 0.obs;
  RxBool isLoading = false.obs;
  RxBool loadingMore = false.obs;
  int pages = 0;
  int count = 30;
  int currentPage = 0;
  late StreamSubscription connectivitySubscription;

  ClientListController() {
    onLoading();
    // enablePagination();
  }

  runFilter(String value) {
    if (value.isEmpty) {
      clientList.value = tempList.value;
    }
    else {
      clientList.value = clientList.value
          .where((p0) =>
              p0.name.toString().toLowerCase().contains(value.toLowerCase()))
          .toList();
    }
  }
  onTapSerarchField(String val){
    ///makes copy of our list when we tap on empty search field (first time tap on search field)
    if(val.isEmpty){
      tempList.addAll(clientList);
    }
  }

  enablePagination() {
    listScrollController.addListener(() {
      if (listScrollController.offset >=
              listScrollController.position.maxScrollExtent - 300 &&
          !loadingMore.value) {
        // print("currentPage: ${currentPage + 1}");
        loadMore(currentPage + 1);
      }
    });
  }

  onLoading() async {
    isLoading.value = true;
    await getFromCache();
    await checkConnectivityAndLoadClients();
    isLoading.value = false;
  }

  refreshData(String search) async {
    // print('function called  $search');
    client.APIResponse resp = await client.ApiClient.get<ClientList>(
        endPoint: "/clients",
        params: "/0/$count/id/DESC",
        fromJson: ClientList.fromMap);
    clientList.clear();
    clientList.addAll(resp["data"].data);
    pages = resp["data"].pages;
    count = int.parse(resp["data"].count);
    currentPage = 0;
    cacheCurrentList();
    debugPrint("Number of clients: ${clientList.length}");
    if (count > clientList.length) {
      refreshData('');
    }
  }

  onUpdate(Client client, int index) {
    clientList[index] = client;
    cacheCurrentList();
  }

  delete(int index) async {
    client.APIResponse resp = await client.ApiClient.delete<ClientList>(
        endPoint: "/items", params: "/${clientList[index].clientId}");

    clientList.removeAt(index);
    cacheCurrentList();
    // debugPrint("$clientList");
    // debugPrint("Delete Response: $resp");
  }

  loadMore(int pageNo) async {
    if (clientList.length < count) {
      loadingMore.value = true;
      // print('function called ');
      client.APIResponse resp = await client.ApiClient.get<ClientList>(
          endPoint: "/clients",
          params: "/$currentPage/10/id/DESC",
          fromJson: ClientList.fromMap);
      clientList = resp["data"].data;
      // print(clientList);
      clientList.addAll(resp["data"].data);
      pages = resp["data"].pages;
      count = resp["data"].count;
      currentPage++;
      loadingMore.value = false;
      cacheCurrentList();
      debugPrint("Number of Clients: ${clientList.length}");
    }
  }

  cacheCurrentList() {
    Mapped.saveFileDirectly(
        file:
            ClientList(currentPage: currentPage, data: clientList, pages: count)
                .toMap(),
        cachedFileName: "clients");
  }

  getFromCache() async {
    var json = await Mapped.loadFileDirectly(cachedFileName: "clients");
    if (json != null) {
      ClientList modal = ClientList.fromMap(json);
      clientList.clear();
      clientList.addAll(modal.data!);
      isLoading.value = false;
      currentPage = modal.currentPage ?? 0;
      count = modal.pages!; /// storing count in pages variable
    }
  }

  checkConnectivityAndLoadClients() {
    ///each time the connection is turned back on .. it will refresh the data itself tranparently.
    connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        refreshData('');
      }
    });
  }
}
