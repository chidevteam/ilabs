import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:ilabs/iLabs/Loadings/loading_pages.dart';
import 'package:ilabs/iLabs/models/client/model.dart';
import 'client_list_controller.dart';
import 'client_cell_vu.dart';
import '../add_client/add_client_vu.dart';
// import '../../../../../../models/item/model.dart';
// import '../../../../models/item/model.dart';

class InvoiceClientListScreen extends StatelessWidget {
  InvoiceClientListScreen({Key? key}) : super(key: key);
  ClientListController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          await controller.refreshData('');
        },
        child: Stack(children: [
          Column(
            children: [
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 15.0),
              //   child: TextField(
              //     onChanged: (value) {
              //       controller.runFilter(value);
              //     },
              //     decoration: InputDecoration(
              //       labelText: 'Search',
              //       suffixIcon: Icon(Icons.search),
              //     ),
              //   ),
              // ),
              Expanded(
                child: Obx(
                  () => ListView.builder(
                      controller: controller.listScrollController,
                      shrinkWrap: true,
                      itemCount: controller.clientList.length,
                      itemBuilder: (context, index) {
                        // debugPrint("Creating client at : $index");
                        Client item = controller.clientList[index];
                        controller.ccc.value++;
                        return Dismissible(
                            key: Key("${controller.ccc.value}"),
                            onDismissed: (direction) {
                              controller.delete(index);
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                      backgroundColor: Colors.green,
                                      duration: const Duration(
                                          seconds: 0, milliseconds: 750),
                                      content: Text('$item dismissed')));
                            },
                            background: Container(color: Colors.red),
                            child: Column(
                              children: [
                                itemCell(context, controller, () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            NewClientScreen(item),
                                      )).then((v) {
                                    // print("back from add update form");
                                    if (v != null) {
                                      // print('value returned ...............$v');
                                      // print('object');
                                      controller.clientList[index] = v;
                                      controller.tempList[index] = v;

                                      //print("done");
                                    }
                                  });
                                }, controller.clientList[index], index),
                                Obx(() {
                                  // print(controller.clientList.length);
                                  // print("<");
                                  // print((controller.count));
                                  return Visibility(
                                      visible:
                                          controller.clientList.length - 1 ==
                                                  index &&
                                              controller.clientList.length <
                                                  controller.count,
                                      child: const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Center(
                                          child: CircularProgressIndicator(),
                                        ),
                                      ));
                                })
                              ],
                            ));
                      }),
                ),
              )
            ],
          ),
          Obx(
            () => Positioned(
                child: controller.isLoading.value
                    ? const GettingRecords('Loading Items')
                    : const Text('')),
          )
        ]),
      ),
      floatingActionButton:
          floatAddItem(context, controller, '/newClientScreen'),
    );
  }

  FloatingActionButton floatAddItem(
      BuildContext context, ClientListController controller, String route) {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => NewClientScreen(null),
            )).then((clientItem) {
          if (clientItem != null) {
            controller.clientList.insert(0, clientItem);
          }
        });
      },
      backgroundColor:
          const Color.fromRGBO(0xff, 0x78, 0x48, 1.0).withOpacity(0.8),
      elevation: 0.0,
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
      heroTag: route,
    );
  }
}
