import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/models/business/model.dart';
import '../../services/api_client.dart';

class OnBoardingController extends GetxController {
  final formKey = GlobalKey<FormState>();
  BusinessDetail businessDetails = BusinessDetail();

  onSaveItem(BuildContext context) async {
    formKey.currentState?.save();

    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    //

    Map<String, dynamic> req = businessDetails.toMap();
    //   "id": '0',
    //   "business_name": businessDetails.businessName,
    //   "business_email": businessDetails.businessEmail,
    //   "business_phone": businessDetails.businessPhone,
    //   "business_website" : ' ',
    //   "business_mobile" : ' ',
    //   "business_number" : ' ',
    //   "business_owner_name" : ' ',
    //   "business_address" : ' ',
    //   "business_address2" : ' ',
    //   "business_address3" : ' ',
    //   "business_logo_url" : ' ',

    // };
    APIResponse resp = await ApiClient.post(
        request: req, endPoint: "/business", fromJson: null);
    debugPrint("$resp");

    //notifyListeners();
    Navigator.pushNamed(context, '/myHomePage');
  }

  // initialize() async{
  //   await guestSignUp();
  // }
}
