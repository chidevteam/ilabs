import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/home/onboarding/onboarding_controller.dart';
import 'package:get/get.dart';
import '../../widgets/input/chi_text_field.dart';

import '../../widgets/input/chi_textfield.dart';
import 'onboarding_controller.dart';

class OnBoardingScreen extends StatelessWidget {
  OnBoardingScreen({Key? key}) : super(key: key);
  OnBoardingController controller = Get.put(OnBoardingController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: controller.formKey,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 10.0, 18.0, 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                bussinessInformationTxt(),
                informationBelowTxt(),
                headerImg(),
                const SizedBox(
                  height: 24.0,
                ),
                CHITextField(
                    heading: 'Business Name',
                    hintText: 'sss',
                    func: (value) {
                      //print(value);
                      controller.businessDetails.businessName = value;
                      //controller.notifyListeners();
                    },
                    validator: null,
                    initialValue: controller.businessDetails.businessName),
                CHITextField(
                    heading: 'Email ',
                    hintText: 'Type Email ',
                    validator: null,
                    func: (value) {
                      controller.businessDetails.businessEmail = value;
                      //viewModel.notifyListeners();
                    },
                    initialValue: controller.businessDetails.businessEmail),
                CHITextField(
                    heading: 'Business Contact',
                    hintText: '0900786901',
                    func: (value) {
                      controller.businessDetails.businessPhone = value;
                      //viewModel.notifyListeners();
                    },
                    initialValue: controller.businessDetails.businessName),
                chiSaveButton("Continue", () {
                  controller.onSaveItem(
                    context,
                  );
                  // Navigator.pop(context, viewModel.businessDetails);
                  //print(viewModel.businessDetails?.businessName);

                  //print(viewModel.businessDetails?.businessEmail);
                  //print(viewModel.businessDetails?.businessPhone);
                }),
                askMeTxtButton()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Padding informationBelowTxt() {
  return Padding(
    padding: const EdgeInsets.only(top: 6.0, left: 12),
    child: Text(
      'Enter Information Below to Complete Account',
      style: TextStyle(
          color: Colors.grey.shade700,
          fontSize: 13.0,
          fontWeight: FontWeight.w400),
    ),
  );
}

Widget bussinessInformationTxt() {
  return const Padding(
    padding: EdgeInsets.only(top: 60.0, left: 12, bottom: 10.0),
    child: Text(
      'Business information',
      style: TextStyle(
          color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
    ),
  );
}

Widget askMeTxtButton() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      TextButton(
          onPressed: () {},
          child: Text(
            'Ask me Letter',
            style: TextStyle(color: Colors.orange.shade800),
          )),
    ],
  );
}

Widget headerImg() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      ClipRRect(
        child: Padding(
          padding: const EdgeInsets.only(top: 32.0),
          child: Container(
            width: 120.0,
            height: 120.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey.shade200,
                border: Border.all(width: 3, color: Colors.grey.shade400)),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Image.network(
                'https://www.reciphergroup.com/assets-new/images/automated-invoicing.png',
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    ],
  );
}
