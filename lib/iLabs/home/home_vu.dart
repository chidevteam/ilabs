import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/client/client_list/client_list_controller.dart';
// import 'package:mov';
import 'package:move_to_background/move_to_background.dart';
import '../language/generated/locale_keys.g.dart';
import 'package:ilabs/iLabs/home/bottomNavbar/item/item_list/item_list_controller.dart';
// import 'end_drawer/end_drawer_vu.dart';
import '../widgets/input/chi_text_field.dart';
import 'action_drawer/action_drawer_vu.dart';
import 'bottomNavbar/invoice/invoice_page_vu.dart';
import 'bottomNavbar/estimate/estimate_page_vu.dart';
import 'bottomNavbar/client/client_list/client_list_vu.dart';
import 'bottomNavbar/item/item_list/item_list_vu.dart';
// import 'bottomNavbar/reports/reports_vu.dart';
import 'bottomNavbar/tab_bar_items.dart';
import 'home_controller.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);
  HomeController controller = Get.put(HomeController());
  ItemListController itemListController = Get.put(ItemListController());
  ClientListController clientListController = Get.put(ClientListController());

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetOptions = <Widget>[
      InvoicePageView(controller),
      EstimatePageView(controller),
      // ReportsPageVU(viewModel),
      InvoiceClientListScreen(),
      InvoiceItemListScreen()
    ];

    return DefaultTabController(
      length: 3,
      child: WillPopScope(
        onWillPop: () async {
          openDialog(context);
          return false;
        },
        child: Obx(() => Scaffold(
              appBar: AppBar(
                leading: Builder(
                  builder: (BuildContext context) {
                    return IconButton(
                      icon: SvgPicture.asset(
                        'iLabsSreens/home/side menu.svg',
                        color: Colors.black54,
                      ),
                      onPressed: () {
                        Scaffold.of(context).openDrawer();
                      },
                      tooltip: MaterialLocalizations.of(context)
                          .openAppDrawerTooltip,
                    );
                  },
                ),
                elevation: 1.0,
                shadowColor: const Color.fromRGBO(0x29, 0x00, 0x00, 1.0)
                    .withOpacity(0.3),
                title: Obx(
                  () => controller.searchActive.value
                      ? searchField()
                      : Text(
                          controller.tabBarTitle.value,
                          style: const TextStyle(color: Colors.black54),
                        ),
                ),
                backgroundColor: Colors.white,
                bottom: controller.tabBars == null
                    ? null
                    : tabBarItems(controller.tabBars!),
                actions: controller.searchActive.value
                    ? searchActiveActions(controller)
                    : normalActions(controller),
              ),
              drawer: MainDrawer(),
              // endDrawer: endActionDrawer(context, viewModel),
              bottomNavigationBar: bottomNavigationBar(controller),
              body: IndexedStack(
                index: controller.index.value,
                children: widgetOptions,
              ),
            )),
      ),
    );
  }

  List<Widget> searchActiveActions(var controller) {
    return <Widget>[
      Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {
              controller.searchActive.value = false;
              // controller.notifyListeners();
              debugPrint('Search X pressed');
            },
            child: const Icon(Icons.close),
          )),
    ];
  }

  List<Widget> normalActions(var viewModel) {
    return <Widget>[
      Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {
              viewModel.searchActive.value = true;
              debugPrint('Search pressed');
              // viewModel.notifyListeners();
            },
            child: SvgPicture.asset(
              'iLabsSreens/home/search.svg',
              color: Colors.black54,
            ),
          )),
      // Padding(
      //     padding: const EdgeInsets.only(right: 20.0),
      //     child: GestureDetector(
      //       onTap: () {
      //         debugPrint('More pressed');
      //       },
      //       child: const Icon(Icons.more_vert),
      //     )),
    ];
  }

  Widget bottomNavigationBar(var controller) {
    return Obx(() => BottomNavigationBar(
          selectedFontSize: 10.0,
          unselectedFontSize: 10.0,
          type: BottomNavigationBarType.fixed,

          onTap: controller.onItemTapped,
          selectedItemColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
          unselectedItemColor: Colors.grey,
          unselectedLabelStyle: const TextStyle(color: Colors.grey),
          showUnselectedLabels: true,

          currentIndex: controller
              .index.value, // this will be set when a new tab is tapped
          items: [
            bottomNavigationItem(
                tr(LocaleKeys.main_home_view_invoices), "Invoice"),
            bottomNavigationItem(
                tr(LocaleKeys.main_home_view_estimates), "Estimates"),
            bottomNavigationItem(
                tr(LocaleKeys.main_home_view_clients), "Clients"),
            bottomNavigationItem(tr(LocaleKeys.main_home_view_items), "Items"),
          ],
        ));
  }

  BottomNavigationBarItem bottomNavigationItem(String lb, String imgName) {
    return BottomNavigationBarItem(
      icon: SvgPicture.asset("iLabsSreens/home/$imgName.svg"),
      activeIcon: SvgPicture.asset("iLabsSreens/home/$imgName active.svg"),
      label: lb,
    );
  }

  Future openDialog(BuildContext context) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: const Text('Exit App'),
            content: const Text('Are you sure you want to exit?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancel')),
              TextButton(
                  onPressed: () {
                    // SystemNavigator.pop();
                    Navigator.pop(context);
                    MoveToBackground.moveTaskToBack();
                  },
                  child: const Text('Exit'))
            ],
          ));
}
