import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ilabs/iLabs/home/action_drawer/action_drawer_vm.dart';
// import '../../Utils/utils.dart';
import '../../language/generated/locale_keys.g.dart';
import '../home_controller.dart';
import 'leading_drawer/d000_business_details/business_details_form_vu.dart';
import 'package:get/get.dart';

Widget drawerCell(
  BuildContext context,
  String lable,
  String icon,
  onTap,
) {
  return Column(
    children: [
      InkWell(
        onTap: () {
          onTap(context);
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(18, 10, 0, 10),
          child: Row(
            children: [
              SizedBox(
                width: 16,
                height: 16,
                child: SvgPicture.asset(icon, color: const Color(0xFF4d4d4d)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  lable,
                  style: const TextStyle(
                      color: Color(0xff4d4d4d),
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ),
      ),
      const Divider(),
    ],
  );
}

Widget drawerHeader(context, MainDrawerVM mainDrawerVM) {
  HomeController homeController = Get.find<HomeController>();
  String email = homeController.businessDetails.businessEmail!;

  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BusinessDetailsFormVU(
                    homeController.businessDetails,
                    endPoint: "/business",
                  ))).then((value) {
        if (value != null) {
          homeController.businessDetails = value;
          mainDrawerVM.notifyListeners();
        }
        // Navigator.pop(context);
      });
    },
    child: DrawerHeader(
      padding: EdgeInsets.zero,
      decoration: const BoxDecoration(color: Color(0xfff7f7f7)),
      child: Padding(
        padding: const EdgeInsets.only(left: 12.0, top: 14.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 75.0,
              width: 75.0,
              child: Row(children: [
                IconButton(
                    onPressed: () {},
                    icon: Image.file(
                      File(homeController.businessDetails.businessLogoFile!),
                      // Image.network(
                      // SERVER_URL2 +
                      //     homeController.businessDetails.businessLogoUrl!,
                      height: 60,
                      width: 60,
                    )),
                // editButton()
              ]),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 3),
              child: Text(
                '${homeController.businessDetails == null ? '' : homeController.businessDetails.businessName}',
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                ),
              ),
            ),
            Text(email,
                style: const TextStyle(color: Color(0xff999999), fontSize: 12)),
          ],
        ),
      ),
    ),
  );
}

Widget editButton() {
  return Container(
    padding: EdgeInsets.zero,
    child: Row(
      children: [
        const Padding(
          padding: EdgeInsets.all(4.0),
          child: Icon(
            Icons.edit,
            color: Color.fromRGBO(0x77, 0x77, 0x77, 1.0),
            size: 18.0,
          ),
        ),
        Text(
          tr(LocaleKeys.action_drawer_view_drawer_header_edit),
          style: const TextStyle(
              color: Color.fromRGBO(0x77, 0x77, 0x77, 1.0),
              fontSize: 13.0,
              fontWeight: FontWeight.w400),
        )
      ],
    ),
    height: 35,
    width: 60,
    decoration: const BoxDecoration(
      color: Colors.white,
      shape: BoxShape.rectangle,
      borderRadius: BorderRadius.horizontal(
        left: Radius.circular(50.0),
      ),
    ),
  );
}
