import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../../models/report_client/model.dart';
import '../../../../../services/api_client.dart';

class ReportClientListVM extends BaseViewModel {
  ReportClientList reportClientList = ReportClientList("1999", -1, -1, []);
  int year = 2021;

  ReportClientListVM(int mYear) {
    year = mYear;
  }

  onRefreshCalled(BuildContext context) {
    refreshData(context);
  }

  refreshData(context,
      {bool isPaginated = false, String searchText = ""}) async {
    // print(".....................in api fuc");

    APIResponse resp = await ApiClient.get<ReportClientList>(
        endPoint: "/reports",
        params: "/clients/$year",
        fromJson: ReportClientList.fromMap);
    reportClientList = resp["data"];

    print(resp);

    notifyListeners();
  }
}
