 import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../home_controller.dart';
import 'report_client_vm.dart';

Widget reportClientlistSummary(ReportClientListVM viewModel) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          // #26000000
          BoxShadow(
            color: const Color.fromRGBO(0x26, 0x00, 0x00, 1.0).withOpacity(0.1),
            spreadRadius: 6,
            blurRadius: 3,
            offset: const Offset(0, 4),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
        child: Row(
          children: [
            Expanded(
              flex: 6,
              child: Text(
                viewModel.reportClientList.year.toString(),
                style: GoogleFonts.poppins(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Text(
                viewModel.reportClientList.invoices.toString(),
                style: GoogleFonts.poppins(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(
                Get.find<HomeController>().userDefault.rXregionCurrency.value +
                    viewModel.reportClientList.amount.toString(),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                textAlign: TextAlign.right,
                style: GoogleFonts.poppins(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }