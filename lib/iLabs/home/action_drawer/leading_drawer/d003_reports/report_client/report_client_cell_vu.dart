 import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../models/report_client/model.dart';
import '../../../../home_controller.dart';

Widget reportClientListCellVU(ReportClient item) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
          child: Row(
            children: [
              Expanded(
                flex: 5,
                child: Text(
                  item.name!,
                  style: GoogleFonts.poppins(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w400,
                    color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  item.invoices.toString(),
                  style: GoogleFonts.poppins(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w400,
                    color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                  ),
                ),
              ),
              Expanded(
                  flex: 2,
                  child: item.amount == null
                      ? Text(
                          Get.find<HomeController>()
                                  .userDefault
                                  .rXregionCurrency
                                  .value +
                              '0',
                          textAlign: TextAlign.right,
                          style: GoogleFonts.poppins(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                          ),
                        )
                      : Text(
                          Get.find<HomeController>()
                                  .userDefault
                                  .rXregionCurrency
                                  .value +
                              item.amount.toString(),
                          textAlign: TextAlign.right,
                          style: GoogleFonts.poppins(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                          ),
                        )),
            ],
          ),
        ),
        const Divider(
          color: Color.fromRGBO(0xe5, 0xe5, 0xe5, 1.0),
        )
      ],
    );
  }