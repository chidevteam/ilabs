import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'report_client_cell_vu.dart';
import 'report_client_header.dart';
import 'report_client_summary.dart';
import 'report_client_vm.dart';

// ignore: must_be_immutable
class ReportClientListScreen
    extends ViewModelBuilderWidget<ReportClientListVM> {
  ReportClientListScreen({Key? key, required this.year}) : super(key: key);
  int year = 2021;
  @override
  Widget builder(
      BuildContext context, ReportClientListVM viewModel, Widget? child) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          reportClientHeader(),
          reportClientList(viewModel),
          reportClientlistSummary(viewModel),
        ],
      ),
    );
  }

  Widget reportClientList(ReportClientListVM viewModel) {
    return Expanded(
      flex: 1,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: viewModel.reportClientList.data!.length,
          itemBuilder: (context, index) {
            return reportClientListCellVU(
                viewModel.reportClientList.data![index]);
          }),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    var vm = ReportClientListVM(year);
    vm.refreshData(context);
    return vm;
  }
}
