import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'report_vm.dart';
import 'reports_vu.dart';

Future<dynamic> filterBottomSheet(context, ReportsPageVM viewModel) {
  return showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return FractionallySizedBox(
              heightFactor: 0.2,
              child: Column(
                children: [
                  Row(
                    children: [
                      const Expanded(flex: 2, child: Text('')),
                      Text("Filter",
                          textAlign: TextAlign.right,
                          style: GoogleFonts.poppins(
                            fontSize: 14.0,
                            color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                          )),
                      const Expanded(child: Text('')),
                      TextButton(
                        onPressed: () {
                          setState(() {
                            Navigator.pop(context, viewModel.onApply());
                          });
                        },
                        child: Text("Apply",
                            style: GoogleFonts.poppins(
                              fontSize: 14.0,
                              color:
                                  const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
                            )),
                      ),
                    ],
                  ),
                  const Divider(
                    color: Color.fromRGBO(0xe5, 0xe5, 0xe5, 1.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
                    child: Row(
                      children: [
                        Text("Select Year",
                            style: GoogleFonts.poppins(
                              fontSize: 14.0,
                              color:
                                  const Color.fromRGBO(0x4d, 0x4d, 0x4d, 1.0),
                            )),
                        const Spacer(),
                        InkWell(
                          onTap: () {
                            reportsYearPicker(context, viewModel);
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.60,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(
                                color:
                                    const Color.fromRGBO(0x4d, 0x4d, 0x4d, 1.0),
                                width: 1,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                              child: Row(
                                children: [
                                  Text(viewModel.year.toString(),
                                      style: GoogleFonts.poppins(
                                          color: const Color.fromRGBO(
                                              0x37, 0x44, 0x54, 1.0),
                                          fontSize: 14)),
                                  const Spacer(),
                                  const RotatedBox(
                                    quarterTurns: 1,
                                    child: Icon(
                                      Icons.arrow_forward_ios_outlined,
                                      color:
                                          Color.fromRGBO(0x4d, 0x4d, 0x4d, 1.0),
                                      size: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // Container(
                        //   width: 30,
                        //   height: 30,
                        //   decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(50.0),
                        //     color: Colors.white,
                        //     boxShadow: [
                        //       BoxShadow(
                        //         color: Colors.grey.withOpacity(0.1),
                        //       ),
                        //     ],
                        //   ),
                        //   child: IconButton(
                        //       onPressed: () {
                        //         setState(() {
                        //           viewModel.onYearForword();
                        //         });
                        //       },
                        //       icon: const Icon(Icons.keyboard_arrow_right),
                        //       color:
                        //           const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                        //       iconSize: 13),
                        // ),
                      ],
                    ),
                  )
                ],
              ));
        });
      });
}
