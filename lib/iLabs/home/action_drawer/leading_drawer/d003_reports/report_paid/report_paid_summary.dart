import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../home/home_controller.dart';
import 'report_paid_vm.dart';

Widget reportPaidSummary(ReportPaidListVM viewModel, HomeController homeCtrl) {
  return Container(
    decoration: BoxDecoration(
      color: Colors.white,
      boxShadow: [
        // #26000000
        BoxShadow(
          color: const Color.fromRGBO(0x26, 0x00, 0x00, 1.0).withOpacity(0.1),
          spreadRadius: 6,
          blurRadius: 3,
          offset: const Offset(0, 4),
        ),
      ],
    ),
    child: Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
      child: Row(
        children: [
          Expanded(
            flex: 6,
            child: Text(
              viewModel.reportPaidList.year.toString(),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: GoogleFonts.poppins(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Text(
              viewModel.reportPaidList.clients.toString(),
              style: GoogleFonts.poppins(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              viewModel.reportPaidList.invoices.toString(),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: GoogleFonts.poppins(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              Get.find<HomeController>().userDefault.rXregionCurrency.value +
                  viewModel.reportPaidList.amount.toString(),
              textAlign: TextAlign.right,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: GoogleFonts.poppins(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
