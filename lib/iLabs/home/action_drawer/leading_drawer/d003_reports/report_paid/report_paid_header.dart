import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:easy_localization/easy_localization.dart';
import '../../../../../language/generated/locale_keys.g.dart';

Container reportPaidHeader() {
  return Container(
    color: const Color.fromRGBO(0xf4, 0xf4, 0xf4, 1.0),
    child: Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 11.0),
      child: Row(
        children: [
          Expanded(
              flex: 4,
              child: Text(
                tr(LocaleKeys.action_drawer_view_reports_screen_tab_2_month),
                style: GoogleFonts.poppins(
                    color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500),
              )),
          Expanded(
            flex: 4,
            child: Text(
              tr(LocaleKeys.action_drawer_view_reports_screen_tab_2_clients),
              style: GoogleFonts.poppins(
                  color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Expanded(
            flex: 4,
            child: Text(
              tr(LocaleKeys.action_drawer_view_reports_screen_tab_2_invoices),
              style: GoogleFonts.poppins(
                  color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              tr(LocaleKeys.action_drawer_view_reports_screen_tab_2_paid),
              textAlign: TextAlign.right,
              style: GoogleFonts.poppins(
                  color: const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    ),
  );
}
