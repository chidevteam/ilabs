import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../../../home/home_controller.dart';

import '../../../../../models/report_paid/model.dart';

Widget reportPaidListCellVU(ReportPaid item) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 11.0, 26.0, 11.0),
        child: Row(
          children: [
            Expanded(
                flex: 6,
                child: Text(item.months!,
                    style: GoogleFonts.poppins(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                    ))),
            Expanded(
              flex: 5,
              child: Text(
                item.clients!,
                style: GoogleFonts.poppins(
                  fontSize: 12.0,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Text(
                item.invoices.toString(),
                style: GoogleFonts.poppins(
                  fontSize: 12.0,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Text(
                Get.find<HomeController>().userDefault.rXregionCurrency.value +
                    item.amount!,
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: GoogleFonts.poppins(
                  fontSize: 12.0,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                ),
              ),
            ),
          ],
        ),
      ),
      const Divider(color: Color.fromRGBO(0xe5, 0xe5, 0xe5, 1.0))
    ],
  );
}
