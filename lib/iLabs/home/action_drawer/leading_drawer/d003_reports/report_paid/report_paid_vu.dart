import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:stacked/stacked.dart';
import '../../../../home_controller.dart';
import 'report_paid_cell_vu.dart';
import 'report_paid_header.dart';
import 'report_paid_summary.dart';
import 'report_paid_vm.dart';

class ReportPaidListScreen extends ViewModelBuilderWidget<ReportPaidListVM> {
  ReportPaidListScreen({Key? key, required this.year}) : super(key: key);
  HomeController homeCtrl = Get.find<HomeController>();

  int year = 2021;
  @override
  Widget builder(
      BuildContext context, ReportPaidListVM viewModel, Widget? child) {
    return Scaffold(
      body: Column(
        children: [
          reportPaidHeader(),
          reportPaidList(viewModel),
          reportPaidSummary(viewModel, homeCtrl),
        ],
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    var vm = ReportPaidListVM(year);
    vm.refreshData(context);
    return vm;
  }
}

reportPaidList(ReportPaidListVM viewModel) {
  return Expanded(
    flex: 1,
    child: ListView.builder(
        shrinkWrap: true,
        itemCount: viewModel.reportPaidList.data!.length,
        itemBuilder: (context, index) {
          return reportPaidListCellVU(viewModel.reportPaidList.data![index]);
        }),
  );
}
