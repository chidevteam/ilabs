import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../../models/report_paid/model.dart';
import '../../../../../services/api_client.dart';

class ReportPaidListVM extends BaseViewModel {
  ReportPaidList reportPaidList = ReportPaidList("1999", -1, -1, -1, []);

  int year = 2021;

  ReportPaidListVM(int mYear) {
    year = mYear;
  }

  onRefreshCalled(BuildContext context) {
    refreshData(context);
  }

  refreshData(context) async {
    print('===========================year======== $year');

    APIResponse resp = await ApiClient.get<ReportPaidList>(
        endPoint: "/reports",
        params: "/paid/$year",
        fromJson: ReportPaidList.fromMap);

    reportPaidList = resp["data"];

    notifyListeners();
  }
}
