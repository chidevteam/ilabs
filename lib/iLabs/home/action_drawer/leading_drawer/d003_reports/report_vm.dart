import 'package:stacked/stacked.dart';

class ReportsPageVM extends BaseViewModel {
  int year = DateTime.now().year;
  DateTime selectedYear = DateTime.now();

  onYearBack() {
    // if (year <= 2000) {
    //   return;
    // } else {
    year--;
    // }

    notifyListeners();
  }

  onYearForword() {
    year++;

    notifyListeners();
  }

  onApply() {
    // notifyListeners();
  }
}
