import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
// import 'package:flutter_svg/svg.dart';
import '../../../../language/generated/locale_keys.g.dart';

import 'package:stacked/stacked.dart';
// import '../../../home/home_vm.dart';
import 'report_client/report_client_vu.dart';
import 'filter_bottom_sheet.dart';
import 'report_paid/report_paid_vu.dart';
import 'report_vm.dart';

class ReportsPageVU extends ViewModelBuilderWidget<ReportsPageVM> {
  const ReportsPageVU({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, ReportsPageVM viewModel, Widget? child) {
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.2,
            shadowColor: Colors.grey.withOpacity(0.8),
            backgroundColor: Colors.white,
            title: Center(
              child: Text(
                tr(LocaleKeys.action_drawer_view_reports_screen_app_bar_title),
                style: GoogleFonts.poppins(
                    color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600),
              ),
            ),
            actions: [
              IconButton(
                icon: SvgPicture.asset(
                  'iLabsSreens/home/side menu.svg',
                  color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
                ),
                onPressed: () {
                  filterBottomSheet(context, viewModel);
                },
              )
            ],
          ),
          body: Column(
            children: [
              reportsTapBar(),
              Expanded(
                child: TabBarView(
                  children: [
                    ReportClientListScreen(
                      year: viewModel.year,
                    ),
                    ReportPaidListScreen(
                      year: viewModel.year,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  ReportsPageVM viewModelBuilder(BuildContext context) {
    return ReportsPageVM();
  }
}

// *****ReportTabBar*****

Widget reportsTapBar() {
  return Padding(
    padding: const EdgeInsets.only(bottom: 4),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.transparent.withOpacity(0.1),
            spreadRadius: 0.9,
            blurRadius: 8,
            offset: const Offset(0, 1),
          ),
        ],
      ),
      child: TabBar(
        unselectedLabelColor: const Color.fromRGBO(0xac, 0xac, 0xac, 1.0),
        indicatorWeight: 1,
        indicatorColor: const Color.fromRGBO(0xff, 0x78, 0x48, 1.0),
        labelColor: const Color.fromRGBO(0xff, 0x78, 0x48, 1.0),
        labelPadding: EdgeInsets.zero,
        tabs: [
          clientsTab(),
          invoicesTab(),
        ],
      ),
    ),
  );
}

Tab invoicesTab() {
  return Tab(
    child: Text(tr(LocaleKeys.action_drawer_view_reports_screen_tab_2_title),
        style: GoogleFonts.poppins(
          fontSize: 12.0,
        )),
  );
}

Widget clientsTab() {
  return Tab(
    child: Container(
      width: double.infinity,
      decoration: const BoxDecoration(
          border: Border(
        right: BorderSide(
          width: 1,
          color: Color.fromRGBO(0xe5, 0xe5, 0xe5, 1.0),
        ),
      )),
      child: Padding(
        padding: const EdgeInsets.only(left: 66),
        child:
            Text(tr(LocaleKeys.action_drawer_view_reports_screen_tab_1_title),
                style: GoogleFonts.poppins(
                  fontSize: 12.0,
                )),
      ),
    ),
  );
}

Future<dynamic> reportsYearPicker(
    BuildContext context, ReportsPageVM viewModel) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Select Year",
            style: GoogleFonts.poppins(
              fontSize: 14.0,
              color: const Color.fromRGBO(0x4d, 0x4d, 0x4d, 1.0),
            )),
        contentPadding: const EdgeInsets.all(10),
        content: Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width * 0.60,
          height: 240.0,
          child: YearPicker(
            firstDate: DateTime(
              DateTime.now().year - 22,
              1,
            ),
            lastDate: DateTime(DateTime.now().year + 100, 1),
            initialDate: DateTime.now(),
            selectedDate: viewModel.selectedYear,
            onChanged: (DateTime dateTime) {
              viewModel.year = dateTime.year;
              viewModel.selectedYear = dateTime;
              viewModel.notifyListeners();
              Navigator.pop(context);
            },
          ),
        ),
      );
    },
  );
}
