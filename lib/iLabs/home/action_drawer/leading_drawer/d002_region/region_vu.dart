import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../language/generated/locale_keys.g.dart';

import '../../../../widgets/input/chi_text_field.dart';
import '../../../../widgets/input/drop_down_bottom_sheet.dart';
import 'region_vm.dart';

class RegionVU extends ViewModelBuilderWidget<RegionVM> {
  const RegionVU({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, RegionVM viewModel, Widget? child) {
    return Scaffold(
      appBar: chiAppBar(
        tr(LocaleKeys.action_drawer_view_region_screen_app_bar_title),
        context,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(16.0, 14.0, 16.0, 16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              dropDownBottomSheet(context,
                  selectedIndex: viewModel.regCont.languageIndex,
                  items: viewModel.regCont.languageOptions,
                  title: tr(LocaleKeys
                      .action_drawer_view_region_screen_set_language_heading),
                  onTap: (index) {
                viewModel.updateLanguageOption(index);
                Locale locale = context.supportedLocales[index];
                context.setLocale(locale);
                Navigator.pop(context);
              }),
              const SizedBox(height: 12.0),
              dropDownBottomSheet(context,
                  selectedIndex: viewModel.regCont.currencyIndex,
                  items:
                      viewModel.regCont.currency.map((e) => e.name!).toList(),
                  title: tr(LocaleKeys
                      .action_drawer_view_region_screen_currency_type_heading),
                  onTap: (index) {
                viewModel.updateCurrencyOptions(index);
                Navigator.pop(context);
              }),
              const SizedBox(height: 12.0),
              dropDownBottomSheet(context,
                  selectedIndex: viewModel.regCont.dateFormatIndex,
                  items:
                      viewModel.regCont.dateFormat.map((e) => e.name!).toList(),
                  title: tr(LocaleKeys
                      .action_drawer_view_region_screen_date_formate_heading),
                  onTap: (index) {
                viewModel.updateDateFormatOptions(index);
                Navigator.pop(context);
              }),
              chiSaveButton(
                  tr(LocaleKeys.action_drawer_view_region_screen_save_button),
                  () {
                viewModel.onSave(context);
                Navigator.pop(context);
              })
            ],
          ),
        ),
      ),
    );
  }

  @override
  RegionVM viewModelBuilder(BuildContext context) {
    return RegionVM();
  }
}
