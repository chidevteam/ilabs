import 'package:stacked/stacked.dart';
import 'package:get/get.dart';
import '../../../home_controller.dart';
import '../../../region_controller.dart';
import '../../../../services/api_client.dart';

class RegionVM extends BaseViewModel {
  HomeController homeController = Get.find<HomeController>();
  late RegionController regCont;

  RegionVM() {
    regCont = homeController.regionController;
  }

  updateLanguageOption(int index) {
    regCont.languageIndex = index;
    notifyListeners();
  }

  updateCurrencyOptions(int index) {
    regCont.currencyIndex = index;
    notifyListeners();
  }

  updateDateFormatOptions(int index) {
    regCont.dateFormatIndex = index;
    notifyListeners();
  }

  onSave(context) async {
    Map<String, dynamic> req = {
      "region_locale": regCont.languageOptions[regCont.languageIndex],
      "region_date_format": regCont.dateFormat[regCont.dateFormatIndex].format,
      "region_currency": regCont.currency[regCont.currencyIndex].currency
    };

    await ApiClient.post(
        request: req, endPoint: "/userdefaults", fromJson: null);
    homeController.userDefault.regionLocale =
        regCont.languageOptions[regCont.languageIndex];
    homeController.userDefault.rXregionCurrency.value =
        regCont.currency[regCont.currencyIndex].currency!;
    homeController.userDefault.regionDateFormat =
        regCont.dateFormat[regCont.dateFormatIndex].format;
  }
}
