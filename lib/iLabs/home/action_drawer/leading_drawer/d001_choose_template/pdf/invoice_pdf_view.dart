import 'package:flutter/material.dart';
import 'package:pdf_viewer_plugin/pdf_viewer_plugin.dart';
import 'dart:io';
import '../choose_template_vm.dart';

Widget invoicePDFview(ChooseTemplateVM vm, File? file, bool? toggle) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
    child: SizedBox(
      width: double.infinity,
      child: Card(
          shape: RoundedRectangleBorder(
            side: const BorderSide(color: Colors.black12, width: 2),
            borderRadius: BorderRadius.circular(5),
          ),
          child: file != null
              ? PdfView(path: file.path)
              : const Text("Here !!!!")),
    ),
  );
}

//Previous implementation, do not delete - Ahmad

// import 'package:flutter/material.dart';
// import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
// import 'dart:io';
// import '../choose_template_vm.dart';

// Widget invoicePDFview(ChooseTemplateVM vm, File? file, bool? toggle) {
//   Widget pdfWidget;
//   if (vm.localPDF) {
//     pdfWidget = file != null ? SfPdfViewer.file(file) : const Text("Here !!!!");
//   } else {
//     String fileName = vm.pdfModel!.fileName!;
//     pdfWidget = SfPdfViewer.network('https://invoice___labs.co/api/$fileName');
//     // Widget pdf =
//     // pdfWidget = file != null ? SfPdfViewer.file(file) : const Text("Here !!!!");
//   }
//   return Padding(
//     padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
//     child: SizedBox(
//       width: double.infinity,
//       child: Card(
//           shape: RoundedRectangleBorder(
//             side: const BorderSide(color: Colors.black12, width: 2),
//             borderRadius: BorderRadius.circular(5),
//           ),
//           child: pdfWidget
//           ),
//     ),
//   );
// }