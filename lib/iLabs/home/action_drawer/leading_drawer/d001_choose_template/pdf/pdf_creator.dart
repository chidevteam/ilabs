import 'package:flutter/material.dart';
import 'dart:io';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:path_provider/path_provider.dart';
import '../../../../../models/choose_template/model.dart';
import '../../../../../material_helpers/color_helper.dart';

double drawText(PdfGraphics g, double offset, String text) {
  g.drawString(text, PdfStandardFont(PdfFontFamily.helvetica, 20),
      brush: PdfSolidBrush(PdfColor(0, 0, 0)),
      bounds: Rect.fromLTWH(0, offset, 500, 50));

  offset += 20;
  return offset;
}

PdfColor rgbFromArray(List arr) {
  return PdfColor(arr[2], arr[1], arr[0]);
}

Future<File> createPDF2(TemplateDataModel templateModel, String colorName,
    String primaryColor, String secondaryColor) async {
  //Create a new PDF document
  File? file;
  PdfDocument document = PdfDocument();

  PdfPage page = document.pages.add();
  PdfGraphics graphics = page.graphics;
  double y = 200;

  // print("Material Colors $primaryColor $secondaryColor");
  PdfColor pdfColorP = rgbFromArray(listFromHex(primaryColor));
  PdfColor pdfColorS = rgbFromArray(listFromHex(secondaryColor));

  // print(listPrimary);
  // print(listSecondary);

  //Add a new page and draw text
  graphics.drawString("Color + ${templateModel.colourId}",
      PdfStandardFont(PdfFontFamily.helvetica, 20),
      brush: PdfSolidBrush(PdfColor(0, 0, 0)),
      bounds: const Rect.fromLTWH(0, 0, 500, 50));

  y = drawText(graphics, y, "DATA:");

  y = drawText(graphics, y, "templateId:  ${templateModel.templateId!}");
  y = drawText(graphics, y, "logoType:  ${templateModel.logoType!}");
  y = drawText(graphics, y, "fontId:  ${templateModel.fontId!}");
  y = drawText(graphics, y, "colourId:  ${templateModel.colourId!}");
  y = drawText(graphics, y, "BussInfo:  ${templateModel.showBusinessInfo!}");

  graphics.drawRectangle(
      brush: PdfSolidBrush(pdfColorP),
      bounds: const Rect.fromLTWH(10, 60, 100, 100));

  graphics.drawRectangle(
      brush: PdfSolidBrush(pdfColorS),
      bounds: const Rect.fromLTWH(200, 60, 100, 100));

  //Save the document
  List<int> bytes = document.save();
  //File('HelloWorld.pdf').writeAsBytes(bytes);
  //Get external storage directory
  final directory = await getApplicationDocumentsDirectory();
  final path = directory.path;

//Create an empty file to write PDF data
  file = File('$path/Output.pdf');

//Write PDF data
  await file.writeAsBytes(bytes, flush: true);

//Open the PDF document in mobile
//OpenFile.open('$path/Output.pdf');

  //Dispose the document
  document.dispose();
  return file;
}
