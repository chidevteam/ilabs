import 'package:ilabs/iLabs/home/home_controller.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import '../../../app_constants.dart';
import 'choose_template_io.dart';
import 'selectors/font_model.dart';
import 'selectors/template_style_vm.dart';
import 'selectors/logo_model.dart';

import '../../../../material_helpers/material_helper.dart';
import '../../../../services/api_client.dart';
import '../../../../models/color/model.dart';
import '../../../../models/choose_template/model.dart';
import 'pdf/pdf_creator.dart';
// import 'package:http/http.dart' as http;
import 'package:get/get.dart';
// import '../../../../services/api_client.dart';

class ChooseTemplateVM extends BaseViewModel {
  PDFModel? pdfModel;
  ColorList? listOfColors;
  File? pdfFile;
  HomeController homeCont = Get.find<HomeController>();

  List<TemplateStyle> templateList = TemplateStyle.templateList;
  List<LogoStyle> logoStyleList = LogoStyle.logoStyleList;
  List<Font> fontsList = Font.fontsList;

  int colorIndex = 0; //Due to complex mapping of colorIndex to templateColor
  bool localPDF = true;

  ChooseTemplateVM(ChooseTemplateInputModel inputModel) {
    pdfModel = inputModel.pdfModel;
    listOfColors = inputModel.listOfColors;
    TemplateDataModel templateModel = pdfModel!.data!;
    for (int i = 0; i < listOfColors!.colorList!.length; i++) {
      if (listOfColors!.colorList![i].id == templateModel.colourId) {
        colorIndex = i;
      }
    }
    onTemplateStyleSelected(templateModel.templateId! - 1);
  }

  Color getColor(index) {
    String colorHex = listOfColors!.colorList![index].primaryCode;
    return colorFromHex(colorHex);
  }

  void onColorSelected(int index) {
    debugPrint("Color Index Selected: $index");
    colorIndex = index;
    TemplateDataModel templateModel = pdfModel!.data!;
    templateModel.colourId = listOfColors!.colorList![index].id;
    updatePDF();
  }

  void onTemplateStyleSelected(int index) {
    TemplateDataModel templateModel = pdfModel!.data!;
    templateModel.templateId = index + 1;
    updatePDF();
  }

  void onLogoStyleSelected(String logoName) {
    TemplateDataModel templateModel = pdfModel!.data!;
    templateModel.logoType = logoName;
    updatePDF();
  }

  void onFontSelected(index) {
    TemplateDataModel templateModel = pdfModel!.data!;
    templateModel.fontId = index + 1;
    updatePDF();
  }

  onToggle() {
    TemplateDataModel templateModel = pdfModel!.data!;
    templateModel.showBusinessInfo = !(templateModel.showBusinessInfo!);
    updatePDF();
  }

  onLocalPDFToggle() {
    localPDF = !localPDF;
    updatePDF();
  }

  Future onSave() async {
    TemplateDataModel templateModel = pdfModel!.data!;
    APIResponse resp = await ApiClient.post<TemplateDataModel>(request: {
      'template_id': templateModel.templateId,
      'colour_id': templateModel.colourId,
      'logo_type': templateModel.logoType,
      'show_business_info': templateModel.showBusinessInfo,
      'font_id': templateModel.fontId
    }, endPoint: "/userdefaults/set-default-template-info");
    debugPrint("$resp");
    notifyListeners();
  }

  updatePDF() async {
    if (localPDF == true) {
      await generateLocalPDF();
    } else {
      await generateRemotePDF();
    }
    notifyListeners();
  }

  Future generateRemotePDF() async {
    TemplateDataModel templateModel = pdfModel!.data!;
    setBusy(true);
    String endPoint = "/invoices/preview-Invoice-template";
    endPoint += "/${templateModel.templateId!}";
    endPoint += "/${templateModel.colourId}";
    endPoint += "/${templateModel.logoType}";
    endPoint += "/${templateModel.showBusinessInfo}";
    endPoint += "/${templateModel.fontId}";

    debugPrint("Getting filename from Network ...... ");
    APIResponse resp = await ApiClient.get<PDFModel>(
        endPoint: endPoint, params: "", fromJson: PDFModel.fromMap);
    pdfModel = resp['data'];
    // final directory = await getApplicationDocumentsDirectory();
    String fileURL = SERVER_URL2 + pdfModel!.fileName!;
    String filePath = homeCont.applicationDir + '/sample.pdf';

    // debugPrint("Downloading from Network ...... ");
    // final response =
    //     await http.get(Uri.parse('https://invoice___labs.co/api/$fileName'));
    // debugPrint("Downloaded from Network ...... ");
    // File file = File(file);
    // await file.writeAsBytes(response.bodyBytes);
    // fileToDownload, fileToStore
    pdfFile =
        await ApiClient.downloadFile(fileURL: fileURL, filePath: filePath);
    setBusy(false);
  }

  generateLocalPDF() async {
    debugPrint("Starting PDF Generation");
    setBusy(true);
    TemplateDataModel templateModel = pdfModel!.data!;
    String colorName = listOfColors!.colorList![colorIndex].name;
    String colorPrimary = listOfColors!.colorList![colorIndex].primaryCode;
    String colorSecondary = listOfColors!.colorList![colorIndex].secondaryCode;
    pdfFile = await createPDF2(
        templateModel, colorName, colorPrimary, colorSecondary);
    debugPrint("..Completed PDF Generation");
    setBusy(false);
  }
}
