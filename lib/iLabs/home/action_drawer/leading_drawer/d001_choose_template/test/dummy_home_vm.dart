import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../../../services/api_client.dart';
import '../../../../../models/choose_template/model.dart';
import '../../../../../models/color/model.dart';

class DummyHomeVM extends BaseViewModel {
  // TemplateDataModel? templateState;
  PDFModel? pdfModel;
  ColorList? listOfColors;

  // void templateModelUpdated() {}
  DummyHomeVM() {
    loadTemplateSettings();
    loadAllColors();
  }

  Future loadTemplateSettings() async {
    setBusy(true);
    APIResponse resp = await ApiClient.get<PDFModel>(
        endPoint:
            "/invoices/preview-Invoice-template/undefined/undefined/undefined/undefined/undefined",
        params: "",
        fromJson: PDFModel.fromMap);
    setBusy(false);
    pdfModel = resp["data"];
    debugPrint("${resp["data"]}");
  }

  Future loadAllColors() async {
    APIResponse resp = await ApiClient.get<ColorList>(
        endPoint: "/colour/undefined/undefined/id/DESC",
        fromJson: ColorList.fromMap);
    listOfColors = resp["data"];
    debugPrint("$listOfColors");
    // updatePDF();
  }
}
