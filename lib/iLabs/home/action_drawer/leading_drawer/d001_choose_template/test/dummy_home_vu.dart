import 'package:flutter/material.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:ilabs/iLabs/models/choose_template/model.dart';
import 'package:stacked/stacked.dart';
import 'dummy_home_vm.dart';

import '../choose_template_vu.dart';
import '../choose_template_io.dart'; //to define input and output models

class DummyHomeVU extends ViewModelBuilderWidget<DummyHomeVM> {
  const DummyHomeVU({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, DummyHomeVM viewModel, Widget? child) {
    // DummyHomeVM vm = viewModel;
    ChooseTemplateInputModel inputModel =
        ChooseTemplateInputModel(viewModel.listOfColors, viewModel.pdfModel);

    return Scaffold(
      appBar: AppBar(title: const Text("DummyDrawer")),
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: showTemplateModel(viewModel),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.plus_one),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        // ChooseTemplateVU(vm.pdfModel!.copy()))).then((value) {
                        ChooseTemplateVU(inputModel))).then((value) {
              if (value != null) {
                viewModel.pdfModel = value;
                viewModel.notifyListeners();
              }
            });
          }),
    );
  }

  Widget? showTemplateModel(DummyHomeVM vm) {
    TemplateDataModel? templateModel;
    if (vm.pdfModel == null) {
      return const Text("Loading data");
    } else {
      templateModel = vm.pdfModel!.data!;
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("templateId: ${templateModel.templateId}"),
          Text("colourId: ${templateModel.colourId}"),
          Text("logoType: ${templateModel.logoType}"),
          Text("fontId: ${templateModel.fontId}"),
          Text("showBusinessInfo: ${templateModel.showBusinessInfo}"),
          // Image.network(
          //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvCMZE__w6ke-EXUXwLzvlDh7i_PRftAZpdHhDOePTaMjETXOwY7IeyN2SlNiZUscy-AA&usqp=CAU',
          //   height: 160,
          //   width: 120,
          //   fit: BoxFit.cover,
          // ),
          const Image(
              image: AssetImage("assets/images/choose_template/invoice.png"))
          // AssetImage("assets/images/choose_template/invoice.png");
        ],
      );
    }
  }

  @override
  viewModelBuilder(BuildContext context) {
    return DummyHomeVM();
  }
}
