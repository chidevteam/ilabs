import 'package:flutter/material.dart';
import '../../../../../services/api_client.dart';
import 'dummy_home_vu.dart';

void main() async {
  //
  await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.orange),
      home: const DummyHomeVU(),
    );
  }
}
