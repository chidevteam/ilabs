import 'package:flutter/material.dart';
import 'logo_model.dart';
import '../choose_template_vm.dart';
import '../../../../../models/choose_template/model.dart';

Widget logoStyleList(BuildContext context, ChooseTemplateVM viewModel) {
  TemplateDataModel templateModel = viewModel.pdfModel!.data!;
  return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: viewModel.logoStyleList.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            viewModel.onLogoStyleSelected(viewModel.logoStyleList[index].logo);
          },
          child:
              (templateModel.logoType! == viewModel.logoStyleList[index].logo)
                  ? selectlogoStyleCell(
                      viewModel,
                      index,
                      const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
                    )
                  : selectlogoStyleCell(viewModel, index, Colors.grey.shade200),
        );
      });
}

Widget selectlogoStyleCell(ChooseTemplateVM viewModel, int index, Color clr) {
  return Padding(
    padding: const EdgeInsets.only(left: 14, right: 12, bottom: 6, top: 6),
    child: Container(
      width: 88.0,
      decoration: BoxDecoration(
        border: Border.all(color: clr, width: 1),
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Column(
        children: [
          checkbox(viewModel, index),
          Container(
            decoration: BoxDecoration(
                color: clr,
                borderRadius: BorderRadius.circular(
                    viewModel.logoStyleList[index].borderRadius)),
            width: 26.0,
            height: 26.0,
          ),
          const SizedBox(
            height: 4.0,
          ),
          Text(
            LogoStyle.dbMapper[viewModel.logoStyleList[index].logo]!,
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey.shade600,
              fontSize: 12.0,
            ),
          )
        ],
      ),
    ),
  );
}

Widget checkbox(ChooseTemplateVM viewModel, int index) {
  TemplateDataModel templateModel = viewModel.pdfModel!.data!;

  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Transform.scale(
        scale: 0.8,
        child: SizedBox(
          width: 24.0,
          height: 24.0,
          child: Visibility(
            visible: (templateModel.logoType! ==
                viewModel.logoStyleList[index].logo),
            child: Checkbox(
              activeColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
              checkColor: Colors.white,
              value: (templateModel.logoType! ==
                  viewModel.logoStyleList[index].logo),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(70)),
              onChanged: (bool? value) {
                viewModel.onTemplateStyleSelected(index);
              },
            ),
          ),
        ),
      ),
    ],
  );
}
