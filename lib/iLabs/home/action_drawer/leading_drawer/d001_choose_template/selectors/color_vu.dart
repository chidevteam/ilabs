import 'package:flutter/material.dart';
import '../choose_template_vm.dart';
// import '../../../../models/choose_template/model.dart';

class ColorVU extends StatelessWidget {
  const ColorVU(this.viewModel, {Key? key}) : super(key: key);

  final ChooseTemplateVM viewModel;
  @override
  Widget build(BuildContext context) {
    // TemplateDataModel templateModel = viewModel.pdfModel!.data!;
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: viewModel.listOfColors!.colorList!.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              viewModel.onColorSelected(index);
            },
            child: Padding(
              padding: const EdgeInsets.only(
                top: 22.0,
                bottom: 20,
                left: 12,
              ),
              child: Container(
                width: 66.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.shade300, width: 0.6),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: (viewModel.colorIndex == index)
                        ? const Icon(
                            Icons.check_rounded,
                            color: Colors.white,
                          )
                        : null,
                    color: viewModel.getColor(index),
                  ),
                ),
              ),
            ),
          );
        });
  }
}
