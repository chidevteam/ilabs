import 'package:flutter/material.dart';
import '../choose_template_vm.dart';
import '../../../../../models/choose_template/model.dart';

Widget fontVU(BuildContext context, ChooseTemplateVM viewModel) {
  TemplateDataModel templateModel = viewModel.pdfModel!.data!;

  return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: viewModel.fontsList.length,
      itemBuilder: (context, index) {
        bool isSelected = (templateModel.fontId! - 1 == index);
        return InkWell(
            onTap: () {
              viewModel.onFontSelected(index);
            },
            child: fontsListCell(
              viewModel,
              index,
              isSelected,
            ));
      });
}

Widget fontsListCell(ChooseTemplateVM viewModel, int index, bool isSelected) {
  Color clr = isSelected == true
      ? const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0)
      : Colors.grey.shade200;
  return Padding(
    padding: const EdgeInsets.only(left: 14, right: 12, bottom: 6, top: 6),
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          checkbox(viewModel, index),
          Icon(
            Icons.text_format_outlined,
            color: clr,
            size: 30.0,
          ),
          Text(
            viewModel.fontsList[index].font,
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
            style: TextStyle(
              color: Colors.grey.shade600,
              fontSize: 12.0,
            ),
          )
        ],
      ),
      width: 88.0,
      decoration: BoxDecoration(
        border: Border.all(color: clr, width: 1),
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
    ),
  );
}

Row checkbox(ChooseTemplateVM viewModel, int index) {
  TemplateDataModel templateModel = viewModel.pdfModel!.data!;

  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Transform.scale(
        scale: 0.6,
        child: SizedBox(
          width: 24.0,
          height: 24.0,
          child: Visibility(
            visible: (templateModel.fontId! - 1 == index),
            child: Checkbox(
              activeColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
              checkColor: Colors.white,
              value: (templateModel.fontId! - 1 == index),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              onChanged: (bool? value) {
                viewModel.onFontSelected(index);
              },
            ),
          ),
        ),
      ),
    ],
  );
}
