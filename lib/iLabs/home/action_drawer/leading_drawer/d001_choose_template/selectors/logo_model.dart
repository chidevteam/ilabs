class LogoStyle {
  String logo;
  double borderRadius;

  LogoStyle(this.logo, this.borderRadius);

  static List<LogoStyle> logoStyleList = [
    LogoStyle(
      "square",
      0,
    ),
    LogoStyle(
      "circle",
      20,
    ),
    LogoStyle(
      "roundcorner",
      7,
    ),
  ];

  static Map<String, String> dbMapper = {
    "square": "Square",
    "circle": "Circle",
    "roundcorner": "Rounded Corners",
  };
}
