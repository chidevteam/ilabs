class TemplateStyle {
  String template;
  String image;

  TemplateStyle(this.template, this.image);

  static List<TemplateStyle> templateList = [
    TemplateStyle(
      "Style 1",
      "assets/images/choose_template/invoice.png",
    ),
    TemplateStyle(
      "Style 2",
      "assets/images/choose_template/invoice.png",
    ),
    TemplateStyle(
      "Style 3",
      "assets/images/choose_template/invoice.png",
    ),
    TemplateStyle(
      "Style 4",
      "assets/images/choose_template/invoice.png",
    ),
    TemplateStyle(
      "Style 5",
      "assets/images/choose_template/invoice.png",
    ),
    TemplateStyle(
      "Style 6",
      "assets/images/choose_template/invoice.png",
    ),
  ];
}
