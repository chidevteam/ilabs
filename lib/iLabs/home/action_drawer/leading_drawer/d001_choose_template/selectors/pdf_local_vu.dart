import 'package:flutter/material.dart';
import '../choose_template_vm.dart';

class PDFLocalVU extends StatelessWidget {
  const PDFLocalVU(this.viewModel, {Key? key}) : super(key: key);

  final ChooseTemplateVM viewModel;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(14.0, 28.0, 8.0, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Generate PDF",
            style: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.w500,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 5,
                child: Text(
                  "Are you sure you want to generate local PDF",
                  style: TextStyle(
                      fontSize: 11.0,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey.shade600),
                ),
              ),
              switchToggleButton(viewModel)
            ],
          ),
        ],
      ),
    );
  }

  Widget switchToggleButton(ChooseTemplateVM viewModel) {
    return Expanded(
      flex: 1,
      child: Transform.scale(
        scale: 0.8,
        child: Switch(
          value: viewModel.localPDF,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          activeColor: Colors.orange[700],
          onChanged: (bool? value) {
            viewModel.onLocalPDFToggle();
          },
        ),
      ),
    );
  }
}
