import 'package:flutter/material.dart';
import '../choose_template_vm.dart';

import '../../../../../models/choose_template/model.dart';

Widget templateStyleVU(BuildContext context, ChooseTemplateVM viewModel) {
  TemplateDataModel templateModel = viewModel.pdfModel!.data!;
  return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: viewModel.templateList.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            viewModel.onTemplateStyleSelected(index);
          },
          child: (templateModel.templateId! - 1 == index)
              ? selectingTemplateCell(
                  viewModel,
                  index,
                  const Color.fromARGB(255, 207, 72, 23),
                )
              : selectingTemplateCell(viewModel, index, Colors.grey.shade200),
        );
      });
}

Widget selectingTemplateCell(ChooseTemplateVM viewModel, int index, Color clr) {
  return Padding(
    padding: const EdgeInsets.only(left: 14, right: 12, top: 6, bottom: 6),
    child: Container(
      child: Column(
        children: [
          checkbox(viewModel, index),
          Container(
              decoration: BoxDecoration(
                border: Border.all(color: clr, width: 0.6),
              ),
              child: Image(
                image: AssetImage(viewModel.templateList[index].image),
                width: 22.0,
                height: 26.0,
                fit: BoxFit.cover,
              )),
          const SizedBox(
            height: 4.0,
          ),
          Text(
            viewModel.templateList[index].template,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.grey.shade700, fontSize: 12.0),
          )
        ],
      ),
      width: 88.0,
      decoration: BoxDecoration(
        border: Border.all(color: clr, width: 1),
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
    ),
  );
}

Widget checkbox(ChooseTemplateVM viewModel, int index) {
  TemplateDataModel templateModel = viewModel.pdfModel!.data!;

  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Transform.scale(
        scale: 0.6,
        child: SizedBox(
          width: 24.0,
          height: 24.0,
          child: Visibility(
            visible: (templateModel.templateId! - 1 == index),
            child: Checkbox(
              activeColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
              checkColor: Colors.white,
              value: (templateModel.templateId! - 1 == index),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(70)),
              onChanged: (bool? value) {
                viewModel.onTemplateStyleSelected(index);
              },
            ),
          ),
        ),
      ),
    ],
  );
}
