class Font {
  String font;

  Font(this.font);

  static List<Font> fontsList = [
    Font(
      "Poppins",
    ),
    Font(
      "Roboto",
    ),
    Font(
      "Noto Serif",
    ),
  ];
}
