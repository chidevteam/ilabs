import '../../../../models/color/model.dart';
import '../../../../models/choose_template/model.dart';

class ChooseTemplateInputModel {
  PDFModel? pdfModel;
  ColorList? listOfColors;

  ChooseTemplateInputModel(this.listOfColors, this.pdfModel);
}

class ChooseTemplateOuputModel {
  TemplateDataModel? templateDataModel;
  ChooseTemplateOuputModel(this.templateDataModel);
}

/*
We can have:
  ChooseTemplateVU(ChooseTemplateInputModel? inputModel); 

When pushed, it will return 
  ChooseTemplateOutputModel?

It will not modify inputModel.
 */