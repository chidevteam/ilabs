import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../language/generated/locale_keys.g.dart';
import 'package:stacked/stacked.dart';
import 'choose_template_vm.dart';
import 'choose_template_io.dart';

import 'pdf/invoice_pdf_view.dart';
import 'selectors/color_vu.dart';
import 'selectors/font_vu.dart';
import 'selectors/logo_style_vu.dart';
import 'selectors/show_name_vu.dart';
import 'selectors/pdf_local_vu.dart';
import 'selectors/template_style_vu.dart';
// import '../../../models/choose_template/model.dart';

// ignore: must_be_immutable
class ChooseTemplateVU extends ViewModelBuilderWidget<ChooseTemplateVM> {
  ChooseTemplateVU(this.inputModel, {Key? key}) : super(key: key);

  ChooseTemplateInputModel? inputModel;

  @override
  Widget builder(
      BuildContext context, ChooseTemplateVM viewModel, Widget? child) {
    Orientation orientation = MediaQuery.of(context).orientation;

    return DefaultTabController(
        length: 6,
        child: orientation == Orientation.portrait
            ? Scaffold(
                appBar: templateAppBar(context),
                body: portraitView(viewModel, context))
            : SafeArea(
                child: Scaffold(body: landscapeView(viewModel, context))));
  }

  AppBar templateAppBar(BuildContext context) {
    return AppBar(
      title: Text(
        tr(LocaleKeys.action_drawer_view_choose_template_screen_app_bar_title),
        style: TextStyle(
            color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.black87,
          size: 21.0,
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget portraitView(ChooseTemplateVM viewModel, BuildContext context) {
    return Column(
      children: [
        Expanded(flex: 15, child: invoicePDF(viewModel)),
        const TabBarPortrait(),
        TabBarContent(viewModel),
        saveButton(context, viewModel)
      ],
    );
  }

  Widget invoicePDF(ChooseTemplateVM viewModel) {
    return viewModel.isBusy
        ? Container(
            padding: const EdgeInsets.all(30.0),
            child: const CircularProgressIndicator(),
          )
        : invoicePDFview(viewModel, viewModel.pdfFile,
            viewModel.pdfModel!.data!.showBusinessInfo);
  }

  Widget landscapeView(ChooseTemplateVM viewModel, BuildContext context) {
    return Row(
      children: [
        Expanded(flex: 1, child: invoicePDF(viewModel)),
        Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Expanded(flex: 5, child: landscapeFiller()),
                  const TabBarPortrait(),
                  TabBarContent(viewModel),
                  saveButton(context, viewModel)
                ],
              ),
            ))
      ],
    );
  }

  Widget landscapeFiller() {
    return Row(children: [
      const Expanded(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Image(image: AssetImage("iLabsSreens/splash/splash icon.png")),
        ),
      ),
      Expanded(
        flex: 2,
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Center(
            child: Column(
              children: const [
                Text("Select design", style: TextStyle(fontSize: 25)),
                Text("Select design", style: TextStyle(fontSize: 15)),
              ],
            ),
          ),
        ),
      ),
    ]);
  }

  Widget saveButton(
    BuildContext context,
    ChooseTemplateVM viewModel,
  ) {
    return Padding(
      padding: const EdgeInsets.only(left: 14.0, bottom: 8.0, right: 14.0),
      child: SizedBox(
        width: double.infinity,
        height: 43.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          onPressed: () {
            viewModel.onSave();
            Navigator.pop(context, viewModel.pdfModel);
          },
          child: Text(
            tr(LocaleKeys
                .action_drawer_view_choose_template_screen_save_button),
            style: TextStyle(
                color: Colors.white,
                fontSize: 13.0,
                fontWeight: FontWeight.normal),
          ),
        ),
      ),
    );
  }

  @override
  ChooseTemplateVM viewModelBuilder(BuildContext context) {
    final vm = ChooseTemplateVM(inputModel!);
    return vm;
  }
}

////////////////////////////////////////////////////////////////////
/// MAIN TAB BAR FUNCTIONALITY
////////////////////////////////////////////////////////////////////

class TabBarPortrait extends StatelessWidget {
  const TabBarPortrait({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Container(
      height: orientation == Orientation.portrait ? 56.0 : 32.0,
      decoration: BoxDecoration(
        // color: Colors.white,
        border: Border.all(color: Colors.grey.shade300, width: 1.0),
      ),
      width: double.infinity,
      child: const TabBar(
        unselectedLabelColor: Colors.black45,
        indicatorWeight: 1,
        indicatorColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
        labelColor: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
        labelPadding: EdgeInsets.zero,
        isScrollable: true,
        tabs: [
          SelectorTab("Template"),
          SelectorTab("Logo Style"),
          SelectorTab("Fonts"),
          SelectorTab("Color"),
          SelectorTab("Settings"),
          SelectorTab("PDF Local"),
        ],
      ),
    );
  }
}

class SelectorTab extends StatelessWidget {
  const SelectorTab(this.title, {Key? key}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Container(
        width: 100.0,
        height: 20,
        decoration: const BoxDecoration(
            border: Border(
          right: BorderSide(width: 1, color: Colors.grey),
        )),
        child: Center(
          child: Text(
            title,
            style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400),
          ),
        ),
      ),
    );
  }
}

class TabBarContent extends StatelessWidget {
  const TabBarContent(this.viewModel, {Key? key}) : super(key: key);

  final ChooseTemplateVM viewModel;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 4,
      child: Padding(
        padding: const EdgeInsets.only(top: 2, bottom: 2),
        child: TabBarView(
          children: [
            templateStyleVU(context, viewModel),
            logoStyleList(context, viewModel),
            fontVU(context, viewModel),
            (viewModel.listOfColors == null)
                ? const Text("Getting data")
                : ColorVU(viewModel),
            ShowNameVU(viewModel),
            PDFLocalVU(viewModel),
          ],
        ),
      ),
    );
  }
}
