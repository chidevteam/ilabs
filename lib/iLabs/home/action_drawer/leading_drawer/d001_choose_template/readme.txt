
ARCHITECTURE
- Assumptions in Choose Template
- The templateDataModel or any of its 5 parameters can't be null
  will always be valid.

  If invalid show dialog. <-----------------------------

  We will push HomeVM which will have:
    - colorArray
    - PDFModel 
  and, get back / pop
    - PDFModel or null (if we pop from back button)
  
  PDFModel is:
    String? filename;
    String? htmlFilename;
    Data? data;
    bool? showPDF;

  ColorArray:
  {
    colors: [{
      int id;
      String name;
      String primaryCode;   "#xxxxxxxx"
      String secondaryCode;
      bool isDelete;
    }, 
    {
      ...
    }]
  }


  
- HomeViewModel will be passed which will have the color info.
  as well as the templateStyle info.

==========================

Choose template
--------------
- A copy of currently selected PDFModel containing the file name as well as 
  TemplateDataModel will be pushed to the ChooseTemplateView
  We will also pass ColorList

  and it will make a copy and modify it. On save :
   - it will send data to internet
   - On confirmation, it will update the original data as well
   - On cancel, it sends null 

Understand:
- SetBusy

Issues:
- Create PDF in Landscape is slow

- Load the default template in the start via
  /invoic es /preview-Invoice-template/undefined/undefined/undefined/undefined/undefined
