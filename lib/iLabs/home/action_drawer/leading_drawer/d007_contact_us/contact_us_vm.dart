import 'package:flutter/material.dart';

import 'package:stacked/stacked.dart';

import '../../../../models/business/model.dart';
import '../../../../services/api_client.dart';

class ContactUsVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();

  String? name;
  String? email;
  String? subject;
  String? message;
  String? phone;

  BusinessDetail? businessDetails;

  onEmail(String? value) {
    email = value!.trim();

    notifyListeners();
  }

  onName(String? value) {
    name = value!.trim();

    notifyListeners();
  }

  onSubject(String? value) {
    subject = value!.trim();
  }

  onMessage(String? value) {
    message = value!.trim();
  }

  onPhone(String? value) {
    phone = value!.trim();
  }

  onSaveItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    debugPrint(
        '$email ............ $subject .......$message ........$name......$phone');
    Map<String, dynamic> req = {
      'email': email,
      'message': message,
      'name': name,
      'phone': phone,
      'subject': subject
    };
    APIResponse resp = await ApiClient.post(
        request: req, endPoint: "/settings/contactus", fromJson: null);
    print(resp);
    notifyListeners();
  }
}
