import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/widgets/input/chi_textfield.dart';
import '../../../../language/generated/locale_keys.g.dart';
import '../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';

import '../../../../models/business/model.dart';
import 'contact_us_vm.dart';

class ContactUsScreen extends ViewModelBuilderWidget<ContactUsVM> {
  ContactUsScreen(this.businessDetails, {Key? key}) : super(key: key);
  BusinessDetail? businessDetails;

  @override
  Widget builder(BuildContext context, ContactUsVM viewModel, Widget? child) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              tr(LocaleKeys.action_drawer_view_contact_us_screen_app_bar_title),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400),
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.pop(
                  context,
                );
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.orange[800],
              ),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  viewModel.onSaveItem(context);
                  Navigator.pop(
                    context,
                  );
                },
                child: Text(
                  tr(LocaleKeys
                      .action_drawer_view_contact_us_screen_app_bar_button),
                  style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
                ),
              ),
            ]),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(14.0, 18, 14.0, 12.0),
          child: SingleChildScrollView(
            child: Form(
              key: viewModel.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CHITextField(
                      heading: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_business_heading),
                      hintText: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_business_hint),
                      func: viewModel.onName,
                      validator: null,
                      initialValue: businessDetails == null
                          ? ""
                          : businessDetails!.businessName!),
                  CHITextField(
                      heading: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_email_heading),
                      hintText: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_email_hint),
                      func: viewModel.onEmail,
                      initialValue: businessDetails == null
                          ? ''
                          : businessDetails!.businessEmail!),
                  CHITextField(
                      heading: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_phone_heading),
                      hintText: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_phone_hint),
                      func: viewModel.onPhone,
                      initialValue: businessDetails == null
                          ? ''
                          : businessDetails!.businessPhone),
                  CHITextField(
                      heading: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_subject_heading),
                      hintText: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_subject_hint),
                      func: viewModel.onSubject,
                      initialValue: ''),
                  CHITextField(
                      heading: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_message_heading),
                      hintText: tr(LocaleKeys
                          .action_drawer_view_contact_us_screen_message_hint),
                      maxLines: 5,
                      func: viewModel.onMessage,
                      initialValue: ''),
                ],
              ),
            ),
          ),
        ));
  }

  @override
  ContactUsVM viewModelBuilder(BuildContext context) {
    return ContactUsVM();
  }
}
