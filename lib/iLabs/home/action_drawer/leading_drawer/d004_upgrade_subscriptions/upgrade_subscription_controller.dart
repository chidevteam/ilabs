import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'dart:async';
import 'dart:io';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:ilabs/iLabs/register/register_vu.dart';
import '../../../../services/api_client.dart';
import 'package:in_app_purchase_ios/store_kit_wrappers.dart';

class UpgradeSubscriptionController extends GetxController {
  UpgradeSubscriptionController() {
    initializeData();
  }

  initializeData() async {
    await initPlatformState();
    await getPurchases();
    await getProduct();
    guestToggle();
    setCheckBoxBasedOnPreviousPurchases();
  }

  RxBool isChecked = false.obs;
  RxBool isChecked2 = false.obs;
  RxBool isChecked3 = false.obs;
  bool isGuest = false;

  StreamSubscription? purchaseUpdatedSubscription;
  StreamSubscription? purchaseErrorSubscription;
  StreamSubscription? conectionSubscription;
  final List<String> productLists = Platform.isAndroid
      ? ['dev_weekly_1', 'dev_1basic_2', '1basic', 'dev_1yearly_1']
      : ['com.cooni.point1000', 'com.cooni.point5000'];

  RxList<IAPItem> items = <IAPItem>[].obs;
  IAPItem? selectedItem;
  RxString selectedItemType = 'free'.obs;
  List<PurchasedItem> purchases_new = <PurchasedItem>[].obs;
  String mPlatformVersion = 'Unknown';
  RxBool isLoading = false.obs;

  guestToggle() {
    if (ApiClient.instance!.guestToken != '') {
      isGuest = true;
    } else {
      isGuest = false;
    }
  }

  Future<void> initPlatformState() async {
    String? platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterInappPurchase.instance.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    // print('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    // if (!mounted) return;

    mPlatformVersion = platformVersion!;

    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAll();
      // print('consumeAllItems: $msg');
    } catch (err) {
      // print('consumeAllItems error: $err');
    }

    conectionSubscription =
        FlutterInappPurchase.connectionUpdated.listen((connected) {
      // print('connected: $connected');
    });

    purchaseUpdatedSubscription =
        FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      // print('purchase-updated: $productItem');
    });

    purchaseErrorSubscription =
        FlutterInappPurchase.purchaseError.listen((purchaseError) {
      // print('purchase-error: $purchaseError');
    });
  }

  // chooseOption(){
  //   if(isGuest){
  //
  //   }
  //   else{
  //
  //   }
  // }

  Future openDialog(BuildContext context) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: const Text('Sign Up confirmation'),
            content: const Text(
                'It looks like you are not signed in. You need to sign in to use this feature'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancel')),
              TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: ((context) {
                      return const RegisterScreen();
                    })));
                  },
                  child: const Text('Sign Up'))
            ],
          ));

  void requestPurchase(IAPItem item) {
    FlutterInappPurchase.instance.requestPurchase(item.productId!);
  }

  Future getPurchases() async {
    print('----------------in get purchaseeeeee-------------------------');
    List<PurchasedItem>? items =
        await FlutterInappPurchase.instance.getAvailablePurchases();
    for (var item in items!) {
      //print('${item.toString()}');
      purchases_new.add(item);
    }
    //this.items = [];
    purchases_new = items;
  }

  Future getProduct() async {
    // print('caliiiinggg function');
    //await FlutterInappPurchase.instance.initialize();
    isLoading.value = true;
    List<IAPItem> items =
        await FlutterInappPurchase.instance.getSubscriptions(productLists);
    //print('$items');
    // print('after api call');
    for (var item in items) {
      print('${item.toString()}');
      this.items.add(item);
    }
    this.items.addAll(items);
    isLoading.value = false;
    // print('---------------------------------------${items[3].localizedPrice}');
    //purchases_new = [];
  }

  onCheckYearly() {
    isChecked.value = !isChecked.value;
    if (isChecked.value) {
      isChecked2.value = false;
      isChecked3.value = false;
    }
    updateSelectedItem(2);
    //notifyListeners();
  }

  onCheckMonthly() {
    isChecked2.value = !isChecked2.value;
    if (isChecked2.value) {
      isChecked.value = false;
      isChecked3.value = false;
    }
    updateSelectedItem(0);
    //notifyListeners();
  }

  onCheckWeekly() {
    isChecked3.value = !isChecked3.value;
    if (isChecked3.value) {
      isChecked.value = false;
      isChecked2.value = false;
    }
    updateSelectedItem(3);
    //getPurchases();
    for (int i = 0; i < purchases_new.length; i++) {
      // print('here');
      print(purchases_new[i].productId);
    }
    //notifyListeners();
  }

  updateSelectedItem(int index) {
    selectedItem = items[index];
  }

  updateSelectItemType(int index) {
    // print('--------------------------$index=------------------------');
    if (index == 0) {
      selectedItemType.value = 'monthly';
    } else if (index == 2) {
      selectedItemType.value = 'yearly';
    } else if (index == 3) {
      selectedItemType.value = 'weekly';
    } else {
      selectedItemType.value = 'free';
    }
  }

  void setCheckBoxBasedOnPreviousPurchases() {
    if (purchases_new.isNotEmpty) {
      if (purchases_new.length > 1) {
        for (int i = 0; i < purchases_new.length; i++) {
          if (purchases_new[i].productId == 'dev_1_yearly_1') {
            onCheckYearly();
            updateSelectItemType(2);
            return;
          }
        }
        for (int i = 0; i < purchases_new.length; i++) {
          if (purchases_new[i].productId == 'dev_weekly_1') {
            onCheckWeekly();
            updateSelectItemType(3);
            return;
          }
        }
        onCheckMonthly();
        updateSelectItemType(0);
      } else if (purchases_new.length == 1) {
        if (purchases_new[0].productId == 'dev_1_yearly_1') {
          onCheckYearly();
          updateSelectItemType(2);
        } else if (purchases_new[0].productId == 'dev_weekly_1') {
          onCheckWeekly();
          updateSelectItemType(3);
        } else {
          onCheckMonthly();
          updateSelectItemType(0);
        }
      }
    } else {
      onCheckYearly();
    }
  }
}
