import 'package:flutter/material.dart';
import 'upgrade_subscriptions_vm.dart';

import 'package:stacked/stacked.dart';

class UpgradeSubscriptionsScreen
    extends ViewModelBuilderWidget<UpgradeSubscriptionsViewModel> {
  const UpgradeSubscriptionsScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, UpgradeSubscriptionsViewModel viewModel,
      Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Subscriptions',
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, false);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
            size: 20.0,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(12, 16, 12, 12),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                'Upgrade Today',
                style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 13.0,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 8.0,
              ),
              Text(
                'Thanks for trying invoice maker',
                style: TextStyle(
                    color: Colors.grey.shade900,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 14.0,
              ),
              images(),
              const SizedBox(
                height: 14.0,
              ),
              Text(
                'Your are currently using our free plan',
                style: TextStyle(
                    color: Colors.lightBlue[800],
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 10.0,
              ),
              Text(
                'We Guarant you,ll love us Or',
                style: TextStyle(
                    color: Colors.grey.shade600,
                    fontSize: 13.0,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 6.0,
              ),
              const Text(
                'Your Money Back',
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 13.0,
                    fontWeight: FontWeight.w400),
              ),
              premiumFeatureTitle(viewModel),
              yearlyCheckbox(viewModel),
              const SizedBox(
                height: 10.0,
              ),
              monthlyCheckbox(viewModel),
              const SizedBox(
                height: 10.0,
              ),
              weeklyCheckBox(viewModel),
              selectContinueButton(viewModel, context)
            ],
          ),
        ),
      ),
    );
  }

  Widget premiumFeatureTitle(UpgradeSubscriptionsViewModel viewModel) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Premium Feature',
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 13.0,
                    fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 10.0,
              ),
              premiumFeature(
                  Icons.check, "Unlimited invoices & estimates", viewModel),
              const SizedBox(
                height: 12.0,
              ),
              premiumFeature(Icons.check, "Send and track invoice", viewModel),
              const SizedBox(
                height: 12.0,
              ),
              premiumFeature(Icons.check, "View monthly report", viewModel),
              const SizedBox(
                height: 12.0,
              ),
              premiumFeature(Icons.check, "Professional template", viewModel),
              const SizedBox(
                height: 12.0,
              ),
              premiumFeatureFreeTrails(
                  Icons.check, "7 Days Free Trails", viewModel),
            ],
          ),
        ),
      ),
    );
  }

  Container weeklyCheckBox(UpgradeSubscriptionsViewModel viewModel) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.grey[200],
        border: Border.all(
            width: 1.0,
            color: viewModel.isChecked3
                ? Colors.lightBlue.shade800
                : Colors.grey.shade200),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 2.0, left: 10.0, bottom: 2.0),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Weekly',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                const Text(
                  '7 Days Free Trails',
                  style: TextStyle(
                      color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
                      fontSize: 10.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            const Spacer(),
            Column(
              children: [
                Text(
                  '\$18.99',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                Text(
                  '1,5825 per Month',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 10.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            Checkbox(
              checkColor: Colors.white,
              value: viewModel.isChecked3,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              onChanged: (bool? value) {
                viewModel.onCheckWeekly();
              },
            )
          ],
        ),
      ),
    );
  }

  Widget monthlyCheckbox(UpgradeSubscriptionsViewModel viewModel) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.grey[200],
        border: Border.all(
            width: 1.0,
            color: viewModel.isChecked2
                ? Colors.lightBlue.shade800
                : Colors.grey.shade200),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 2.0, left: 10.0, bottom: 2.0),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Monthly',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                const Text(
                  '7 Days Free Trails',
                  style: TextStyle(
                      color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
                      fontSize: 10.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            const Spacer(),
            Column(
              children: [
                Text(
                  '\$18.99',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                Text(
                  '1,5825 per Month',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 10.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            Checkbox(
              checkColor: Colors.white,
              value: viewModel.isChecked2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              onChanged: (bool? value) {
                viewModel.onCheckMonthly();
              },
            )
          ],
        ),
      ),
    );
  }

  Widget yearlyCheckbox(UpgradeSubscriptionsViewModel viewModel) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.grey[200],
        border: Border.all(
            width: 1.0,
            color: viewModel.isChecked
                ? Colors.lightBlue.shade800
                : Colors.grey.shade200),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 2.0, left: 10.0, bottom: 2.0),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Yearly',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                const Text(
                  '7 Days Free Trails',
                  style: TextStyle(
                      color: Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
                      fontSize: 10.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            const Spacer(),
            Column(
              children: [
                Text(
                  '\$18.99',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 6.0,
                ),
                Text(
                  '1,5825 per Month',
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 10.0,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            Checkbox(
              checkColor: Colors.white,
              value: viewModel.isChecked,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              onChanged: (bool? value) {
                viewModel.onCheckYearly();
              },
            )
          ],
        ),
      ),
    );
  }

  Widget images() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: Image.network(
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeaT4_DD413xXcIxmD8cj5BKdO057g0bnC0A&usqp=CAU',
            width: 100.0,
            height: 100.0,
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }

  Widget premiumFeature(
      IconData icon, String label, UpgradeSubscriptionsViewModel viewModel) {
    return Row(
      children: [
        Icon(
          icon,
          size: 14.0,
          color: Colors.grey.shade700,
        ),
        const SizedBox(
          width: 3.0,
        ),
        Text(
          label,
          style: TextStyle(
              color: Colors.grey.shade700,
              fontSize: 12.0,
              fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  Widget premiumFeatureFreeTrails(
      IconData icon, String label, UpgradeSubscriptionsViewModel viewModel) {
    return Row(
      children: [
        Icon(
          icon,
          size: 14.0,
          color: Colors.blue,
        ),
        const SizedBox(
          width: 3.0,
        ),
        Text(
          label,
          style: TextStyle(
              color: Colors.lightBlue[800],
              fontSize: 12.0,
              fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  Widget selectContinueButton(viewModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: SizedBox(
        width: double.infinity,
        height: 43.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          onPressed: () {},
          child: const Text(
            'Select & Continue',
            style: TextStyle(
                color: Colors.white,
                fontSize: 13.0,
                fontWeight: FontWeight.normal),
          ),
        ),
      ),
    );
  }

  @override
  UpgradeSubscriptionsViewModel viewModelBuilder(BuildContext context) {
    return UpgradeSubscriptionsViewModel();
  }
}
