import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../upgrade_subscription_controller.dart';

Obx yearlyCellView(UpgradeSubscriptionController subController) {
  return Obx(
        () => Expanded(
      child: Column(
        children: [
          subController.isChecked.value
              ? Container(
            width: 80,
            height: 20,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: Text("Yearly",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500)),
            ),
          )
              : Text(''),
          InkWell(
            onTap: () {
              subController.onCheckYearly();
            },
            child: Container(
              height: subController.isChecked.value ? 130 : 90,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7.0),
                color: const Color.fromRGBO(0xf4, 0xf7, 0xfa, 1.0),
                border: Border.all(
                  width: 1.0,
                  color: subController.isChecked.value
                      ? const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0)
                      : const Color.fromRGBO(0xf4, 0xf7, 0xfa, 1.0),
                ),
              ),
              child: Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Obx(
                              () => Checkbox(
                            checkColor: Colors.white,
                            materialTapTargetSize:
                            MaterialTapTargetSize.shrinkWrap,
                            activeColor:
                            const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
                            value: subController.isChecked.value,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            onChanged: (bool? value) {
                              subController.onCheckYearly();
                            },
                          ),
                        )
                      ],
                    ),
                    subController.isChecked.value
                        ? Text(
                        subController.isLoading.value
                            ? ''
                            : '${subController.items.length == 0 ? "" : subController.items[2].localizedPrice}',
                        style: GoogleFonts.poppins(

// #3a3938
                            color:
                            const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                            fontSize: 22.0,
                            fontWeight: FontWeight.w500))
                        : Text("Yearly",
                        style: GoogleFonts.poppins(
                            color:
                            const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
                            fontSize: 12.0,
                            fontWeight: FontWeight.w500)),
                    subController.isChecked.value
                        ? Text(
                        subController.isLoading.value
                            ? ''
                            : '${subController.items.length == 0 ? "" : num.parse(subController.items[2].price!)} per Month',
                        style: GoogleFonts.poppins(
                            color:
                            const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400))
                        : Text(
                        subController.isLoading.value
                            ? ''
                            : '${subController.items.length == 0 ? "" : subController.items[2].localizedPrice}',
                        style: GoogleFonts.poppins(
                            color:
                            const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),

// #707070
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
