import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../upgrade_subscription_controller.dart';

Obx weeklyCellView(UpgradeSubscriptionController subController) {
  return Obx(
        () => Expanded(
      child: Column(
        children: [

          subController.isChecked3.value
              ? Container(
            width: 80,
            height: 20,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: Text("Weekly",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500)),
            ),
          )
              : Text(''),
          InkWell(
            onTap: () {
              subController.onCheckWeekly();
            },
            child: Container(
              height: subController.isChecked3.value ? 108 : 90,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: const Color.fromRGBO(0xf4, 0xf7, 0xfa, 1.0),
                border: Border.all(
                  width: 1.0,
                  color: subController.isChecked3.value
                      ? const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0)
                      : const Color.fromRGBO(0xf4, 0xf7, 0xfa, 1.0),
                ),
              ),
              child: Column(
                children: [
                  Obx(
                        () => Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Checkbox(
                          checkColor: Colors.white,
                          activeColor:
                          const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
                          materialTapTargetSize:
                          MaterialTapTargetSize.shrinkWrap,
                          value: subController.isChecked3.value,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          onChanged: (bool? value) {
                            subController.onCheckWeekly();
                          },
                        ),
                      ],
                    ),
                  ),
                  // #006b99
                  subController.isChecked3.value
                      ? Text(
                      subController.isLoading.value
                          ? ''
                          : '${subController.items.length == 0 ? "" : subController.items[3].localizedPrice}',
                      style: GoogleFonts.poppins(
                          color:
                          const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                          fontSize: 22,
                          fontWeight: FontWeight.w500))
                      : Text("Weekly",
                      style: GoogleFonts.poppins(
                          color:
                          const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500)),
                  subController.isChecked3.value
                      ? Text('')
                      : Text(
                      subController.isLoading.value
                          ? ''
                          : '${subController.items.length == 0 ? "" : subController.items[3].localizedPrice}',
                      style: GoogleFonts.poppins(
                          color:
                          const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                          fontSize:
                          subController.isChecked3.value ? 22 : 12,
                          fontWeight: FontWeight.w500))
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
