import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../upgrade_subscription_controller.dart';

Obx monthlyCellView(UpgradeSubscriptionController subController) {
  return Obx(
        () => Expanded(
      child: Column(
        children: [
          subController.isChecked2.value
              ? Container(
            width: 80,
            height: 20,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: Text("Monthly",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500)),
            ),
          )
              : Text(''),
          InkWell(
            onTap: () {
              subController.onCheckMonthly();
            },
            child: Container(
              height: subController.isChecked2.value ? 108 : 90,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7.0),

// #f4f7fa
                color: const Color.fromRGBO(0xf4, 0xf7, 0xfa, 1.0),
                border: Border.all(
                  width: 1.0,
                  color: subController.isChecked2.value
                      ? const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0)
                      : const Color.fromRGBO(0xf4, 0xf7, 0xfa, 1.0),
                ),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Obx(
                            () => Checkbox(
                          activeColor:
                          const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
                          materialTapTargetSize:
                          MaterialTapTargetSize.shrinkWrap,
                          checkColor: Colors.white,
                          value: subController.isChecked2.value,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          onChanged: (bool? value) {
                            subController.onCheckMonthly();
                          },
                        ),
                      )
                    ],
                  ),
                  subController.isChecked2.value
                      ? Text(
                      subController.isLoading.value
                          ? ''
                          : subController.items.length == 0
                          ? ""
                          : subController.items[0].localizedPrice
                          .toString(),
                      style: GoogleFonts.poppins(
                          color:
                          const Color.fromRGBO(0x3a, 0x39, 0x38, 1.0),
                          fontSize: 22.0,
                          fontWeight: FontWeight.w500))
                      : Text("Monthly",
                      style: GoogleFonts.poppins(
                          color:
                          const Color.fromRGBO(0x00, 0x6b, 0x99, 1.0),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500)),
                  subController.isChecked2.value
                      ? Text(
                      '${subController.items.length == 0 ? "" : num.parse(subController.items[0].price!) / 4} Per Week',
                      style: GoogleFonts.poppins(
                          color:
                          const Color.fromRGBO(0x70, 0x70, 0x70, 1.0),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w400))
                      : Text(
                    subController.isLoading.value
                        ? ''
                        : subController.items.length == 0
                        ? ""
                        : subController.items[0].localizedPrice
                        .toString(),
                    style: TextStyle(
                        color: Colors.grey.shade800,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
