import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilabs/iLabs/Loadings/loading_pages.dart';
import 'cell_views/monthly_cell_vu.dart';
import 'cell_views/weekly_cell_vu.dart';
import 'cell_views/yearly_cell_vu.dart';
import 'upgrade_subscription_controller.dart';
import 'package:get/get.dart';

class UpgradeSubView extends StatelessWidget {
  UpgradeSubView({Key? key}) : super(key: key);

  UpgradeSubscriptionController subController = UpgradeSubscriptionController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar(context),
      body: Stack(children: [
        Obx(
          () => Positioned(
              child: subController.isLoading.value
                  ? const GettingRecords('Loading Items')
                  : const Text('')),
        ),
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15.0, 16, 15.0, 12),
            child: Column(
              children: [
                Obx(
                  () => upgradeToday(),
                ),
                invoiceMakerTxt(),
                const SizedBox(
                  height: 10.0,
                ),
                images(),
                const SizedBox(
                  height: 10.0,
                ),
                Obx(
                  () => freePlanTxt(),
                ),
                const SizedBox(
                  height: 4.0,
                ),
                text(),
                moneyBackTxt(),
                premiumFeatureTitle(),
                const SizedBox(
                  height: 14.0,
                ),
                cellView(),
                selectContinueButton(context)
              ],
            ),
          ),
        ),
      ]),
    );
  }

  Text moneyBackTxt() {
    return Text('Your Money Back',
        style: GoogleFonts.poppins(
            color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
            fontSize: 12.0,
            fontWeight: FontWeight.w400));
  }

  Text text() {
    return Text("We Guarantee that You'll Love Us Or",
        style: GoogleFonts.poppins(
            color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
            fontSize: 12.0,
            fontWeight: FontWeight.w400));
  }

  Text freePlanTxt() {
    return Text(
        'You are currently using our ${subController.selectedItemType} plan',
        style: GoogleFonts.poppins(
            color: const Color.fromRGBO(0x0d, 0x6b, 0x99, 1.0),
            fontWeight: FontWeight.w500));
  }

  Text invoiceMakerTxt() {
    return Text(
      'Thanks for trying invoice maker',
      style: GoogleFonts.poppins(
          color: const Color.fromRGBO(0x33, 0x33, 0x33, 1.0),
          fontSize: 14.0,
          fontWeight: FontWeight.w500),
    );
  }

  Text upgradeToday() {
    return Text(
        subController.selectedItemType.value == 'free' ? 'Upgrade Today' : '',
        style: GoogleFonts.poppins(
            color: const Color.fromRGBO(0x80, 0x80, 0x80, 1.0),
            fontSize: 14.0,
            fontWeight: FontWeight.w500));
  }

  AppBar appBar(BuildContext context) {
    return AppBar(
      elevation: 0.8,
      backgroundColor: Colors.white,
      title: Text('Subscriptions',
          style: GoogleFonts.poppins(
              color: const Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
              fontSize: 18.0,
              fontWeight: FontWeight.w400)),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context, false);
        },
        icon: const Icon(
          Icons.arrow_back,
          color: Color.fromRGBO(0x19, 0x31, 0x45, 1.0),
          size: 20.0,
        ),
      ),
    );
  }

  Widget cellView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        weeklyCellView(subController),
        const SizedBox(
          width: 5.0,
        ),
        yearlyCellView(subController),
        const SizedBox(
          width: 5.0,
        ),
        monthlyCellView(subController),
      ],
    );
  }

  Widget premiumFeatureTitle() {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 10),
        child: Material(
          child: Card(
            color: Colors.white,
            elevation: 1,
            shadowColor: Colors.white.withOpacity(0.0),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text('Premium Feature',
                          style: GoogleFonts.poppins(
                              color:
                                  const Color.fromRGBO(0x33, 0x33, 0x33, 1.0),
                              fontSize: 12.0,
                              fontWeight: FontWeight.w500)),
                      const Spacer(),
                      Image.network(
                        'https://play-lh.googleusercontent.com/qOJFMpgmBTaHnKpih-hH2jP0TgpLOTFmhhg2GEKAphHuxXpEGfkMRN0lbWyIqALmW9Hf',
                        width: 32.0,
                        height: 32.0,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                  premiumFeature(Icons.check, "Unlimited invoices & estimates"),
                  const SizedBox(
                    height: 5.0,
                  ),
                  premiumFeature(Icons.check, "Send and track invoice"),
                  const SizedBox(
                    height: 5.0,
                  ),
                  premiumFeature(Icons.check, "View monthly report"),
                  const SizedBox(
                    height: 5.0,
                  ),
                  premiumFeature(Icons.check, "Professional template"),
                  const SizedBox(
                    height: 5.0,
                  ),
                  subController.isChecked.value
                      ? premiumFeatureFreeTrails(
                          Icons.check, "7 Days Free Trial")
                      : const Text(''),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget images() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: Image.network(
            'https://www.billdu.com/wp-content/uploads/2018/04/Billdu_How-to-avoid-double-invoicing.png',
            width: 108.0,
            height: 108.0,
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }

  Widget premiumFeature(IconData icon, String label) {
    return Row(
      children: [
        Icon(
          icon,
          size: 12.0,
          color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
        ),
        const SizedBox(
          width: 3.0,
        ),
        Text(label,
            style: GoogleFonts.poppins(
                color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
                fontSize: 12.0,
                fontWeight: FontWeight.w400)),
      ],
    );
  }

  Widget premiumFeatureFreeTrails(IconData icon, String label) {
    return Row(
      children: [
        Icon(
          icon,
          size: 12.0,
          color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
        ),
        const SizedBox(
          width: 3.0,
        ),
        Text(label,
            style: GoogleFonts.poppins(
                color: const Color.fromRGBO(0x66, 0x66, 0x66, 1.0),
                fontSize: 12.0,
                fontWeight: FontWeight.w400)),
      ],
    );
  }

  Widget selectContinueButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 20.0,
      ),
      child: SizedBox(
        width: double.infinity,
        height: 40.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          onPressed: () {
            if (subController.isGuest) {
              subController.openDialog(context);
            } else {
              subController.requestPurchase(subController.selectedItem!);
            }
          },
          child: Text(
            'Select & Continue',
            style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.w400),
          ),
        ),
      ),
    );
  }
}
