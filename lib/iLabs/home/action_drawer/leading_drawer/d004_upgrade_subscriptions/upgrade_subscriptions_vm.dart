import 'package:stacked/stacked.dart';

class UpgradeSubscriptionsViewModel extends BaseViewModel {
  bool isChecked = false;
  bool isChecked2 = false;
  bool isChecked3 = false;

  onCheckYearly() {
    isChecked = !isChecked;
    if (isChecked) {
      isChecked2 = false;
      isChecked3 = false;
    }
    notifyListeners();
  }

  onCheckMonthly() {
    isChecked2 = !isChecked2;
    if (isChecked2) {
      isChecked = false;
      isChecked3 = false;
    }

    notifyListeners();
  }

  onCheckWeekly() {
    isChecked3 = !isChecked3;
    if (isChecked3) {
      isChecked = false;
      isChecked2 = false;
    }

    notifyListeners();
  }
}
