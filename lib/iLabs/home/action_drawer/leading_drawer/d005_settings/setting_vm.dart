import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/home/home_controller.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import 'package:ilabs/iLabs/models/userdefaults/model.dart';
import 'package:stacked/stacked.dart';
import '../../../../services/api_client.dart';
import 'tax/tax_model.dart';

class SettingViewModel extends BaseViewModel {
  HomeController homeCtrl = Get.find<HomeController>();

  // Customize? customize;
  OldTaxModel? taxModel;
  UserDefault? userDefaultx;
  bool isToggle = false;
  bool saveAsDefault = false;

  onToggle() {
    isToggle = !isToggle;
    notifyListeners();
  }

  setSendEmailCopy(bool sendCopy) async {
    debugPrint("Sending Email COPY UPDATE : $sendCopy");

    homeCtrl.userDefault.emailCopy = sendCopy;
    Request req = {"email_copy": sendCopy};
    APIResponse resp = await ApiClient.post(
        request: req, endPoint: "/userdefaults", fromJson: null);
    debugPrint('$resp["data"]');
  }

  uploadSignature(String imgPath) async {
    homeCtrl.signatureFilePath = imgPath;
    // signaturePath = imgPath;
    notifyListeners();
    Request addReq = {};

    var res = await ApiClient.postMultiPart(
        request: addReq,
        endPoint: "/userdefaults",
        fromJson: UserDefault.fromMap,
        filePath: imgPath,
        imageKey: 'signature');

    homeCtrl.userDefault.signature = res['data'].signature!;
  }

  uploadSignatureFromInvoice(String imgPath, InvoiceData? invoiceData) async {
    homeCtrl.signatureFilePath = imgPath;
    // signaturePath = imgPath;
    notifyListeners();
    Request addReq = {};

    String endPoint = "/invoices/invoicesignature/${invoiceData!.id}";
    var res = await ApiClient.postMultiPart(
        request: addReq,
        endPoint: endPoint,
        fromJson: null,
        filePath: imgPath,
        imageKey: 'signature',
        requestType: "PATCH");

    // invoiceData!.signature = res['data'].signature!;
    invoiceData.signature = res['data']['signature'];
    print('invoice caaaaaaaaaaaaaaaaaaalingggggg==========>>>>>>>');
    print(invoiceData.signature);
  }
}
