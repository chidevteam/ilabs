import 'package:flutter/material.dart';
import 'new_tax_vm.dart';
import '../../../../../../models/tax/model.dart';
import 'package:stacked/stacked.dart';
import './widgets/on_deducted_widget.dart';
import './widgets/on_total_widget.dart';
import './widgets/per_item_widget.dart';

class NewTaxVU extends ViewModelBuilderWidget<NewTaxVM> {
  TaxModel? tempInputModel;
  NewTaxVU(this.tempInputModel, {Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, NewTaxVM viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Tax',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              // print(controller.selectedOption.toString());

              // controller.selectedOption.value =
              //     controller.preSelectedOption.value;
              Navigator.pop(context, null);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                viewModel.onSave();
                Navigator.pop(context, viewModel.outputModel);
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: WillPopScope(
        onWillPop: () async {
          // controller.selectedOption.value = controller.preSelectedOption.value;
          Navigator.pop( context,null );
          return true;
        },
        child: Form(
          key: viewModel.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  viewModel.updateOptions(0, context);},
                                child: Text(
                                  viewModel.options[0],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  viewModel.updateOptions(1, context);
                                },
                                child: Text(
                                  viewModel.options[1],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  viewModel.updateOptions(2, context);
                                },
                                child: Text(
                                  viewModel.options[2],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                          
                          ],
                        );
                      });
                },
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Tax',
                              style: TextStyle(
                                  color: Colors.grey[700],
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w400),
                            ),
                            Visibility(
                              visible: true,
                              child: Text(
                                viewModel.outputModel.taxType,
                                style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                  child: viewModel.outputModel.taxType == 'None'
                      ? (Container())
                      : viewModel.outputModel.taxType == 'On The Total'
                          ? (OnTheTotalWidget(viewModel))
                          : viewModel.outputModel.taxType == 'Deducted'
                              ? (OnDeductedWidget(viewModel))
                              : (OnPerItemWidget(viewModel))),
            ],
          ),
        ),
      ),
    );
  }

  @override
  NewTaxVM viewModelBuilder(BuildContext context) {
    NewTaxVM vm = NewTaxVM(tempInputModel);
    return vm;
  }
}
