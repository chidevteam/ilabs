import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stacked/stacked.dart';
import '../../../../../home_controller.dart';
import '../../../../../../services/api_client.dart';
import '../../../../../../models/tax/model.dart';

class NewTaxVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  HomeController homeController = Get.find<HomeController>();
  TaxModel? inputModel;
  List<String> options = ['On The Total', 'Deducted', 'Per Item'];
  TaxModel outputModel = TaxModel();

  NewTaxVM(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  onSignToggle() {
    outputModel.taxInclusive = !outputModel.taxInclusive;
    notifyListeners();
  }

  updateOptions(int index, BuildContext context) {
    // preSelectedOption.value = selectedOption.value;
    outputModel.taxType = options[index];
    notifyListeners();
    Navigator.pop(context);
    //  Get.back();
  }

  onSave() {
    // preSelectedOption.value = selectedOption.value;
    formKey.currentState!.save();
    postingTax();
    //if (formKey.currentState.validate()) {}
    // taxModel = TaxModel(rate, isInclusiveToggle.value, selectedOption.value);
    // postingTax();
    //  notifyListeners();
  }

  Future postingTax() async {
    homeController.userDefault.taxType = outputModel.taxType;
    homeController.userDefault.taxRate = outputModel.taxRate;
    homeController.userDefault.taxInclusive = outputModel.taxInclusive;

    Request req = {
      "tax_type": homeController.userDefault.taxType,
      "tax_rate": homeController.userDefault.taxRate,
      "tax_inclusive": homeController.userDefault.taxInclusive
    };
    // num? rate;
    // bool isInclusiveToggle;
    // String selectedOption;

    APIResponse resp = await ApiClient.post(
        request: req, endPoint: "/userdefaults", fromJson: null);
    debugPrint("$resp");
  }

  // savePaymentInstructions(bool callAPI) async {
  //   formKey.currentState!.save();

  //   if (callAPI) {
  //     APIResponse? resp = await ApiClient.post<NewTaxVM>(
  //         request: outputModel.toMap(),
  //         endPoint: "/paymentinstructions",
  //         fromJson: null);
  //     setBusy(false);
  //   }
  // notifyListeners();
}

//  updateOptions(int index, BuildContext context) {
//     // preSelectedOption.value = selectedOption.value;
//     selectedOption.value = options[index];
//     Navigator.pop(context);
//     //  Get.back();
//   }

 




  // getPaymentInstructions() async {
  //   APIResponse resp = await ApiClient.get<PaymentInstructions>(
  //       endPoint: "/paymentinstructions",
  //       params: "",
  //       fromJson: PaymentInstructions.fromMap);
  //   paymentInstuctions = resp["data"];
  //   notifyListeners();
  // }