import 'package:flutter/material.dart';
import '../new_tax_vm.dart';

// import '../tax_model.dart';

class OnPerItemWidget extends StatelessWidget {
  NewTaxVM vm;

  OnPerItemWidget(this.vm, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Column(
        children: [
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                      flex: 10,
                      child: Text(
                        'Inclusive',
                        style: TextStyle(
                            fontSize: 13.0, fontWeight: FontWeight.w400),
                      )),
                  Expanded(
                      flex: 1,
                      child: Transform.scale(
                        scale: 0.8,
                        child: Switch(
                          value: vm.outputModel.taxInclusive,
                          activeColor: Colors.orange[700],
                          onChanged: (value) {
                            vm.onSignToggle();
                          },
                        ),
                      ))
                ],
              ),
              Row(
                children: [
                  Text(
                    'Turn on if price already included Tax',
                    style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
