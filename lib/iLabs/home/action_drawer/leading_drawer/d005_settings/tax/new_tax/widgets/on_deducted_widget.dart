import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../new_tax_vm.dart';
import '../../../../../../../widgets/input/chi_text_field.dart';

// import '../tax_model.dart';

class OnDeductedWidget extends StatelessWidget {
  NewTaxVM vm;
  // TaxController viewModel = Get.find();
  OnDeductedWidget(this.vm, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(flex: 2, child: Text('Rate:')),
              Expanded(
                  flex: 1,
                  child: TextFormField(
                    //  controller: viewModel.userInput,
                    initialValue: vm.outputModel.taxRate,
                    autofocus: false,
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.end,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      //labelText: viewModel.percentage.toString(),
                      hintText: '0' + '%',
                    ),
                    onSaved: (String? value) {
                      vm.outputModel.taxRate = value!;
                      // print(viewModel.rate);
                    },
                  ))
            ],
          )

          // Row(
          //   children: [
          //     Expanded(
          //         child: chiNumberFormField(
          //       fieldType: TextInputType.phone,
          //       inputFormat: <TextInputFormatter>[
          //         FilteringTextInputFormatter(RegExp(r'^\d+\.?\d{0,100}'),
          //             allow: true),
          //       ],
          //       heading: 'Rate',
          //       hintText: vm.outputModel.taxRate + '%',
          //       func: (value) {
          //         vm.outputModel.taxRate = value;
          //       },
          //       initialValue: vm.outputModel.taxRate,
          //     ))
          //   ],
          // ),
        ],
      ),
    );
  }
}
