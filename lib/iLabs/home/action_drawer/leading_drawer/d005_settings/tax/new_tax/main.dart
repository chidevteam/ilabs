import 'package:flutter/material.dart';
import 'new_tax_vu.dart';
import '../../../../../../models/tax/model.dart';
import '../../../../../../models/tax/test/response_none.dart';
import '../../../../../../models/tax/test/response_on_the_total.dart';
import '../../../../../../models/tax/test/response_deducted.dart';
import '../../../../../../models/tax/test/response_per_item.dart';

void main() {
  debugPrint("Starting Item View");

  // String email = "ahmadhassanch@hotmail.com";
  // String password = "test1234";
  // APIResponse resp = await ApiClient.login(email, password);
  // debugPrint("$resp");

  runApp(MaterialApp(
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  TaxModel model1 = TaxModel.fromMap(response1);
  TaxModel model2 = TaxModel.fromMap(response2);
  TaxModel model3 = TaxModel.fromMap(response3);
  TaxModel model4 = TaxModel.fromMap(response4);
  @override
  Widget build(BuildContext context) {
    // PaymentInstructions pInst = PaymentInstructions();
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.orange),
        //    home: const PaymentInstructionVU(null));

        home: Scaffold(
          body: Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: () {
                      // print('object11');
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NewTaxVU(model1),
                          )).then((value) {
                        model1 = value;
                      });
                    },
                    child: Text("None")),
                // ElevatedButton(
                //     onPressed: () {
                //       Navigator.push(
                //           context,
                //           MaterialPageRoute(
                //               builder: (context) => NewTaxVU(model2)));
                //     },
                //     child: Text("On The Totel")),
                // ElevatedButton(
                //     onPressed: () {
                //       Navigator.push(
                //           context,
                //           MaterialPageRoute(
                //               builder: (context) => NewTaxVU(model3)));
                //     },
                //     child: Text("Deducted")),
                // ElevatedButton(
                //     onPressed: () {
                //       Navigator.push(
                //           context,
                //           MaterialPageRoute(
                //               builder: (context) => NewTaxVU(model4)));
                //     },
                //     child: Text("Per Item")),
              ],
            ),
          ),
        ));
  }
}
