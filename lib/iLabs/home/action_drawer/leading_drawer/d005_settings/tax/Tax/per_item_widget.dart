import 'package:flutter/material.dart';
import 'package:get/get.dart';

// import '../tax_model.dart';
import 'tax_vu_controller.dart';

class OnPerItemWidget extends StatelessWidget {
  // TaxModel? taxModel;
  TaxController viewModel = Get.find();
  OnPerItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Column(
        children: [
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                      flex: 10,
                      child: Text(
                        'Inclusive',
                        style: TextStyle(
                            fontSize: 13.0, fontWeight: FontWeight.w400),
                      )),
                  Expanded(
                      flex: 1,
                      child: Transform.scale(
                        scale: 0.8,
                        child: Obx(
                          () => Switch(
                            value: viewModel.isInclusiveToggle.value,
                            activeColor: Colors.orange[700],
                            onChanged: (value) {
                              viewModel.onSignToggle();
                            },
                          ),
                        ),
                      ))
                ],
              ),
              Row(
                children: [
                  Text(
                    'Turn on if price already included Tax',
                    style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
