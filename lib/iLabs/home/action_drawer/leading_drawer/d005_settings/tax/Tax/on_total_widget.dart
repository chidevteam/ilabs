import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../../../../../widgets/input/chi_text_field.dart';
import 'package:intl/intl.dart';

import '../tax_model.dart';
import 'tax_vu_controller.dart';

class OnTheTotalWidget extends StatelessWidget {
  OldTaxModel? taxModel;
  TaxController viewModel = Get.find();
  OnTheTotalWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: chiNumberFormField(
                fieldType: TextInputType.phone,
                inputFormat: <TextInputFormatter>[
                  FilteringTextInputFormatter(RegExp(r'^\d+\.?\d{0,100}'),
                      allow: true),
                ],
                heading: 'Rate',
                hintText: viewModel.rate.value.toString(),
                func: viewModel.onPercentage,
                initialValue: viewModel.rate.value.toString(),
              ))
            ],
          ),
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                      flex: 10,
                      child: Text(
                        'Inclusive',
                        style: TextStyle(
                            fontSize: 13.0, fontWeight: FontWeight.w400),
                      )),
                  Expanded(
                    flex: 1,
                    child: Obx(
                      () => Switch(
                        value: viewModel.isInclusiveToggle.value,
                        activeColor: Colors.orange[700],
                        onChanged: (value) {
                          viewModel.onSignToggle();
                        },
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'Turn on if price already included Tax',
                    style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 13.0,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
