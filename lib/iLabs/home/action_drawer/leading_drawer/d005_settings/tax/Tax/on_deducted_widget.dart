import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../../../../../widgets/input/chi_text_field.dart';

// import '../tax_model.dart';
import 'tax_vu_controller.dart';

class OnDeductedWidget extends StatelessWidget {
  // TaxModel? taxModel;
  TaxController viewModel = Get.find();
  OnDeductedWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: chiNumberFormField(
                fieldType: TextInputType.phone,
                inputFormat: <TextInputFormatter>[
                  FilteringTextInputFormatter(RegExp(r'^\d+\.?\d{0,100}'),
                      allow: true),
                ],
                heading: 'Rate',
                hintText: viewModel.rate.toString() + '%',
                func: viewModel.onPercentage,
                initialValue: viewModel.rate.value.toString(),
              ))
            ],
          ),
        ],
      ),
    );
  }
}
