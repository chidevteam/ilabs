import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../home_controller.dart';
import '../../../../../../services/api_client.dart';
import 'dart:async';
// import '../tax_model.dart';

class TaxController extends GetxController {
  final formKey = GlobalKey<FormState>();
  HomeController homeController = Get.find<HomeController>();
  RxDouble rate = 0.0.obs;
  RxBool isInclusiveToggle = false.obs;
  // TaxModel? taxModel;
  RxList<String> options = ['None', 'On The Total', 'Deducted', 'Per Item'].obs;
  RxString selectedOption = 'None'.obs;
  RxString preSelectedOption = 'None'.obs;

  TaxController() {
    selectedOption.value = homeController.userDefault.taxType!;
    rate.value = double.parse(homeController.userDefault.taxRate!);
    isInclusiveToggle.value = homeController.userDefault.taxInclusive!;
  }

  updateOptions(int index, BuildContext context) {
    // preSelectedOption.value = selectedOption.value;
    selectedOption.value = options[index];
    Navigator.pop(context);
    //  Get.back();
  }

  onPercentage(String? value) {
    rate.value = double.parse(value!.trim());
    //  notifyListeners();
  }

  onSave() {
    preSelectedOption.value = selectedOption.value;
    formKey.currentState!.save();
    // taxModel = TaxModel(rate, isInclusiveToggle.value, selectedOption.value);
    postingTax();
    //  notifyListeners();
  }

  onSignToggle() {
    isInclusiveToggle.value = !isInclusiveToggle.value;
    // notifyListeners();
  }

  Future postingTax() async {
    homeController.userDefault.taxType = selectedOption.value;
    homeController.userDefault.taxRate = rate.toString();
    homeController.userDefault.taxInclusive = isInclusiveToggle.value;

    Request req = {
      "tax_type": homeController.userDefault.taxType,
      "tax_rate": homeController.userDefault.taxRate.toString(),
      "tax_inclusive": homeController.userDefault.taxInclusive
    };
    // num? rate;
    // bool isInclusiveToggle;
    // String selectedOption;

    APIResponse resp = await ApiClient.post(
        request: req, endPoint: "/userdefaults", fromJson: null);
    debugPrint("$resp");
  }
}
