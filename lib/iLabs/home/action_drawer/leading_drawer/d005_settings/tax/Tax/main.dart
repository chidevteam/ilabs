import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import '../tax_model.dart';
import 'tax_vu.dart';
import '../../../../../../services/api_client.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.orange),
      home: TaxViewScreen(null),
    );
  }
}

void main() async {
  //
  await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
  debugPrint("Starting Item View");

  debugPrint("Starting3");
  runApp(const MyApp());
}
