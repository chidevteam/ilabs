import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'per_item_widget.dart';

import '../tax_model.dart';
import 'on_deducted_widget.dart';
import 'tax_vu_controller.dart';
import 'on_total_widget.dart';

class TaxViewScreen extends StatelessWidget {
  OldTaxModel? taxModel;
  TaxController controller = Get.put(TaxController());
  TaxViewScreen(this.taxModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TaxController taxController = Get.find();
    // print(controller.selectedOption.toString());
    // print(controller.preSelectedOption.toString());

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Tax',
            style: TextStyle(
                color: Colors.black,
                fontSize: 16.0,
                fontWeight: FontWeight.w400),
          ),
          leading: IconButton(
            onPressed: () {
              // print(controller.selectedOption.toString());

              controller.selectedOption.value =
                  controller.preSelectedOption.value;
              Navigator.pop(
                context,
              );
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.orange[800],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                controller.onSave();
                Navigator.pop(context);
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.orange[800], fontSize: 16.0),
              ),
            ),
          ]),
      body: WillPopScope(
        onWillPop: () async {
          controller.selectedOption.value = controller.preSelectedOption.value;
          Navigator.pop(
            context,
          );
          return true;
        },
        child: Form(
          key: controller.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  controller.updateOptions(0, context);
                                  // OnTheTotalWidget();
                                  // Navigator.pop(context);
                                },
                                child: Text(
                                  controller.options[0],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  controller.updateOptions(1, context);
                                },
                                child: Text(
                                  controller.options[1],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  controller.updateOptions(2, context);
                                },
                                child: Text(
                                  controller.options[2],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: TextButton(
                                onPressed: () {
                                  controller.updateOptions(3, context);
                                },
                                child: Text(
                                  controller.options[3],
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      });
                },
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Tax',
                              style: TextStyle(
                                  color: Colors.grey[700],
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w400),
                            ),
                            Obx(
                              () => Visibility(
                                visible: true,
                                child: Text(
                                  controller.selectedOption.value,
                                  style: TextStyle(
                                      color: Colors.grey[700],
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Obx(() => Container(
                  child: controller.selectedOption.value == 'None'
                      ? (Container())
                      : controller.selectedOption.value == 'On The Total'
                          ? (OnTheTotalWidget())
                          : controller.selectedOption.value == 'Deducted'
                              ? (OnDeductedWidget())
                              : (OnPerItemWidget()))),
            ],
          ),
        ),
      ),
    );
  }
}
