class OldTaxModel {
  num? rate;
  bool isInclusiveToggle;
  String selectedOption;
  OldTaxModel(this.rate, this.isInclusiveToggle, this.selectedOption);
}
