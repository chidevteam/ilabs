import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'default_email_message_model.dart';
import '../../../../../services/api_client.dart';

class DefaultEmailMessageViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  DefaultEmailMessage? inputModel;
  DefaultEmailMessage outputModel = DefaultEmailMessage();

  DefaultEmailMessageViewModel(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  onSave(bool callAPI) async {
    formKey.currentState!.save();

    if (callAPI) {
      APIResponse? resp = await ApiClient.post(
          request: outputModel.toMap(),
          endPoint: "/userdefaults",
          fromJson: DefaultEmailMessage.fromMap);
      debugPrint("${resp['data']}");
    }
    notifyListeners();
  }
}
