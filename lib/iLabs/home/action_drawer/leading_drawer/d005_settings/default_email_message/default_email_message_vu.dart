import 'package:flutter/material.dart';
import '../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';

import '../../../../../widgets/input/chi_textfield.dart';
import 'default_email_message_vm.dart';
import 'default_email_message_model.dart';

class DefaultEmailMessageScreen
    extends ViewModelBuilderWidget<DefaultEmailMessageViewModel> {
  const DefaultEmailMessageScreen(this.eMessage,
      {Key? key, this.callAPI = true})
      : super(key: key);
  final DefaultEmailMessage? eMessage;
  final bool callAPI;

  @override
  Widget builder(BuildContext context, DefaultEmailMessageViewModel viewModel,
      Widget? child) {
    return Scaffold(
      appBar: chiAppBar('Default Email Message', context),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CHITextField(
                    heading: 'Email Message',
                    hintText: 'Type Email Message',
                    maxLines: 10,
                    func: (String value) =>
                        viewModel.outputModel.message = value,
                    initialValue: viewModel.outputModel.message,
                    // maxline: 5,
                    validator: null),
                chiSaveButton('SAVE AS DEFAULT', () {
                  viewModel.onSave(callAPI);
                  Navigator.pop(context, viewModel.outputModel);
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  DefaultEmailMessageViewModel viewModelBuilder(BuildContext context) {
    DefaultEmailMessageViewModel vm = DefaultEmailMessageViewModel(eMessage);
    return vm;
  }
}
