class DefaultEmailMessage {
  String message;

  DefaultEmailMessage({this.message = ""});

  static DefaultEmailMessage fromMap(Map<String, dynamic> resp) {
    return DefaultEmailMessage(
      message: resp['email_message'] ?? "",
    );
  }

  Map<String, dynamic> toMap() {
    return {"email_message": message};
  }

  DefaultEmailMessage copy() {
    return fromMap(toMap());
  }

  @override
  String toString() {
    String st = "=== $runtimeType ===\n";
    Map map = toMap();
    for (String key in map.keys) {
      st += "$key :: ${map[key]}\n";
    }
    return st;
  }
}
