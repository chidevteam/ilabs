import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/home/action_drawer/leading_drawer/d005_settings/default_email_message/default_email_message_model.dart';
import 'package:ilabs/iLabs/models/userdefaults/model.dart';
import 'package:stacked/stacked.dart';

import 'package:easy_localization/easy_localization.dart';

import 'package:flutter_svg/flutter_svg.dart';
import '../../../../Utils/utils.dart';
import '../../../../language/generated/locale_keys.g.dart';
import 'tax/Tax/tax_vu.dart';

import '../../../../models/due_days/due_days_vu.dart';
import '../../../../models/due_days/due_days_model.dart';

import 'customize/customize_vu.dart';
import 'customize/customize_model.dart';

import 'default_email_message/default_email_message_vu.dart';
import '../../../../models/default_notes/default_notes_form_vu.dart';
import '../../../../models/default_notes/model.dart';
import 'invoice_estimate_number/invoice_number_vu.dart';
import 'invoice_estimate_number/invoice_number_model.dart';
import './tax/new_tax/new_tax_vu.dart';
import 'signature/signature_vu.dart';

import 'package:get/get.dart';
import '../../../home_controller.dart';

import '../../../../models/payment_instructions/payment_instruction_form-vu.dart';
import 'setting_vm.dart';
import 'dart:io';

// ignore: must_be_immutable
class SettingScreen extends ViewModelBuilderWidget<SettingViewModel> {
  SettingScreen({Key? key}) : super(key: key) {
    debugPrint("Setting screen called");
  }
  HomeController homeCont = Get.find<HomeController>();
  UserDefault? userDefault;
  @override
  Widget builder(
      BuildContext context, SettingViewModel viewModel, Widget? child) {
    userDefault = homeCont.userDefault;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          tr(LocaleKeys.action_drawer_view_settigs_screen_app_bar_title),
          style: const TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
            viewModel.notifyListeners();
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.orange[800],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              signaturePic(context, viewModel),
              paymentInstCell(context, viewModel),
              taxCell(context, viewModel),
              dueDateCell(context, viewModel),
              notesCell(context, viewModel),
              signatureCell(context, viewModel),
              invoiceEstimateNoCell(context, viewModel),
              invoiceEstimateTitleCell(context, viewModel),
              defaultEmailMessageCell(context, viewModel),
              sendMeCopiesCell(context, viewModel),
            ],
          ),
        ),
      ),
    );
  }

  // Image.network(SERVER_URL + '/' + homeCont.userDefault.signature!));
  Widget signaturePic(BuildContext context, SettingViewModel viewModel) {
    if (viewModel.homeCtrl.signatureFilePath != null)
      print("=============+++" + viewModel.homeCtrl.signatureFilePath!);
    return Container(
        width: 200,
        height: 200,
        child: viewModel.homeCtrl.signatureFilePath == null
            ? const Text("No Image")
            : Image.file(File(viewModel.homeCtrl.signatureFilePath!)));
    //viewModel.image == null ? Text("No Image") : Text("Image"));
    // ?
    // homeCont.userDefault.signature != ""
    //     ? Image.network(Utils.imageURl(homeCont.userDefault.signature!))
    //     : const Center(child: Text("no signature yet"))
    // : UIImage(viewModel.image!));
  }

  Widget paymentInstCell(BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_payment_inst),
        iconPath: 'iLabsSreens/settings/Payment Instructions.svg',
        tapCallBack: () {
          Utils.push(
                  context,
                  PaymentInstructionFormVU(homeCont.paymentInst!,
                      callAPI: true))
              .then((value) {
            if (value != null) {
              viewModel.homeCtrl.paymentInst = value;
              viewModel.notifyListeners();
            }
          });
        });
  }

  Widget notesCell(BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_default_notes),
        iconPath: 'iLabsSreens/settings/due in days.svg',
        tapCallBack: () {
          DefaultNote dNote = DefaultNote(
              invoiceNote: viewModel.homeCtrl.userDefault.invoiceNote ?? "",
              estimateNote: viewModel.homeCtrl.userDefault.estimateNote ?? "");
          Utils.push(context, DefaultNoteFormVU(dNote)).then((value) {
            if (value != null) {
              // viewModel.defaultNote = value;
              viewModel.homeCtrl.userDefault.invoiceNote = value.invoiceNote;
              viewModel.homeCtrl.userDefault.estimateNote = value.estimateNote;
            }
          });
        });
  }

  Widget taxCell(BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_tax),
        iconPath: 'iLabsSreens/settings/Default Noties.svg',
        tapCallBack: () {
          Utils.push(context, NewTaxVU(homeCont.taxModel)).then((value) {
            if (value != null) {
              // viewModel.taxModel = value;
              homeCont.taxModel = value;
              // viewModel.homeCtrl.userDefault.taxLabel =
              //     viewModel.taxModel!.selectedOption;
              // viewModel.homeCtrl.userDefault.taxRate =
              //     viewModel.taxModel!.rate.toString();
              // viewModel.homeCtrl.userDefault.taxInclusive =
              //     viewModel.taxModel!.isInclusiveToggle;
            }
            //   viewModel.taxModel = value;
            //   viewModel.notifyListeners();
            // });
          });
        });
  }

  Widget dueDateCell(BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        endWidget: Text('${viewModel.homeCtrl.userDefault.dueDate}'),
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_due_in_days),
        iconPath: 'iLabsSreens/settings/due in days.svg',
        tapCallBack: () {
          UserDefault userDefault = viewModel.homeCtrl.userDefault;
          DueDaysModel dModel = DueDaysModel(dueDays: userDefault.dueDate!);
          Utils.push(context, DueDaysVU(dModel)).then((value) {
            if (value != null) {
              userDefault.dueDate = value.dueDays;
              viewModel.notifyListeners();
            }
          });
        });
  }

  Widget signatureCell(BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_dafault_sign),
        iconPath: 'iLabsSreens/settings/default signature.svg',
        tapCallBack: () {
          Utils.push(context, DefaultSignatureScreen('fromSettings', null))
              .then((value) async {
            if (value != null) {
              // viewModel.image = value;
              viewModel.uploadSignature(value);
            }
            viewModel.notifyListeners();
          });
        });
  }

  Widget invoiceEstimateNoCell(
      BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_invoice_no),
        iconPath: 'iLabsSreens/settings/Invoice Number.svg',
        tapCallBack: () {
          InvoiceEstimateNo invoiceEstimateNo = InvoiceEstimateNo(
            invoiceNumber: userDefault!.invoiceNumber!,
            estimateNumber: userDefault!.estimateNumber!,
            // invoiceCount: userDefault!.invoiceCount!,
            // estimateCount: userDefault!.estimateCount!
          );

          Utils.push(context, InvoiceEstimateScreen(invoiceEstimateNo))
              .then((value) {
            if (value != null) {
              userDefault!.invoiceNumber = value.invoiceNumber;
              userDefault!.estimateNumber = value.estimateNumber;
            }
          });
        });
  }

  Widget invoiceEstimateTitleCell(
      BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title: tr(LocaleKeys.action_drawer_view_settigs_screen_customize),
        iconPath: 'iLabsSreens/settings/Default Email Message.svg',
        tapCallBack: () {
          Customize customize = Customize(
              invoicetitle: userDefault!.invoiceTitle!,
              estimatetitle: userDefault!.estimateTitle!,
              businessnumber: userDefault!.businessNumber!);
          Utils.push(context, CustomizeScreen(customize)).then((value) {
            if (value != null) {
              userDefault!.invoiceTitle = value.invoiceTitle;
              userDefault!.estimateTitle = value.estimateTitle;
              userDefault!.businessNumber = value.businessNumber;
            }
          });
        });
  }

  Widget defaultEmailMessageCell(
      BuildContext context, SettingViewModel viewModel) {
    return settingCell(
        title:
            tr(LocaleKeys.action_drawer_view_settigs_screen_dafault_email_msg),
        iconPath: 'iLabsSreens/settings/Customize.svg',
        tapCallBack: () {
          DefaultEmailMessage eMsg = DefaultEmailMessage(
              message: viewModel.homeCtrl.userDefault.emailMessage ?? "");
          Utils.push(context, DefaultEmailMessageScreen(eMsg)).then((value) {
            if (value != null) {
              viewModel.homeCtrl.userDefault.emailMessage = value.message;
            }
          });
        });
  }

  Widget sendMeCopiesCell(BuildContext context, SettingViewModel viewModel) {
    bool sendCopy = viewModel.homeCtrl.userDefault.emailCopy!;
    return settingCell(
        endWidget: Switch(
            value: sendCopy,
            onChanged: (val) {
              viewModel.homeCtrl.userDefault.emailCopy =
                  !viewModel.homeCtrl.userDefault.emailCopy!;
              viewModel.notifyListeners();
              viewModel
                  .setSendEmailCopy(viewModel.homeCtrl.userDefault.emailCopy!);
            }),
        // sendCopy == true ? const Icon(Icons.done) : const Icon(Icons.close),
        title: tr(LocaleKeys
            .action_drawer_view_settigs_screen_send_me_copy_of_emails),
        iconPath: 'iLabsSreens/settings/Send me copy of emails.svg',
        tapCallBack: () {
          // viewModel.homeCtrl.userDefault.emailCopy =
          //     !viewModel.homeCtrl.userDefault.emailCopy!;
          // viewModel.notifyListeners();
          // viewModel.setSendEmailCopy(viewModel.homeCtrl.userDefault.emailCopy!);
          // print("viewModel.homeController.userDefault.emailCopy");
        });
  }

  Widget settingCell({
    String? title,
    Widget? endWidget,
    String? iconPath,
    void Function()? tapCallBack,
  }) {
    return InkWell(
      onTap: tapCallBack,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 0.7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
          child: Row(
            children: [
              SvgPicture.asset(
                iconPath!,
                color: Colors.black54,
              ),
              const SizedBox(
                width: 8.0,
              ),
              Text(
                title!,
                style: const TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.black54),
              ),
              const Spacer(),
              endWidget ?? Container()
            ],
          ),
        ),
      ),
    );
  }

  @override
  SettingViewModel viewModelBuilder(BuildContext context) {
    SettingViewModel vm = SettingViewModel();
    // vm.initialize();
    return vm;
  }
}
