import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';

import 'package:stacked/stacked.dart';

import '../../../../home_controller.dart';

class NotesViewModel extends BaseViewModel {
  bool shouldSaveAsDraft = false;
  final formKey = GlobalKey<FormState>();
  String? paymentNotes  = "";
  NotesViewModel(String? notes){
  paymentNotes = notes??Get.find<HomeController>().userDefault.invoiceNote;
  }

  // PaymentInstructionNotes? paymentInstructionNotes;
  saveAsDraftSwitch() {
    shouldSaveAsDraft = !shouldSaveAsDraft;
    notifyListeners();
  }

  onSaveNote(value) {
    paymentNotes = value;
  }

  onAddNow() {
    formKey.currentState!.save();
    if(shouldSaveAsDraft)
    {
      HomeController homeController = Get.find<HomeController>();
    homeController.updateDefaultInvoiceNotes(paymentNotes??"");
    }
    notifyListeners();
  }
}
