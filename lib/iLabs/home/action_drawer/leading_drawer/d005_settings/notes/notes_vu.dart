import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';

import 'package:stacked/stacked.dart';

import 'notes_vm.dart';

class NotesScreen extends ViewModelBuilderWidget<NotesViewModel> {
   NotesScreen( this.paymentNotes,{Key? key}) : super(key: key);
String? paymentNotes ;
  @override
  Widget builder(
      BuildContext context, NotesViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Notes',
          style: TextStyle(
              color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.orange[800],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 18.0, right: 14.0, left: 14.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Notes',
                  style: TextStyle(
                      fontSize: 13.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.grey),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                typeHere(viewModel, context),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Row(
                    children: [
                      const Text(
                        'Save as Draft',
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey),
                      ),
                      Transform.scale(
                        scale: 0.8,
                        child: Switch(
                          value: viewModel.shouldSaveAsDraft,
                          activeColor: Colors.orange[700],
                          onChanged: (value) {
                            viewModel.saveAsDraftSwitch();
                          },
                        ),
                      )
                    ],
                  ),
                ),
                addNowButton(context, viewModel)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget typeHere(NotesViewModel viewModel, context) {
    return TextFormField(
      minLines: 4,
      maxLines: null,
      initialValue: viewModel.paymentNotes,
      onSaved: viewModel.onSaveNote,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Type here',
        hintStyle: TextStyle(
            fontWeight: FontWeight.w500, fontSize: 13, color: Colors.grey[500]),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(
              color: Colors.orange,
            )),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey.shade300),
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
      ),
    );
  }

  Widget addNowButton(BuildContext context, NotesViewModel viewModel) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0),
      child: SizedBox(
        width: double.infinity,
        height: 42.0,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.orange[600],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          onPressed: () {
            viewModel.onAddNow();
            Navigator.pop(context, viewModel.paymentNotes);
          },
          child: const Text(
            'ADD NOW',
            style: TextStyle(
                color: Colors.white,
                fontSize: 12.0,
                fontWeight: FontWeight.normal),
          ),
        ),
      ),
    );
  }

  @override
  NotesViewModel viewModelBuilder(BuildContext context) {
    return NotesViewModel(paymentNotes);
  }
}
