import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import 'package:stacked/stacked.dart';
import 'package:signature/signature.dart';
// import 'signature_canvas.dart';
import '../../../../app_constants.dart';
import '../../../../home_controller.dart';
import '../setting_vm.dart';
import 'dart:io';
import '../../../../../Utils/utils.dart';
import 'package:get/get.dart';

class DefaultSignatureScreen extends ViewModelBuilderWidget<SettingViewModel> {
  InvoiceData? invoiceData;
  String? previousVuName;
  DefaultSignatureScreen(this.previousVuName, this.invoiceData, {Key? key})
      : super(key: key) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    print(previousVuName);
    print(invoiceData);
  }
  SignatureController? controller;

  @override
  Widget builder(
      BuildContext context, SettingViewModel viewModel, Widget? child) {
    bool saveAsdefaultValue;
    return Scaffold(
        body: WillPopScope(
      onWillPop: () {
        setOrientationToLandScape();
        Navigator.pop(context, null);
        return Future(() => false);
      },
      child: SafeArea(
          child: Column(
        children: [buttonRow1(context, viewModel), signature()],
      )),
    ));
  }

  Widget buttonRow() {
    return const Text("here");
  }

  Widget buttonRow1(BuildContext context, SettingViewModel viewModel) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(3.0),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: signatureButton(
                "Back",
                Icons.arrow_back,
                () {
                  setOrientationToLandScape();
                  Navigator.pop(context, null);
                },
                const Color(0xffFFBC80),
              ),
            ),
            Expanded(
                flex: 1,
                child: signatureButton(
                  "Clear",
                  Icons.clear,
                  () {
                    // print("Clear");
                    controller!.clear();
                  },
                  const Color(0xffFFBC80),
                )),
            Expanded(
              flex: 1,
              child: signatureButton(
                "Save",
                Icons.save,
                () async {
                  debugPrint("Save");
                  controller!.toImage().then((uiImg) async {
                    debugPrint("$uiImg");

                    HomeController homeCtrl = Get.find<HomeController>();
                    String dt =
                        DateTime.now().millisecondsSinceEpoch.toString();
                    String tempPath =
                        homeCtrl.applicationDir + '/$dt' + SIGNATURE_FILE_NAME;
                    File signatureFile =
                        await Utils.imageToFile2(tempPath, uiImg!);
                    if (previousVuName == 'fromInvoice' &&
                        viewModel.saveAsDefault) {
                      viewModel.uploadSignatureFromInvoice(
                          signatureFile.path, invoiceData!);
                    }
                    Navigator.pop(context, signatureFile.path);
                  });
                  setOrientationToLandScape();
                },
                const Color(0xffE65100),
              ),
            ),
            previousVuName == 'fromInvoice'
                ? Expanded(
                    flex: 1,
                    child: saveAsDefaultButton(
                      viewModel,
                      "Save as Default",
                      () {
                        // print("Clear");
                        // controller!.clear();
                      },
                      const Color(0xffffffff),
                    ))
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget signatureButton(String title, IconData iconData, void Function() func,
      Color containerColor) {
    return InkWell(
      onTap: func,
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Container(
            height: double.infinity,
            decoration: BoxDecoration(
                color: containerColor,
                borderRadius: const BorderRadius.all(Radius.circular(8))),
            // width: double.infinity,

            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(iconData),
                Text(title),
                const Text(""),
              ],
            )),
      ),
    );
  }

  Widget saveAsDefaultButton(SettingViewModel vm, String title,
      void Function() func, Color containerColor) {
    return InkWell(
      onTap: func,
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Container(
            height: double.infinity,
            decoration: BoxDecoration(
                color: containerColor,
                borderRadius: const BorderRadius.all(Radius.circular(8))),
            // width: double.infinity,

            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //  Icon(iconData),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(title),
                ),
                Switch(
                  value: vm.saveAsDefault,
                  activeColor: Colors.orange[700],
                  onChanged: (value) {
                    vm.saveAsDefault = value;
                    vm.notifyListeners();
                  },
                ),
              ],
            )),
      ),
    );
  }

  Widget signature() {
    controller = SignatureController(
        penStrokeWidth: 3,
        penColor: Colors.black,
        exportBackgroundColor: Colors.white,
        onDrawStart: () => debugPrint('onDrawStart called!'),
        onDrawEnd: () => debugPrint('onDrawEnd called!'));

    controller!.addListener(() => debugPrint('Value changed'));

    return Expanded(
      flex: 5,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Signature(
          controller: controller!,
          height: double.infinity,
          backgroundColor: Colors.white,
        ),
      ),
    );
  }

  setOrientationToLandScape() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  SettingViewModel viewModelBuilder(BuildContext context) {
    return SettingViewModel();
  }
}
