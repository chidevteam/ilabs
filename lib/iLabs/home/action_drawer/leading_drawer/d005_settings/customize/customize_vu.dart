import 'package:flutter/material.dart';
import '../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import '../../../../../widgets/input/chi_textfield.dart';
import 'customize_model.dart';
import 'customize_vm.dart';

class CustomizeScreen extends ViewModelBuilderWidget<CustomizeVM> {
  const CustomizeScreen(this.inputModel, {Key? key, this.callAPI = true})
      : super(key: key);
  final Customize? inputModel;
  final bool callAPI;

  @override
  Widget builder(BuildContext context, CustomizeVM viewModel, Widget? child) {
    Customize outModel = viewModel.outputModel;
    return Scaffold(
      appBar: chiAppBar('Customize', context),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CHITextField(
                    heading: 'Invoice Title',
                    hintText: 'Invoice',
                    func: (value) => outModel.invoicetitle = value,
                    initialValue: outModel.invoicetitle,
                    validator: null),
                CHITextField(
                    heading: 'Estimates Title',
                    hintText: 'Estimates',
                    func: (value) => outModel.estimatetitle = value,
                    initialValue: outModel.estimatetitle,
                    validator: null),
                CHITextField(
                    heading: 'Business Number',
                    hintText: 'Business',
                    func: (value) => outModel.businessnumber = value,
                    initialValue: outModel.businessnumber,
                    validator: null),
                chiSaveButton('SAVE AS DEFAULT', () {
                  viewModel.onSave(callAPI);
                  Navigator.pop(context, outModel);
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  CustomizeVM viewModelBuilder(BuildContext context) {
    return CustomizeVM(inputModel);
  }
}
