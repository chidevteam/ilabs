import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'customize_model.dart';
import '../../../../../services/api_client.dart';

class CustomizeVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  Customize? inputModel;
  Customize outputModel = Customize();

  CustomizeVM(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  onSave(bool callAPI) async {
    formKey.currentState!.save();

    if (callAPI) {
      APIResponse? resp = await ApiClient.post(
          request: outputModel.toMap(),
          endPoint: "/userdefaults",
          fromJson: Customize.fromMap);
      debugPrint("$resp['data']}");
    }
    notifyListeners();
  }
}
