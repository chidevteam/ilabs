class Customize {
  String invoicetitle;
  String estimatetitle;
  String businessnumber;

  Customize(
      {this.invoicetitle = "",
      this.estimatetitle = "",
      this.businessnumber = ""});

  static Customize fromMap(Map<String, dynamic> resp) {
    return Customize(
      invoicetitle: resp['invoice_title'] ?? "",
      estimatetitle: resp['estimate_title'] ?? "",
      businessnumber: resp['business_number'] ?? "",
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'invoice_title': invoicetitle,
      'estimate_title': estimatetitle,
      'business_number': businessnumber,
    };
  }

  Customize copy() {
    return fromMap(toMap());
  }

  @override
  String toString() {
    String st = "=== $runtimeType ===\n";
    Map map = toMap();
    for (String key in map.keys) {
      st += "$key :: ${map[key]}\n";
    }
    return st;
  }
}
