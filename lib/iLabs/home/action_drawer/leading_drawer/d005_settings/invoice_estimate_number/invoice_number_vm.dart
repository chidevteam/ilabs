import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'invoice_number_model.dart';

class InvoiceEstimateViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  InvoiceEstimateNo? inputModel;
  InvoiceEstimateNo outputModel = InvoiceEstimateNo();

  InvoiceEstimateViewModel(this.inputModel) {
    if (inputModel != null) {
      outputModel = inputModel!.copy();
    }
  }

  onSave() {
    formKey.currentState!.save();
    notifyListeners();
  }
}
