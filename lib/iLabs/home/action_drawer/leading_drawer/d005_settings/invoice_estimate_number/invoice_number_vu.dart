import 'package:flutter/material.dart';
import '../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';

import '../../../../../widgets/input/chi_textfield.dart';
import 'invoice_number_model.dart';
import 'invoice_number_vm.dart';

class InvoiceEstimateScreen
    extends ViewModelBuilderWidget<InvoiceEstimateViewModel> {
  final InvoiceEstimateNo? tempInputModel;
  const InvoiceEstimateScreen(this.tempInputModel, {Key? key})
      : super(key: key);

  @override
  Widget builder(
      BuildContext context, InvoiceEstimateViewModel viewModel, Widget? child) {
    InvoiceEstimateNo model = viewModel.outputModel;
    return Scaffold(
      appBar: chiAppBar('Invoice Number', context),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CHITextField(
                    heading: 'Invoice Number',
                    hintText: 'Invoice',
                    func: (value) => model.invoiceNumber = value,
                    initialValue: model.invoiceNumber,
                    // maxline: 1,
                    validator: null),
                CHITextField(
                    heading: 'Estimates Number',
                    hintText: 'Estimates',
                    func: (value) => model.estimateNumber = value,
                    initialValue: model.estimateNumber,
                    // maxline: 1,
                    validator: null),
                chiSaveButton('SAVE AS DEFAULT', () {
                  viewModel.onSave();
                  Navigator.pop(context, model);
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  InvoiceEstimateViewModel viewModelBuilder(BuildContext context) {
    return InvoiceEstimateViewModel(tempInputModel);
  }
}
