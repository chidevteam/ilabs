import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilabs/iLabs/models/invoice_data/model.dart';
import 'package:stacked/stacked.dart';
import '../../../../widgets/input/chi_text_field.dart';
// import '../../../../widgets/app_bars/chi_form_app_bar.dart';
import 'package:ilabs/iLabs/models/business/model.dart';
import '../../../../widgets/input/chi_textfield.dart';
import '../../../../widgets/input/image_picker.dart';
import '../../../../widgets/loading/loading.dart';
import 'business_detail_form_vm.dart';

// import 'package:image_picker/image_picker.dart';
// ignore: must_be_immutable
class BusinessDetailsFormVU
    extends ViewModelBuilderWidget<BusinessDetailsFormVM> {
  BusinessDetailsFormVU(
    this.tempInputModel, {
    Key? key,
    this.endPoint,
    this.requestType = "POST",
    this.invoicePatchFunction,
  }) : super(key: key);

  final String requestType;
  String? endPoint;
  final BusinessDetail? tempInputModel;
  Future<InvoiceData?> Function(num? id)? invoicePatchFunction;

  @override
  Widget builder(
      BuildContext context, BusinessDetailsFormVM viewModel, Widget? child) {
    return Scaffold(
        appBar: chiAppBar("Business Info", context, actions: [
          TextButton(
            onPressed: () async {
              await viewModel.onSave(endPoint, requestType);
              Navigator.pop(context, viewModel.outputModel);
            },
            child: Text(
              "Save",
              style: GoogleFonts.poppins(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: const Color.fromRGBO(0xfd, 0x78, 0x48, 1.0),
              ),
            ),
          ),
        ]),
        body: CircularWaitOnBusy(
            isBusy: viewModel.isBusy,
            // formKey: viewModel.formKey,
            body: SingleChildScrollView(
              child: Form(
                key: viewModel.formKey,
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      chiImageField(
                          context: context,
                          doCrop: true,
                          imageName: viewModel.outputModel.businessLogoFile ??
                              "", //TODO: handle null
                          callback: (value) async {
                            if (value != null) {
                              viewModel.outputModel.businessLogoFile = value;
                              viewModel.notifyListeners();
                            }
                          }),

                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.transparent.withOpacity(0.2),
                                spreadRadius: 0.1,
                                blurRadius: 3,
                                offset: const Offset(2, 0),
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                right: 12, left: 12, bottom: 10),
                            child: Column(children: [
                              CHITextField(
                                  heading: 'Business Name',
                                  hintText: '',
                                  func: (value) => viewModel
                                      .outputModel.businessName = value,
                                  validator: null,
                                  initialValue:
                                      viewModel.outputModel.businessName),
                              CHITextField(
                                  heading: 'Business ID Number',
                                  hintText: '',
                                  func: (value) => viewModel
                                      .outputModel.businessNumber = value,
                                  initialValue:
                                      viewModel.outputModel.businessNumber),
                              CHITextField(
                                  heading: 'Email',
                                  hintText: '',
                                  func: (value) => viewModel
                                      .outputModel.businessEmail = value,
                                  initialValue:
                                      viewModel.outputModel.businessEmail),
                              CHITextField(
                                  heading: 'Website',
                                  hintText: '',
                                  func: (value) => viewModel
                                      .outputModel.businessWebsite = value,
                                  initialValue:
                                      viewModel.outputModel.businessWebsite),
                              CHITextField(
                                  heading: 'Phone',
                                  hintText: '',
                                  func: (value) => viewModel
                                      .outputModel.businessPhone = value,
                                  initialValue:
                                      viewModel.outputModel.businessPhone),
                            ]),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.transparent.withOpacity(0.2),
                                spreadRadius: 0.1,
                                blurRadius: 3,
                                offset: const Offset(2, 0),
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 12.0, right: 12, bottom: 10),
                            child: Column(
                              children: [
                                CHITextField(
                                    heading: 'Business Owner Name',
                                    hintText: '',
                                    func: (value) => viewModel
                                        .outputModel.businessOwnerName = value,
                                    initialValue: viewModel
                                        .outputModel.businessOwnerName),
                                CHITextField(
                                    heading: 'Owner Contact Number',
                                    hintText: '',
                                    func: (value) => viewModel
                                        .outputModel.businessMobile = value,
                                    validator: null,
                                    initialValue:
                                        viewModel.outputModel.businessMobile),
                              ],
                            ),
                          ),
                        ),
                      ),

                      // chiSaveButton("SAVE CHANGES", () async {
                      //   await viewModel.onSave(callAPI);
                      //   Navigator.pop(context, viewModel.outputModel);
                      // })
                    ],
                  ),
                ),
              ),
            )));
  }

  @override
  BusinessDetailsFormVM viewModelBuilder(BuildContext context) {
    // print(tempInputModel);
    return BusinessDetailsFormVM(tempInputModel,
        invoicePatchFunction: invoicePatchFunction);
  }
}
