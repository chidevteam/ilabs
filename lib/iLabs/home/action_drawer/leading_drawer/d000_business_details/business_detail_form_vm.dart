import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:ilabs/iLabs/models/business/model.dart';
import '../../../../models/invoice_data/model.dart';
import '../../../../services/api_client.dart';
import 'dart:async';

class BusinessDetailsFormVM extends BaseViewModel {
  final formKey = GlobalKey<FormState>();

  final BusinessDetail? inputModel;
  BusinessDetail outputModel = BusinessDetail();
  Future<InvoiceData?> Function(num? id)? invoicePatchFunction;

  BusinessDetailsFormVM(this.inputModel, {this.invoicePatchFunction}) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }

  // /invoi ces/invoicebusiness/21523
  Future<void> updateImageForInvoice(Request req, String endPoint) async {}

  Future<BusinessDetail?> onSave(String? endPoint, String requestType) async {
    formKey.currentState!.save();

    if (formKey.currentState == null) {
      return null;
    }

    if (!formKey.currentState!.validate()) {
      return null;
    }

    if (endPoint != null) {
      setBusy(true);
      if (endPoint.split("/").last == "null") {
        InvoiceData? invoiceData = await invoicePatchFunction!.call(null);
        String preFix = "";
        if (invoiceData != null) {
          if (invoiceData.invoiceInfo.isInvoice == true) {
            preFix = "/invoices";
          } else {
            preFix = "/estimates";
          }
          endPoint = "$preFix/invoicebusiness/${invoiceData.id}";
        }
      }

      String logoFile = outputModel.businessLogoFile!;
      Request req = outputModel.toMap();
      req.remove('id');
      req.remove('business_logo_url');
      req.remove('business_downloaded_logo_file');

      // req.remove('business_logo_url');

      var resp = await ApiClient.postMultiPart<BusinessDetail>(
          requestType: requestType,
          request: req,
          endPoint: endPoint,
          fromJson: null,
          filePath: outputModel.businessLogoFile!,
          imageKey: 'image');

      if (requestType == 'PATCH') {
        print("=============================");
        print(resp['data']["invoice"]["business_logo_url"]);
        outputModel = BusinessDetail.fromMap(resp["data"]["invoice"]);
      } else {
        outputModel = BusinessDetail.fromMap(resp["data"]);
      }
      // outputModel.businessLogoUrl =
      outputModel.businessLogoFile = logoFile;
      notifyListeners();
    }
    return null;
  }
}
