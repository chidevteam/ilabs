import 'package:flutter/material.dart';
import '../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'help_vm.dart';

class HelpScreen extends ViewModelBuilderWidget<HelpViewModel> {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, HelpViewModel viewModel, Widget? child) {
    return SafeArea(
      child: Scaffold(
        appBar: chiAppBar("Help", context),
        body: Stack(
          children: [
            WebView(
              initialUrl: 'https://invoicelabs.co/invoice-templates/',
              onPageFinished: (finish) {
                viewModel.onCirculerProgress();
              },
              javascriptMode: JavascriptMode.unrestricted,
            ),
            Visibility(
              visible: viewModel.isLoading,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  HelpViewModel viewModelBuilder(BuildContext context) {
    return HelpViewModel();
  }
}
