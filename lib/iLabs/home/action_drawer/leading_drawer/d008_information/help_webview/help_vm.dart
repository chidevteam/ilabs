import 'package:stacked/stacked.dart';

class HelpViewModel extends BaseViewModel {
  bool isLoading = true;

  onCirculerProgress() {
    isLoading = !isLoading;
    notifyListeners();
  }
}