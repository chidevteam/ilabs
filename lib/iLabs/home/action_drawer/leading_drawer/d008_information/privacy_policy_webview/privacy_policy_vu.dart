import 'package:flutter/material.dart';
import '../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'privacy_policy_vm.dart';

class PrivacyPolicyScreen extends ViewModelBuilderWidget<PrivacyPolicyVM> {
  const PrivacyPolicyScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, PrivacyPolicyVM viewModel, Widget? child) {
    return SafeArea(
      child: Scaffold(
        appBar: chiAppBar("Privacy Policy", context),
        body: Stack(
          children: [
            WebView(
              initialUrl: 'https://invoicelabs.co/blog/',
              onPageFinished: (finish) {
                viewModel.onCirculerProgress();
              },
              javascriptMode: JavascriptMode.unrestricted,
            ),
            Visibility(
              visible: viewModel.isLoading,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  PrivacyPolicyVM viewModelBuilder(BuildContext context) {
    return PrivacyPolicyVM();
  }
}
