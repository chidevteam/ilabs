import 'package:stacked/stacked.dart';

class PrivacyPolicyVM extends BaseViewModel {
  bool isLoading = true;

  onCirculerProgress() {
    isLoading = !isLoading;
    notifyListeners();
  }
}
