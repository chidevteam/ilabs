import 'package:flutter/material.dart';
import '../../../../../widgets/input/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'term_of_use_vm.dart';

class TermOfUseScreen extends ViewModelBuilderWidget<TermOfUseVM> {
  const TermOfUseScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, TermOfUseVM viewModel, Widget? child) {
    return SafeArea(
      child: Scaffold(
        appBar: chiAppBar("Term of use", context),
        body: Stack(
          children: [
            WebView(
              initialUrl: 'https://invoicelabs.co/invoice-templates/',
              onPageFinished: (finish) {
                viewModel.onCirculerProgress();
              },
              javascriptMode: JavascriptMode.unrestricted,
            ),
            Visibility(
              visible: viewModel.isLoading,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  TermOfUseVM viewModelBuilder(BuildContext context) {
    return TermOfUseVM();
  }
}
