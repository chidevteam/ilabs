import 'package:stacked/stacked.dart';

class TermOfUseVM extends BaseViewModel {
  bool isLoading = true;

  onCirculerProgress() {
    isLoading = !isLoading;
    notifyListeners();
  }
}
