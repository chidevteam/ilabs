import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../language/generated/locale_keys.g.dart';
import '../../../../widgets/input/chi_text_field.dart';
import 'help_webview/help_vu.dart';
import 'information_vm.dart';
import 'privacy_policy_webview/privacy_policy_vu.dart';

import 'package:stacked/stacked.dart';

import 'term_of_use_webview/term_of_use_vu.dart';

class InformationScreen extends ViewModelBuilderWidget<InformationVM> {
  const InformationScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, InformationVM viewModel, Widget? child) {
    return SafeArea(
      child: Scaffold(
        appBar: chiAppBar(
            tr(LocaleKeys.action_drawer_view_info_screen_app_bar_title),
            context),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(14.0, 6.0, 14.0, 8.0),
          child: Column(
            children: [
              about(),
              termOfUse(context),
              privacyPolicy(context),
              help(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget about() {
    return Card(
        elevation: 0.6,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 14, 8.0, 14.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr(LocaleKeys.action_drawer_view_info_screen_about),
                style: TextStyle(fontSize: 12, color: Colors.grey.shade600),
              ),
              Text(
                'V1.3.8',
                style: TextStyle(fontSize: 14, color: Colors.grey.shade600),
              )
            ],
          ),
        ));
  }

  Widget termOfUse(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: InkWell(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => TermOfUseScreen()));
          },
          child: Card(
              elevation: 0.6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 14, 8.0, 14.0),
                child: Text(
                  tr(LocaleKeys.action_drawer_view_info_screen_term_of_use),
                  style: TextStyle(fontSize: 12, color: Colors.grey.shade600),
                ),
              )),
        ),
      ),
    );
  }

  Widget privacyPolicy(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: InkWell(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => PrivacyPolicyScreen()));
          },
          child: Card(
              elevation: 0.6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 14, 8.0, 14.0),
                child: Text(
                  tr(LocaleKeys.action_drawer_view_info_screen_privacy_policy),
                  style: TextStyle(fontSize: 12, color: Colors.grey.shade600),
                ),
              )),
        ),
      ),
    );
  }

  Widget help(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => HelpScreen()));
          },
          child: Card(
              elevation: 0.6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 14, 8.0, 14.0),
                child: Text(
                  tr(LocaleKeys.action_drawer_view_info_screen_help),
                  style: TextStyle(fontSize: 12, color: Colors.grey.shade600),
                ),
              )),
        ),
      ),
    );
  }

  @override
  InformationVM viewModelBuilder(BuildContext context) {
    return InformationVM();
  }
}
