import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:ilabs/iLabs/Utils/Preferences.dart';
import 'package:ilabs/iLabs/Utils/utils.dart';
import 'package:stacked/stacked.dart';
import '../../language/generated/locale_keys.g.dart';
import 'package:ilabs/iLabs/home/home_controller.dart';

import 'package:ilabs/iLabs/login/google_sign_in_api.dart';
import 'package:ilabs/iLabs/services/api_client.dart';
import 'action_drawer_cell.dart';

import 'leading_drawer/d001_choose_template/choose_template_vu.dart';
import 'leading_drawer/d001_choose_template/choose_template_io.dart'; //to define input and output models
import 'leading_drawer/d002_region/region_vu.dart';
import 'leading_drawer/d003_reports/reports_vu.dart';
import 'leading_drawer/d004_upgrade_subscriptions/upgrade_sub_cont_vu.dart';
import 'leading_drawer/d005_settings/settings_vu.dart';
import 'leading_drawer/d007_contact_us/contact_us_vu.dart';
import 'leading_drawer/d008_information/information_vu.dart';
import '../../models/logout/model.dart';
import 'action_drawer_vm.dart';
import 'package:get/get.dart';

class MainDrawer extends ViewModelBuilderWidget<MainDrawerVM> {
  MainDrawer({Key? key}) : super(key: key);
  HomeController homeController = Get.find<HomeController>();

  @override
  Widget builder(BuildContext context, MainDrawerVM viewModel, Widget? child) {
    return Container(
      width: 255,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            drawerHeader(context, viewModel),
            drawerCell(
                context,
                tr(LocaleKeys.action_drawer_view_choose_template),
                'iLabsSreens/side_menu/assets/template icon.svg',
                chooseTemplate),
            drawerCell(context, tr(LocaleKeys.action_drawer_view_region),
                'iLabsSreens/side_menu/assets/Region.svg', chooseRegion),
            drawerCell(context, tr(LocaleKeys.action_drawer_view_reports),
                'iLabsSreens/side_menu/assets/Report.svg', reports),
            drawerCell(
                context,
                tr(LocaleKeys.action_drawer_view_upgrade_Subscription),
                'iLabsSreens/side_menu/assets/Upgrade Subscription.svg',
                subscription),
            drawerCell(context, tr(LocaleKeys.action_drawer_view_settigs),
                'iLabsSreens/side_menu/assets/Settings.svg', settings),
            drawerCell(
                context,
                tr(LocaleKeys.action_drawer_view_switch_account),
                'iLabsSreens/side_menu/assets/Account.svg',
                switchAccount),
            drawerCell(context, tr(LocaleKeys.action_drawer_view_contact_us),
                'iLabsSreens/side_menu/assets/Contact Us.svg', contactUs),
            drawerCell(context, tr(LocaleKeys.action_drawer_view_info),
                'iLabsSreens/side_menu/assets/info.svg', information),
            drawerCell(context, tr(LocaleKeys.action_drawer_view_logout),
                'iLabsSreens/side_menu/assets/Log Out.svg', logout),
          ],
        ),
      ),
    );
  }

  void reports(BuildContext context) {
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const ReportsPageVU();
    })).then((value) {});
  }

  void information(BuildContext context) {
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const InformationScreen();
    })).then((value) {});
  }

  void chooseTemplate(BuildContext context) {
    Navigator.pop(context);
    ChooseTemplateInputModel inputModel = ChooseTemplateInputModel(
        homeController.listOfColors, homeController.pdfModel!.copy());

    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ChooseTemplateVU(inputModel);
    })).then((value) {
      // viewModel.templateModelUpdated();
      if (value != null) {
        homeController.pdfModel = value;
        // viewModel.notifyListeners();
      }
    });
  }

  void chooseRegion(BuildContext context) {
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const RegionVU();
    })).then((value) {});
  }

  void subscription(BuildContext context) {
    Navigator.pop(context);
    Navigator.push(
            context, MaterialPageRoute(builder: (context) => UpgradeSubView()))
        .then((value) {});
  }

  settings(BuildContext context) {
    Navigator.pop(context);
    Navigator.push(
            context, MaterialPageRoute(builder: (context) => SettingScreen()))
        .then((value) {});
  }

  void logout(BuildContext context) async {
    //Navigator.pop(context);
    GoogleSigninApi googleSigninApi = GoogleSigninApi();
    APIResponse resp = await ApiClient.get<LogOutModel>(
        endPoint: '/auth/logout', fromJson: LogOutModel.fromMap);
    if (resp['status'] == 'success') {
      googleSigninApi.googleLogout();
      ApiClient.instance!.mAuthToken = "";
      await UserPreferences.setAuthToken(ApiClient.instance!.mAuthToken);
      debugPrint('-----------Token Cleared');
      Utils.clearCache();
      // ApiClient.instance.
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/logIn', (Route<dynamic> route) => false);
      // Navigator.of(context).popUntil((ModalRoute.withName('/logIn')));
    } else {
      print('failed to logout');
    }

    // Navigator.push(
    //         context, MaterialPageRoute(builder: (context) => LoginScreen()))
    //     .then((value) {
    //   // viewModel.templateModelUpdated();
    // });
  }

  void switchAccount(BuildContext context) {
    Navigator.pushNamed(context, '/logIn');
    // Navigator.push(
    //             context, MaterialPageRoute(builder: (context) => LoginScreen()))
    //         .then((value) {
    //       // viewModel.templateModelUpdated();
    //     });
  }

  void contactUs(
    BuildContext context,
  ) {
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ContactUsScreen(homeController.businessDetails);
    })).then((value) {});
  }

  @override
  MainDrawerVM viewModelBuilder(BuildContext context) {
    MainDrawerVM vm = MainDrawerVM();
    return vm;
  }
}


// Drawer drawer(BuildContext context) {
//   return Drawer(
//     child: ListView(
//       padding: EdgeInsets.zero,
//       children: [
//         drawerHeader(context),
//         drawerCell(context, "Business Details ", Icons.note_add,
//             '/businessDetailsScreen'),
//         drawerCell(context, "Settings", Icons.settings, '/settingScreen'),
//         drawerCell(context, "Choose Template", Icons.pending_actions, ''),
//         drawerCell(context, "Region", Icons.local_airport, ''),
//         drawerCell(context, "Upgrade Subscription", Icons.ac_unit, ''),
//         drawerCell(context, "Switch Account", Icons.manage_accounts, ''),
//         drawerCell(context, "Contact Us", Icons.contact_mail, ''),
//         drawerCell(context, "Help", Icons.help, ''),
//         drawerCell(context, "Termsof Use", Icons.theater_comedy_sharp, ''),
//         drawerCell(context, "Privacy Plicy", Icons.privacy_tip_outlined, ''),
//         drawerCell(context, "Log Out", Icons.logout, '/logIn'),
//       ],
//     ),
//   );
// }


// import 'dart:async';
// import 'package:flutter/material.dart';

// import '../welcome/welcome_vu.dart';
// import '../login/login_vu.dart';
// import '../home/home_vu.dart';
// import '../notes/notes_vu.dart';
// import '../settings/settings_vu.dart';
// import '../settings/signature/signature_vu.dart';

// import 'bottomNavbar/item/add_item/add_item_vu.dart';
// import 'bottomNavbar/client/add_client_vu.dart';
// import 'bottomNavbar/invoice/add_invoice/sec03_client_info/add_client_to_invoice/select_client_for_invoice_vu.dart';
// import 'bottomNavbar/invoice/add_invoice/sec04_items/add_item_in_invoice/select_item_for_invoice_vu.dart';
// import 'action_drawer/leading_drawer/contact_us/contact_us_vu.dart';
// import 'action_drawer/leading_drawer//upgrade_subscriptions/upgrade_subscriptions_vu.dart';
// // import 'action_drawer/leading_drawer/region/region_vu.dart';

// Map<String, WidgetBuilder> appRoutes = {
//   '/logIn': (context) => const LoginScreen(),
//   '/myHomePage': (context) => const HomeScreen(),
//   '/notesScreen': (context) => const NotesScreen(),
//   '/settingScreen': (context) => const SettingScreen(),
//   '/contactUs': (context) => const ContactUsScreen(),
//   '/subscriptionScreen': (context) => const UpgradeSubscriptionsScreen(),
//   '/defaultSignatureScreen': (context) => DefaultSignatureScreen(),
// };

