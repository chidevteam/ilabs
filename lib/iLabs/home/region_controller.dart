import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:ilabs/iLabs/home/app_constants.dart';
import 'package:ilabs/iLabs/home/home_controller.dart';
import '../models/region_currency/region_currency.dart';
import '../models/region_dateformat/region_dateformat.dart';
import '../services/api_client.dart';

class RegionController {
  int languageIndex = 0;
  int currencyIndex = -1;
  int dateFormatIndex = -1;
  List<Currency> currency = [];
  List<DateFormatIL> dateFormat = AppConstants.dayFormatOptions;
  List<String> languageOptions = AppConstants.languageOptions;

  RegionController() {
    // getAllCurrency();
    // getDateFormat();
  }

  Future<List<Currency>> getAllCurrency() async {
    print("getting currency");
    http.Response res =
        await http.get(Uri.parse(ApiClient.instance!.mBaseUrl + '/currency'));
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      currency = body.map((dynamic item) => Currency.fromJson(item)).toList();
      return currency;
    } else {
      throw "Unable to retrieve Currency.";
    }
  }

  updateCurrencyIndex(String selectedCurrency) {
    for (int i = 0; i < currency.length; i++) {
      if (currency[i].currency! == selectedCurrency) {
        currencyIndex = i;
        break;
      }
    }
    print("currency::: $selectedCurrency  $currencyIndex");
  }

  updateDateIndex(String selectedFormat) async {
    for (int i = 0; i < dateFormat.length; i++) {
      if (dateFormat[i].format! == selectedFormat) {
        dateFormatIndex = i;
        break;
      }
    }
    if (dateFormatIndex == -1) {
      dateFormatIndex = 1;
      Map<String, dynamic> req = {
        "region_date_format": dateFormat[dateFormatIndex].format,
      };
      await ApiClient.post(
          request: req, endPoint: "/userdefaults", fromJson: null);
      HomeController hCont = Get.find();
      hCont.userDefault.regionDateFormat = dateFormat[dateFormatIndex].format;
    }
    print("currency::: $selectedFormat  $dateFormatIndex");
  }

  // Future<dynamic> getDateFormat() async {
  //   print("getting dateformat");
  //   // setBusy(true);
  //   http.Response response =
  //       await http.get(Uri.parse(ApiClient.instance!.mBaseUrl + '/dateformat'));

  //   if (response.statusCode == 200) {
  //     List<dynamic> body = jsonDecode(response.body);

  //     dateFormat =
  //         body.map((dynamic item) => DateFormatIL.fromJson(item)).toList();
  //     // for (int i = 0; i < dateFormat.length; i++) {
  //     //   dateFormatOptions.add(dateFormat[i].name!);
  //     // }
  //     // setBusy(false);
  //     dateFormatIndex = 0;
  //     // notifyListeners();
  //     return dateFormat;
  //   } else {
  //     throw "Unable to retrieve posts.";
  //   }
  // }
}
