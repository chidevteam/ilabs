import 'package:flutter/material.dart';

import '../welcome/welcome_vu.dart';
import '../login/login_vu.dart';
import '../home/home_vu.dart';
import '../login_or_register/login_or_register_vu.dart';
import 'action_drawer/leading_drawer/d004_upgrade_subscriptions/upgrade_subscriptions_vu.dart';

Map<String, WidgetBuilder> appRoutes = {
  '/welcome': (context) => const WelcomeScreen(),
  '/logIn': (context) => LoginScreen(),
  '/myHomePage': (context) => HomeScreen(),
  '/subscriptionScreen': (context) => const UpgradeSubscriptionsScreen(),
  '/loginOrReg' : (context) => const LoginOrRegisterScreen()
};
