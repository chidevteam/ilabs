import 'dart:convert';
import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:ilabs/iLabs/home/action_drawer/leading_drawer/d005_settings/default_email_message/default_email_message_model.dart';
import 'package:ilabs/iLabs/home/app_constants.dart';
import '../language/generated/locale_keys.g.dart';
import '../services/api_client.dart';
import '../models/business/model.dart';
import '../models/tax/model.dart';
import '../models/choose_template/model.dart';
import '../models/color/model.dart';
import '../models/userdefaults/model.dart';
import '../models/payment_instructions/model.dart';
import 'region_controller.dart';
import "dart:io";
import 'package:path_provider/path_provider.dart';

class HomeController extends GetxController {
  String? signatureFilePath;
  // String? busLogoFilePath;
  RxBool searchActive = false.obs;
  RxInt index = 0.obs;
  double elevation = 0.0;
  List<String> tabBarTitles = [
    tr(LocaleKeys.main_home_view_invoices),
    tr(LocaleKeys.main_home_view_estimates),
    tr(LocaleKeys.main_home_view_clients),
    tr(LocaleKeys.main_home_view_items),
  ];
  late RegionController regionController; // = RegionController();
  RxString tabBarTitle = "".obs;
  List<String>? tabBars;
  UserDefault userDefault = UserDefault();
  late BusinessDetail businessDetails; // = BusinessDetail(); //make Rx
  PaymentInstructions? paymentInst;
  TaxModel? taxModel;
  // DefaultNote? defaultNote;
  // Customize? customize;
  DefaultEmailMessage? defaultEmail;
  PDFModel? pdfModel;
  ColorList? listOfColors;
  var isLoading = false.obs;
  String applicationDir = "";

  HomeController() {
    onItemTapped(0);
    warmUpApplication();
  }

  warmUpApplication() async {
    regionController = RegionController();
    Future currencyFuture = regionController.getAllCurrency();
    Future userDefFuture = loadUserDefaults();
    Future f3 = loadBusinessDetail();
    Future f4 = getPaymentInst();
    Future f5 = loadTemplateSettings();
    Future f6 = loadAllColors();
    Future f7 = getAppicationDirectory();

    // debugPrint("called done, wating for results");
    await currencyFuture;
    await userDefFuture;
    regionController.updateCurrencyIndex(userDefault.rXregionCurrency.value);
    regionController.updateDateIndex(userDefault.regionDateFormat!);

    // await f2;
    await f3;
    await f4;

    await f5;
    await f6;
    await f7;

    //for PDF template
  }

  getAppicationDirectory() async {
    applicationDir = (await getApplicationDocumentsDirectory()).path;
  }

  getPaymentInst() async {
    APIResponse resp = await ApiClient.get<PaymentInstructions>(
        endPoint: "/paymentinstructions",
        params: "",
        fromJson: PaymentInstructions.fromMap);
    paymentInst = resp["data"];
  }

  Future<void> loadUserDefaults() async {
    APIResponse resp = await ApiClient.get<UserDefault>(
        endPoint: "/userdefaults", params: "", fromJson: UserDefault.fromMap);
    userDefault = resp["data"];
    getTaxValues();

    if (userDefault.signature! != "") {
      String fileURL = SERVER_URL2 + userDefault.signature!;
      String dt = DateTime.now().millisecondsSinceEpoch.toString();
      String tempFile = applicationDir + "/$dt" + SIGNATURE_FILE_NAME;
      File? sigFile =
          await ApiClient.downloadFile(fileURL: fileURL, filePath: tempFile);
      if (sigFile != null) signatureFilePath = sigFile.path;
    }
  }

  updateinvoiceNumberinUserDefualts({String? invoiceNumber}) async {
    int nextInvoiceCount = userDefault.invoiceCount! + 1;
userDefault.invoiceNumber = invoiceNumber ?? "${userDefault.invoiceTitle}${nextInvoiceCount + 1}";
userDefault.invoiceCount = nextInvoiceCount ;
userDefualtPatch();
  }

updateDefaultInvoiceNotes(String notes) async {
userDefault.invoiceNote = notes;
userDefualtPatch();
}

 userDefualtPatch() async {
   var request = userDefault.toMap();
request.remove('colour_id');
log(jsonEncode(request));
    ///not actually a patch but will work as patch so function name is patch as name convention
    APIResponse response = await ApiClient.post(
        request: request,
        endPoint: "/userdefaults",
        fromJson: UserDefault.fromMap);
    if (response['data'] != null) {
      userDefault = response['data'];
    }
  }

  Future loadBusinessDetail() async {
    APIResponse resp = await ApiClient.get<BusinessDetail>(
        endPoint: "/business", params: "", fromJson: BusinessDetail.fromMap);
    businessDetails = resp["data"];
    // debugPrint("$businessDetails");

    if (businessDetails.businessLogoUrl != "") {
      String fileURL = SERVER_URL2 + businessDetails.businessLogoUrl!;
      String dt = DateTime.now().millisecondsSinceEpoch.toString();
      String tempFile = applicationDir + "/$dt" + BUSINESS_FILE_NAME;
      File? businessLogoFile =
          await ApiClient.downloadFile(fileURL: fileURL, filePath: tempFile);
      if (businessLogoFile != null) {
        // busLogoFilePath = businessLogoFile.path;
        businessDetails.businessLogoFile = businessLogoFile.path;
      }
    }
  }

  onItemTapped(int tappedIndex) {
    index.value = tappedIndex;
    // debugPrint("index = $index");
    tabBarTitle.value = tabBarTitles[index.value];
    if (index.value == 0) {
      tabBars = ["All", "Outstanding", "Paid"];
    } else if (index.value == 1) {
      tabBars = ["All", "Open", "Closed"];
    } else {
      tabBars = null;
    }
  }

  Future loadTemplateSettings() async {
    isLoading.value = true;
    APIResponse resp = await ApiClient.get<PDFModel>(
        endPoint:
            "/invoices/preview-Invoice-template/undefined/undefined/undefined/undefined/undefined",
        params: "",
        fromJson: PDFModel.fromMap);
    isLoading.value = false;
    pdfModel = resp["data"];
    // debugPrint("${resp["data"]}");
  }

  Future loadAllColors() async {
    APIResponse resp = await ApiClient.get<ColorList>(
        endPoint: "/colour/undefined/undefined/id/DESC",
        fromJson: ColorList.fromMap);
    listOfColors = resp["data"];
    // debugPrint("$listOfColors");
    // updatePDF();
  }

  TaxModel? getTaxValues() {
    taxModel = TaxModel(
        taxType: userDefault.taxType!,
        taxLabel: userDefault.taxLabel!,
        taxRate: userDefault.taxRate!,
        taxInclusive: userDefault.taxInclusive!);
    return taxModel;
  }
}
