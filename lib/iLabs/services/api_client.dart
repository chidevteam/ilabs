// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart' as dioClient;
// import '../models/invoice_data/model.dart';
import 'package:http_parser/http_parser.dart';
import '../home/app_constants.dart';

typedef Request = Map<String, dynamic>;
typedef APIResponse = Map<String, dynamic>;
typedef ModelFromJson = dynamic Function(Map<String, dynamic> resp);

class ApiClient {
  static ApiClient? _sInstance;
  String mBaseUrl;
  String mAuthToken = "";
  String guestToken = "";
  bool isGuestToken = false;

  ApiClient(this.mBaseUrl);
  static void create(String baseURL) {
    // instance!.mBaseUrl = baseURL;
  }

  static ApiClient? get instance {
    return _sInstance ??= ApiClient(SERVER_URL);
  }

  static Future<APIResponse> login(String email, String password) async {
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    Map<String, dynamic> req = {"email": email, "password": password};

    http.Response resp = await http.post(
      Uri.parse(ApiClient.instance!.mBaseUrl + "/auth/login"),
      headers: makeHeader(),
      body: jsonEncode(req),
    );

    var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    print("Time taken: ${endTime - startTime}");
    APIResponse json = jsonDecode(resp.body);
    ApiClient.instance!.mAuthToken =
        // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NzI3LCJlbWFpbCI6ImFobWFkaGFzc2FuY2hAaG90bWFpbC5jb20iLCJpYXQiOjE2NDYzODkxNDV9.QBAmxbH8ictDE8-FZNfe33tX2EpkNh7JJu_I_h6NoT4';
        json["accessToken"];

    return json;
  }

  static Future<APIResponse> register(String email, String password) async {
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    Map<String, dynamic> req = {
      "email": email,
      "password": password,
      "guest_token": ApiClient.instance!.guestToken
    };

    http.Response resp = await http.post(
      Uri.parse(ApiClient.instance!.mBaseUrl + "/auth/register"),
      headers: makeHeader(),
      body: jsonEncode(req),
    );

    var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    print("Time taken: ${endTime - startTime}");
    APIResponse json = jsonDecode(resp.body);
    if (json.containsKey('accessToken')) {
      ApiClient.instance!.mAuthToken = json["accessToken"];
    }
    return json;
  }

  static void printDesc() {
    print("BaseURL:${instance!.mBaseUrl}\nAuth: ${instance!.mAuthToken}");
  }

  static Map<String, String> makeHeader() {
    String token = ApiClient.instance!.mAuthToken;
    return <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      // 'Accept': 'application/json, text/plain, */*',
      // 'Accept-Encoding': 'gzip, deflate, br',
      // 'Accept-Language': 'en-US,en;q=0.9',
      'Authorization': "Bearer " + token
    };
  }

  static Map<String, String> makeHeader2() {
    String token = ApiClient.instance!.mAuthToken;
    return <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': "Bearer " + token
    };
  }

  static Future<APIResponse> get<T>(
      {required String endPoint,
      String params = "",
      ModelFromJson? fromJson}) async {
    // print("======================================================** ");
    print("GET FROM:  $endPoint ");
    // print("====================================================== ");
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance!.mBaseUrl + endPoint + params;
    Map<String, String> header = makeHeader();
    // print(header);
    final response = await http.get(Uri.parse(url), headers: header);

    // print(response);
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      var json = jsonDecode(response.body);
      // print("======================");
      // print(json);
      if (fromJson != null) {
        json = fromJson(json);
      }
      // print("$model");
      return {"data": json, "time": endTime - startTime, "status": "success"};
    } else {
      throw Exception('Error Fetching Error from $endPoint');
    }
  }

  static Future<APIResponse> delete<T>(
      {required String endPoint,
      String params = "",
      ModelFromJson? fromJson}) async {
    // print("======================================================** ");
    print("Delete EndPoint:  $endPoint ");
    // print("====================================================== ");
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance!.mBaseUrl + endPoint + params;
    final response = await http.delete(Uri.parse(url), headers: makeHeader());

    // print(response);
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      final json = jsonDecode(response.body);
      // print("======================");
      print(json);
      // T model = fromJson(json);
      // print("$model");
      return {"data": json, "time": endTime - startTime, "status": "success"};
    } else {
      throw Exception('Error Fetching Error from $endPoint');
    }
  }

  static Future<APIResponse> post<T>(
      {required Request request,
      required String endPoint,
      ModelFromJson? fromJson}) async {
    print("======================================================** ");
    print("POST TO:  $endPoint ");
    print("====================================================== ");

    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance!.mBaseUrl + endPoint;
    final response = await http.post(
      Uri.parse(url),
      headers: makeHeader(),
      body: jsonEncode(request),
    );
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      var json = jsonDecode(response.body);
      if (fromJson != null) json = fromJson(json);
      return {"data": json, "time": endTime - startTime, "status": "success"};
    } else {
      print(response.statusCode);
      throw Exception('Failed to Post to $endPoint');
    }
  }

  static Future<APIResponse> postFormData<T>(
      {required Request request,
      required String endPoint,
      required ModelFromJson? fromJson}) async {
    print("======================================================** ");
    print("POST TO:  $endPoint ");
    print("====================================================== ");

    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance!.mBaseUrl + endPoint;
    final response =
        await http.post(Uri.parse(url), headers: makeHeader2(), body: request);
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      var json = jsonDecode(response.body);
      if (fromJson != null) json = fromJson(json);
      return {"data": json, "time": endTime - startTime, "status": "success"};
    } else {
      throw Exception('Failed to Post to $endPoint');
    }
  }

  //REF:https://dev.to/carminezacc/
  //advanced-flutter-networking-part-1-uploading-a-file-to-a
  //-rest-api-from-flutter-using-a-multi-part-form-data-post-request-2ekm
  //REF: github.com/lakshydeep-14/MultipartRequest/blob/master/lib/home.dart
  static Future<APIResponse> postMultiPart<T>(
      {required Request request,
      required String endPoint,
      ModelFromJson? fromJson,
      String? filePath,
      String? imageKey,
      requestType = "POST"}) async {
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance!.mBaseUrl + endPoint;

    print("======================================================** ");
    print("MULTIPART $requestType TO:  $endPoint ");
    print("====================================================== ");

    var multiReq = http.MultipartRequest(requestType, Uri.parse(url));
    if (filePath != null) {
      http.MultipartFile file =
          await http.MultipartFile.fromPath(imageKey!, filePath);
      multiReq.files.add(file);
    }
    multiReq.headers.addAll(makeHeader());
    for (String key in request.keys) {
      if (request[key] != null) {
        print("$key  ${request[key].toString()}");
        multiReq.fields[key] = request[key].toString();
      }
    }

    http.StreamedResponse response = await multiReq.send();
    print(response.reasonPhrase);

    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      var responseFromStream = await http.Response.fromStream(response);
      var json = jsonDecode(responseFromStream.body);
      if (fromJson != null) json = fromJson(json);

      return {"data": json, "time": endTime - startTime, "status": "success"};
    } else {
      throw Exception('Failed to Post to $endPoint');
    }
  }

  static Future<APIResponse> patch<T>(
      {required Request request,
      required String endPoint,
      required ModelFromJson? fromJson}) async {
    print("======================================================** ");
    print("POST TO:  $endPoint ");
    print("====================================================== ");

    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance!.mBaseUrl + endPoint;
    log(jsonEncode(request));
    final response = await http.patch(
      Uri.parse(url),
      headers: makeHeader(),
      body: jsonEncode(request),
    );
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      var json = jsonDecode(response.body);
      if (fromJson != null) json = fromJson(json);
      return {"data": json, "time": endTime - startTime, "status": "success"};
    } else {
      throw Exception('Failed to Post to $endPoint');
    }
  }

  static Future<File?> downloadFile(
      {required String fileURL, required String filePath}) async {
    // String fileName = fileName;
    print("Downloading from Network ...... ");
    print("...... $fileURL");
    print("...... $filePath");
    // String url = ApiClient.instance!.mBaseUrl + '/' + fileURL;
    final response = await http.get(Uri.parse(fileURL));

    print("Downloaded from Network ...... ");
    if (response.statusCode == 200) {
      File file = File(filePath);
      await file.writeAsBytes(response.bodyBytes);
      return file;
    }
    return null;
    // File file = File('${directory.path}/sample.pdf');
  }

  ///------------------------------------------Written by Ikram Ulhaq----------------------------------------------///
  //
  // static Future<Photo?> uploadInvoiceImageData(num? photoID, File image,
  //     String invoiceID, String title, String description) async {
  //   ///after testing API calls will be shifted to api client file
  //   try {
  //     dioClient.Dio dio = dioClient.Dio();
  //     String fileName = image.path.split('/').last;
  //     dioClient.FormData formData = dioClient.FormData.fromMap({
  //       "image": await dioClient.MultipartFile.fromFile(
  //         image.path, filename: fileName,
  //         contentType: MediaType(
  //             "image", fileName.split('.').last), //fileName.split('.').last
  //       ),
  //       "description": title,
  //       "additional_details": description
  //     });
  //     dioClient.Response response = photoID == null
  //
  //         ///check weather item is new or not
  //         ? await dio.post(
  //             ApiClient.instance!.mBaseUrl + "/invoicephotos/$invoiceID",
  //             options: dioClient.Options(headers: makeHeader()),
  //             data: formData)
  //
  //         ///call the path api for updating
  //         : await dio.patch(
  //             ApiClient.instance!.mBaseUrl + "/invoicephotos/$photoID",
  //             options: dioClient.Options(headers: makeHeader()),
  //             data: formData);
  //     if (response.statusCode == 200) {
  //       return Photo.fromMap(response.data);
  //     } else {
  //       // print(response.statusCode);
  //       return null;
  //       // print(response.data);
  //     }
  //   } catch (e) {
  //     print(e);
  //     return null;
  //   }
  // }

  // required Request request,
  //     required String endPoint,
  //     ModelFromJson? fromJson,
  //     String? filePath,
  //     String? imageKey,
  //     requestType = "POST"}

  static Future<APIResponse> postPatchWithImage<T>({
    Map<String, dynamic>? request,
    required String endPoint, //endpoint of server
    required ModelFromJson? fromJson,
    required String filePath,
    required String imageKey,
    String requestType = "POST",
  }) async {
    try {
      request ??= {};
      dioClient.Dio dio = dioClient.Dio();
      String fileName = filePath.split('/').last;
      dioClient.MultipartFile multipartImage =
          await dioClient.MultipartFile.fromFile(
        filePath, filename: fileName,
        contentType: MediaType(
            "image", fileName.split('.').last), //image type will be stored here
      );
      // Map<String, dynamic> request = {};
      // request.addAll(payload??{});
      request[imageKey] = multipartImage;

      dioClient.FormData formData = dioClient.FormData.fromMap(request);
      var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;

      dioClient.Response<dynamic> response;

      if (requestType == 'PATCH') {
        response = await dio.patch(ApiClient.instance!.mBaseUrl + endPoint,
            options: dioClient.Options(headers: makeHeader()), data: formData);
      } else {
        response = await dio.post(ApiClient.instance!.mBaseUrl + endPoint,
            options: dioClient.Options(headers: makeHeader()), data: formData);
      }
      if (response.statusCode == 200) {
        var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
        var json = response.data;
        if (fromJson != null) json = fromJson(json);
        // T model = fromJson == null ? null : ;
        return {"data": json, "time": endTime - startTime, "status": "success"};
      } else {
        throw Exception('Error Fetching Error from $endPoint');
      }
    } catch (e) {
      print(e);
      return {};
    }
  }
}
