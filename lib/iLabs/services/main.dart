// ignore_for_file: avoid_print
import 'api_client.dart';
// import '../models/item/model.dart';

main() async {
  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  // print(resp);

  // await getItemList();
  await addItem();
}

/*
{
  offset: 0,
  limit: 10,
  order:{"key":"id", "dir":"DESC"}
}
 */

// Future getItemList() async {
//   Response resp = await ApiClient.get<InvoiceItemList>(
//       endPoint: "/items",
//       params: "/0/10/id/DESC",
//       fromJson: InvoiceItemList.fromMap);
//   InvoiceItemList iList = resp["data"];
//   print(iList);
// }

Future addItem() async {
  Request addItemReq = {
    "taxable": 1,
    "description": "NewAPI",
    "additional_detail": "This unit",
    "unit_cost": 4490
  };
  APIResponse resp = await ApiClient.post(
      request: addItemReq, endPoint: "/items", fromJson: null);
  // print(resp);
}
