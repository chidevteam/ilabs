import 'api_client.dart';
import '../models/business/model.dart';

main() async {
  String email = "ahmadhassanch@hotmail.com";
  String password = "test1234";
  APIResponse resp = await ApiClient.login(email, password);
  print(resp);

  // await getItemList();
  await addBusinessReq();
  print("----done-----");
}

Future addBusinessReq() async {
  Request addItemReq = {
    "business_name": "Ditta and Co.",
    "business_owner_name": "Allah Dita Khan",
    "business_number": "42342123",
    "business_address": "H69, St96, I-8/4",
    "business_email": "aaaa@abc.com",
    "business_phone": "222",
    "business_mobile": "0300",
    "business_website": "chi.com"
  };

  var res = await ApiClient.postMultiPart<BusinessDetail>(
      request: addItemReq,
      endPoint: "/business",
      fromJson: BusinessDetail.fromMap,
      filePath: '../../../assets/photo.jpg',
      imageKey: 'image');
  print("=============================");
  print(res['data']);
  // Response resp = await ApiClient.postFormData(
  //     request: addItemReq, endPoint: "/business", fromJson: null);
  // print("=============================");
  // print(resp);
}
