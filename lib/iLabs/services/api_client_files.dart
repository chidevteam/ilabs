// // ignore_for_file: avoid_print
// import 'dart:convert';
// import 'package:http/http.dart' as http;
// import 'dart:developer';

// import 'dart:io';
// import 'package:dio/dio.dart' as dioClient;
// import '../models/invoice_data/model.dart';
// import 'package:http_parser/http_parser.dart';
// import 'api_client.dart';

// typedef Request = Map<String, dynamic>;
// typedef APIResponse = Map<String, dynamic>;
// typedef ModelFromJson = dynamic Function(Map<String, dynamic> resp);

// class ApiClientFiles extends ApiClient {
//   ApiClientFiles() : super("");

//   ///------------------------------------------Written by Ikram Ulhaq----------------------------------------------///

//   static Future<Photo?> uploadInvoiceImageData(num? photoID, File image,
//       String invoiceID, String title, String description) async {
//     ///after testing API calls will be shifted to api client file
//     try {
//       dioClient.Dio dio = dioClient.Dio();
//       String fileName = image.path.split('/').last;
//       dioClient.FormData formData = dioClient.FormData.fromMap({
//         "image": await dioClient.MultipartFile.fromFile(
//           image.path, filename: fileName,
//           contentType: MediaType(
//               "image", fileName.split('.').last), //fileName.split('.').last
//         ),
//         "description": title,
//         "additional_details": description
//       });
//       dioClient.Response response = photoID == null

//           ///check weather item is new or not
//           ? await dio.post(
//               ApiClient.instance.mBaseUrl + "/invoicephotos/$invoiceID",
//               options: dioClient.Options(headers: ApiClient.makeHeader()),
//               data: formData)

//           ///call the path api for updating
//           : await dio.patch(
//               ApiClient.instance.mBaseUrl + "/invoicephotos/$photoID",
//               options: dioClient.Options(headers: ApiClient.makeHeader()),
//               data: formData);
//       if (response.statusCode == 200) {
//         return Photo.fromMap(response.data);
//       } else {
//         // print(response.statusCode);
//         return null;
//         // print(response.data);
//       }
//     } catch (e) {
//       print(e);
//       return null;
//     }
//   }

//   static Future<APIResponse> uploadImage<T>(
//       {required String endPoint, //endpoint of server
//       required File image,

//       ///image to upload to server
//       required String imageKey,

//       ///key in which we will send out image value .. e.g {"imageKey": imageFile}
//       Map<String, dynamic> payload = const {},

//       ///optional json to send with image  e.g {"user name":"abc"}
//       required ModelFromJson fromJson

//       ///modal that will be returned from this function later
//       }) async {
//     dioClient.Dio dio = dioClient.Dio();
//     String fileName = image.path.split('/').last;
//     payload[imageKey] = await dioClient.MultipartFile.fromFile(
//       image.path, filename: fileName,
//       contentType: MediaType(
//           "image", fileName.split('.').last), //image type will be stored here
//     );

//     dioClient.FormData formData = dioClient.FormData.fromMap(payload);
//     var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
//     final response = await dio.post(ApiClient.instance.mBaseUrl + endPoint,
//         options: dioClient.Options(headers: ApiClient.makeHeader()),
//         data: formData);

//     if (response.statusCode == 200) {
//       var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
//       final json = response.data;
//       T model = fromJson(json);
//       return {"data": model, "time": endTime - startTime, "status": "success"};
//     } else {
//       throw Exception('Error Fetching Error from $endPoint');
//     }
//   }
// }
