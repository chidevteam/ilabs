
- if a function is async (has an await in it), then it returns future.
- An async function will return future as soon as it encounters "await"
- The return type of async function has to be Future
  when it wakes from await, it needs to return future of appropriate type
  if we have defined it.