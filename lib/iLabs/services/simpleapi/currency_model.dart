// ignore_for_file: avoid_print

class CurrencyList {
  List<Currency>? currencyList;

  CurrencyList({required this.currencyList});

  static CurrencyList fromJson(List json) {
    List<Currency> list = [];
    for (Map<String, dynamic> m in json) {
      Currency currency = Currency.fromJson(m);
      list.add(currency);
    }
    return CurrencyList(currencyList: list);
  }

  @override
  String toString() {
    String st = "";
    for (Currency currency in currencyList!) {
      st += currency.toString();
    }
    return st;
  }
}

class Currency {
  final int id;
  final String name;
  final String currency;

  const Currency({
    required this.id,
    required this.name,
    required this.currency,
  });

  factory Currency.fromJson(Map<String, dynamic> json) {
    return Currency(
      id: json['id'],
      name: json['name'],
      currency: json['currency'],
    );
  }

  @override
  String toString() {
    String st = "";
    st += "ID : $id\n";
    st += "name    : $name\n";
    st += "currency : $currency\n";
    return st;
  }
}
