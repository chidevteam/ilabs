// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:convert';

// import 'package:flutter/material.dart';
import 'model.dart';
import 'currency_model.dart';
import 'package:http/http.dart' as http;

Future<dynamic> fetchAlbum() async {
  final response = await http
      .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/1'));

  // print("test123");
  // print("${jsonDecode(response.body)}");
  if (response.statusCode == 200) {
    return Album.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}

Future<AlbumList> fetchAlbums() async {
  final response =
      await http.get(Uri.parse('https://jsonplaceholder.typicode.com/albums'));

  // print("test123");
  // print("${jsonDecode(response.body)}");
  if (response.statusCode == 200) {
    return AlbumList.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}

// Future<CurrencyList> fetchCurrency() async {
//   final response =
//       await http.get(Uri.parse('https://invoice___labs.co/api/currency'));

//   // print("${jsonDecode(response.body)}");
//   if (response.statusCode == 200) {
//     return CurrencyList.fromJson(jsonDecode(response.body));
//   } else {
//     throw Exception('Failed to load album');
//   }
// }

main() async {
  // print("hello world !!");
  // dynamic album = await fetchAlbum();
  // print(album);

  // AlbumList albumList = await fetchAlbums();
  // print(albumList);

  // CurrencyList currencyList = await fetchCurrency();
  // print(currencyList);
}
