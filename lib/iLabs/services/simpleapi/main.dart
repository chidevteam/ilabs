// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:http/http.dart' as http;
import 'model.dart';
import 'dart:convert';

void main() {
  runApp(MyListVU());
}

class MyListVU extends ViewModelBuilderWidget<MyListVM> {
  @override
  Widget builder(BuildContext context, MyListVM viewModel, Widget? child) {
    const title = 'Mixed List';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: makeList(viewModel, context),
      ),
    );
  }

  Widget makeList(MyListVM viewModel, BuildContext context) {
    if (viewModel.aList == null) return const Text("Loading data");

    List<Album> list = viewModel.aList!.albumList!;
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(list[index].title!),
          subtitle: const Text('subtitle'),
        );
      },
    );
  }

  @override
  MyListVM viewModelBuilder(BuildContext context) {
    MyListVM vm = MyListVM();
    vm.loadData();
    return vm;
  }
}

class MyListVM extends BaseViewModel {
  AlbumList? aList;

  MyListVM() {
    // loadData();
  }

  loadData() async {
    http.Response resp = await http
        .get(Uri.parse('https://jsonplaceholder.typicode.com/albums'));

    aList = AlbumList.fromJson(jsonDecode(resp.body));
    notifyListeners();
  }
}
