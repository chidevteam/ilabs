// ignore_for_file: avoid_print

class AlbumList {
  List<Album>? albumList;

  AlbumList({required this.albumList});

  static AlbumList fromJson(List json) {
    List<Album> list = [];

    for (var m in json) {
      Album album = Album.fromJson(m);
      list.add(album);
    }

    return AlbumList(albumList: list);
  }

  @override
  String toString() {
    return albumList.toString();
  }
}

class Album {
  int? id;
  int userId;
  String? title;

  Album({
    required this.userId,
    this.id,
    this.title,
  });

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
    );
  }

  @override
  String toString() {
    String st = "";
    st += "UserID: $userId\n";
    st += "id    : $id\n";
    st += "title : $title\n";
    return st;
  }
}
