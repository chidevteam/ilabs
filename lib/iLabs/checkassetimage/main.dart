import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Asset Image")),
        ),
        body: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SvgPicture.asset(
                  'iLabsSreens/login_signup/Invoice Labs Invoicing Made Easy.svg',
                ),
                SvgPicture.asset(
                    'iLabsSreens/settings/Payment Instructions.svg'),
                SvgPicture.asset(
                  'iLabsSreens/settings/Default Noties.svg',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(const MyApp());
}
