import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

void main() {
  debugPrint("starting the table delte app");
  runApp(const MyView());
}

class MyView extends ViewModelBuilderWidget<MyViewModel> {
  const MyView({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, MyViewModel viewModel, Widget? child) {
    return MaterialApp(
      title: viewModel.title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(viewModel.title),
        ),
        body: ListView.builder(
          itemCount: viewModel.items.length,
          itemBuilder: (context, index) {
            debugPrint("Item at inde------: $index");
            final item = viewModel.items[index];
            return Dismissible(
              // Each Dismissible must contain a Key. Keys allow Flutter to
              // uniquely identify widgets.
              key: Key(item),
              // Provide a function that tells the app
              // what to do after an item has been swiped away.
              onDismissed: (direction) {
                // Remove the item from the data source.

                viewModel.items.removeAt(index);
                // setState(() {});
                viewModel.notifyListeners();

                // Then show a snackbar.
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text('$item dismissed')));
              },
              // Show a red background as the item is swiped away.
              background: Container(color: Colors.red),
              child: ListTile(
                title: Text(item),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  MyViewModel viewModelBuilder(BuildContext context) {
    return MyViewModel();
  }
}

class MyViewModel extends BaseViewModel {
  String title = 'Dismissing Items';

  final items = List<String>.generate(10, (i) => 'ItemZZZ ${i + 1}');
}
