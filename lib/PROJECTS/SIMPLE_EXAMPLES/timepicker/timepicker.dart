import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter TimePicker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  String selectedTime = "...";
  String selectedTime2 = "...";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter TimePicker"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () async {
                selectedTime = await _selectTime(context);
                setState(() {
                  // selectedTime = timeOfDay;
                });
              },
              child: const Text("Choose Time"),
            ),
            Text(selectedTime),
            ElevatedButton(
              onPressed: () async {
                selectedTime2 = await _selectTime(context);
                setState(() {
                  // selectedTime = timeOfDay;
                });
              },
              child: const Text("Choose Time"),
            ),
            Text(selectedTime2),
          ],
        ),
      ),
    );
  }

  Future<String> _selectTime(BuildContext context) async {
    final TimeOfDay? timeOfDay = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.dial,
    );
    String daytime = "None ...";
    if (timeOfDay != null) {
      daytime = "${timeOfDay.hour}:${timeOfDay.minute}";
    }
    return daytime;
  }
}
