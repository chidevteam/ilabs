// import 'package:flutter/material.dart';
// import 'package:flutter_search_bar/flutter_search_bar.dart';

// void main() {
//   runApp(const SearchBarDemoApp());
// }

// class SearchBarDemoApp extends StatelessWidget {
//   const SearchBarDemoApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         title: 'Search Bar Demo',
//         theme: ThemeData(primarySwatch: Colors.blue),
//         home: SearchBarDemoHome());
//   }
// }

// class SearchBarDemoHome extends StatefulWidget {
//   const SearchBarDemoHome({Key? key}) : super(key: key);

//   @override
//   _SearchBarDemoHomeState createState() => _SearchBarDemoHomeState();
// }

// class _SearchBarDemoHomeState extends State<SearchBarDemoHome> {
//   SearchBar? searchBar;
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

//   AppBar buildAppBar(BuildContext context) {
//     return AppBar(
//         title: const Text('Search Bar Demo'),
//         actions: [searchBar!.getSearchAction(context)]);
//   }

//   void onSubmitted(String value) {
//     setState(() {
//       ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//           backgroundColor: Colors.green,
//           duration: const Duration(seconds: 0, milliseconds: 750),
//           content: Text('you wrote $value')));
//     });
//   }

//   _SearchBarDemoHomeState() {
//     searchBar = SearchBar(
//         inBar: false,
//         buildDefaultAppBar: buildAppBar,
//         setState: setState,
//         onSubmitted: onSubmitted,
//         onCleared: () {
//           debugPrint("cleared");
//         },
//         onClosed: () {
//           debugPrint("closed");
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: searchBar!.build(context),
//       key: _scaffoldKey,
//       body: const Center(
//           child: Text("Don't look at me! Press the search button!")),
//     );
//   }
// }
