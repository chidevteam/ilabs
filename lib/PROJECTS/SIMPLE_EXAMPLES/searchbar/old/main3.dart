import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Personal Journal15'),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () {
              showSearch(context: context, delegate: DataClass());
            },
            icon: const Icon(Icons.search),
          )
        ],
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Center(
            child: Text('Hi'),
          ),
        ],
      ),
    );
  }
}

class DataClass extends SearchDelegate<String> {
  int selectedIndex = 0;
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [IconButton(onPressed: () {}, icon: const Icon(Icons.clear))];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, "");
        },
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation));
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestionList = query.isEmpty
        ? recentCities
        : cities
            .where((p) =>
                p.toLowerCase().startsWith(cities[selectedIndex].toLowerCase()))
            .toList();
    debugPrint(query);
    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
              onTap: () {
                showResults(context);
                debugPrint(query + "--");
              },
              leading: const Icon(Icons.location_city_outlined),
              title: Text(suggestionList[index]));
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = cities
        .where((p) => p.toLowerCase().startsWith(query.toLowerCase()))
        .toList();
    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
              onTap: () {
                showResults(context);
                selectedIndex = index;
                debugPrint(query + "--");
              },
              leading: const Icon(Icons.location_city),
              title: Text(suggestionList[index]));
        });
  }
}

List<String> recentCities = [
  "Rawalpindi",
  "Peshawar",
  "Quetta",
];

List<String> cities = [
  "Karachi",
  "Lahore",
  "Faisalabad",
  "Rawalpindi",
  "Gujranwala",
  "Peshawar",
  "Multan",
  "Hyderabad",
  "Islamabad",
  "Quetta",
  "Bahawalpur",
  "Sargodha",
  "Sialkot",
  "Sukkur",
  "Larkana",
  "Rahim",
  "Sheikhupura",
  "Jhang",
  "Dera",
  "Gujrat",
  "Sahiwal",
  "Wah",
  "Mardan",
  "Kasur",
  "Okara",
  "Mingora",
  "Nawabshah",
  "Chiniot",
  "Kotri",
  "Kāmoke",
  "Hafizabad",
  "Sadiqabad",
];
