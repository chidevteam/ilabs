List<String> recentCities = [
  "Rawalpindi",
  "Peshawar",
  "Quetta",
];

List<String> cities = [
  "Karachi",
  "Lahore",
  "Faisalabad",
  "Rawalpindi",
  "Gujranwala",
  "Peshawar",
  "Multan",
  "Hyderabad",
  "Islamabad",
  "Quetta",
  "Bahawalpur",
  "Sargodha",
  "Sialkot",
  "Sukkur",
  "Larkana",
  "Rahim",
  "Sheikhupura",
  "Jhang",
  "Dera",
  "Gujrat",
  "Sahiwal",
  "Wah",
  "Mardan",
  "Kasur",
  "Okara",
  "Mingora",
  "Nawabshah",
  "Chiniot",
  "Kotri",
  "Kāmoke",
  "Hafizabad",
  "Sadiqabad",
];
