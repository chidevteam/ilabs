import 'package:flutter/material.dart';
import 'cities.dart';

class DataClass extends SearchDelegate<String> {
  int selectedIndex = 0;

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [IconButton(onPressed: () {}, icon: const Icon(Icons.clear))];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, "");
        },
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation));
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestionList = cities;

    //  query.isEmpty
    //     ? recentCities
    //     : cities
    //         .where((p) =>
    //             p.toLowerCase().startsWith(cities[selectedIndex].toLowerCase()))
    //         .toList();
    debugPrint(query);
    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
              onTap: () {
                showResults(context);
                debugPrint(query + "--");
              },
              leading: const Icon(Icons.location_city_outlined),
              title: Text(suggestionList[index]));
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = cities
        .where((p) => p.toLowerCase().startsWith(query.toLowerCase()))
        .toList();
    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
              onTap: () {
                showResults(context);
                selectedIndex = index;
                query = suggestionList[index];
                debugPrint("$query -- ${suggestionList[index]}");
              },
              leading: const Icon(Icons.location_city),
              title: Text(suggestionList[index]));
        });
  }
}
