import 'dart:core';
// import 'all.dart';

class SystemLanguage {
  final String languageId;
  final String languageName;
  final String languageNativeName;
  final String languageVersion;

  SystemLanguage(
    this.languageId,
    this.languageName,
    this.languageNativeName,
    this.languageVersion,
  );

  static SystemLanguage fromMap(Map<String, dynamic> resp) {
    return SystemLanguage(
      resp['language_id'] as String,
      resp['language_name'] as String,
      resp['language_native_name'] as String,
      resp['language_version'] as String,
    );
  }
}

class LoginResult {
  final String token;
  final String userType;

  LoginResult(this.token, this.userType);

  static LoginResult fromMap(Map<String, dynamic> resp) {
    return LoginResult(resp['Token'] as String, resp['user_type'] as String);
  }
}

