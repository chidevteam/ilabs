class FileResponse {
  final String? fileId;
  final String? fileUrl;
  final String? fileName;
  final String? fileExt;
  final String? fileType;

  FileResponse(
      this.fileId, this.fileUrl, this.fileName, this.fileExt, this.fileType);

  static FileResponse fromMap(Map<String, dynamic> resp) {
    return FileResponse(
      resp['file_id'] as String?,
      resp['file_url'] as String?,
      resp['file_name'] as String?,
      resp['file_ext'] as String?,
      resp['file_type'] as String?,
    );
  }
}
