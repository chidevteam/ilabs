import 'dart:async';
import 'dart:convert';
import 'dart:io';

//import 'package:charms_doctor_app_v4x/components.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import '../models/general.dart';

typedef ApiPayload = Map<String, dynamic>;

class ApiClient {
  static ApiClient? _instance;

  String mBaseUrl;
  String mSiteCode;
  String? mAuthToken;
  BuildContext mContext;

  ApiClient(this.mSiteCode, this.mBaseUrl, this.mContext);

  static ApiClient get instance => _instance!;

  static ApiClient create(siteCode, BuildContext context) {
    String baseUrl = 'https://$siteCode.cognitivehealthintl.com';
    _instance = ApiClient(siteCode, baseUrl, context);

    return _instance!;
  }

  static void setAuthToken(authToken) {
    _instance?.mAuthToken = authToken;
  }

  Future<ApiSingleResponse<T>> callFileApi<T>(
      {required String endPoint,
      required ModelFromJson fromJson,
      MultipartFile? file}) async {
    Map<String, String> headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };

    if (mAuthToken != null) {
      headers['Authorization'] = mAuthToken!;
    }

    String url = '$mBaseUrl/$endPoint';

    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.files.add(file!);
    request.headers.addAll(headers);

    final http.Response response;

    try {
      print(
          "request -------------------: ${request} ----- ${request.fields.toString()} --------- ${request.files.toString()}");

      response = await http.Response.fromStream(await request.send());
    } on TimeoutException catch (e) {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': e.message,
        'ErrorCode': -1,
      });
    } on SocketException catch (e) {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': e.message,
        'ErrorCode': -1,
      });
    }

    if (response.statusCode == 401) {
      //Preferences.clearKey(Preferences.kToken);
      Navigator.of(mContext)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }

    if (response.statusCode == 502) {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': response.reasonPhrase,
        'ErrorCode': -1,
      });
    }

    final parsed = jsonDecode(response.body)?.cast<String, dynamic>();
    if (parsed['data']['file_url'] != '') {
      parsed['data']['file_url'] =
          parsed['data']['file_url'] + '?token=$mAuthToken';
    }

    return compute(parseObjectResponse, {
      'response': jsonEncode(parsed),
      'code': response.statusCode,
      'fromJson': fromJson,
    });
  }

  Future<ApiSingleResponse<T>> callObjectApi<T>({
    required String endPoint,
    ApiPayload? req,
    required ModelFromJson fromJson,
  }) async {
    Map<String, String> headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };

    if (mAuthToken != null) {
      headers['Authorization'] = mAuthToken!;
    }

    String url = '$mBaseUrl/$endPoint';
    String payload = req == null ? '{}' : jsonEncode(req);

    print('Request: URL --> $url ----- $payload');

    final http.Response response;

    try {
      response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: payload,
      );
    } on TimeoutException catch (e) {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': e.message,
        'ErrorCode': -1,
      });
    } on SocketException catch (e) {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': e.message,
        'ErrorCode': -1,
      });
    }

    if (response.statusCode == 401) {
      //Preferences.clearKey(Preferences.kToken);
      Navigator.of(mContext)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }

    if (response.statusCode == 502) {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': response.reasonPhrase,
        'ErrorCode': -1,
      });
    }

    Map<String, dynamic> parsed =
        jsonDecode(response.body)?.cast<String, dynamic>();

    if (parsed.containsKey('data') && parsed['data'].runtimeType != int) {
      if (parsed['data'].containsKey('data')) {
        for (int i = 0; i < parsed['data']['data'].length; i++) {
          if (parsed['data']['data'][i].containsKey('image')) {
            if (parsed['data']['data'][i]['image'] != null) {
              parsed['data']['data'][i]['image'] =
                  parsed['data']['data'][i]['image'] + '?token=$mAuthToken';
            }
          }

          if (parsed['data']['data'][i].containsKey('icon')) {
            if (parsed['data']['data'][i]['icon'] != null) {
              parsed['data']['data'][i]['icon'] =
                  parsed['data']['data'][i]['icon'] + '?token=$mAuthToken';
            }
          }

          if (parsed['data']['data'][i].containsKey('device_image')) {
            if (parsed['data']['data'][i]['device_image'] != null) {
              parsed['data']['data'][i]['device_image'] = parsed['data']['data']
                      [i]['device_image'] +
                  '?token=$mAuthToken';
            }
          }

          if (parsed['data']['data'][i].containsKey('report_url')) {
            if (parsed['data']['data'][i]['report_url'] != null) {
              parsed['data']['data'][i]['report_url'] = parsed['data']['data']
                      [i]['report_url'] +
                  '?token=$mAuthToken';
            }
          }

          if (parsed['data']['data'][i].containsKey('avatar')) {
            if (parsed['data']['data'][i]['avatar'] != null) {
              parsed['data']['data'][i]['avatar'] =
                  parsed['data']['data'][i]['avatar'] + '?token=$mAuthToken';
            }
          }

          if (parsed['data']['data'][i].containsKey('file_url')) {
            if (parsed['data']['data'][i]['file_url'] != null) {
              parsed['data']['data'][i]['file_url'] =
                  parsed['data']['data'][i]['file_url'] + '?token=$mAuthToken';
            }
          }
          if (parsed['data']['data'][i].containsKey('pdf_file_url')) {
            if (parsed['data']['data'][i]['pdf_file_url'] != null) {
              parsed['data']['data'][i]['pdf_file_url'] = parsed['data']['data']
                      [i]['pdf_file_url'] +
                  '?token=$mAuthToken';
            }
          }

          if (parsed['data']['data'][i].containsKey('urls')) {
            for (int j = 0; j < parsed['data']['data'][i]['urls'].length; j++) {
              if (parsed['data']['data'][i]['urls'][j]['file_url'] != null) {
                parsed['data']['data'][i]['urls'][j]['file_url'] =
                    parsed['data']['data'][i]['urls'][j]['file_url'] +
                        '?token=$mAuthToken';
              }
            }
          }
        }
      } else {
        if (parsed['data'].containsKey('image')) {
          if (parsed['data']['image'] != null) {
            parsed['data']['image'] =
                parsed['data']['image'] + '?token=$mAuthToken';
          }
        }
        if (parsed['data'].containsKey('icon')) {
          if (parsed['data']['icon'] != null) {
            parsed['data']['icon'] =
                parsed['data']['icon'] + '?token=$mAuthToken';
          }
        }
      }
    }

    return compute(parseObjectResponse, {
      'response': jsonEncode(parsed),
      'code': response.statusCode,
      'fromJson': fromJson,
    });

/*
    final Map<String, dynamic> parsed;
    try {
      parsed = jsonDecode(response.body)?.cast<String, dynamic>();
    } on FormatException {
      return ApiSingleResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': response.body,
        'ErrorCode': response.statusCode,
      });
    }

    if (response.statusCode != 200) {
      return ApiSingleResponse(null, parsed);
    }

    return ApiSingleResponse<T>.fromMap(parsed, fromJson);
*/
  }

  Future<ApiListResponse<T>> callListApi<T>({
    required String endPoint,
    ApiPayload? req,
    required ModelFromJson fromJson,
  }) async {
    Map<String, String> headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };

    if (mAuthToken != null) {
      headers['Authorization'] = mAuthToken!;
    }

    String url = '$mBaseUrl/$endPoint';
    String payload = req == null ? '{}' : jsonEncode(req);
    print('Request: URL --> $url ----- $payload');

    final http.Response response;

    try {
      response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: payload,
      );
    } on TimeoutException catch (e) {
      return ApiListResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': e.message,
        'ErrorCode': -1,
      });
    } on SocketException catch (e) {
      return ApiListResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': e.message,
        'ErrorCode': -1,
      });
    }

    if (response.statusCode == 401) {
      //Preferences.clearKey(Preferences.kToken);
      Navigator.of(mContext)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }

    if (response.statusCode == 502) {
      return ApiListResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': response.reasonPhrase,
        'ErrorCode': -1,
      });
    }

    Map<String, dynamic> parsed =
        jsonDecode(response.body)?.cast<String, dynamic>();

    if (parsed.containsKey('data')) {
      for (int i = 0; i < parsed['data'].length; i++) {
        if (parsed['data'][i].containsKey('icon')) {
          if (parsed['data'][i]['icon'] != null) {
            parsed['data'][i]['icon'] =
                parsed['data'][i]['icon'] + '?token=$mAuthToken';
          }
        }

        if (parsed['data'][i].containsKey('avatar')) {
          if (parsed['data'][i]['avatar'] != null) {
            parsed['data'][i]['avatar'] =
                parsed['data'][i]['avatar'] + '?token=$mAuthToken';
          }
        }

        if (parsed['data'][i].containsKey('audio')) {
          if (parsed['data'][i]['audio'] != null) {
            parsed['data'][i]['audio'] =
                parsed['data'][i]['audio'] + '?token=$mAuthToken';
          }
        }

        if (parsed['data'][i].containsKey('image')) {
          if (parsed['data'][i]['image'] != null) {
            parsed['data'][i]['image'] =
                parsed['data'][i]['image'] + '?token=$mAuthToken';
          }
        }

        if (parsed['data'][i].containsKey('report')) {
          if (parsed['data'][i]['report'] != null) {
            parsed['data'][i]['report'] =
                parsed['data'][i]['report'] + '?token=$mAuthToken';
          }
        }
      }
    }

    return compute(parseListResponse, {
      'response': jsonEncode(parsed),
      'code': response.statusCode,
      'fromJson': fromJson,
    });

/*
    final Map<String, dynamic> parsed;

    try {
      parsed = jsonDecode(response.body)?.cast<String, dynamic>();
    } on FormatException {
      return ApiListResponse(null, <String, dynamic>{
        'Status': 'Error',
        'ErrorMessage': response.body,
        'ErrorCode': response.statusCode,
      });
    }

    if (response.statusCode != 200) {
      return ApiListResponse(null, parsed);
    }

    return ApiListResponse<T>.fromMap(parsed, fromJson);
*/
  }
}

ApiListResponse<T> parseListResponse<T>(Map<String, dynamic> parameters) {
  final body = parameters['response'];
  final statusCode = parameters['code'];
  final fromJson = parameters['fromJson'];

  final Map<String, dynamic> parsed;

  try {
    parsed = jsonDecode(body)?.cast<String, dynamic>();
  } on FormatException {
    return ApiListResponse(null, <String, dynamic>{
      'Status': 'Error',
      'ErrorMessage': body,
      'ErrorCode': statusCode,
    });
  }

  if (statusCode != 200) {
    return ApiListResponse(null, parsed);
  }

  return ApiListResponse<T>.fromMap(parsed, fromJson);
}

ApiSingleResponse<T> parseObjectResponse<T>(Map<String, dynamic> parameters) {
  final body = parameters['response'];
  final statusCode = parameters['code'];
  final fromJson = parameters['fromJson'];

  final Map<String, dynamic> parsed;
  try {
    print('BODY: $body');
    parsed = jsonDecode(body)?.cast<String, dynamic>();
  } on FormatException {
    return ApiSingleResponse(null, <String, dynamic>{
      'Status': 'Error',
      'ErrorMessage': body,
      'ErrorCode': statusCode,
    });
  }

  if (statusCode != 200) {
    return ApiSingleResponse(null, parsed);
  }

  return ApiSingleResponse<T>.fromMap(parsed, fromJson);
}
