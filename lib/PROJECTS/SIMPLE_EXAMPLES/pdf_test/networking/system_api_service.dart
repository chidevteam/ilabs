import 'package:http/http.dart';
import '../models/general.dart';
import '../models/system_models.dart';
import '../models/upload_model.dart';

import 'api_client.dart';

abstract class SystemApiService {
  static Future<ApiSingleResponse<FileResponse>> upload(MultipartFile req) {
    return ApiClient.instance.callFileApi(
        file: req,
        endPoint: 'api/user_files/Upload',
        fromJson: FileResponse.fromMap);
  }

  static Future<ApiSingleResponse<LoginResult>> login() {
    return ApiClient.instance.callObjectApi(
      req: {
        'username': 'kashifd',
        'password': 'Kashif123@',
        'pn_type': 'None',
        'pn_token': 'None',
        'app_version': '4.0.1',
        'app_type': 'Doctor',
        'lang': 'en',
        'device_id': 'android',
        'device_name': 'android',
        'device_type': 'android',
        'device_model': 'android',
        'os_name': 'android',
        'os_version': 'android'
      },
      endPoint: 'auth/Login2',
      fromJson: LoginResult.fromMap,
    );
  }
}
