import 'package:flutter/material.dart';
import 'pdf_home.dart';
import 'screens/image_viewer/image_viewer_vu.dart';
import 'screens/pdf_viewer/pdf_viewer_vu.dart';
import 'forms/email_form.dart';

Map<String, WidgetBuilder> appRoutes = {
  '/': (context) => const PDFHomeScreen(),
  '/pdf_viewer': (context) => const PDFViewerScreen(),
  '/image_viewer': (context) => ImageViewerScreen(),
  '/email_form': (context) => const EmailFormScreen(),
};
