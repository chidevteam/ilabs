import 'package:flutter/material.dart';
import '../pdf_test/pdf_home_vm.dart';
import 'package:stacked/stacked.dart';

class PDFHomeScreen extends ViewModelBuilderWidget<PDFHomeViewModel> {
  const PDFHomeScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, PDFHomeViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('PDF Test'),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
                onPressed: () {
                  viewModel.onItemClick(context);
                },
                child: const Text('PDF from URL')),
            ElevatedButton(
                onPressed: () {
                  viewModel.onImageButtonClick(context);
                },
                child: const Text('Network Image'))
          ],
        ),
      ),
    );
  }

  @override
  PDFHomeViewModel viewModelBuilder(BuildContext context) {
    return PDFHomeViewModel();
  }
}
