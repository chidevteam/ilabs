import 'package:flutter/material.dart';
import 'test_vm.dart';
import 'package:stacked/stacked.dart';

class TestViewScreen extends ViewModelBuilderWidget<TestViewModel> {
  @override
  Widget builder(BuildContext context, TestViewModel viewModel, Widget? child) {
    return Center(
      child: Text(viewModel.str.toString()),
    );
  }

  @override
  TestViewModel viewModelBuilder(BuildContext context) {
    // TODO: implement viewModelBuilder
    return TestViewModel();
  }
}
