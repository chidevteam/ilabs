import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:stacked/stacked.dart';
import 'dart:io';
import 'package:flutter/material.dart';
// import 'package:gallery_saver/gallery_saver.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import '../../networking/system_api_service.dart';
import '../../networking/api_client.dart';

class ImageViewerViewModel extends BaseViewModel {
  String imgUrl = '';
  bool isSharing = false;
  File? image;
  XFile? mImg;
  String uploadedImage = '';
  
  Future<bool> checkPermission() async {
    if (!await Permission.storage.isGranted) {
      PermissionStatus status = await Permission.storage.request();
      if (status != PermissionStatus.granted &&
          status != PermissionStatus.permanentlyDenied) {
        return false;
      }
    }
    return true;
  }

  Future pickImage() async {
    final tempImage =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    if (tempImage == null) {
      return;
    }
    image = File(tempImage.path);
    mImg = tempImage;
    notifyListeners();
    uploadFile();
  }

  onLogin(BuildContext context) async {
    ApiClient.create('charms-qa', context);
    final resp = await SystemApiService.login();
    if (resp.isSuccess) {
      //print('ppppppppppppppp');
      ApiClient.setAuthToken(resp.data!.token);
      print('-------------success---------');
    }
  }

  uploadFile() async {
   

    final req = await http.MultipartFile.fromPath('UserFile', mImg!.path,
        filename: mImg!.name, contentType: MediaType('image', 'jpg'));
    final resp = await SystemApiService.upload(req);

    if (resp.isSuccess) {
      uploadedImage = resp.data!.fileUrl!;
      print('-----------upload sucessssssssssssssssssful------ $uploadedImage');
    } else {
      print('000000000000000 upload failed ----------------');
    }
  }

  // downloadImage() async {
  //   bool hasPermission = await checkPermission();
  //
  //   if (hasPermission)
  //   {
  //     isDownloading = true;
  //     //setState(() {});
  //
  //     final isImageSaved = await GallerySaver.saveImage(imgUrl);
  //     if(isImageSaved!)
  //     {
  //       isDownloading = false;
  //       //setState(() {});
  //       notifyListeners();
  //
  //       //showSuccessAlert(context, title: 'Downloaded', message: 'Image downloaded successfully', onConfirmClick: (){Navigator.pop(context);});
  //     }
  //     else
  //     {
  //       isDownloading = false;
  //       //setState(() {});
  //       notifyListeners();
  //       //showErrorAlert(context, title: 'Download Error', message: 'Error while downloading', onConfirmClick: (){Navigator.pop(context);});
  //     }
  //
  //     //
  //     // final imageUrl = imgUrl.split('?token').first;
  //     // final imgName = imageUrl.split('/').last;
  //     // print('---------downloadImage----------------------->$imgName' );
  //     // setState(() {});
  //     // Uri imgUri = Uri.parse(imgUrl);
  //     // final response = await http.get(imgUri);
  //     // final imageName = path.basename(imgName);
  //     // Directory dir = Directory('/storage/emulated/0/Download');
  //     //
  //     // // final appDir = await pathProvider.getExternalStorageDirectory();
  //     // print('---------appDir----------------------->$dir' );
  //     //
  //     // final localPath = path.join(dir.path, imageName);
  //     // print('---------localPath----------------------->$localPath' );
  //     //
  //     // final imageFile = File(localPath);
  //     // print('---------imageFile----------------------->$imageFile' );
  //     //
  //     // await imageFile.writeAsBytes(response.bodyBytes);
  //
  //     // setState(() {
  //     //   isDownloading = false;
  //     //   downloadedFile = imageFile;
  //     //   showSuccessAlert(context, title: 'Downloaded', message: 'Image downloaded successfully', onConfirmClick: (){Navigator.pop(context);});
  //     // });
  //   }
  //   else
  //   {
  //     print('error');
  //   }
  // }
}
