// import 'package:pdf_test/screens/image_viewer/image_viewer_vm.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';

import 'image_viewer_vm.dart';


class ImageViewerScreen extends ViewModelBuilderWidget<ImageViewerViewModel>{
  @override
  Widget builder(BuildContext context, ImageViewerViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('Image Viewer'),
        ),
      ),
      body: viewModel.image!= null? Image.file(
         viewModel.image!,
        height: 160,
        width: 160,
      ): const Text('chal oye'),
      bottomSheet: Row(
        children: [
          ElevatedButton(
              onPressed: (){
                viewModel.pickImage();
              },
              child: const Icon(
                Icons.upload_rounded,
                color: Colors.white,
              )
          )
        ],
      ),
    );
  }

  @override
  ImageViewerViewModel viewModelBuilder(BuildContext context) {
   final vm = ImageViewerViewModel();
   vm.onLogin(context);
   return vm;
  }
  
}