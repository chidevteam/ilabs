import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'pdf_viewer_vm.dart';
import 'package:stacked/stacked.dart';

class PDFViewerScreen extends ViewModelBuilderWidget<PDFViewerViewModel> {
  const PDFViewerScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, PDFViewerViewModel viewModel, Widget? child) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('PDF Viewer'),
          ),
        ),
        body: const PDF().cachedFromUrl(viewModel.url),
        floatingActionButton: SpeedDial(
          animatedIcon: AnimatedIcons.menu_close,
          children: [
            SpeedDialChild(
                onTap: () {
                  Navigator.pushNamed(context, '/email_form');
                },
                child: const Icon(Icons.mail),
                label: 'email'),
            SpeedDialChild(
                onTap: () {
                  viewModel.onPDFShare(context, viewModel.url);
                },
                child: const Icon(Icons.share),
                label: 'Share')
          ],
        ));
  }

  @override
  PDFViewerViewModel viewModelBuilder(BuildContext context) {
    final vm = PDFViewerViewModel();
    vm.onGetPreviousData(context);
    return vm;
  }
}
