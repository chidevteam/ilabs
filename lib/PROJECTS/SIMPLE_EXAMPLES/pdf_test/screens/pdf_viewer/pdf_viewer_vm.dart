import 'package:stacked/stacked.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:share_plus/share_plus.dart';
import 'dart:io';


class PDFViewerViewModel extends BaseViewModel{

  String url = ' ';

  onGetPreviousData(BuildContext context){
    url = ModalRoute.of(context)!.settings.arguments! as String;
  }


  onPDFShare(BuildContext context, String url) async {
    //showProgress(context);
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    final bytes = response.bodyBytes;
    final temp = await pathProvider.getTemporaryDirectory();
    final path = '${temp.path}/file.pdf';
    File(path).writeAsBytesSync(bytes);
    //hideProgress(context);
    await Share.shareFiles([path], text: 'PDF Share');
  }
}