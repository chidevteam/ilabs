import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';

class PDFHomeViewModel extends BaseViewModel {
  onItemClick(BuildContext context) {
    Navigator.pushNamed(context, '/pdf_viewer',
        arguments: 'http://africau.edu/images/default/sample.pdf');
  }

  onImageButtonClick(BuildContext context) {
    Navigator.pushNamed(context, '/image_viewer');
  }
}
