// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en_US = {
  "title": "Invoice Labs Invoicing Made Easy",
  "sub_title": "invoice Labs makes invoicing simple and effective. We allows you to create professional invoices with our invoice Generator and estimates With in seconds, so that you can focus on your work",
  "loginregisterview_login": "Login",
  "loginregisterview_createaccount": "Create Account",
  "continue_button": "Continue as Guest",
  "switch_eng": "Switch Eng",
  "switch_urdu": "Switch Urdu",
  "add_new_item": {
    "title": "Add new Item",
    "itemname": "Item Name*",
    "unitcost": "Unit Cost*",
    "textable": "Textable",
    "saveBtn": "Save"
  },
  "add_new_client": {
    "title": "Create New Client",
    "clientName": "Client Name",
    "clientEmail": "Email",
    "mobileNumber": "Mobile Number",
    "officePhone": "Office Phone",
    "fax": "Fax",
    "officeAddress": "Office Address",
    "addNowBtn": "ADD NOW"
  },
  "msg": "Hello {} in the {} world ",
  "msg_named": "{} are written in the {lang} language",
  "clickMe": "Click me",
  "profile": {
    "reset_password": {
      "label": "Reset Password",
      "username": "Username",
      "password": "password"
    }
  },
  "clicked": {
    "zero": "You clicked {} times!",
    "one": "You clicked {} time!",
    "two": "You clicked {} times!",
    "few": "You clicked {} times!",
    "many": "You clicked {} times!",
    "other": "You clicked {} times!"
  },
  "amount": {
    "zero": "Your amount : {} ",
    "one": "Your amount : {} ",
    "two": "Your amount : {} ",
    "few": "Your amount : {} ",
    "many": "Your amount : {} ",
    "other": "Your amount : {} "
  },
  "gender": {
    "male": "Hi man ;) ",
    "female": "Hello girl :)",
    "with_arg": {
      "male": "Hi man ;) {}",
      "female": "Hello girl :) {}"
    }
  },
  "reset_locale": "Reset Language"
};
static const Map<String,dynamic> ur_PK = {
  "title": "انوائس لیبز انوائسنگ کو آسان بنا دیا گیا۔",
  "sub_title": "انوائس لیبز انوائس کو آسان اور موثر بناتی ہے۔ ہم آپ کو اپنے انوائس جنریٹر کے ساتھ پیشہ ورانہ رسیدیں بنانے اور سیکنڈوں میں تخمینہ لگانے کی اجازت دیتے ہیں، تاکہ آپ اپنے کام پر توجہ مرکوز کر سکیں",
  "loginregisterview_login": "لاگ ان کریں",
  "loginregisterview_createaccount": "اکاؤنٹ بنائیں",
  "continue_button": "بطور مہمان جاری رکھیں",
  "switch_eng": "انگریزی سوئچ کریں",
  "switch_urdu": "اردو تبدیل کریں۔",
  "add_new_item": {
    "title": "عنوان",
    "itemname": "شے کا نام",
    "unitcost": "یونٹ لاگت",
    "textable": "ٹیکسٹ ایبل",
    "saveBtn": "بٹن محفوظ کریں"
  },
  "add_new_client": {
    "title": "نیا کلائنٹ بنائیں",
    "clientName": "کلائنٹ کا نام",
    "clientEmail": "ای میل",
    "mobileNumber": "موبائل فون کانمبر",
    "officePhone": "دفتر فون",
    "fax": "فیکس",
    "officeAddress": "دفتر کا پتہ",
    "addNowBtn": "ابھی شامل کریں۔"
  },
  "msg": "Hello {} in the {} world ",
  "msg_named": "{} are written in the {lang} language",
  "clickMe": "Click me",
  "profile": {
    "reset_password": {
      "label": "Reset Password",
      "username": "Username",
      "password": "password"
    }
  },
  "clicked": {
    "zero": "You clicked {} times!",
    "one": "You clicked {} time!",
    "two": "You clicked {} times!",
    "few": "You clicked {} times!",
    "many": "You clicked {} times!",
    "other": "You clicked {} times!"
  },
  "amount": {
    "zero": "Your amount : {} ",
    "one": "Your amount : {} ",
    "two": "Your amount : {} ",
    "few": "Your amount : {} ",
    "many": "Your amount : {} ",
    "other": "Your amount : {} "
  },
  "gender": {
    "male": "Hi man ;) ",
    "female": "Hello girl :)",
    "with_arg": {
      "male": "Hi man ;) {}",
      "female": "Hello girl :) {}"
    }
  },
  "reset_locale": "Reset Language"
};
static const Map<String,dynamic> ru = {
  "title": "Привет!",
  "msg": "Привет! {} добро пожаловать {} мир! ",
  "msg_named": "{} написан на языке {lang}",
  "clickMe": "Нажми на меня",
  "profile": {
    "reset_password": {
      "label": "Сбросить пароль",
      "username": "Логин",
      "password": "Пароль"
    }
  },
  "clicked": {
    "zero": "Ты кликнул {} раз!",
    "one": "Ты кликнул {} раз!",
    "two": "Ты кликнул {} раза!",
    "few": "Ты кликнул {} раз!",
    "many": "Ты кликнул {} раз!",
    "other": "Ты кликнул {} раз!"
  },
  "amount": {
    "zero": "Твой счет : {} ",
    "one": "Твой счет : {} ",
    "two": "Твой счет : {} ",
    "few": "Твой счет : {} ",
    "many": "Твой счет : {} ",
    "other": "Твой счет : {} "
  },
  "gender": {
    "male": "Привет мужык ;) ",
    "female": "Привет девчуля :)",
    "with_arg": {
      "male": "Привет мужык ;) {}",
      "female": "Привет девчуля :) {}"
    }
  },
  "reset_locale": "Сбросить язык"
};
static const Map<String,dynamic> en = {
  "title": "Hello",
  "msg": "Hello {} in the {} world ",
  "msg_named": "{} are written in the {lang} language",
  "clickMe": "Click me",
  "profile": {
    "reset_password": {
      "label": "Reset Password",
      "username": "Username",
      "password": "password"
    }
  },
  "clicked": {
    "zero": "You clicked {} times!",
    "one": "You clicked {} time!",
    "two": "You clicked {} times!",
    "few": "You clicked {} times!",
    "many": "You clicked {} times!",
    "other": "You clicked {} times!"
  },
  "amount": {
    "zero": "Your amount : {} ",
    "one": "Your amount : {} ",
    "two": "Your amount : {} ",
    "few": "Your amount : {} ",
    "many": "Your amount : {} ",
    "other": "Your amount : {} "
  },
  "gender": {
    "male": "Hi man ;) ",
    "female": "Hello girl :)",
    "with_arg": {
      "male": "Hi man ;) {}",
      "female": "Hello girl :) {}"
    }
  },
  "reset_locale": "Reset Language"
};
static const Map<String,dynamic> ar = {
  "title": "السلام",
  "msg": "السلام عليكم يا {} في عالم {}",
  "msg_named": "{} مكتوبة باللغة {lang}",
  "clickMe": "إضغط هنا",
  "profile": {
    "reset_password": {
      "label": "اعادة تعين كلمة السر",
      "username": "المستخدم",
      "password": "كلمة السر"
    }
  },
  "clicked": {
    "zero": "لم تنقر بعد!",
    "one": "لقد نقرت مرة واحدة!",
    "two": "لقد قمت بالنقر مرتين!",
    "few": " لقد قمت بالنقر {} مرات!",
    "many": "لقد قمت بالنقر {} مرة!",
    "other": "{} نقرة!"
  },
  "amount": {
    "zero": "المبلغ : {}",
    "one": " المبلغ : {}",
    "two": " المبلغ : {}",
    "few": " المبلغ : {}",
    "many": " المبلغ : {}",
    "other": " المبلغ : {}"
  },
  "gender": {
    "male": " مرحبا يا رجل",
    "female": " مرحبا بك يا فتاة",
    "with_arg": {
      "male": "{} مرحبا يا رجل",
      "female": "{} مرحبا بك يا فتاة"
    }
  },
  "reset_locale": "إعادة ضبط اللغة"
};
static const Map<String,dynamic> ar_DZ = {
  "title": "السلام",
  "loginregisterview_login": "تسجيل الدخول",
  "loginregisterview_createaccount": "إنشاء حساب",
  "msg": "السلام عليكم يا {} في عالم {}",
  "msg_named": "{} مكتوبة باللغة {lang}",
  "clickMe": "إضغط هنا",
  "profile": {
    "reset_password": {
      "label": "اعادة تعين كلمة السر",
      "username": "المستخدم",
      "password": "كلمة السر"
    }
  },
  "clicked": {
    "zero": "لم تنقر بعد!",
    "one": "لقد نقرت مرة واحدة!",
    "two": "لقد قمت بالنقر مرتين!",
    "few": " لقد قمت بالنقر {} مرات!",
    "many": "لقد قمت بالنقر {} مرة!",
    "other": "{} نقرة!"
  },
  "amount": {
    "zero": "المبلغ : {}",
    "one": " المبلغ : {}",
    "two": " المبلغ : {}",
    "few": " المبلغ : {}",
    "many": " المبلغ : {}",
    "other": " المبلغ : {}"
  },
  "gender": {
    "male": " مرحبا يا رجل",
    "female": " مرحبا بك يا فتاة",
    "with_arg": {
      "male": "{} مرحبا يا رجل",
      "female": "{} مرحبا بك يا فتاة"
    }
  },
  "reset_locale": "إعادة ضبط اللغة"
};
static const Map<String,dynamic> de = {
  "title": "Hallo",
  "msg": "Hallo {} in der {} welt ",
  "msg_named": "{} ist in {lang} geschrieben",
  "clickMe": "Click mich",
  "profile": {
    "reset_password": {
      "label": "Password zurücksetzten",
      "username": "Name",
      "password": "Password"
    }
  },
  "clicked": {
    "zero": "Du hast {} mal geklickt",
    "one": "Du hast {} mal geklickt",
    "two": "Du hast {} mal geklickt",
    "few": "Du hast {} mal geklickt",
    "many": "Du hast {} mal geklickt",
    "other": "Du hast {} mal geklickt"
  },
  "amount": {
    "zero": "Deine Klicks: {}",
    "one": "Deine Klicks: {}",
    "two": "Deine Klicks: {}",
    "few": "Deine Klicks: {}",
    "many": "Deine Klicks: {}",
    "other": "Deine Klicks: {}"
  },
  "gender": {
    "male": "Hi Mann ;) ",
    "female": "Hallo Frau :)",
    "with_arg": {
      "male": "Hi Mann ;) {}",
      "female": "Hallo Frau :) {}"
    }
  },
  "reset_locale": "Sprache zurücksetzen"
};
static const Map<String,dynamic> de_DE = {
  "title": "Hallo",
  "msg": "Hallo {} in der {} welt ",
  "msg_named": "{} ist in {lang} geschrieben",
  "clickMe": "Click mich",
  "profile": {
    "reset_password": {
      "label": "Password zurücksetzten",
      "username": "Name",
      "password": "Password"
    }
  },
  "clicked": {
    "zero": "Du hast {} mal geklickt",
    "one": "Du hast {} mal geklickt",
    "two": "Du hast {} mal geklickt",
    "few": "Du hast {} mal geklickt",
    "many": "Du hast {} mal geklickt",
    "other": "Du hast {} mal geklickt"
  },
  "amount": {
    "zero": "Deine Klicks: {}",
    "one": "Deine Klicks: {}",
    "two": "Deine Klicks: {}",
    "few": "Deine Klicks: {}",
    "many": "Deine Klicks: {}",
    "other": "Deine Klicks: {}"
  },
  "gender": {
    "male": "Hi Mann ;) ",
    "female": "Hallo Frau :)",
    "with_arg": {
      "male": "Hi Mann ;) {}",
      "female": "Hallo Frau :) {}"
    }
  },
  "reset_locale": "Sprache zurücksetzen"
};
static const Map<String,dynamic> ru_RU = {
  "title": "Привет!",
  "msg": "Привет! {} добро пожаловать {} мир! ",
  "msg_named": "{} написан на языке {lang}",
  "clickMe": "Нажми на меня",
  "profile": {
    "reset_password": {
      "label": "Сбросить пароль",
      "username": "Логин",
      "password": "Пароль"
    }
  },
  "clicked": {
    "zero": "Ты кликнул {} раз!",
    "one": "Ты кликнул {} раз!",
    "two": "Ты кликнул {} раза!",
    "few": "Ты кликнул {} раз!",
    "many": "Ты кликнул {} раз!",
    "other": "Ты кликнул {} раз!"
  },
  "amount": {
    "zero": "Твой счет : {} ",
    "one": "Твой счет : {} ",
    "two": "Твой счет : {} ",
    "few": "Твой счет : {} ",
    "many": "Твой счет : {} ",
    "other": "Твой счет : {} "
  },
  "gender": {
    "male": "Привет мужык ;) ",
    "female": "Привет девчуля :)",
    "with_arg": {
      "male": "Привет мужык ;) {}",
      "female": "Привет девчуля :) {}"
    }
  },
  "reset_locale": "Сбросить язык"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en_US": en_US, "ur_PK": ur_PK, "ru": ru, "en": en, "ar": ar, "ar_DZ": ar_DZ, "de": de, "de_DE": de_DE, "ru_RU": ru_RU};
}
