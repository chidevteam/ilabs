// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const title = 'title';
  static const sub_title = 'sub_title';
  static const loginregisterview_login = 'loginregisterview_login';
  static const loginregisterview_createaccount = 'loginregisterview_createaccount';
  static const continue_button = 'continue_button';
  static const switch_eng = 'switch_eng';
  static const switch_urdu = 'switch_urdu';
  static const add_new_item_title = 'add_new_item.title';
  static const add_new_item_itemname = 'add_new_item.itemname';
  static const add_new_item_unitcost = 'add_new_item.unitcost';
  static const add_new_item_textable = 'add_new_item.textable';
  static const add_new_item_saveBtn = 'add_new_item.saveBtn';
  static const add_new_item = 'add_new_item';
  static const add_new_client_title = 'add_new_client.title';
  static const add_new_client_clientName = 'add_new_client.clientName';
  static const add_new_client_clientEmail = 'add_new_client.clientEmail';
  static const add_new_client_mobileNumber = 'add_new_client.mobileNumber';
  static const add_new_client_officePhone = 'add_new_client.officePhone';
  static const add_new_client_fax = 'add_new_client.fax';
  static const add_new_client_officeAddress = 'add_new_client.officeAddress';
  static const add_new_client_addNowBtn = 'add_new_client.addNowBtn';
  static const add_new_client = 'add_new_client';
  static const msg = 'msg';
  static const msg_named = 'msg_named';
  static const clickMe = 'clickMe';
  static const profile_reset_password_label = 'profile.reset_password.label';
  static const profile_reset_password_username = 'profile.reset_password.username';
  static const profile_reset_password_password = 'profile.reset_password.password';
  static const profile_reset_password = 'profile.reset_password';
  static const profile = 'profile';
  static const clicked = 'clicked';
  static const amount = 'amount';
  static const gender_with_arg = 'gender.with_arg';
  static const gender = 'gender';
  static const reset_locale = 'reset_locale';

}
