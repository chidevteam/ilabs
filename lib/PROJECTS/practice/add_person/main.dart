import 'package:flutter/material.dart';
import 'add_person_vu.dart';


void main() async {
  debugPrint("Starting Item View");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.orange),
      home:  AddPerson(),
    );
  }
}
