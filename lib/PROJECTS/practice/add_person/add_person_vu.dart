import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'add_person_vm.dart';
import '../common/chi_text_field.dart';

class AddPerson extends ViewModelBuilderWidget<AddPersonViewModel> {
  AddPerson({Key? key}) : super(key: key) {
    debugPrint("const called");
  }

  @override
  Widget builder(
      BuildContext context, AddPersonViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: chiAppBar("New Person", context),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
        child: SingleChildScrollView(
          child: Form(
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CHITextField(
                  initialValue: '',
                    heading: 'Name',
                    hintText: 'Enter Name',
                    func: (value) => viewModel.person = value, validator: null),
                    
                chiFloatFormField(
                    heading: 'Age',
                    hintText: 'Enter Age',
                    func: (value) => viewModel.personAge = value, validator: null),
                chiSaveButton('Save', () {
                  viewModel.onSave(context);
                  Navigator.pop(context, viewModel.person);
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    return AddPersonViewModel();
  }
}
