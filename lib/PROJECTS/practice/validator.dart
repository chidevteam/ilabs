import 'package:flutter/material.dart';

import 'package:stacked/stacked.dart';

import 'add_person/add_person_vm.dart';


class ValidatorScreen extends ViewModelBuilderWidget<AddPersonViewModel> {
  const ValidatorScreen({ Key? key }) : super(key: key);

  @override
  Widget builder(BuildContext context, AddPersonViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(title: const Text('Validator Practice'),),
      body: Form(
        key: viewModel.formKey,
        child: Column(
          children: [
            TextFormField(
              onSaved: viewModel.onSaveField,
              validator: viewModel.onFieldValidator,
            ),

            IconButton(onPressed: (){viewModel.onSave(context);}, icon: Icon(Icons.add))
          ],

      )),
    );
  }

  @override
  AddPersonViewModel viewModelBuilder(BuildContext context) {
    return AddPersonViewModel();
  }

}