import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Text headingText(String title) {
  return Text(
    title,
    style: TextStyle(
        color: Colors.grey[700], fontSize: 13.0, fontWeight: FontWeight.w400),
  );
}

chiFloatFormField({
  required String heading,
  required String hintText,
  required Function func,
  FormFieldValidator<String>? validator,
  List<TextInputFormatter>? inputFormate,
  String initialValue = "",
  TextInputType fieldType =
      const TextInputType.numberWithOptions(signed: false, decimal: false),
}) {
  return chiTextFormField2(
      initialValue: initialValue,
      fieldType: fieldType,
      func: func,
      heading: heading,
      hintText: hintText,
      validator: validator,
      inputFormate: inputFormate);
}

chiNumberFormField({
  required String heading,
  required String hintText,
  required Function func,
  FormFieldValidator<String>? validator,
  List<TextInputFormatter>? inputFormate,
  String initialValue = "",
  TextInputType fieldType = TextInputType.number,
}) {
  return chiTextFormField2(
      initialValue: initialValue,
      fieldType: fieldType,
      func: func,
      heading: heading,
      hintText: hintText,
      validator: validator,
      inputFormate: inputFormate);
}

CHITextField(
    {required String heading,
    required String hintText,
    required Function func,
    FormFieldValidator<String>? validator,
    required String? initialValue,
    TextInputType fieldType = TextInputType.text,
    int maxLines = 1}) {
  return chiTextFormField2(
      initialValue: initialValue,
      fieldType: fieldType,
      func: func,
      hintText: hintText,
      heading: heading,
      validator: validator,
      maxLines: maxLines);
}

chiTextFormField2(
    {required String heading,
    required String hintText,
    required Function func,
    int maxLines = 1,
    FormFieldValidator<String>? validator,
    List<TextInputFormatter>? inputFormate,
    ValueChanged<String>? onChangeFunc,
    String? initialValue = "",
    TextInputType fieldType = TextInputType.text}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const SizedBox(
        height: 16.0,
      ),
      headingText(heading),
      const SizedBox(
        height: 14.0,
      ),
      chiTextFormField3(
          hintText, func, validator, inputFormate, onChangeFunc,
          initialValue: initialValue!, fieldType: fieldType, maxLines: maxLines)
    ],
  );
}

Widget chiTextFormField3(
  String hintText,
  Function func,
  FormFieldValidator<String>? validator,
  List<TextInputFormatter>? inputFormate,
  ValueChanged<String>? onChangeFunc, {
  String initialValue = "",
  TextInputType fieldType = TextInputType.text,
  int maxLines = 1,
}) {
  return TextFormField(
      key: Key(initialValue),
      inputFormatters: inputFormate,
      onChanged: onChangeFunc,
      textCapitalization: TextCapitalization.sentences,
      initialValue: initialValue,
      keyboardType: fieldType,
      validator: validator,
      maxLines: maxLines,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
        fillColor: Colors.grey[200],
        filled: true,
        hintText: hintText,
        hintStyle: const TextStyle(
            fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(
              color: Colors.orange,
            )),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey.shade400),
          borderRadius: const BorderRadius.all(
            Radius.circular(12.0),
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(12.0),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(12.0),
        ),
      ),
      onSaved: (String? value) {
        func(value);
      });
}

Widget chiSaveButton(String title, VoidCallback func) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 16.0),
    child: SizedBox(
      width: double.infinity,
      height: 45.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Colors.orange[600],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            )),
        onPressed: func,
        child: Text(
          title,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
        ),
      ),
    ),
  );
}

AppBar chiAppBar(String title, BuildContext context) {
  return AppBar(
    backgroundColor: Colors.white,
    title: Text(
      title,
      style: const TextStyle(
          color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400),
    ),
    leading: IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: Icon(
        Icons.arrow_back,
        color: Colors.orange[800],
      ),
    ),
  );
}

TextField searchField() {
  return const TextField(
    decoration: InputDecoration(
      contentPadding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
      // fillColor: Colors.grey[200],
      prefixIcon: Icon(Icons.search),
      filled: true,
      hintText: "Search...",
      hintStyle: TextStyle(
          fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
          borderSide: BorderSide(
            color: Colors.orange,
          )),
      // enabledBorder: OutlineInputBorder(
      //   borderSide: BorderSide(color: Colors.grey.shade400),
      //   borderRadius: BorderRadius.all(
      //     Radius.circular(12.0),
      //   ),
      // ),
      // errorBorder: OutlineInputBorder(
      //   borderSide:  BorderSide(color: Colors.red),
      //   borderRadius: BorderRadius.circular(12.0),
      // ),
      // focusedErrorBorder: OutlineInputBorder(
      //   borderSide:  BorderSide(color: Colors.red),
      //   borderRadius: BorderRadius.circular(12.0),
      // ),
    ),
  );
}
