import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:stacked/stacked.dart';
// import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import '../add_person/add_person_vu.dart';

import 'person_cell_vu.dart';
import 'person_list_vm.dart';

class PersonListScreen extends ViewModelBuilderWidget<PersonListViewModel> {
  const PersonListScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, PersonListViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan[900],
        title: const Text(
          'Persons',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                LazyLoadScrollView(
                  scrollOffset: 10,
                  onEndOfPage: () {
                    viewModel.onProgressIndicator();
                  },
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: viewModel.showAbleList.length,
                      itemBuilder: (context, index) {
                        return itemCell(context, viewModel,
                            viewModel.showAbleList[index], index);
                      }),
                ),
                Positioned(
                    left: 160,
                    top: 560,
                    child: viewModel.isLoading
                        ? const CircularProgressIndicator(
                            backgroundColor: Colors.black,
                            color: Colors.grey,
                          )
                        : const Text(''))
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: floatAddItem(context, viewModel),
    );
  }

  FloatingActionButton floatAddItem(
    BuildContext context,
    PersonListViewModel vm,
  ) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddPerson(),
            )).then((value) {
          vm.person = value;
          vm.onAddData();
        });
      },
      backgroundColor: const Color.fromRGBO(0x00, 0x00, 0xff, 1.0),
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  @override
  viewModelBuilder(BuildContext context) {
    var vm = PersonListViewModel();
    vm.onDbList();
    vm.onPopulateShowAbleList();
    return vm;
  }
}
