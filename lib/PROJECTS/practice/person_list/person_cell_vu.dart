import 'package:flutter/material.dart';
import '../add_person/add_person_vu.dart';
import '../model/person_model.dart';
import 'person_list_vm.dart';

Widget itemCell(BuildContext context, PersonListViewModel viewModel,
    Person item, int index) {
  return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddPerson(),
            )).then((v) {
          viewModel.onUpdate(item, index);
        });
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 0.7,
          child: IntrinsicHeight(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 14.0, 12.0, 14.0),
              child: Row(
                children: [
                  Text(
                    item.name!,
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const Spacer(),
                  Text(
                    item.age.toString(),
                    style: const TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.grey,
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        viewModel.onDelete(index);
                      },
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red[900],
                      ))
                ],
              ),
            ),
          ),
        ),
      ));
}
