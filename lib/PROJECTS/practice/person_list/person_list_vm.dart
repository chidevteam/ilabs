import 'package:flutter/material.dart';
// import 'package:ilabs/PROJECTS/practice/model/album.dart';
import 'package:stacked/stacked.dart';

import '../model/person_model.dart';
// import 'package:http/http.dart' as http;

class PersonListViewModel extends BaseViewModel {
  Person? person;
  bool isLoading= false;
  
  List<Person> dbList=[];
  List<Person> showAbleList=[];  
  List<Person> items = [
    Person('ALi', 23),
    Person('Hamza', 34),
    Person('Ahsan', 25),
    Person('Sara', 29),
    Person('Omair', 24),
    Person('ALi Hassan', 23),
    Person('Hamza Mehmood', 34),
    Person('Ahsan Farooq', 25),
    Person('Sara Qarab', 29),
    Person('Hafiz Omair ', 24),
  ];

// Future<Album> fetchAlbum() async {
//   final response = await http
//       .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/1'));

//   if (response.statusCode == 200) {
//     print(response.body);
//     albumList = (jsonDecode(response.body) as List)
//     .map((data) => Album.fromJson(data))
//     .toList();
//      notifyListeners();
//   } else {
//     throw Exception('Failed to load album');
//   }
// }

  onDbList(){
    for (int i=0; i<100; i++){
      dbList.add(Person('User$i', i));
      notifyListeners();
    }
  }

  onPopulateShowAbleList(){
    for(int i=0; i<=10; i++){
      showAbleList.add(dbList[i]);
      notifyListeners();
    }
    
  }

  onLoadMore(){
    
    for(int i=showAbleList.length; i<showAbleList.length+10; i++){
      print(i);
      if(i<dbList.length) {
        showAbleList.add(dbList[i]);
      }
      if((i>=20) && (i%10 == 0)){
        break;
      }
      if(i==99){
      break;
    }
    }
  }
  
  onProgressIndicator() async {
      isLoading = true;
    notifyListeners();
    await Future.delayed(const Duration(seconds: 2));
    isLoading = false;
    await onLoadMore();
     notifyListeners();
  } 


  onAddData() {
    items.add(person!);
    notifyListeners();
  }

  onUpdate(Person item, int index) {
    items[index] = item;
    notifyListeners();
  }

  onDelete(int index) {
    items.removeAt(index);
    notifyListeners();
    debugPrint("$items");
    notifyListeners();
  }
}
