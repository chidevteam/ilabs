class Person {
  final String? name;
  final int? age;

  Person(
    this.name,
    this.age,
  );

  @override
  String toString() {
    String st = '';
    st += "description:: $name\n";
    st += "unitCost:: $age\n";
    return st;
  }
}
