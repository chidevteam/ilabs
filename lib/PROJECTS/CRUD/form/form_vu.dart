import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'form_vm.dart';
import '../model/person.dart';

class FormViewScreen extends ViewModelBuilderWidget<FormModel> {
  FormViewScreen(this.person, {Key? key}) : super(key: key);

  Person? person;

  @override
  Widget builder(BuildContext context, FormModel viewModel, Widget? child) {
    debugPrint("Person $person");
    // person = Person("Imran khan", 70, 1.99);
    return Scaffold(
        appBar: AppBar(title: Text(viewModel.title)),
        body: SafeArea(
            child: Form(
          key: viewModel.formKey,
          child: Column(
            children: [
              Text(viewModel.title),
              TextFormField(
                  onSaved: viewModel.onSaveName,
                  initialValue: person == null ? "" : person!.name),
              TextFormField(
                  onSaved: viewModel.onSaveAge,
                  initialValue: person == null ? "" : person!.age.toString()),
              ElevatedButton(
                  onPressed: () {
                    viewModel.onSave();
                    Navigator.pop(context, viewModel.person);
                  },
                  child: const Text("Save"))
            ],
          ),
        )));
  }

  @override
  FormModel viewModelBuilder(BuildContext context) {
    return FormModel();
  }
}
