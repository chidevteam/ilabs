
To make a form define formKey in VM
- final formKey = GlobalKey<FormState>();
- In VU, add the key: in Form()
    key: viewModel.formKey,
  The textFields are the children of form

- Define a field, e.g. 
    TextFormField(
                  onSaved: viewModel.onSaveName,
                                     ----------
                  initialValue: person == null ? "" : person!.name),

- Corresponding to field, implement the save func in VM
  onSaveName(value) {
    name = value;
  }

- In VM onSave do the following:
     onSave() {
    formKey.currentState!.save(); ,------ will call the onSaveXX methods
    person = Person(name!, int.parse(age!), 22.3);
    notifyListeners();
  }


