import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../model/person.dart';

class FormModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();

  String title = 'List View';
  Person? person;
  String? name;
  String? age;

  onSaveName(value) {
    name = value;
  }

  onSaveAge(value) {
    age = value;
  }

  onSave() {
    formKey.currentState!.save();
    person = Person(name!, int.parse(age!), 22.3);
    notifyListeners();
  }
}
