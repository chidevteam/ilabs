import 'package:flutter/material.dart';
import 'list/list_vu.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.orange),
      home: const ListViewScreen(),
    );
  }
}

void main() {
  debugPrint("Starting3");
  runApp(const MyApp());
}
