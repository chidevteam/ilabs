
11:10am Start
11:23am Made base structure, running now
11:33am Added list, made a widget of Cell
11:50am Added floating button and pushed
11:52am pop added

=====================================================


BASE APP / STRUCTURE (10 MIN)
- make hello world app using vu/vm

READ (10-15min)
- Add list of persons
  Person Model
    name, email, height, age
- Add Listview builder
  
CREATE (10-15min)
- Add String and Integer inputs, float
- On press of button print

LINK CREATE AND READ (10min)
- Add button in read
  Push Create
  Pop in Create 
  Update data

UPDATE (5-10min)
- Tap on List cell to open UPDATE/CREAT
  on pop, just update

DELETE (5min)
    