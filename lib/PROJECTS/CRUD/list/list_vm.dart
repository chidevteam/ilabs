import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../model/person.dart';

class ListViewModel extends BaseViewModel {
  String title = 'List View';
  List<Person> personList = [];
  int ccc = 0;

  ListViewModel() {
    for (int i = 0; i < 10; i += 1) {
      personList.add(Person("Ahmad", 55, 1.87));
      personList.add(Person("Ali", 22, 1.77));
      personList.add(Person("Omair", 52, 1.97));
      personList.add(Person("Ahmad", 55, 1.87));
      personList.add(Person("Ali", 22, 1.77));
      personList.add(Person("Omair", 52, 1.97));
    }
  }

  addPerson(Person p) {
    personList.add(p);
    notifyListeners();
  }

  delete(int index) {
    personList.removeAt(index);
    notifyListeners();
    debugPrint("$personList");
  }

  update(int index, Person p) {
    personList[index] = p;
    notifyListeners();
  }
}
