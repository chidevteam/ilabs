import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'list_vm.dart';
import 'cell_vu.dart';
import '../form/form_vu.dart';
import '../model/person.dart';

class ListViewScreen extends ViewModelBuilderWidget<ListViewModel> {
  const ListViewScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, ListViewModel viewModel, Widget? child) {
    // debugPrint(viewModel.personList.length);
    return Scaffold(
      appBar: AppBar(title: Text(viewModel.title)),
      body: ListView.builder(
          itemCount: viewModel.personList.length,
          itemBuilder: (context, index) {
            debugPrint("Creating item at : $index");
            Person item = viewModel.personList[index];
            viewModel.ccc++;
            return Dismissible(
                key: Key("${viewModel.ccc}"),
                onDismissed: (direction) {
                  viewModel.delete(index);
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      backgroundColor: Colors.green,
                      duration: const Duration(seconds: 0, milliseconds: 750),
                      content: Text('$item dismissed')));
                },
                background: Container(color: Colors.red),
                child: CellView(viewModel, item, index));
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FormViewScreen(null),
              )).then((value) {
            viewModel.addPerson(value);
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  @override
  ListViewModel viewModelBuilder(BuildContext context) {
    return ListViewModel();
  }
}
