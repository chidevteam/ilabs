import 'package:flutter/material.dart';
import '../model/person.dart';
import '../form/form_vu.dart';
import 'list_vm.dart';

class CellView extends StatelessWidget {
  const CellView(this.viewModel, this.person, this.index, {Key? key})
      : super(key: key);

  final ListViewModel viewModel;
  final Person person;
  final int index;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FormViewScreen(person),
            )).then((value) {
          viewModel.update(index, value);
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          children: [
            Text(
              person.name + " $index",
              style: const TextStyle(fontSize: 26),
            ),
            const Spacer(),
            IconButton(
                onPressed: () {
                  // print("$index");
                  viewModel.delete(index);
                },
                icon: const Icon(Icons.delete))
          ],
        ),
      ),
    );
  }
}
