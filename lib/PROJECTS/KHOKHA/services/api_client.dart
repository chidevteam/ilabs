// ignore_for_file: avoid_print
// import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;

typedef Request = Map<String, dynamic>;
typedef Response = Map<String, dynamic>;
typedef ModelFromJson = dynamic Function(Map<String, dynamic> resp);

class ApiClient {
  static ApiClient? _instance;
  String mBaseUrl = "";
  String? mAuthToken = "";

  static void create(String baseURL) => instance.mBaseUrl = baseURL;
  static ApiClient get instance => _instance ??= ApiClient();

  static Future<Response> login(String email, String password) async {
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    Map<String, dynamic> req = {"email": email, "password": password};

    http.Response resp = await http.post(
      Uri.parse(ApiClient.instance.mBaseUrl + "/auth/login"),
      headers: makeHeader(),
      body: jsonEncode(req),
    );

    var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    print("Time taken: ${endTime - startTime}");
    Response json = jsonDecode(resp.body);
    String? token = json["data"]["auth_token"] as String?;
    ApiClient.instance.mAuthToken = token;
    return json;
  }

  static void printDesc() {
    print("BaseURL:${instance.mBaseUrl}\nAuth: ${instance.mAuthToken}");
  }

  static Map<String, String> makeHeader() {
    String? token = ApiClient.instance.mAuthToken;
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
    };
    if (token != null) header['Authorization'] = token;
    return header;
  }

  static Future<Response> get<T>(
      {required String endPoint,
      String params = "",
      required ModelFromJson? fromJson}) async {
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance.mBaseUrl + endPoint + params;
    final response = await http.get(Uri.parse(url), headers: makeHeader());

    // print(response);
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      final json = jsonDecode(response.body);
      print("======================");
      print(json);
      T model = fromJson!(json);
      print("$model");
      return {"data": model, "time": endTime - startTime, "status": "success"};
    } else {
      throw Exception('Error Fetching Error from $endPoint');
    }
  }

  static Future<Response> post<T>(
      {required Request request,
      required String endPoint,
      required ModelFromJson? fromJson}) async {
    var startTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
    String url = ApiClient.instance.mBaseUrl + endPoint;
    Map<String, String> header = makeHeader();
    final response = await http.post(
      Uri.parse(url),
      headers: header,
      body: jsonEncode(request),
    );
    if (response.statusCode == 200) {
      var endTime = DateTime.now().millisecondsSinceEpoch / 1000.0;
      var json = jsonDecode(response.body);
      T model = fromJson!(json);
      return {"data": model, "time": endTime - startTime, "status": "success"};
    } else {
      print(response);
      throw Exception('Failed to Post to $endPoint');
    }
  }
}
