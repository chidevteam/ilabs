import 'package:flutter/cupertino.dart';
import 'screens/home/home_vu.dart';
import 'screens/add_item/add_item_vu.dart';

Map<String, WidgetBuilder> appRoutes = {
  '/': (context) => const HomeScreen(),
  '/additems': (context) => const AddItemScreen()
};
