Map<String, dynamic> response = {
  "status": "Ok",
  "data": [
    {
      "entry_id": 6,
      "name": "Ahmad Hassan",
      "entry_type": "Time Entry",
      "start": 1643530000,
      "stop": 1643530090,
      "duration": 90,
      "notes": "na"
    },
    {
      "entry_id": 7,
      "name": "Ahmad Hassan",
      "entry_type": "Time Entry",
      "start": 1643771894,
      "stop": 1643771900,
      "duration": 6,
      "notes": null
    },
    {
      "entry_id": 8,
      "name": "Ahmad Hassan",
      "entry_type": "Break",
      "start": 1643529000,
      "stop": 1643529090,
      "duration": 90,
      "notes": "just relax break"
    }
  ],
  "message": "TimeEntry List fetched successfully."
}
;