class TimeStartResponse {
  final String? status;
  final TimeStart? data;
  final String? message;

  TimeStartResponse(
    this.status,
    this.data,
    this.message,
  );

  static TimeStartResponse fromMap(Map<String, dynamic> resp) {
    return TimeStartResponse(
      resp['status'] as String?,
      TimeStart.fromMap(resp['data']),
      resp['message'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "status:: ${status}\n";
    st += "data:: ${data.toString()}\n";
    st += "message:: ${message}\n";
    return st;
  }
}

class TimeStart {
  final int? entryId;
  final int? start;
  final String? notes;

  TimeStart(
    this.entryId,
    this.start,
    this.notes,
  );

  static TimeStart fromMap(Map<String, dynamic> resp) {
    return TimeStart(
      resp['entry_id'] as int?,
      resp['start'] as int?,
      resp['notes'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "status:: ${entryId.toString()}\n";
    st += "data:: ${start.toString()}\n";
    st += "message:: ${notes}\n";
    return st;
  }
}
