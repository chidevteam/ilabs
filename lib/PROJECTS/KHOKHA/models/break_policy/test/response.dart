Map<String, dynamic> response = {
  "status": "Ok",
  "data": [
    {"policy_id": 3, "policy_name": "Lunch Break", "allotted_duration": 3600},
    {"policy_id": 4, "policy_name": "Prayer Break", "allotted_duration": 900},
    {
      "policy_id": 5,
      "policy_name": "Tea / Smoke Break",
      "allotted_duration": 900
    }
  ],
  "message": "BreakPolicy List fetched successfully."
};
