// ignore_for_file: avoid_print
import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  ApiClient.create("https://time-track.cognitivehealthintl.com");
  String email = "ahmad@email.com";
  String password = "Ahmad@123";
  Response resp = await ApiClient.login(email, password);
  print(resp);
  await testApi();
}

Future testApi() async {
  Response resp = await ApiClient.post<SessionResponse>(
      request: {},
      endPoint: "/api/app/session",
      fromJson: SessionResponse.fromMap);
  SessionResponse iList = resp["data"];
  print(iList);
}
