Map<String, dynamic> response = {
  "status": "Ok",
  "data": {
    "session_id": "71b29e207b8f435aac97db88d540f8df",
    "user_id": 7,
    "exp": 1643854080.0358512,
    "user": {
      "user_id": 7,
      "name": "Ahmad Hassan",
      "role": "user",
      "detail": {
        "organization_id": 2,
        "organization_name": "Test Organization",
        "industry": "IT",
        "logo": "/assets/img/logo.png",
        "address": null,
        "start_of_week": 0,
        "date_added": 1643720372,
        "date_updated": 1643720372
      }
    },
    "ip_address": "10.8.0.21"
  },
  "message": "Session data fetched successfully"
}
;