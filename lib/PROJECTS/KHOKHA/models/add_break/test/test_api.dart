// ignore_for_file: avoid_print
import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  ApiClient.create("https://time-track.cognitivehealthintl.com");
  String email = "ahmad@email.com";
  String password = "Ahmad@123";
  Response resp = await ApiClient.login(email, password);
  print(resp);
  await testApi();
}

Future testApi() async {
  Request req = {
    "entry_type": "Time Entry",
    "start": 1643530000,
    "stop": 1643530090,
    "notes": "just relax break",
    "policy_id": 1
  };
  Response resp = await ApiClient.post<TimeEntry>(
      request: req, endPoint: "/api/app/add_time", fromJson: TimeEntry.fromMap);
  TimeEntry iList = resp["data"];
  print(iList);
}
