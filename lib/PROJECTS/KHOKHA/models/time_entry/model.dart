class TimeEntry {
  final String? status;
  final int? data;
  final String? message;

  TimeEntry(
    this.status,
    this.data,
    this.message,
  );

  static TimeEntry fromMap(Map<String, dynamic> resp) {
    return TimeEntry(
      resp['status'] as String?,
      resp['data'] as int?,
      resp['message'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "status:: ${status}\n";
    st += "data:: ${data.toString()}\n";
    st += "message:: ${message}\n";
    return st;
  }
}
