import 'package:flutter/material.dart';
import 'routes.dart';
// import 'home_vu.dart';
import '../../iLabs/services/api_client.dart';

void main() async {
  ApiClient.create("https://invoicelabs.co/api");
  // await ApiClient.login(email!, password!);
  // await ApiClient.login("vocalmatrix@gmail.com", "test1234");
  await ApiClient.login("ahmadhassanch@hotmail.com", "test1234");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: appRoutes,
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      // home: const HomeScreen(),
    );
  }
}
