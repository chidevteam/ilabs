import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'home_vm.dart';
import '../../models/Item.dart';
import '../invoice/invoice.dart';
// import '../estimate/estimate_screen.dart';
// import '../../../ilabs/screens/estimate/estimate_list_vu.dart';
import '../../../../iLabs/home/bottomNavbar/estimate/estimate_list_vu.dart';
import '../chat/clients_vu.dart';
import '../items/items_vu.dart';

class HomeScreen extends ViewModelBuilderWidget<HomeViewModel> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget? child) {
    List<Widget> widgetOptions = [];

    widgetOptions.add(InvoiceScreen(viewModel));
    // widgetOptions.add(const EstimateListScreen());
    widgetOptions.add(ClientScreenView(viewModel));
    widgetOptions.add(ItemScreen(viewModel));
    debugPrint("elevation = ${viewModel.elevation}");

    return Scaffold(
      appBar: AppBar(
        title: Text(viewModel.title),
        elevation: viewModel.elevation,
      ),
      body: IndexedStack(
        index: viewModel.selectedIndex,
        children: widgetOptions,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.article),
            label: 'Invoices',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calculate),
            label: 'Estimates',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Clients',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.card_travel), label: 'Items'),
        ],
        currentIndex: viewModel.selectedIndex,
        onTap: viewModel.onItemTapped,
      ),
      floatingActionButton: floatingButton(context, '/additems', viewModel),
    );
  }

  FloatingActionButton floatingButton(
      BuildContext context, String route, HomeViewModel viewModel) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.pushNamed(context, route).then((value) {
          viewModel.temp = value as Item;
          viewModel.itemList.add(viewModel.temp!);
          viewModel.notifyListeners();
        });
      },
      backgroundColor: const Color.fromRGBO(0xfa, 0x00, 0x00, 1.0),
      child: const Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    HomeViewModel vm = HomeViewModel();
    vm.fetchAlbum();
    return vm;
  }
}
