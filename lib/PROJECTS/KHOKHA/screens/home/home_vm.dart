import 'dart:convert';

import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import '../../models/Item.dart';
import '../../models/album.dart';
import 'package:http/http.dart' as http;

class HomeViewModel extends BaseViewModel {
  BuildContext? mContext;
  double elevation = 0.0;
  String title = "No title";
  List<String> titles = ["Invoices", "Estimates", "Clients", "Items"];

  int selectedIndex = 0;
  List<Item> itemList = [
    Item('Pen', '10'),
    Item('Pencil', '10'),
  ];
  Item? temp;
  // static const TextStyle optionStyle =
  // TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void onItemTapped(int index) {
    selectedIndex = index;
    debugPrint("index = $index");
    if (index == 3) {
      elevation = 5.0;
    } else {
      elevation = 0.0;
    }
    title = titles[index];

    notifyListeners();
  }

  List<Album> albumList = [];
  List<Future<Album>>? albumAPIList;

  Future<dynamic> fetchAlbum() async {
    // setBusy(true);
    print("Gsdfdsafsdfoing to fetch Album ................******************");
    final response = await http
        .get(Uri.parse('https://jsonplaceholder.typicode.com/albums'));

    if (response.statusCode == 200) {
      // setBusy(false);
      print('API call successful');
      // If the server did return a 200 OK response,
      // then parse the JSON.
      albumList = (jsonDecode(response.body) as List)
          .map((data) => Album.fromJson(data))
          .toList();
      notifyListeners();
      //print(albumList[0].title);
    } else {
      // setBusy(false);
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  updateList(Item value) {
    temp = value;
    itemList.add(temp!);
    notifyListeners();
  }

  //add item view Model
  final formKey = GlobalKey<FormState>();
  String? itemName = '';
  String? unitCode = '';
  //InvoiceItemListViewModel prevScreenVM = InvoiceItemListViewModel();

  onItemName(String? value) async {
    itemName = value!.trim();
    notifyListeners();
  }

  String? onItemNameValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onItemUnitCode(String? value) async {
    unitCode = value!.trim();

    notifyListeners();
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    debugPrint('$itemName');
    debugPrint('$unitCode');

    // Request addItemReq = {
    //   "taxable": true,
    //   "description": itemName,
    //   "additional_detail": "This unit",
    //   "unit_cost": unitCode
    // };
    // ApiClient.post(request: addItemReq, endPoint: "/items", fromJson: null);
    // // var item = await addItemFirebase(addItemReq);
    // // notifyListeners();
    // Navigator.pop(context);
    // prevScreenVM.refreshData(context);
  }
}
