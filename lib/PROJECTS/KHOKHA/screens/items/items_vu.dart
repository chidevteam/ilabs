import 'package:flutter/material.dart';
import '../home/home_vm.dart';
// import 'package:stacked/stacked.dart';

class ItemScreen extends StatelessWidget {
  final HomeViewModel viewModel;
  const ItemScreen(this.viewModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.blue,
        child: const Text('new Item Screen'),
      ),
    );
  }
}
