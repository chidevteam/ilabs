import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'all.dart';
import 'outstanding.dart';
import 'paid.dart';

import '../home/home_vm.dart';

class InvoiceScreen extends ViewModelBuilderWidget<HomeViewModel> {
  final HomeViewModel viewModel;

  const InvoiceScreen(this.viewModel);

  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget? child) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            flexibleSpace: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                TabBar(tabs: [
                  Tab(text: 'All'),
                  Tab(text: 'Outsanding'),
                  Tab(text: 'Paid'),
                ])
              ],
            ),
          ),
          body: TabBarView(
            children: [
              AllScreen(viewModel),
              const OutstandingScreen(),
              const PaidScreen(),
            ],
          )),
    );
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    return viewModel;
  }
}
