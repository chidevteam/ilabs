import 'package:flutter/material.dart';

class PaidScreen extends StatelessWidget {
  const PaidScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Paid'),
    );
  }
}
