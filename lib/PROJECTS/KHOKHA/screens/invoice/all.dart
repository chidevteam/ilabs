import 'package:flutter/material.dart';
import '../home/home_vm.dart';

class AllScreen extends StatelessWidget {
  final HomeViewModel viewModel;
  const AllScreen(this.viewModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: ListView.builder(
            itemCount: viewModel.itemList.length,
            itemBuilder: (context, index) {
              return Center(
                child: Text(viewModel.itemList[index].itemName.toString()),
              );
            }),
      ),
    );
  }
}
