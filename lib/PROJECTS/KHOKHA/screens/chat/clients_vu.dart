// ignore_for_file: must_be_immutable, use_key_in_widget_constructors

import 'package:flutter/material.dart';
// import 'package:stacked/stacked.dart';
import '../home/home_vm.dart';

class ClientScreenView extends StatelessWidget {
  HomeViewModel viewModel;
  ClientScreenView(this.viewModel);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: viewModel.fetchAlbum,
      child: viewModel.isBusy
          ? const Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: viewModel.albumList.length,
              itemBuilder: (context, index) {
                return Center(
                    child: Text(viewModel.albumList[index].title.toString()));
              }),
    );
  }

  // @override
  // Widget builder(BuildContext context, HomeViewModel viewModel, Widget? child) {
  //   return viewModel.isBusy
  //       ? const Center(child: CircularProgressIndicator())
  //       : ListView.builder(
  //           itemCount: viewModel.albumList.length,
  //           itemBuilder: (context, index) {
  //             return Center(
  //                 child: Text(viewModel.albumList[index].title.toString()));
  //           });
  // }

  // @override
  // HomeViewModel viewModelBuilder(BuildContext context) {
  //   viewModel.fetchAlbum();
  //   return viewModel;
  // }
}
