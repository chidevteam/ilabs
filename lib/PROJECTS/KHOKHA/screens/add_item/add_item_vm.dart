import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../models/Item.dart';

class AddItemViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String? itemName = '';
  String? unitCode = '';

  List<Item>? itemList = [];
  Item? item;
  //InvoiceItemListViewModel prevScreenVM = InvoiceItemListViewModel();

  onItemName(String? value) async {
    itemName = value!.trim();
    notifyListeners();
  }

  String? onItemNameValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onItemUnitCode(String? value) async {
    unitCode = value!.trim();

    notifyListeners();
  }

  String? onItemUnitCodeValidator(value) {
    if (value == null || value.isEmpty) {
      return 'item is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();

    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    print('$itemName');
    print('$unitCode');
    //itemList!.add(Item(itemName, unitCode));
    item = Item(itemName, unitCode);
    print('${item!.itemName}');
    print('${item!.unitCost}');
    //print('$unitCode');
    // Request addItemReq = {
    //   "taxable": true,
    //   "description": itemName,
    //   "additional_detail": "This unit",
    //   "unit_cost": unitCode
    // };
    // ApiClient.post(request: addItemReq, endPoint: "/items", fromJson: null);
    // // var item = await addItemFirebase(addItemReq);
    // // notifyListeners();
    Navigator.pop(context, item);
    // prevScreenVM.refreshData(context);
  }

// Future<void> addItemFirebase(Map item) {
//   // Call the user's CollectionReference to add a new user
//   CollectionReference items = FirebaseFirestore.instance.collection('items');
//   return items
//       .add(item)
//       .then((value) => print("Item Added2"))
//       .catchError((error) => print("Failed to add item: $error"));
// }
}
