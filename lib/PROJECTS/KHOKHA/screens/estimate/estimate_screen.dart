import 'package:flutter/material.dart';

class EstimateScreen extends StatelessWidget {
  const EstimateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('build for CameraScreen called');
    return const Center(
      child: Text(
        'Index 1: Camera from other file',
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      ),
    );
  }
}
