// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_core/firebase_core.dart';
// ignore_for_file: prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'counter_vm.dart';

class CounterScreen extends ViewModelBuilderWidget<CounterViewModel> {
  const CounterScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, CounterViewModel viewModel, Widget? child) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
          top: 30,
        ),
        child: TextButton(
          onPressed: viewModel.buttonPressed,
          child: const Text('TextButton 1'),
        ),
      ),
    );
  }

  @override
  CounterViewModel viewModelBuilder(BuildContext context) {
    return CounterViewModel();
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.orange),
        home: const CounterScreen());
  }
}

void main() async {
  // print("Starting");
  // WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp(
  //   options: DefaultFirebaseOptions.currentPlatform,
  // );
  // await addUser();
  runApp(const MyApp());
}
