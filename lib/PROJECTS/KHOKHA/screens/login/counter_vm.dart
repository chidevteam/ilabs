import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
// import '../../services/api_client.dart';

class CounterViewModel extends BaseViewModel {
  int counter = 0;

  void buttonPressed() {
    counter += 1;
    debugPrint("Here $counter");
  }
}
