// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const title = 'title';
  static const sub_title = 'sub_title';
  static const loginregisterview_login = 'loginregisterview_login';
  static const loginregisterview_createaccount =
      'loginregisterview_createaccount';
  static const continue_button = 'continue_button';
  static const switch_eng = 'switch_eng';
  static const switch_urdu = 'switch_urdu';
  static const msg = 'msg';
  static const msg_named = 'msg_named';
  static const clickMe = 'clickMe';
  static const profile_reset_password_label = 'profile.reset_password.label';
  static const profile_reset_password_username =
      'profile.reset_password.username';
  static const profile_reset_password_password =
      'profile.reset_password.password';
  static const profile_reset_password = 'profile.reset_password';
  static const profile = 'profile';
  static const clicked = 'clicked';
  static const amount = 'amount';
  static const gender_with_arg = 'gender.with_arg';
  static const gender = 'gender';
  static const reset_locale = 'reset_locale';
}
