import 'package:flutter/material.dart';

Drawer makeDrawer(BuildContext context) {
  return Drawer(
    child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          decoration:
              const BoxDecoration(color: Color.fromRGBO(0xff, 0x97, 0x57, 1.0)),
          accountName: const Text("Abass Makinde"),
          accountEmail: const Text("abs@gmail.com"),
          currentAccountPicture: GestureDetector(
            child: const CircleAvatar(
              child: Text(
                "AM",
                style: TextStyle(
                    color: Color.fromRGBO(0xff, 0x97, 0x57, 1.0),
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
              backgroundColor: Colors.white,
            ),
          ),
        ),
        drawerCell(context, "Business Details ", Icons.note_add,
            '/businessDetailsScreen'),
        drawerCell(context, "Settings", Icons.settings, '/settingScreen'),
        drawerCell(context, "Choose Template", Icons.pending_actions, ''),
        drawerCell(context, "Region", Icons.local_airport, ''),
        drawerCell(context, "Upgrade Subscription", Icons.ac_unit, ''),
        drawerCell(context, "Switch Account", Icons.manage_accounts, ''),
        drawerCell(context, "Contact Us", Icons.contact_mail, ''),
        drawerCell(context, "Help", Icons.help, ''),
        drawerCell(context, "Termsof Use", Icons.theater_comedy_sharp, ''),
        drawerCell(context, "Privacy Plicy", Icons.privacy_tip_outlined, ''),
        drawerCell(context, "Log Out", Icons.logout, '/logIn'),
      ],
    ),
  );
}

ListTile drawerCell(
    BuildContext context, String lable, IconData icon, String route) {
  return ListTile(
    title: Column(
      children: [
        Row(
          children: [
            Icon(
              icon,
              color: const Color.fromRGBO(0xff, 0x97, 0x57, 1.0),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Text(
                lable,
                style: const TextStyle(
                    color: Colors.black54,
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
        const Divider(),
      ],
    ),
    onTap: () {
      Navigator.pop(context);
      Navigator.pushNamed(context, route);
    },
  );
}
