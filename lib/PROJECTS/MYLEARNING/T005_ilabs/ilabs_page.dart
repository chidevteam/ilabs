import 'package:flutter/material.dart';
import 'drawer_pink.dart';

class BusinessScreen extends StatelessWidget {
  const BusinessScreen(this.title, this.tbar, {Key? key}) : super(key: key);

  final String title;
  final bool tbar;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: appBarr(title, tbar),
        drawer: makeDrawer(context),
        body: const TabBarView(
          children: [
            Icon(Icons.directions_car),
            Icon(Icons.directions_transit),
            Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }
}

AppBar appBarr(String title, bool isTabBar) {
  return AppBar(
    title: Center(
      child: Text(
        title,
        style: const TextStyle(color: Colors.black),
      ),
    ),
    actions: [
      Builder(
        builder: (context) => IconButton(
          color: Colors.black,
          icon: const Icon(Icons.home),
          onPressed: () {
            Scaffold.of(context).openEndDrawer();
          },
        ),
      ),
    ],
    bottom: isTabBar ? tabBarItems(title) : null,
    leading: Builder(
      builder: (context) => IconButton(
        color: Colors.black,
        icon: const Icon(Icons.menu),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
      ),
    ),
    backgroundColor: Colors.white,
  );
}

TabBar tabBarItems(String lable) {
  return TabBar(
    unselectedLabelColor: Colors.black45,
    indicatorWeight: 1,
    indicatorColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
    labelColor: const Color.fromRGBO(0xfa, 0x77, 0x47, 1.0),
    labelPadding: EdgeInsets.zero,
    tabs: [
      Tab(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border(
            right: BorderSide(width: 1, color: Colors.grey.shade400),
          )),
          child: const Padding(
            padding: EdgeInsets.only(left: 46),
            child: Text(
              'All',
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ),
      Tab(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              (lable.contains("Invoices") ? 'Outstanding' : "Open"),
              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
            ),
          ],
        ),
      ),
      Tab(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border(
            left: BorderSide(width: 1, color: Colors.grey.shade400),
          )),
          child: Padding(
            padding: const EdgeInsets.only(left: 42),
            child: Text(
              lable.contains("Invoices") ? 'Paid' : "Closed",
              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ),
    ],
  );
}
