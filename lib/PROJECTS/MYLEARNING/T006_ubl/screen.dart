import 'package:flutter/material.dart';

Widget ubl1() {
  return Container(
    width: 382,
    height: 800,
    color: Color(0xff2d2d2d),
    child: Stack(
      children: [
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              width: 116,
              height: 116,
              child: FlutterLogo(size: 116),
            ),
          ),
        ),
        Positioned.fill(
          child: Align(
            alignment: Alignment.topLeft,
            child: Container(
              width: 382,
              height: 47,
              child: FlutterLogo(size: 47),
            ),
          ),
        ),
        Positioned(
          left: 22,
          top: 55,
          child: Container(
            width: 36,
            height: 36,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xff3b3b3b),
            ),
          ),
        ),
        Positioned(
          left: 66,
          top: 55,
          child: Container(
            width: 36,
            height: 36,
            child: Stack(
              children: [
                Container(
                  width: 36,
                  height: 36,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color(0xff3b3b3b),
                  ),
                ),
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 19,
                      height: 18,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: FlutterLogo(size: 18),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 21,
          top: 118,
          child: Text(
            "Good Evening",
            style: TextStyle(
              color: Color(0xffc1c1c1),
              fontSize: 15,
            ),
          ),
        ),
        Positioned(
          left: 21,
          top: 138,
          child: Text(
            "M Atif Mehmood",
            style: TextStyle(
              color: Color(0xffc1c1c1),
              fontSize: 25,
              fontFamily: "Gilroy",
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Positioned(
          left: 33,
          top: 65,
          child: Container(
            width: 13.45,
            height: 15.31,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
            ),
            child: FlutterLogo(size: 13.451745986938477),
          ),
        ),
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              width: 382,
              height: 62,
              color: Color(0xff1f1f1f),
              padding: const EdgeInsets.symmetric(
                horizontal: 36,
                vertical: 11,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 56,
                    height: 40,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 19.32,
                          height: 19.32,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: FlutterLogo(size: 19.31968879699707),
                        ),
                        SizedBox(height: 5.68),
                        Text(
                          "Accounts",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 67),
                  Container(
                    width: 59,
                    height: 39,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 29.01,
                          height: 19,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: FlutterLogo(size: 19.0007266998291),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Payments",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 67),
                  Container(
                    width: 61,
                    height: 39,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 18,
                          height: 17,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xfff8f8f8),
                          ),
                        ),
                        SizedBox(height: 7),
                        Text(
                          "Favourites",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: 65,
          top: 311,
          child: Container(
            width: 251,
            height: 369,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 251,
                  height: 251,
                  child: Stack(
                    children: [
                      Container(
                        width: 251,
                        height: 251,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xff12859e),
                              blurRadius: 24,
                              offset: Offset(6, 17),
                            ),
                          ],
                          color: Color(0xff4a4a4a),
                        ),
                      ),
                      Container(
                        width: 251,
                        height: 251,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0x77426368),
                              blurRadius: 24,
                              offset: Offset(-6, -27),
                            ),
                          ],
                          color: Color(0xff4a4a4a),
                        ),
                      ),
                      Container(
                        width: 251,
                        height: 251,
                        child: Stack(
                          children: [
                            Container(
                              width: 251,
                              height: 251,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0x87000000),
                                    blurRadius: 0,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                                color: Color(0xff4a4a4a),
                              ),
                            ),
                            Positioned(
                              left: 131,
                              top: 0,
                              child: Text(
                                "Show\nBalance",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontFamily: "Gilroy",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 61),
                Container(
                  width: 57,
                  height: 57,
                  child: Stack(
                    children: [
                      Container(
                        width: 57,
                        height: 57,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff1f1f1f),
                        ),
                      ),
                      Positioned(
                        left: -13,
                        top: 20,
                        child: Container(
                          width: 18,
                          height: 14.62,
                          padding: const EdgeInsets.only(
                            right: 2702,
                            top: 2601,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Transform.rotate(
                                angle: 1.57,
                                child: Container(
                                  width: double.infinity,
                                  height: 2,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(0xfff8f8f8),
                                      width: 1,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 0.97),
                              Transform.rotate(
                                angle: 1.57,
                                child: Container(
                                  width: double.infinity,
                                  height: 2,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(0xfff8f8f8),
                                      width: 1,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 0.97),
                              Transform.rotate(
                                angle: 1.57,
                                child: Container(
                                  width: double.infinity,
                                  height: 2,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(0xfff8f8f8),
                                      width: 1,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 0.97),
                              Transform.rotate(
                                angle: 1.57,
                                child: Container(
                                  width: double.infinity,
                                  height: 2,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(0xfff8f8f8),
                                      width: 1,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 0.97),
                              Transform.rotate(
                                angle: 1.57,
                                child: Container(
                                  width: double.infinity,
                                  height: 2,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(0xfff8f8f8),
                                      width: 1,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 21,
          top: 311,
          child: Container(
            width: 325,
            height: 417,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Opacity(
                  opacity: 0,
                  child: Container(
                    width: 251,
                    height: 251,
                    child: Stack(
                      children: [
                        Container(
                          width: 251,
                          height: 251,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xff12859e),
                                blurRadius: 61,
                                offset: Offset(-26, 35),
                              ),
                            ],
                            color: Color(0xff4a4a4a),
                          ),
                        ),
                        Container(
                          width: 251,
                          height: 251,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xff528b94),
                                blurRadius: 47,
                                offset: Offset(-6, -37),
                              ),
                            ],
                            color: Color(0xff4a4a4a),
                          ),
                        ),
                        Container(
                          width: 251,
                          height: 251,
                          child: Stack(
                            children: [
                              Container(
                                width: 251,
                                height: 251,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0x87000000),
                                      blurRadius: 0,
                                      offset: Offset(0, 2),
                                    ),
                                  ],
                                  color: Color(0xff4a4a4a),
                                ),
                              ),
                              Positioned(
                                left: 119,
                                top: -203,
                                child: Text(
                                  "Hide\nBalance",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontFamily: "Gilroy",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 8.67),
                Container(
                  width: 198,
                  height: 74,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        width: 141,
                        height: 58,
                        child: Stack(
                          children: [
                            Positioned(
                              left: 21,
                              top: 606,
                              child: Container(
                                width: 58,
                                height: 58,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(29),
                                  color: Color(0xff1f1f1f),
                                ),
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  width: 117,
                                  height: 35,
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Rs.",
                                        style: TextStyle(
                                          color: Color(0xffc1c1c1),
                                          fontSize: 19,
                                          fontFamily: "Gilroy",
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      SizedBox(width: 11),
                                      Text(
                                        "27,570",
                                        style: TextStyle(
                                          color: Color(0xfff8f8f8),
                                          fontSize: 29,
                                          fontFamily: "Gilroy",
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 57,
                        height: 57,
                        child: Stack(
                          children: [
                            Container(
                              width: 57,
                              height: 57,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff1f1f1f),
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Container(
                                  width: 18,
                                  height: 14.62,
                                  padding: const EdgeInsets.only(
                                    right: 2702,
                                    top: 2601,
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Transform.rotate(
                                        angle: 1.57,
                                        child: Container(
                                          width: double.infinity,
                                          height: 2,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Color(0xfff8f8f8),
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 0.97),
                                      Transform.rotate(
                                        angle: 1.57,
                                        child: Container(
                                          width: double.infinity,
                                          height: 2,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Color(0xfff8f8f8),
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 0.97),
                                      Transform.rotate(
                                        angle: 1.57,
                                        child: Container(
                                          width: double.infinity,
                                          height: 2,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Color(0xfff8f8f8),
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 0.97),
                                      Transform.rotate(
                                        angle: 1.57,
                                        child: Container(
                                          width: double.infinity,
                                          height: 2,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Color(0xfff8f8f8),
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 0.97),
                                      Transform.rotate(
                                        angle: 1.57,
                                        child: Container(
                                          width: double.infinity,
                                          height: 2,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Color(0xfff8f8f8),
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8.67),
                Container(
                  width: 115,
                  height: 33,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "LAST DEBIT",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xffc1c1c1),
                          fontSize: 9,
                        ),
                      ),
                      SizedBox(height: 849.67),
                      Container(
                        width: 31,
                        height: 31,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xfffd5548),
                        ),
                      ),
                      SizedBox(height: 849.67),
                      Text(
                        "Rs. 5,000",
                        style: TextStyle(
                          color: Color(0xfff8f8f8),
                          fontSize: 19,
                          fontFamily: "Gilroy",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: 849.67),
                      Transform.rotate(
                        angle: -1.57,
                        child: Container(
                          width: 10,
                          height: 8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xfff8f8f8),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8.67),
                Container(
                  width: 113,
                  height: 33,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "LAST CREDIT",
                        style: TextStyle(
                          color: Color(0xffc1c1c1),
                          fontSize: 9,
                        ),
                      ),
                      SizedBox(height: 849.67),
                      Container(
                        width: 31,
                        height: 31,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff017dc5),
                        ),
                      ),
                      SizedBox(height: 849.67),
                      Text(
                        "Rs. 1,500",
                        style: TextStyle(
                          color: Color(0xfff8f8f8),
                          fontSize: 19,
                          fontFamily: "Gilroy",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: 849.67),
                      Transform.rotate(
                        angle: 1.57,
                        child: Container(
                          width: 10,
                          height: 8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xfff8f8f8),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
