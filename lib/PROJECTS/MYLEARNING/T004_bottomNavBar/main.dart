// import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

import 'package:flutter/material.dart';
// import '../T002_drawer/drawer.dart';
import '../T005_ilabs/drawer_pink.dart';
import '../T005_ilabs/ilabs_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    return const MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 1;
  static const List _widgetOptions = [
    BusinessScreen("Title1", true),
    BusinessScreen("Title2", false),
    BusinessScreen("Title3", true),
    BusinessScreen("Title4", true),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('BottomNavigationBar Sample'),
      // ),
      drawer: makeDrawer(context),
      body: _widgetOptions[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          bottomNavigationItem('Invoices', Icons.home),
          bottomNavigationItem('Estimates', Icons.calculate),
          bottomNavigationItem('Clients', Icons.people),
          bottomNavigationItem('Items', Icons.countertops)
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[700],
        unselectedItemColor: Colors.black26,
        onTap: _onItemTapped,
      ),
    );
  }
}

BottomNavigationBarItem bottomNavigationItem(String lb, IconData icon) {
  return BottomNavigationBarItem(
    icon: Icon(icon),
    label: lb,
  );
}
