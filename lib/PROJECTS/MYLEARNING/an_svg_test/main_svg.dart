import 'package:flutter/material.dart';
import 'package:flutter_svg/avd.dart';
import 'package:flutter_svg/flutter_svg.dart';

const List<String> _assetNames = <String>[
  // 'assets/notfound.svg', // uncomment to test an asset that doesn't exist.
  'iLabsSreens/home/Invoice.svg',
  'iLabsSreens/home/Invoice active.svg',
  'iLabsSreens/home/Items.svg',
  'iLabsSreens/home/Items active.svg',
  'iLabsSreens/home/Estimates.svg',
  'iLabsSreens/home/Estimates active.svg',
  'iLabsSreens/home/Clients.svg',
  'iLabsSreens/home/Clients active.svg',

  'iLabsSreens/home/calender.svg',
  'iLabsSreens/home/edit_icon.svg',
  'iLabsSreens/home/side menu.svg',
  'iLabsSreens/home/add new icon.svg',
  'iLabsSreens/home/search.svg',
  'iLabsSreens/contact_us/get_in_touch_icon.svg',
  'iLabsSreens/side_menu/assets/info.svg', //
  'iLabsSreens/side_menu/assets/Settings.svg',
  'iLabsSreens/side_menu/assets/Business Info.svg',
  'iLabsSreens/side_menu/assets/Contact Us.svg',
  'iLabsSreens/side_menu/assets/template icon.svg',
  'iLabsSreens/side_menu/assets/Account.svg',
  'iLabsSreens/side_menu/assets/Region.svg',
  'iLabsSreens/side_menu/assets/Log Out.svg',
  'iLabsSreens/side_menu/assets/Privacy Policy.svg',
  'iLabsSreens/side_menu/assets/Upgrade Subscription.svg',
  'iLabsSreens/side_menu/assets/Terms of Use.svg',
  'iLabsSreens/splash/Invoice Labs Invoicing Made Easy.svg',
  'iLabsSreens/splash/signup illustration.svg',
  'iLabsSreens/splash/splash bg.svg',
  'iLabsSreens/splash/splash logo.svg',
  'iLabsSreens/splash/login illustration.svg',
  'iLabsSreens/settings/Payment Instructions.svg',
  'iLabsSreens/settings/Invoice Number.svg',
  'iLabsSreens/settings/add_business_logo.svg',
  'iLabsSreens/settings/Default Noties.svg',
  'iLabsSreens/settings/Tax.svg',
  'iLabsSreens/settings/Sync.svg',
  'iLabsSreens/settings/Default Email Message.svg',
  'iLabsSreens/settings/Customize.svg',
  'iLabsSreens/settings/Send me copy of emails.svg',
  'iLabsSreens/settings/Export Spreadsheet.svg',
  'iLabsSreens/login_signup/Invoice Labs Invoicing Made Easy.svg',
  'iLabsSreens/login_signup/signup illustration.svg',
  'iLabsSreens/login_signup/facebook icon.svg',
  'iLabsSreens/login_signup/view_icon.svg',
  'iLabsSreens/login_signup/google_icon.svg',
  'iLabsSreens/login_signup/login illustration.svg',
  'iLabsSreens/create_invoice/Mask Group 274.svg',
  'iLabsSreens/create_invoice/Mask Group 315.svg',
  'iLabsSreens/create_invoice/History_active.svg',
  'iLabsSreens/create_invoice/History.svg',
  'iLabsSreens/create_invoice/cross.svg',
  'iLabsSreens/create_invoice/edit_icon.svg',
  'iLabsSreens/create_invoice/Edit_active.svg',
  'iLabsSreens/create_invoice/Preview_active.svg',
  'iLabsSreens/create_invoice/clear.svg',
  'iLabsSreens/create_invoice/Edit.svg',
  'iLabsSreens/create_invoice/add_icon.svg',
  'iLabsSreens/create_invoice/save_icon.svg',
  'iLabsSreens/create_invoice/send_icon.svg',
  'iLabsSreens/create_invoice/radio_buttion_active.svg',
  'iLabsSreens/create_invoice/search_icon.svg',
  'iLabsSreens/create_invoice/radio_buttion unactive.svg',
  'iLabsSreens/create_invoice/calender_edit.svg',
  'iLabsSreens/create_invoice/Preview.svg',
];

/// Assets treated as "icons" - using a color filter to render differently.
const List<String> iconNames = <String>[
  'assets/svg_test/deborah_ufw/new-action-expander.svg',
  'assets/svg_test/deborah_ufw/new-camera.svg',
  'assets/svg_test/deborah_ufw/new-gif-button.svg',
  'assets/svg_test/deborah_ufw/new-gif.svg',
  'assets/svg_test/deborah_ufw/new-image.svg',
  'assets/svg_test/deborah_ufw/new-mention.svg',
  'assets/svg_test/deborah_ufw/new-pause-button.svg',
  'assets/svg_test/deborah_ufw/new-play-button.svg',
  'assets/svg_test/deborah_ufw/new-send-circle.svg',
  'assets/svg_test/deborah_ufw/numeric_25.svg',
];

/// Assets to test network access.
const List<String> uriNames = <String>[
  'http://upload.wikimedia.org/wikipedia/commons/0/02/SVG_logo.svg',
  'https://dev.w3.org/SVG/tools/svgweb/samples/svg-files/410.svg',
  'https://upload.wikimedia.org/wikipedia/commons/b/b4/Chess_ndd45.svg',
];

void main() {
  runApp(_MyApp());
}

class _MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const _MyHomePage(title: 'Flutter SVG Demo'),
    );
  }
}

class _MyHomePage extends StatefulWidget {
  const _MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<_MyHomePage> {
  final List<Widget> _painters = <Widget>[];
  late double _dimension;

  @override
  void initState() {
    super.initState();
    _dimension = 203.0;
    debugPrint("loading image");
    for (String assetName in _assetNames) {
      _painters.add(Column(
        children: [
          Container(
            width: 150,
            height: 150,
            color: Colors.yellow[100],
            child: SvgPicture.asset(assetName),
          ),
          Expanded(child: Text(assetName))
        ],
      ) //, color: Colors.red, width: 50, height: 50),
          );
    }

    _painters.add(const Text("Icon Images"));

    for (int i = 0; i < iconNames.length; i++) {
      _painters.add(
        Directionality(
          textDirection: TextDirection.rtl,
          child: SvgPicture.asset(
            iconNames[i],
            color: Colors.blueGrey[(i + 1) * 100],
            matchTextDirection: true,
          ),
        ),
      );
    }

    _painters.add(const Text("Internet Images"));

    for (String uriName in uriNames) {
      _painters.add(
        SvgPicture.network(
          uriName,
          placeholderBuilder: (BuildContext context) => Container(
              padding: const EdgeInsets.all(30.0),
              child: const CircularProgressIndicator()),
        ),
      );
    }
    // Shows an example of an SVG image that will fetch a raster image from a URL.
    _painters.add(SvgPicture.string('''<svg viewBox="0 0 200 200"
  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <image xlink:href="https://mdn.mozillademos.org/files/6457/mdn_logo_only_color.png" height="200" width="200"/>
</svg>'''));
    _painters.add(
        AvdPicture.asset('assets/svg_test/android_vd/battery_charging.xml'));
  }

  @override
  Widget build(BuildContext context) {
    if (_dimension > MediaQuery.of(context).size.width - 10.0) {
      _dimension = MediaQuery.of(context).size.width - 10.0;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.grey,
        child: Column(children: <Widget>[
          Slider(
              min: 5.0,
              max: MediaQuery.of(context).size.width - 10.0,
              value: _dimension,
              onChanged: (double val) {
                setState(() => _dimension = val);
              }),
          Expanded(
            child: GridView.extent(
              shrinkWrap: true,
              maxCrossAxisExtent: _dimension,
              padding: const EdgeInsets.all(4.0),
              mainAxisSpacing: 4.0,
              crossAxisSpacing: 4.0,
              children: _painters.toList(),
            ),
          ),
        ]),
      ),
    );
  }
}
