import 'package:flutter/material.dart';
import 'counter_vu.dart';

void main() {
  runApp(MaterialApp(
      title: 'Counter Demo',
      home: Scaffold(
        appBar: AppBar(title: const Text("title")),
        body: const CounterView(),
      )));
}
