import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class CounterViewModel extends BaseViewModel {
  int counter = 122;

  onPressed() {
    counter++;
    debugPrint("$counter");
    notifyListeners();
  }
}
