import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'counter_vm.dart';

// ignore_for_file: prefer_const_constructors

// ignore: must_be_immutable
class CounterView extends ViewModelBuilderWidget<CounterViewModel> {
  const CounterView({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, CounterViewModel viewModel, Widget? child) {
    return Column(children: [
      Text('${viewModel.counter}'),
      ElevatedButton(
        child: Text(
          'Add Counter',
          style: TextStyle(fontSize: 20.0),
        ),
        onPressed: viewModel.onPressed,
      )
    ]);
  }

  @override
  CounterViewModel viewModelBuilder(BuildContext context) {
    return CounterViewModel();
  }
}
