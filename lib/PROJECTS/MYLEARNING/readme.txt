
Drawer:
- Just add "Drawer()" in "drawer:" argument of Scaffold.
- It has just one argument "child:", we can add a list to it.

TabBar:
- Add DefaultTabController in say MaterialApp child:
- Define number of items in "length" = N
- The child is scaffold
- DefaultTabController-> Scaffold -> Appbar -> Bottom -> Tabbar(tabs:[Tab(),Tab(), ... N])
- In Scaffold->body: TabBarView(children:[...N...])

Theory of TabBar:
- The tabbar controller, harmonizes the TabBar and TabBarView
- TabBar is the bottom: of Appbar
- TabBar view will be in body: of Scaffold.

BottomNavigationBar:
- Declare a widget array which will be used in main body
    currentIndex = 0
    List widgets = [Widget1, Widget2, Widget3] 
- Scaffold
    appbar: 
    body: widget[index]
    bottomNavigationBar: [ BottomNavigationBarItem(), ....]
    currentIndex: currentIndex
    selectedItemColor: ...
    onTap: ()>>> index <<<<<<<<<<<<<<<<<<< will give index