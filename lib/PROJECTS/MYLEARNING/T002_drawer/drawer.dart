import 'package:flutter/material.dart';

void main() => runApp(const MyApp());
// ColorScheme cc = const ColorScheme(
//   primary: Color(0xff6200ee),
//   primaryVariant: Color(0xff3700b3),
//   secondary: Color(0xff03dac6),
//   secondaryVariant: Color(0xff018786),
//   surface: Colors.white,
//   background: Colors.white,
//   error: Color(0xffb00020),
//   onPrimary: Colors.white,
//   onSecondary: Colors.black,
//   onSurface: Colors.black,
//   onBackground: Colors.black,
//   onError: Colors.white,
//   brightness: Brightness.light,
// );

// }
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const appTitle = 'Drawer Demo';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: const MyHomePage(title: appTitle),
      theme: ThemeData(
        // Define the default brightness and colors.
        // brightness: Brightness.dark,
        primaryColor: Colors.pinkAccent,
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.orange),

        // Define the default font family.
        // fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        // textTheme: const TextTheme(
        //   headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
        //   headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
        //   bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        // ),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      drawer: const MyDrawer(),
      endDrawer: const MyDrawer(),
      body: const Center(
        child: Text('My Page2233!'),
      ),
    );
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("test")),
        drawer: const MyDrawer(),
        endDrawer: const MyDrawer(),
        body: const Center(
          child: Text('My Page2233!'),
        ));
  }
}
