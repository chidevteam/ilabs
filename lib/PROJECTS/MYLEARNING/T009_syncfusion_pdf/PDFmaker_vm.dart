import 'dart:typed_data';

import 'package:stacked/stacked.dart';

class PDFMakerViewModel extends BaseViewModel {
  List<int>? bytes;
  Uint8List? bytes8;
  dynamic directory;
}
