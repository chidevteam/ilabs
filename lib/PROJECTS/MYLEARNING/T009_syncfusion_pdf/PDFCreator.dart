import 'dart:typed_data';

import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'PDFmaker_vm.dart';

void main() {
  runApp(const PDFCreator());
}

class PDFCreator extends StatelessWidget {
  const PDFCreator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: Center(
            child:
                TextButton(onPressed: _createPDF, child: Text('Create PDF'))));
  }
}

Future<void> _createPDF() async {
  //Create a new PDF document
  PdfDocument document = PdfDocument();

  //Add a new page and draw text
  document.pages.add().graphics.drawString(
      'Hello World!', PdfStandardFont(PdfFontFamily.helvetica, 20),
      brush: PdfSolidBrush(PdfColor(0, 0, 0)),
      bounds: Rect.fromLTWH(0, 0, 500, 50));

  //Save the document
  List<int> bytes = document.save();
//Get external storage directory
  final directory = await getApplicationDocumentsDirectory();

//Get directory path
  final path = directory.path;

//Create an empty file to write PDF data
  File file = File('$path/Output.pdf');

//Write PDF data
  await file.writeAsBytes(bytes, flush: true);

//Open the PDF document in mobile
  OpenFile.open('$path/Output.pdf');
  //Dispose the document
  document.dispose();
}
