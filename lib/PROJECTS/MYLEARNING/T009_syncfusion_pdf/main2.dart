import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'dart:io';
// import 'graphics.dart';

main() {
// Create a new PDF document.
  final PdfDocument document = PdfDocument();
// Add a PDF page and draw text.
  document.pages.add().graphics.drawString(
        'Hello World!', PdfStandardFont(PdfFontFamily.helvetica, 12),
        brush: PdfSolidBrush(PdfColor(0, 0, 0)),
        // bounds: const Rect.fromLTWH(0, 0, 150, 20)
      );
// Save the document.
  File('HelloWorld.pdf').writeAsBytes(document.save());
// Dispose the document.
  document.dispose();
}
