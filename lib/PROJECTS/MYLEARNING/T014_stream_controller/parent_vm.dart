import 'dart:async';

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ParentVM extends BaseViewModel {
  int year = 2022;
  StreamController? streamController;

  ParentVM() {
    debugPrint("hre");
    makeStream();
  }
  onYearBack() {
    year--;
    streamController!.add(9989);
    notifyListeners();
  }

  makeStream() {
    streamController = StreamController(
      onPause: () => print('Paused'),
      onResume: () => print('Resumed'),
      onCancel: () => print('Cancelled'),
      onListen: () => print('Listens'),
    );
  }
}
