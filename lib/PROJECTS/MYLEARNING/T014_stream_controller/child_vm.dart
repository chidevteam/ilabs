import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'dart:async';

class ChildVM extends BaseViewModel {
  int year2 = 0;
  StreamController streamController;

  onModelReady() {
    print("Model is ready");
  }

  ChildVM(this.year2, this.streamController) {
    debugPrint("hre");

    streamController.stream.listen(
      (event) => print('Event: $event'),
      onDone: () => print('Done'),
      onError: (error) => print(error),
    );
  }
}
