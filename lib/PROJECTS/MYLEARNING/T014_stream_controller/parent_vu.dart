import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'parent_vm.dart';
import 'child_vu.dart';

class ParentVU extends ViewModelBuilderWidget<ParentVM> {
  const ParentVU({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, ParentVM viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(title: const Text('Welcome to Parent'), actions: [
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            print("yeart back pressed ${viewModel.year}");
            viewModel.onYearBack();
            // filterBottomSheet(context, viewModel);
          },
        )
      ]),
      body: Center(
        child: ChildVU(viewModel.year, viewModel.streamController),
      ),
    );
  }

  @override
  ParentVM viewModelBuilder(BuildContext context) {
    ParentVM vm = ParentVM();
    return vm;
  }
}
