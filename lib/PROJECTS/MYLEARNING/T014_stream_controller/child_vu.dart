import 'dart:async';

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'child_vm.dart';

class ChildVU extends ViewModelBuilderWidget<ChildVM> {
  ChildVU(this.year, this.streamCont, {Key? key}) : super(key: key);
  int year;
  StreamController? streamCont;

  // @override
  // bool get reactive => false;

  // @override
  // bool get createNewModelOnInsert => true;

  @override
  bool get disposeViewModel => true;

  @override
  Widget builder(BuildContext context, ChildVM viewModel, Widget? child) {
    // viewModel.year2 += 10;
    return Column(
      children: [
        ElevatedButton(
            onPressed: () {
              viewModel.year2++;
              viewModel.notifyListeners();
            },
            child: const Text("Press me")),
        Center(
          child: Text('Hello World Child $year   ${viewModel.year2}'),
        ),
      ],
    );
  }

  @override
  ChildVM viewModelBuilder(BuildContext context) {
    ChildVM vm = ChildVM(year, streamCont!);
    return vm;
  }
}
