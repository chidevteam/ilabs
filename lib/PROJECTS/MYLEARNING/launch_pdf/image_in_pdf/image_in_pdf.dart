import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../pdf_utils/pdf_image.dart';
//Local imports
import '../file_write/save_file_mobile.dart'
    if (dart.library.html) 'save_file_web.dart';

class ImageInPDF {
  Future<void> createPDF() async {
    //Create a new PDF document.
    PdfDocument document = PdfDocument();
    document.pageSettings.size = PdfPageSize.a4;
    PdfPage page1 = document.pages.add();
    // PdfPage page2 = document.pages.add();
    final Size pageSize = page1.getClientSize();

    PdfBitmap image = await readImageData('assets/photo.jpg');
    // PdfBitmap image = await readImageData('assets/invoice_labs.png');
    // PdfBitmap image = await readImageData('assets/images/choose_template/invoice.png');
    Rect imageRect;

    debugPrint("${image.width} ${image.height}");

    imageRect = const Rect.fromLTWH(200, 0, 100, 150);
    drawImageRounded(page1, image, imageRect, 0, fillType: "fit", debug: true);

    imageRect = const Rect.fromLTWH(200, 200, 150, 100);
    drawImageRounded(page1, image, imageRect, 5, fillType: "fit", debug: true);

    page1.graphics.drawString(
        'INVOICE3', PdfStandardFont(PdfFontFamily.helvetica, 30),
        brush: PdfBrushes.red,
        bounds: Rect.fromLTWH(0, 0, pageSize.width - 115, 90),
        format: PdfStringFormat(lineAlignment: PdfVerticalAlignment.top));

    // imageRect = const Rect.fromLTWH(200, 0, 100, 100);
    // drawImageRounded(page1, image, imageRect, 0, fillType: "fit1", debug: true);

    // imageRect = const Rect.fromLTWH(200, 200, 100, 100);
    // drawImageRounded(page1, image, imageRect, 5, fillType: "fit2", debug: true);

    // 595, 842
    // final Size pageSize = page1.getClientSize();
    // page1.graphics.drawRectangle(
    //     brush: PdfSolidBrush(PdfColor(91, 126, 215, 255)),
    //     bounds: Rect.fromLTWH(0, 0, pageSize.width - 115, 90));

    //Save and dispose the document.
    final List<int> bytes = document.save();
    document.dispose();

    return await showPDF(bytes, 'Invoice.pdf');

    //await saveOnly(bytes, 'Invoice.pdf');
    // await saveAndLaunchFile(bytes, 'Invoice.pdf');
    // await saveOnly(bytes, 'Invoice.pdf');
  }
}

// String currentDate = 'DATE ' + DateFormat.yMMMd().format(DateTime.now());
// 