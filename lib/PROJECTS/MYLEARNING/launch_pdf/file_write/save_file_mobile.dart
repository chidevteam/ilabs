import 'dart:io';

import 'package:open_file/open_file.dart' as open_file;
import 'package:path_provider/path_provider.dart' as path_provider;
// import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

///To save the pdf file in the device
Future launchFile(String fullFilePath) async {
  if (Platform.isAndroid || Platform.isIOS) {
    //Launch the file (used open_file package)
    await open_file.OpenFile.open(fullFilePath);
  } else if (Platform.isWindows) {
    await Process.run('start', <String>[fullFilePath], runInShell: true);
  } else if (Platform.isMacOS) {
    await Process.run('open', <String>[fullFilePath], runInShell: true);
  } else if (Platform.isLinux) {
    await Process.run('xdg-open', <String>[fullFilePath], runInShell: true);
  }
}

///To save the pdf file in the device
Future<String> saveOnly(List<int> bytes, String fileName) async {
  final Directory directory =
      await path_provider.getApplicationSupportDirectory();
  String path = directory.path;

  String fullPath = Platform.isWindows ? '$path\\$fileName' : '$path/$fileName';
  final File file = File(fullPath);
  await file.writeAsBytes(bytes, flush: true);
  return fullPath;
}

showPDF(List<int> bytes, String fileName) async {
  String fullPath = await saveOnly(bytes, fileName);
  if (Platform.isLinux) {
    launchFile(fullPath);
  }
  return fullPath;
}
