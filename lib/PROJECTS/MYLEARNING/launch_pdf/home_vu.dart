import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'real_multi_page/real_multi_page_doc.dart';
import 'multi_page/multi_page.dart';
import 'simple_invoice/simple_invoice.dart';
import 'image_in_pdf/image_in_pdf.dart';
import 'ilabs_invoice/ilabs_design_1/invoice_design_1.dart';
import 'package:pdf_viewer_plugin/pdf_viewer_plugin.dart';
import 'dart:io';
import 'home_vm.dart';

// ignore: must_be_immutable
class HomeVU extends ViewModelBuilderWidget<HomeVM> {
  RealMultiPageDoc realMultiPage = RealMultiPageDoc();
  MultiPageInvoice multipageInvoice = MultiPageInvoice();

  // SimpleInvoice simpleInvoice = SimpleInvoice();
  ImageInPDF imageInPDF = ImageInPDF();

  HomeVU({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, HomeVM viewModel, Widget? child) {
    // iLabs1.createPDF();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create PDF document'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: invoicePDFview(viewModel.fileName)),
            Row(children: [
              pdfButton1('IlabsInvoice', viewModel, () async {
                InvoiceDesign1 design1 = InvoiceDesign1();
                viewModel.fileName = null;
                viewModel.notifyListeners();
                viewModel.fileName = await design1.createPDF();
                viewModel.notifyListeners();
              }),
              pdfButton1('SimpleInvoice', viewModel, () async {
                SimpleInvoice simpleInvoice = SimpleInvoice();
                viewModel.fileName = null;
                viewModel.notifyListeners();
                viewModel.fileName = await simpleInvoice.createPDF();
                viewModel.notifyListeners();
              }),
              pdfButton('Multi Page', viewModel, multipageInvoice.createPDF)
            ]),
            Row(children: [
              pdfButton('RealMultiPage', viewModel, realMultiPage.createPDF),
              pdfButton('Image in PDF', viewModel, imageInPDF.createPDF)
            ])
          ],
        ),
      ),
    );
  }

  Padding pdfButton1(String title, HomeVM vm, callback) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
          onPressed: callback,
          child: Text(title),
          style: TextButton.styleFrom(
            primary: Colors.white,
            backgroundColor: Colors.lightBlue,
            onSurface: Colors.grey,
          )),
    );
  }

  Padding pdfButton(String title, HomeVM vm, callback) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
          onPressed: () async {
            vm.fileName = null;
            vm.notifyListeners();
            vm.fileName = await callback();
            vm.notifyListeners();
          },
          child: Text(title),
          style: TextButton.styleFrom(
            primary: Colors.white,
            backgroundColor: Colors.lightBlue,
            onSurface: Colors.grey,
          )),
    );
  }

  Widget invoicePDFview(String? fileName) {
    Widget pdfWidget = const Text("Empty PDF");
    if (fileName != null) {
      if (Platform.isLinux) {
        pdfWidget = Text("$fileName: PDFview on LInux not supported");
      } else {
        pdfWidget = PdfView(path: fileName);
      }
    }
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
        child: SizedBox(
            width: double.infinity,
            child: Container(
                color: Colors.green,
                child: Container(
                  color: Colors.blue,
                  child: pdfWidget,
                ))));
  }
  // ? PdfView(path: file.path)

  @override
  HomeVM viewModelBuilder(BuildContext context) {
    return HomeVM();
  }
}
