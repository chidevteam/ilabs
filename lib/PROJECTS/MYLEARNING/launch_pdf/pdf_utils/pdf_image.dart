import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

Future<PdfBitmap> readImageData(String name) async {
  final ByteData data = await rootBundle.load(name);
  List<int> datax =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  PdfBitmap bitmap = PdfBitmap(datax);
  return bitmap;
}

void drawImageRounded(
    dynamic page, PdfBitmap image, Rect imageRectx, double radius,
    {String fillType = "fit", bool debug = true}) {
  // bool debug = false;
  PdfGraphics graphics = page.graphics;
  Rect imageRect = imageRectx;
  double x0, y0, x1, y1;
  x0 = imageRectx.left;
  y0 = imageRectx.top;
  x1 = imageRectx.right;
  y1 = imageRectx.bottom;
  double iW = image.width.toDouble();
  double iH = image.height.toDouble();
  double pW = (x1 - x0).round().toDouble();
  double pH = (y1 - y0).round().toDouble();

  double sx = pW / iW;
  double sy = pH / iH;
  // double s = sx;
  // if (sy > s) s = sy;

  if (fillType == 'fit' && sx < sy) {
    // debugPrint("iX  $iX  iY $iY  bx $bX  by $bY");
    // double pWnew = iW * pW / iW;
    double pHnew = iH * pW / iW; //
    double dh = (pH - pHnew) / 2;
    imageRect = Rect.fromLTRB(x0, y0 + dh, x1, y1 - dh);
  }

  if (fillType == 'fit' && sx > sy) {
    double pWnew = iW * pH / iH; //
    double dx = (pW - pWnew) / 2;
    imageRect = Rect.fromLTRB(x0 + dx, y0, x1 - dx, y1);
  }

  // if (fillType == 'fill' && sx <= sy) {
  //   // debugPrint("iX  $iX  iY $iY  bx $bX  by $bY");
  //   double bXp = pW / iH * iW;
  //   double dX = (pW - bXp) / 2;
  //   imageRect = Rect.fromLTRB(x0 + dX, y0, x1 - dX, y1);
  // }

  // if (fillType == 'fill' && sx > sy) {
  //   // debugPrint("iX  $iX  iY $iY  bx $bX  by $bY");
  //   double bYp = pH / iW * iH;
  //   double dY = (pH - bYp) / 2;
  //   imageRect = Rect.fromLTRB(x0, y0 + dY, x1, y1 - dY);
  // }

  // if (fillType == 'none') {
  //   imageRect = Rect.fromLTRB(x0 + 2, y0 + 2, x1 - 4, y1 - 4);
  // }

  if (debug) {
    // graphics.drawRectangle(brush: PdfBrushes.greenYellow, bounds: imageRectx);
  }
//Close the path
  graphics.save(); // Save the graphics before adding the clip.
  graphics.setClip(path: geRoundedRect(imageRectx, radius));
  graphics.drawImage(image, imageRect);
  graphics.restore();

  // PdfPen pen = PdfPen(PdfColor(0, 0, 0), width: 2);
  if (debug) {
    PdfPen pen = PdfPen(PdfColor(0, 0, 0), width: 2);
    graphics.drawRectangle(pen: pen, bounds: imageRectx);
    PdfPen pen1 = PdfPen(PdfColor(255, 0, 0), width: 2);
    graphics.drawRectangle(pen: pen1, bounds: imageRect);
  }
}

PdfPath geRoundedRect(Rect imageRect, double radius) {
  //Draw the rounded rectangle as separate 4 arcs with same dimensions of width and height
  double x0 = imageRect.left;
  double y0 = imageRect.top;
  double x1 = imageRect.right;
  double y1 = imageRect.bottom;
  Rect topLeft = Rect.fromLTWH(x0, y0, 2 * radius, 2 * radius);
  Rect topRight = Rect.fromLTWH(x1 - 2 * radius, y0, 2 * radius, 2 * radius);
  Rect bottomRight =
      Rect.fromLTWH(x1 - 2 * radius, y1 - 2 * radius, 2 * radius, 2 * radius);
  Rect bottomLeft = Rect.fromLTWH(x0, y1 - 2 * radius, 2 * radius, 2 * radius);

  PdfPath graphicsPath = PdfPath();
  graphicsPath.addArc(topLeft, 180, 90);
  graphicsPath.addArc(topRight, 270, 90);
  graphicsPath.addArc(bottomRight, 0, 90);
  graphicsPath.addArc(bottomLeft, 90, 90);
  graphicsPath.closeFigure();

  return graphicsPath;
}

/*


 */
