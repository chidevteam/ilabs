import 'package:flutter/material.dart';
import "home_vu.dart";

void main() {
  runApp(const CreatePdfWidget());
}

/// Represents the PDF widget class.
class CreatePdfWidget extends StatelessWidget {
  const CreatePdfWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeVU(),
    );
  }
}

/*
- unclear planning.

- Draw the address space and then the table below that
- Keep testing for multipage.

*/ 