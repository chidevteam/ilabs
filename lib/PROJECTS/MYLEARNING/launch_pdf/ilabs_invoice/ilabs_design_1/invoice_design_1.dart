import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../../file_write/save_file_mobile.dart';
// import '../../pdf_utils/pdf_image.dart';
import 'letterhead1.dart';
import '../../pdf_utils/pdf_image.dart';
import '../invoice_base/invoice_base.dart';

// import 'package:syncfusion_flutter_pdf/';
// import '../../drawing/drawing.dart';

// class PdfLayoutResultHelper {
//   /// internal method
//   static PdfLayoutResult2 load(PdfPage page, Rect bounds) {
//     return PdfLayoutResult2._(page, bounds);
//   }
// }
class InvoiceDesign1 extends Letterhead1 {
  InvoiceDesign1(
      {Size pageSize = PdfPageSize.a4,
      Rect pMargins = Rect.zero,
      Rect letterheadMargins = Rect.zero})
      : super(
            pageSize: PdfPageSize.a4,
            pMargins: const Rect.fromLTRB(40, 40, 40, 10),
            letterheadMargins: const Rect.fromLTRB(0, 80, 0, 20),
            indentSize: const Size(50, 50)) {
    debug = false;
  }

  Future<String> createPDF() async {
    await addHeader(document.template.top!);
    addFooter(document.template.bottom!);

    PdfPage page = document.pages.add();

    String primaryCode = "#E54B4B";
    String secondaryCode = "#FDF1F1";

    // addColors(page, primaryCode, secondaryCode);

    Size size = page.getClientSize();

    PdfLayoutResult result = newParagraph("Invoice to:", page,
        rect: Rect.fromLTWH(0, 50, size.width * .4, 30), font: contentFont);

    addDateSection(page: result.page);
    for (int i = 0; i < 1; i++) {
      result = addBusinessInfo(result, size.width * .4);
    }

    PdfColor bgColor = PdfColor(0, 0, 128);

    // Rect rect = Rect.fromLTWH(0, result.bounds.bottom, size.width, 20);

    // page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);
    // Rect rb = result.bounds;
    // Rect bounds = Rect.fromLTRB(rb.left, rb.top, rb.right, rb.bottom);

    // PdfLayoutResult2 result2 = PdfLayoutResult2(result.page, bounds);

    final PdfGrid grid = getGrid();

    result = drawGrid(result.page, grid, result);
    await drawSignature(result.page, result);

    var bytes = document.save();
    document.dispose();
    return await showPDF(bytes, 'Invoice.pdf');
  }

  addDateSection({required PdfPage page}) {
    int r = 163;
    int g = 73;
    int b = 141;
    int dr = ((255 - 163) ~/ 3);
    int dg = ((255 - 73) ~/ 3);
    int db = ((255 - 141) ~/ 3);
    Size size = page.getClientSize();

    Rect rect = Rect.fromLTWH(size.width * .4, 0, size.width * .2, 60);
    drawPairRect(
        page: page,
        rect: rect,
        title: "Due Date",
        detail: "22-03-2022",
        bgColor: PdfColor(r + 0 * dr, g + 0 * dg, b + 0 * db),
        align: PdfTextAlignment.left);

    rect = Rect.fromLTWH(size.width * .6, 0, size.width * .2, 60);

    drawPairRect(
        page: page,
        rect: rect,
        title: "Due Date",
        detail: "22-03-2022",
        bgColor: PdfColor(r + 1 * dr, g + 1 * dg, b + 1 * db),
        align: PdfTextAlignment.center);

    rect = Rect.fromLTWH(size.width * .8, 0, size.width * .2, 60);

    drawPairRect(
        page: page,
        rect: rect,
        title: "Due Date",
        detail: "22-03-2022",
        bgColor: PdfColor(r + 2 * dr, g + 2 * dg, b + 2 * db),
        align: PdfTextAlignment.right);
  }

  drawPairRect(
      {required PdfPage page,
      required Rect rect,
      required String title,
      required String detail,
      required PdfColor bgColor,
      PdfTextAlignment align = PdfTextAlignment.center}) {
    page.graphics.drawRectangle(brush: PdfSolidBrush(bgColor), bounds: rect);

    page.graphics.drawString(title, contentFont,
        brush: PdfSolidBrush(MyPDFColor.white),
        bounds: Rect.fromLTWH(
            rect.left + 10, rect.top + 10, rect.width - 20, rect.height),
        format: PdfStringFormat(
            // lineAlignment: PdfVerticalAlignment.middle,
            alignment: align));

    final PdfStandardFont myFont =
        PdfStandardFont(PdfFontFamily.helvetica, 13, style: PdfFontStyle.bold);

    page.graphics.drawString(detail, myFont,
        brush: PdfSolidBrush(MyPDFColor.black1),
        bounds: Rect.fromLTWH(
            rect.left + 10, rect.top + 23, rect.width - 20, rect.height),
        format: PdfStringFormat(
            // lineAlignment: PdfVerticalAlignment.middle,
            alignment: align));
  }

  PdfLayoutResult addBusinessInfo(PdfLayoutResult result, double width) {
    result = addParagraph("Mr. John Doyle" + ",", result.page, result,
        font: h1bFont);

    String text = "";

    text =
        'United States, California, San Mateo, 9920 BridgePointe Parkway, 9365550136';
    result = addParagraph(text, result.page, result, width: width);

    text = 'Date: Monday 04, January 2021';
    result = addParagraph(text, result.page, result, width: width);
    return result;
  }

  Future drawSignature(PdfPage page, PdfLayoutResult result) async {
    PdfBitmap image = await readImageData('assets/signature.png');

    double pageWidth = contentRect.width;
    // double heightRatio = 1.0;
    double imHeight = 70;
    double topOfImage = result.bounds.bottom;
    double imWidth = image.width * imHeight / image.height;

    debugPrint("${image.width} ${image.height}");
    Rect imRect =
        Rect.fromLTWH(pageWidth - imWidth, topOfImage, imWidth, imHeight);
    print("BRECT:  $imRect");

    drawImageRounded(page, image, imRect, 0, fillType: 'fit', debug: debug);
  }
  // PdfLayoutResult addBusinessInfo(PdfPage page) {}

// rgb(161,75,140)  #a14b8c
// rgb(180,155,177)  #b49bb1
// rgb(207,187,203)  #cfbbcb

  PdfLayoutResult drawGrid(PdfPage page, PdfGrid grid, PdfLayoutResult result) {
    PdfLayoutFormat format = PdfLayoutFormat();
    final Size size = page.getClientSize();
    format.paginateBounds = Rect.fromLTWH(0, 0, size.width, size.height - 15);
    //Draw grid into PDF page
    PdfLayoutResult? gridResult = grid.draw(
        page: page,
        bounds: Rect.fromLTWH(
            0, result.bounds.bottom + 15, size.width, size.height - 15));

    final PdfStandardFont h0Font =
        PdfStandardFont(PdfFontFamily.helvetica, 15, style: PdfFontStyle.bold);

    final PdfTextElement element =
        PdfTextElement(text: 'Grand Total: 14740.84', font: h0Font);
    result = element.draw(
        page: gridResult!.page,
        bounds: Rect.fromLTWH(
            0, gridResult.bounds.bottom + 15, size.width, size.height - 15),
        format: PdfLayoutFormat(
            paginateBounds:
                Rect.fromLTWH(0, 15, size.width, size.height - 15)))!;
    return result;
  }

  //Create PDF grid and return
  PdfGrid getGrid() {
    //Create a PDF grid
    final PdfGrid grid = PdfGrid();
    //Secify the columns count to the grid.
    grid.columns.add(count: 5);
    //Create the header row of the grid.
    final PdfGridRow headerRow = grid.headers.add(1)[0];
    //Set style
    headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(68, 114, 196));
    headerRow.style.textBrush = PdfBrushes.white;
    headerRow.cells[0].value = 'S.No.';
    headerRow.cells[0].stringFormat.alignment = PdfTextAlignment.center;
    headerRow.cells[1].value = 'Product Name';
    headerRow.cells[2].value = 'Price';
    headerRow.cells[3].value = 'Quantity';
    headerRow.cells[4].value = 'Total';
    // headerRow.cells[4].stringFormat.alignment = PdfTextAlignment.center;
    //Add rows
    for (int i = 1; i < 100; i++) {
      addProducts('$i', 'AWC Logo Cap',
          'This cap is really good for most things ', 8.99, 2, 17.98, grid);
    }
    //Apply the table built-in style
    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    //Set gird columns width
    grid.columns[0].width = 50;
    grid.columns[1].width = 250;
    for (int i = 0; i < headerRow.cells.count; i++) {
      headerRow.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }
    for (int i = 0; i < grid.rows.count; i++) {
      final PdfGridRow row = grid.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final PdfGridCell cell = row.cells[j];
        if (j == 0 || j == 3) {
          cell.stringFormat.alignment = PdfTextAlignment.center;
        }
        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }
    return grid;
  }

  //Create and row for the grid.
  void addProducts(String productId, String productName, String productDetail,
      double price, int quantity, double total, PdfGrid grid) {
    final PdfGridRow row = grid.rows.add();
    row.cells[0].value = productId;
    row.cells[1].value = "" + productName + "" + "\n" + productDetail;
    row.cells[2].value = price.toString();
    row.cells[3].value = quantity.toString();
    row.cells[4].value = total.toString();
  }
}
