import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title!),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextButton(
                child: const Text(
                  'Generate PDF',
                  style: TextStyle(color: Colors.white),
                ),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.resolveWith(
                        (states) => Colors.blue)),
                onPressed: drawImage,
              )
            ],
          ),
        ));
  }

  Future<void> drawImage() async {
    //Create a new PDF document.
    PdfDocument document = PdfDocument();
    //Add the page and get the graphics.
    PdfGraphics graphics = document.pages.add().graphics;
    //Set clip to the graphics to make a circular shape.
    graphics.save(); // Save the graphics before adding the clip.
    //Create path and set clip.
    graphics.setClip(path: addPath());
    // PdfPath()..addEllipse(const Rect.fromLTWH(107, 50, 300, 300)));
    //Draw an image.
    graphics.drawImage(PdfBitmap(await _readImageData('photo.jpg')),
        const Rect.fromLTWH(0, 0, 300, 300));
    //Restore the graphics.
    graphics.restore();
    //Save and dispose the document.
    final List<int> bytes = document.save();
    document.dispose();

    //Get external storage directory
    Directory directory = (await getApplicationDocumentsDirectory());
    //Get directory path
    String path = directory.path;
    //Create an empty file to write PDF data
    File file = File('$path/Output.pdf');
    //Write PDF data
    await file.writeAsBytes(bytes, flush: true);
    //Open the PDF document in mobile
    OpenFile.open('$path/Output.pdf');
  }

  Future<List<int>> _readImageData(String name) async {
    final ByteData data = await rootBundle.load('assets/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  PdfPath addPath() {
    //Create the Rectangle
    Rect rect = const Rect.fromLTWH(0, 0, 300, 300);
// int dimension = 30;

//Create the GraphicsPath
    PdfPath graphicsPath = PdfPath();

//Draw the rounded rectangle as separate 4 arcs with same dimensions of width and height
    graphicsPath.addArc(rect, 180, 90);
    // graphicsPath.addArc(rect, 270, 90);
    // graphicsPath.addArc(rect, 0, 90);
    // graphicsPath.addArc(rect, 90, 90);

//Close the path
    graphicsPath.closeFigure();

//Get the path points
// PointF[] points = graphicsPath.PathPoints;

// //Get the path types
// byte[] pathTypes = graphicsPath.PathTypes;

// //Create the PdfPath
// PdfPath pdfPath = new PdfPath(PdfPens.Black, points, pathTypes);

// //Draw the path on PDF page
// graphics.DrawPath(PdfPens.Black, pdfPath);
    return graphicsPath;
  }
}
