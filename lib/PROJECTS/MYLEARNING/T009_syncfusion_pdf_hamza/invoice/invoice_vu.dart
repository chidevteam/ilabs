import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'invoice_vm.dart';

class InvoicePDFScreen extends ViewModelBuilderWidget<InvoicePDFViewModel>{
  @override
  Widget builder(BuildContext context, InvoicePDFViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(title: const Text('Invoice PDF'),),
      body: viewModel.file!= null?
          SfPdfViewer.file(viewModel.file!)
          : Text(''),
    );
  }

  @override
  InvoicePDFViewModel viewModelBuilder(BuildContext context) {
    final vm = InvoicePDFViewModel();
    vm.createInvoicePDF();
    return vm;
  }

}