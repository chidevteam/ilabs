import 'dart:typed_data';

import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'dart:io';
import 'package:http/http.dart' as http;



class OurInvoiceViewModel extends BaseViewModel{


  File? file;
  Future<void> createPDF() async{
    PdfDocument document = PdfDocument();
    PdfPage page = document.pages.add();
    PdfGraphics graphics = page.graphics;

    // image
    graphics.drawRectangle(brush: PdfBrushes.blue, bounds: Rect.fromLTWH(10, 10, 100, 100));
    // document.pages.add().graphics.drawRectangle(
    //     brush: PdfBrushes.chocolate, bounds: Rect.fromLTWH(10, 10, 100, 100));


    // bussiness info
    graphics.drawRectangle(brush: PdfBrushes.black, bounds: Rect.fromLTWH(400, 10, 100, 200));

    //invoice info
    graphics.drawRectangle(brush: PdfBrushes.chocolate, bounds: Rect.fromLTWH(5, 220, 800, 90));

    //table
    graphics.drawRectangle(brush: PdfBrushes.aquamarine, bounds: Rect.fromLTWH(10, 320, 780, 100));

    //payment info
    graphics.drawRectangle(brush: PdfBrushes.orange, bounds: Rect.fromLTWH(10, 430, 180, 100));

    //balance and total
    graphics.drawRectangle(brush: PdfBrushes.green, bounds: Rect.fromLTWH(370, 430, 180, 150));

    //signature
    graphics.drawRectangle(brush: PdfBrushes.red, bounds: Rect.fromLTWH(370, 590, 80, 50));


    List<int> bytes = document.save();
    //File('HelloWorld.pdf').writeAsBytes(bytes);
    //Get external storage directory
    final directory = await getApplicationDocumentsDirectory();

    final path = directory.path;
    //mPath = path;

    //print(mPath);

//Create an empty file to write PDF data
    file = File('$path/Output.pdf');

//Write PDF data
    await file!.writeAsBytes(bytes, flush: true);
    notifyListeners();

    document.dispose();
  }

  // onPDFShare(BuildContext context, String url) async {
  //   //showProgress(context);
  //   final uri = Uri.parse(url);
  //   final response = await http.get(uri);
  //   final bytes = response.bodyBytes;
  //   final temp = await getTemporaryDirectory();
  //   final path = '${temp.path}/file.pdf';
  //   File(path).writeAsBytesSync(bytes);
  //   //hideProgress(context);
  //   await Share.shareFiles([path], text: 'PDF Share');
  // }

  onPDFFileShare(File mfile) async{
    Uint8List? bytes;

    await file!.readAsBytes().then((value) {
      bytes = Uint8List.fromList(value);
    });
    final temp = await getTemporaryDirectory();
    final path = '${temp.path}/file.pdf';
    File(path).writeAsBytesSync(bytes!);
    await Share.shareFiles([path], text: 'PDF Share');

  }
}