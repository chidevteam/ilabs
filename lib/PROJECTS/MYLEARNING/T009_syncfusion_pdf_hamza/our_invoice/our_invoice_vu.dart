import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:ilabs/PROJECTS/MYLEARNING/T009_syncfusion_pdf_hamza/forms/email_form.dart';
// import 'package:ilabs/iLabs/pdf_test/forms/email_form.dart';
import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'our_invoice_vm.dart';

class OurInvoiceScreen extends ViewModelBuilderWidget<OurInvoiceViewModel> {
  @override
  Widget builder(
      BuildContext context, OurInvoiceViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Our Custom pdf'),
      ),
      body:
          viewModel.file != null ? SfPdfViewer.file(viewModel.file!) : Text(''),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        children: [
          SpeedDialChild(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return EmailFormView();
                }));
              },
              child: const Icon(Icons.mail),
              label: 'email'),
          SpeedDialChild(
              onTap: () {
                //viewModel.onPDFShare(context, url)
                viewModel.onPDFFileShare(viewModel.file!);
              },
              child: const Icon(Icons.share),
              label: 'Share')
        ],
      ),
    );
  }

  @override
  OurInvoiceViewModel viewModelBuilder(BuildContext context) {
    final vm = OurInvoiceViewModel();
    vm.createPDF();
    return vm;
  }
}
