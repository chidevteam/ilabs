// import 'dart:io';

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'PdfFromFileViewModel.dart';

class PdfFromFileScreen extends ViewModelBuilderWidget<PdfFromFileViewModel> {
  const PdfFromFileScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, PdfFromFileViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('File PDFViewer'),
      ),
      body: Container(
        child: viewModel.mPath != ''
            ? SfPdfViewer.file(viewModel.file!)
            : const Text('File not loaded'),
      ),
    );
  }

  @override
  PdfFromFileViewModel viewModelBuilder(BuildContext context) {
    final vm = PdfFromFileViewModel();
    vm.createPDF();
    return vm;
  }
}
