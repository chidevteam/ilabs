import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'dart:ui';
import 'dart:io';
// import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class PdfFromFileViewModel extends BaseViewModel {
  File? file;
  String mPath = "";
  Future<void> createPDF() async {
    //Create a new PDF document
    PdfDocument document = PdfDocument();

    //Add a new page and draw text
    document.pages.add().graphics.drawString(
        'Hello World!', PdfStandardFont(PdfFontFamily.helvetica, 20),
        brush: PdfSolidBrush(PdfColor(0, 0, 0)),
        bounds: const Rect.fromLTWH(0, 0, 500, 50));

    //Save the document
    List<int> bytes = document.save();
    //File('HelloWorld.pdf').writeAsBytes(bytes);
    //Get external storage directory
    final directory = await getApplicationDocumentsDirectory();

//Get directory path
    final path = directory.path;
    mPath = path;
    notifyListeners();
    print(mPath);

//Create an empty file to write PDF data
    file = File('$path/Output.pdf');

//Write PDF data
    await file!.writeAsBytes(bytes, flush: true);

//Open the PDF document in mobile
    //OpenFile.open('$path/Output.pdf');

    //Dispose the document
    //document.dispose();
  }
}
