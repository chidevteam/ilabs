import 'package:flutter/cupertino.dart';
import 'package:stacked/stacked.dart';

class EmailFormViewModel extends BaseViewModel{
  final formKey = GlobalKey<FormState>();
  String? toEmail;
  String? fromEmail;
  String emailBody = ' ';

  onToEmail(String? value) async {
    toEmail = value!.trim();
    notifyListeners();
  }

  onReplyToEmail(String? value) async{
    fromEmail = value!.trim();
    notifyListeners();
  }

  onMessage(String? value) async{
    emailBody = value!.trim();
  }
  String? onEmailValidator(value) {
    print("yes it is being called");
    if (value == null || value.isEmpty) {
      print('costvalue null called.');
      return 'field is required';
    }
    return null;
  }

  onFormSave(){
    print('api will be called ');
  }
}