import 'package:flutter/material.dart';
import 'package:ilabs/PROJECTS/practice/common/chi_text_field.dart';
import 'package:stacked/stacked.dart';
import 'email_from_vm.dart';


class EmailFormView extends ViewModelBuilderWidget<EmailFormViewModel>{
  @override
  Widget builder(BuildContext context, EmailFormViewModel viewModel, Widget? child) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('Email'),
          ),
        ),
        body: SingleChildScrollView(
          child: Form(
            key:viewModel.formKey,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 0, 10, 10 ),
              child: Column(
                children: [
                  CHITextField(
                    heading: 'To',
                    hintText: 'Type Email',
                    func:(value)  {  viewModel.onToEmail(value);},
                    initialValue: '',
                    validator:viewModel.onEmailValidator,
                  ),
                  CHITextField(
                    heading: 'Reply to ',
                    hintText: 'Type Email of sender',
                    func:(value)  {  viewModel.onReplyToEmail(value);},
                    initialValue: '',
                    validator:viewModel.onEmailValidator,
                  ),
                  CHITextField(
                    heading: 'Message ',
                    hintText: 'Type Message',
                    func:(value)  {  viewModel.onMessage(value);},
                    initialValue: '',
                    validator:viewModel.onEmailValidator,
                    maxLines: 6
                  ),
                  chiSaveButton('Save', viewModel.onFormSave)
                ],
              ),
            ),
          ),
        )
    );
  }

  @override
  EmailFormViewModel viewModelBuilder(BuildContext context) {
    return EmailFormViewModel();
  }

}



