import 'package:flutter/material.dart';

import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:stacked/stacked.dart';
import 'home_vm.dart';
import 'pdf_from_file/PdfFromFileScreen.dart';
import 'pdf_screen.dart';

import 'invoice/invoice_vu.dart';
import 'our_invoice/our_invoice_vu.dart';

class HomeViewScreen extends ViewModelBuilderWidget<HomeViewModel>{
  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PDF Viewer'),
      ),
      body: Column(
        children: [
          Center(
            child: ElevatedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return const PDFScreen();
                }));
              },
              child: const Text('View PDF from Internet'),
            ),
          ),
          Center(
            child: ElevatedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return  PdfFromFileScreen();
                }));
              },
              child: const Text('Create PDF'),
            ),
          ),
          Center(
            child: ElevatedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return  InvoicePDFScreen();
                }));
              },
              child: const Text('Create Complex PDF example'),
            ),
          ),
          Center(
            child: ElevatedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return  OurInvoiceScreen();
                }));
              },
              child: const Text('Create our own Custom PDF'),
            ),
          ),
        ],
      )
    );
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    return HomeViewModel();
  }

}


