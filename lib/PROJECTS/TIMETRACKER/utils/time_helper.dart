import 'package:intl/intl.dart';

String timeStampToTime(timeStamp) {
  final DateTime date = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
  String formattedDate = DateFormat('hh:mm a').format(date);
  return formattedDate;
}

String convertToHrMinSec(int tx1) {
  String hr = "${tx1 ~/ 3600}".padLeft(2, '0');
  int tx = tx1 % 3600;
  String min = "${tx ~/ 60}".padLeft(2, '0');
  tx = tx % 60;
  String sec = "$tx".padLeft(2, '0');
  // print("TX  $tx1:  $hr:$min:$sec, ");
  // return "$hr : $min :  $sec";
  return "$hr:$min:$sec";
}
