
Munib
- when clock first starts, we should only checkout, switch from 
  working to break hours.
- Add pagination
- Ability to sort w.r.t time and ID
- Today 

Ali Hassan
- Refresh of timesheet should work
- Add floating button on timesheet page
- Add tabbar on timesheet page (all, break, work)
- Add profile page (low priority)
- would like to refresh the timesheet on punching also
- Add some animation and disable button when call in progress.
- overlapping times should be 


===================================================
Feb 6. - Ahmad
- Called the add_time API
  fixed the timestamp issues
- Improved the time_sheet page
- Time wasted due to a bug in backend, it is not returning all time sheet


Feb 5. Ahmad 
- Wrote the VU and VM from scratch for the form
- Cleaned up the code.
- Did not integrate with API.
       

Feb 5. Ali
- Around 2-3 Hours
  Tried to make input form with bugs and lot of code

Feb 4. Ahmad
- In the morning cleaned up the code
- 

========================================================================

Feb 2, 2022 - ALI
8:45am   Start:
9:45am   Login and Session API working
10:20am  Explained the work to Omair and Ali
10:23am  Going to make a model of time_entry
10:33am  Finished to make a model of time_entry
10:34am  Going to make a model of time_start
10:41am  Finished to make a model of time_start
10:45am  did some gupshup
10:56am  break policy api working
11:01am  gupshup, going do the add_break
11:04am  complete
11:10am  Planning UI
12:18pm  Time got wasted while setting things up, getting code from Hamza
         Looking at two computers, etc.
12:21pm  Going understand the UI code structure.         
==============================================================================
12:30pm  Started understanding an cleaning up code of Tab based basic application
         with a possibility of a common viewModel.
         Removed bugs of the tab bar app    

5:12pm   Fixed the issues in Khokha, completed, no known issues.
         now going to remove appbar


- Also add styling for tab bar as Salman's, appbar
- Remove floating button, Each view will have its own floating point button
- Add Login
- Add estimate screen

===========================================================================

TimeTracker Screens
    9:00   Login screen adding
    9:04   added with testing(add route)
    9:05   adding route of home screen to login button
    9:08   ready skeleton of timetracker app with screen 2 Screens
    9:09   setting  HomeScreen
    9:42   home screen and record screen set with same app bar
    9:43   planning for home screen
    10:00  start api call with data fetching on record screen
    11:20  done api call with data fetching on view(testing complete)
    11:24  starting work on Home view
    11:30  api was not working
    01:05  set postman ...run api there and testing 
    break
    2:15   starting work on profile screen
    3:28   added home screen, api called and data populated   
================================================================================

