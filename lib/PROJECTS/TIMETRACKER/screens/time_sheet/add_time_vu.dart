import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'add_time_vm.dart';
import 'datetimepicker.dart';

class AddTimeEntry extends ViewModelBuilderWidget<AddTimeViewModel> {
  const AddTimeEntry({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, AddTimeViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Break"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            DialogInputField(
              onPressed: () async {
                viewModel.updateDate(await datePicker(context));
              },
              title: viewModel.entryDate,
            ),
            DialogInputField(
              onPressed: () async {
                viewModel.updateStartTime(await timePicker(context));
              },
              title: viewModel.startTime,
            ),
            DialogInputField(
              onPressed: () async {
                viewModel.updateEndTime(await timePicker(context));
              },
              title: viewModel.endTime,
            ),
            DialogInputField(
              onPressed: () {
                viewModel.save();
                Navigator.pop(context);
              },
              title: "SAVE",
            ),
          ],
        ),
      ),
    );
  }

  @override
  AddTimeViewModel viewModelBuilder(BuildContext context) {
    final vm = AddTimeViewModel();
    return vm;
  }
}

class DialogInputField extends StatelessWidget {
  final void Function() onPressed;
  final String title;
  const DialogInputField(
      {required this.onPressed, required this.title, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        )),
        onPressed: onPressed,
        child: Text(
          title,
          style: const TextStyle(
              color: Colors.white, fontSize: 14, letterSpacing: 1),
        ));
  }
  // primary: Colors.orange[700],
}
