import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import '../../models/add_break/model.dart';
// import '../../models/break_policy/model.dart';
import '../../services/api_client.dart';

class AddTimeViewModel extends BaseViewModel {
  String entryDate = "Add date";
  String startTime = "Add start Time";
  String endTime = "Add end Time";

  String _formatTime(TimeOfDay? timeOfDay) {
    timeOfDay ??= TimeOfDay.now();
    String hour = "${timeOfDay.hour}".padLeft(2, '0');
    String minute = "${timeOfDay.minute}".padLeft(2, '0');
    return "$hour:$minute";
  }

  void updateStartTime(TimeOfDay? timeOfDay) {
    startTime = _formatTime(timeOfDay);
    notifyListeners();
  }

  void updateEndTime(TimeOfDay? timeOfDay) {
    endTime = _formatTime(timeOfDay);
    notifyListeners();
  }

  void updateDate(DateTime? timeOfDay) {
    timeOfDay ??= DateTime.now();
    String year = "${timeOfDay.year}";
    String month = "${timeOfDay.month}".padLeft(2, '0');
    String day = "${timeOfDay.day}".padLeft(2, '0');
    entryDate = "$year-$month-$day";
    notifyListeners();
  }

  void save() async {
    debugPrint("$entryDate:   $startTime --  $endTime ");
    DateTime dt1 = DateTime.parse("$entryDate $startTime");
    int startTimeStamp = dt1.millisecondsSinceEpoch ~/ 1000;
    DateTime dt2 = DateTime.parse("$entryDate $endTime");
    int endTimeStamp = dt2.millisecondsSinceEpoch ~/ 1000;

    Request req = {
      "entry_type": "Time Entry",
      "start": startTimeStamp,
      "stop": endTimeStamp,
      "notes": "$entryDate:   $startTime --  $endTime",
      // "policy_id": 1
    };

    Response resp = await ApiClient.post<TimeEntry>(
        request: req,
        endPoint: "/api/app/add_time",
        fromJson: TimeEntry.fromMap);
    TimeEntry timeEntry = resp["data"];
    debugPrint('TimeEntry: $timeEntry');
  }
}
