import 'package:flutter/material.dart';
import '../../models/time_sheet/model.dart';
import '../../utils/time_helper.dart';

Widget timeEntryCell(TimeEntry item) {
  String notes = (item.notes == null) ? "" : item.notes!;
  return Padding(
    padding: const EdgeInsets.fromLTRB(8.0, 2, 8.0, 2),
    child: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              children: [
                Icon(
                  Icons.date_range_outlined,
                  color: Colors.grey.shade500,
                  size: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: Text(
                    item.name!,
                    style: const TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ),
                const Spacer(),
                Text(
                  item.entryId!.toString() + " " + item.entryType!,
                  style: const TextStyle(color: Colors.grey, fontSize: 12),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              children: [
                Text(
                  timeStampToTime(item.start),
                  style: const TextStyle(color: Colors.black87, fontSize: 15),
                ),
                const Spacer(),
                Text(
                  timeStampToTime(item.stop),
                  style: const TextStyle(
                    color: Colors.black87,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 8),
            child: Text(
              convertToHrMinSec(item.duration!.toInt()) + "  " + notes,
              style: const TextStyle(color: Colors.blue, fontSize: 14),
            ),
          )
        ],
      ),
    ),
  );
}
