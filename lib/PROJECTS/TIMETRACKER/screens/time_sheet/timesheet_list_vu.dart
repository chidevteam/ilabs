import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'timesheet_list_vm.dart';
import 'timesheet_cell_vu.dart';

class TimeSheetScreen extends ViewModelBuilderWidget<TimeSheetViewModel> {
  const TimeSheetScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, TimeSheetViewModel viewModel, Widget? child) {
    return RefreshIndicator(
        onRefresh: () async {
          await viewModel.refreshData();
        },
        child: Center(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: viewModel.timeSheet.length,
              itemBuilder: (context, index) {
                return timeEntryCell(viewModel.timeSheet[index]);
              }),
        ));
  }

  @override
  TimeSheetViewModel viewModelBuilder(BuildContext context) {
    TimeSheetViewModel vm = TimeSheetViewModel();
    vm.refreshData();
    return vm;
  }
}
