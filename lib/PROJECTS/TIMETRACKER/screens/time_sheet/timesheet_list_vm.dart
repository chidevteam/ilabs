import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import '../../services/api_client.dart';
import '../../models/time_sheet/model.dart';

class TimeSheetViewModel extends BaseViewModel {
  List<TimeEntry> timeSheet = [];

  refreshData() async {
    Response resp = await ApiClient.post<TimeSheet>(
        request: {},
        endPoint: "/api/app/time_sheet",
        fromJson: TimeSheet.fromMap);
    List<TimeEntry> entries = resp["data"].data;

    timeSheet.clear();
    // sort in reverse order
    for (int i = entries.length - 1; i >= 0; i--) {
      timeSheet.add(entries[i]);
    }
    // timeSheet.addAll(iList.data!);
    debugPrint('Timesheet Length :  ${timeSheet.length}');
    notifyListeners();
  }
}
