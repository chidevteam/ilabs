import 'package:flutter/material.dart';

Future<TimeOfDay?> timePicker(
  BuildContext context,
) async {
  final TimeOfDay? timeOfDay = await showTimePicker(
    context: context,
    initialTime: TimeOfDay.now(),
    initialEntryMode: TimePickerEntryMode.dial,
  );
  return timeOfDay;
}

Future<DateTime?> datePicker(BuildContext context,
    {String nullValue = "---"}) async {
  final DateTime? timeOfDay = await showDatePicker(
    context: context,
    initialDate: DateTime.now(),
    firstDate: DateTime(2010),
    lastDate: DateTime(2025),
  );
  return timeOfDay;
}
