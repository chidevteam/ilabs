// import 'package:flutter/material.dart';
// import 'package:stacked/stacked.dart';
// import 'add_break_vm.dart';

// class AddBreak extends ViewModelBuilderWidget<AddBreakViewModel> {
//   const AddBreak({Key? key}) : super(key: key);

//   @override
//   Widget builder(
//       BuildContext context, AddBreakViewModel viewModel, Widget? child) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         title: const Text(
//           'Add Entry',
//           style: TextStyle(
//               color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400),
//         ),
//         leading: IconButton(
//           onPressed: () {
//             Navigator.pop(context);
//           },
//           icon: Icon(
//             Icons.arrow_back,
//             color: Colors.orange[800],
//           ),
//         ),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.only(top: 24.0, left: 18.0, right: 18.0),
//         child: SingleChildScrollView(
//           child: Form(
//             autovalidateMode: AutovalidateMode.onUserInteraction,
//             key: viewModel.formKey,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 headingText('Strat at'),
//                 const SizedBox(
//                   height: 14.0,
//                 ),
//                 startTime(viewModel, context),
//                 const SizedBox(
//                   height: 16.0,
//                 ),
//                 headingText('End at'),
//                 const SizedBox(
//                   height: 14.0,
//                 ),
//                 endTime(viewModel, context),
//                 const SizedBox(
//                   height: 14.0,
//                 ),
//                 headingText('Entry Type'),
//                 const SizedBox(
//                   height: 16.0,
//                 ),
//                 addEntryType(viewModel),
//                 const SizedBox(
//                   height: 14.0,
//                 ),
//                 addBreakPolicy(viewModel),
//                 saveButton(viewModel, context),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   Visibility addBreakPolicy(AddBreakViewModel viewModel) {
//     return Visibility(
//       visible: viewModel.addType == 'Break' ? true : false,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           headingText('Break Policy'),
//           const SizedBox(
//             height: 16.0,
//           ),
//           SizedBox(
//             height: 56,
//             child: DropdownButtonFormField(
//               decoration: const InputDecoration(
//                 enabledBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.all(Radius.circular(12.0)),
//                   borderSide: BorderSide(
//                     color: Colors.blue,
//                     width: 0.0,
//                   ),
//                 ),
//                 hintText: 'Break Policy',
//               ),
//               items: viewModel.breakPolicyItems,
//               onChanged: viewModel.onBreakPlicySaved,
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   Text headingText(String text) {
//     return Text(
//       text,
//       style: TextStyle(
//           color: Colors.grey[700], fontSize: 13.0, fontWeight: FontWeight.w400),
//     );
//   }

//   Widget addEntryType(AddBreakViewModel viewModel) {
//     return SizedBox(
//       height: 56,
//       child: DropdownButtonFormField(
//         decoration: const InputDecoration(
//           enabledBorder: OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(12.0)),
//             borderSide: BorderSide(
//               color: Colors.blue,
//               width: 0.0,
//             ),
//           ),
//           hintText: 'Select Type',
//         ),
//         items: viewModel.addTypeItems,
//         onChanged: viewModel.onAddTypeSaved,
//       ),
//     );
//   }

//   Widget startTime(AddBreakViewModel viewModel, context) {
//     return TextFormField(
//       readOnly: true,
//       onSaved: viewModel.onstartSaved,
//       controller: viewModel.startController,
//       decoration: InputDecoration(
//         prefixIcon: IconButton(
//           icon: const Icon(Icons.timer),
//           onPressed: () {
//             viewModel.onStartTime(context);
//           },
//         ),
//         contentPadding: const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
//         fillColor: Colors.grey[200],
//         filled: true,
//         hintText: viewModel.selectedTime.format(context).toString(),
//         hintStyle: const TextStyle(
//             fontWeight: FontWeight.w500, fontSize: 16, color: Colors.black),
//         focusedBorder: const OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(12.0)),
//             borderSide: BorderSide(
//               color: Colors.orange,
//             )),
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: Colors.grey.shade400),
//           borderRadius: const BorderRadius.all(
//             Radius.circular(12.0),
//           ),
//         ),
//         focusedErrorBorder: OutlineInputBorder(
//           borderSide: const BorderSide(color: Colors.red),
//           borderRadius: BorderRadius.circular(12.0),
//         ),
//       ),
//     );
//   }

//   Widget endTime(AddBreakViewModel viewModel, context) {
//     return TextFormField(
//       controller: viewModel.endController,
//       readOnly: true,
//       onSaved: viewModel.onendSaved,
//       decoration: InputDecoration(
//         prefixIcon: IconButton(
//           icon: const Icon(Icons.timer),
//           onPressed: () {
//             viewModel.onEndTime(context);
//           },
//         ),
//         contentPadding: const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
//         fillColor: Colors.grey[200],
//         filled: true,
//         hintText: viewModel.selectedTime.format(context).toString(),
//         hintStyle: const TextStyle(
//             fontWeight: FontWeight.w500, fontSize: 16, color: Colors.black),
//         focusedBorder: const OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(12.0)),
//             borderSide: BorderSide(
//               color: Colors.orange,
//             )),
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: Colors.grey.shade400),
//           borderRadius: const BorderRadius.all(
//             Radius.circular(12.0),
//           ),
//         ),
//         focusedErrorBorder: OutlineInputBorder(
//           borderSide: const BorderSide(color: Colors.red),
//           borderRadius: BorderRadius.circular(12.0),
//         ),
//       ),
//     );
//   }

//   Widget saveButton(AddBreakViewModel viewModel, BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 16.0),
//       child: SizedBox(
//         width: double.infinity,
//         height: 45.0,
//         child: ElevatedButton(
//           style: ElevatedButton.styleFrom(
//               primary: Colors.orange[600],
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(12),
//               )),
//           onPressed: () {
//             viewModel.onSave(context);
//           },
//           child: const Text(
//             'SAVE',
//             style: TextStyle(
//               color: Colors.white,
//               fontSize: 14,
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   @override
//   AddBreakViewModel viewModelBuilder(BuildContext context) {
//     final vm = AddBreakViewModel();
//     return vm;
//   }
// }
