// import 'package:flutter/material.dart';
// import 'package:stacked/stacked.dart';
// import '../../models/add_break/model.dart';
// import '../../models/break_policy/model.dart';
// import '../../services/api_client.dart';

// class AddBreakViewModel extends BaseViewModel {
//   final formKey = GlobalKey<FormState>();
//   BreakPolicyResponse? breakPloicices;
//   String? addType;
//   String? breakPolicy;
//   TimeOfDay selectedTime = TimeOfDay.now();
//   DateTime newTime = DateTime.now();
//   final startController = TextEditingController();
//   final endController = TextEditingController();
//   int? startTime;
//   int? endTime;

//   onStartTime(BuildContext context) async {
//     startTime = await onSelectTime(context);
//   }

//   onstartSaved(value) {
//     startTime = value;
//     debugPrint('type of Policy$breakPolicy');
//     notifyListeners();
//   }

//   onEndTime(BuildContext context) async {
//     endTime = await onSelectTime(context);
//   }

//   onendSaved(value) {
//     endTime = value;
//     debugPrint('type of Policy$breakPolicy');
//     notifyListeners();
//   }

//   onAddTypeSaved(value) {
//     addType = value;
//     debugPrint('type of entry$addType');
//     notifyListeners();
//   }

//   onBreakPlicySaved(value) {
//     breakPolicy = value;
//     debugPrint('type of Policy$breakPolicy');
//     notifyListeners();
//   }

//   onSelectTime(BuildContext context) async {
//     final TimeOfDay? timeOfDay = await showTimePicker(
//       context: context,
//       initialTime: selectedTime,
//       initialEntryMode: TimePickerEntryMode.dial,
//     );
//     if (timeOfDay != null && timeOfDay != selectedTime) {
//       selectedTime = timeOfDay;
//       DateTime myTime = DateTime.now();
//       newTime = DateTime(myTime.year, myTime.month, myTime.day,
//           selectedTime.hour, selectedTime.minute);
//       debugPrint('selected time${newTime.millisecondsSinceEpoch}');
//       debugPrint(selectedTime.minute.toString());
//       notifyListeners();
//       return newTime.millisecondsSinceEpoch;
//     }
//   }

//   String? onEndValidator(value) {
//     if (value == null || value.isEmpty) {
//       return 'Please select first';
//     }
//     return null;
//   }

//   onSave(BuildContext context) async {
//     //   if (formKey.currentState == null) {
//     //   return;
//     // }
//     // if (!formKey.currentState!.validate()) {
//     //   return;
//     // }

//     formKey.currentState!.save();
//     debugPrint('staratttttttttttt $startTime');

//     Response respBreak = await ApiClient.post<BreakPolicyResponse>(
//         request: {},
//         endPoint: "/api/app/policies",
//         fromJson: BreakPolicyResponse.fromMap);
//     BreakPolicyResponse breakPloicy = respBreak["data"];
//     breakPloicices = breakPloicy;

//     debugPrint("$breakPloicy");

//     Request req = {
//       "entry_type": addType,
//       "start": startTime!,
//       "stop": endTime!,
//       "notes": "just relax break",
//       "policy_id": 1
//     };
//     Response resp = await ApiClient.post<TimeEntry>(
//         request: req,
//         endPoint: "/api/app/add_time",
//         fromJson: TimeEntry.fromMap);
//     TimeEntry addTime = resp["data"];
//     print('starattttttttttttedddddd$startTime');

//     Navigator.pop(context);
//     notifyListeners();
//   }

//   List<DropdownMenuItem<String>> get addTypeItems {
//     List<DropdownMenuItem<String>> menuItems = [
//       const DropdownMenuItem(child: Text("Break"), value: "Break"),
//       const DropdownMenuItem(child: Text("Time Entry"), value: "TimeEntry"),
//     ];
//     return menuItems;
//   }

//   List<DropdownMenuItem<String>> get breakPolicyItems {
//     List<DropdownMenuItem<String>> menuItems = [
//       const DropdownMenuItem(child: Text("Tea Break"), value: "Tea Break"),
//       const DropdownMenuItem(
//           child: Text("Prayer Break"), value: "Prayer Break"),
//       const DropdownMenuItem(child: Text("Lunch Break"), value: "Lunch Break"),
//     ];
//     return menuItems;
//   }
// }
