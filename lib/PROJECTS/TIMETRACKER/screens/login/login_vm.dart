import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../services/api_client.dart';

class LoginViewModel extends BaseViewModel {
  final formKey = GlobalKey<FormState>();
  String? email = '';
  String? password = '';

  onEmail(String? value) async {
    email = value!.trim();

    notifyListeners();
  }

  String? onEmailValidator(value) {
    if (value == null || value.isEmpty) {
      return 'email is required';
    }
    return null;
  }

  onPassword(String? value) async {
    password = value!.trim();

    notifyListeners();
  }

  String? onPasswordValidator(value) {
    if (value == null || value.isEmpty) {
      return 'password is required';
    }
    return null;
  }

  onClickItem(BuildContext context) async {
    formKey.currentState!.save();
    if (formKey.currentState == null) {
      return;
    }

    if (!formKey.currentState!.validate()) {
      return;
    }
    debugPrint('$email');
    debugPrint('$password');

    ApiClient.create("https://time-track.cognitivehealthintl.com");
    email = "ali@email.com";
    password = "Ali@123";
    Response resp = await ApiClient.login(email!, password!);
    debugPrint("$resp");
    Navigator.pushNamed(context, '/appscreen');

    notifyListeners();
  }
}
