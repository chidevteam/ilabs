// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_core/firebase_core.dart';
// ignore_for_file: prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'login_vm.dart';

class LoginScreen extends ViewModelBuilderWidget<LoginViewModel> {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, viewModel, Widget? child) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
          top: 30,
        ),
        child: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: viewModel.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.arrow_back,
                        color: Colors.grey,
                        size: 18,
                      ),
                    ),
                  ],
                ),
                loginImage(),
                const SizedBox(
                  height: 24.0,
                ),
                const Text(
                  'Welcome Back!',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                const Text(
                  'Enter information Below to Login your Account',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w500),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 16, 20.0, 0),
                  child: email(viewModel),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 8.0, 20.0, 8.0),
                  child: password(viewModel),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 12.0),
                  child: loginButton(viewModel, context),
                ),
                TextButton(
                  onPressed: () {},
                  child: const Text(
                    'Forgot Password?',
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500),
                  ),
                  style: TextButton.styleFrom(
                    minimumSize: Size.zero,
                    padding: EdgeInsets.zero,
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                ),
                loginWithSocialMedia(),
                googleFacebookImage()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget googleFacebookImage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
          child: Image.network(
            'https://logowik.com/content/uploads/images/985_google_g_icon.jpg',
            height: 40,
            width: 40,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          width: 20.0,
        ),
        ClipRRect(
          child: Image.network(
            'https://www.kindpng.com/picc/m/1-14152_facebook-twitter-instagram-pinterest-facebook-twitter-facebook-icon.png',
            height: 36,
            width: 36,
            fit: BoxFit.cover,
          ),
        )
      ],
    );
  }

  Image loginImage() {
    return Image.network(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRukCYipD_R-qSrLu5C2oUlndZZ4mGp42JjKKCas079DFeHK5HMWNglHsjemlfy-MT4wAg&usqp=CAU',
      width: 100,
      height: 100,
      fit: BoxFit.cover,
    );
  }

  Widget email(
    LoginViewModel viewModel,
  ) {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        labelText: 'Email',
        labelStyle: TextStyle(
            fontWeight: FontWeight.w600, fontSize: 14, color: Colors.grey),
      ),
      validator: viewModel.onEmailValidator,
      onSaved: (String? value) {
        viewModel.onEmail(value);
      },
    );
  }

  Widget password(
    LoginViewModel viewModel,
  ) {
    return TextFormField(
        keyboardType: TextInputType.visiblePassword,
        decoration: const InputDecoration(
          labelText: 'Password',
          labelStyle: TextStyle(
              fontWeight: FontWeight.w600, fontSize: 14, color: Colors.grey),
        ),
        validator: viewModel.onPasswordValidator,
        onSaved: (String? value) {
          viewModel.onPassword(value);
        });
  }

  Widget loginButton(LoginViewModel viewModel, context) {
    return SizedBox(
      width: double.infinity,
      height: 44.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            // primary: Colors.orange[700],
            shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        )),
        onPressed: () {
          // auth.createUserWithEmailAndPassword(
          //     email: viewModel.email!, password: viewModel.password!);
          viewModel.onClickItem(context);
          //Navigator.pushNamed(context, '/addClientDetailScreen');
          // Navigator.pushNamed(context, '/myHomePage');
        },
        child: const Text(
          'Login',
          style: TextStyle(color: Colors.white, fontSize: 14, letterSpacing: 1),
        ),
      ),
    );
  }

  Widget loginWithSocialMedia() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 12.0),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Divider(
              color: Colors.grey[300],
              thickness: 1.0,
            ),
          ),
          Expanded(
            flex: 6,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(2, 7, 2, 10),
              child: Text(
                "Or login with social media",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey.shade500,
                    fontSize: 12,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Divider(
              color: Colors.grey[300],
              thickness: 1.0,
            ),
          )
        ],
      ),
    );
  }

  @override
  LoginViewModel viewModelBuilder(BuildContext context) {
    return LoginViewModel();
  }
}
