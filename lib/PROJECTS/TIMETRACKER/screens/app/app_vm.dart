import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../models/break_policy/model.dart';
import '../../services/api_client.dart';

class AppViewModel extends BaseViewModel {
  String title = "Time Tracker";
  int selectedIndex = 0;

  AppViewModel() {
    breakPolicy();
  }

  onBottomNavTap(int index) {
    selectedIndex = index;
    notifyListeners();
  }

  void breakPolicy() async {
    Response respBreak = await ApiClient.post<BreakPolicyResponse>(
        request: {},
        endPoint: "/api/app/policies",
        fromJson: BreakPolicyResponse.fromMap);
    BreakPolicyResponse breakPolicy = respBreak["data"];
    debugPrint("$breakPolicy");
  }
}
