import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../time_sheet/timesheet_list_vu.dart';
import '../punchinout/punchinout_vu.dart';
import 'app_vm.dart';

class AppScreen extends ViewModelBuilderWidget<AppViewModel> {
  const AppScreen({Key? key}) : super(key: key);

  @override
  Widget builder(BuildContext context, AppViewModel viewModel, Widget? child) {
    List<Widget> widgetOptions = [];

    widgetOptions.add(const PunchInOutScreen());
    widgetOptions.add(const TimeSheetScreen());

    return Scaffold(
      appBar: AppBar(
        title: Text(viewModel.title),
        leading: IconButton(
          icon: const Icon(Icons.person),
          onPressed: () {
            Navigator.pushNamed(context, '/profile');
          },
        ),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      body: IndexedStack(
        index: viewModel.selectedIndex,
        children: widgetOptions,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_filled),
            label: 'Punch In/Out',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Time Sheet',
          ),
        ],
        currentIndex: viewModel.selectedIndex,
        onTap: viewModel.onBottomNavTap,
      ),
    );
  }

  @override
  AppViewModel viewModelBuilder(BuildContext context) {
    AppViewModel vm = AppViewModel();

    return vm;
  }
}
