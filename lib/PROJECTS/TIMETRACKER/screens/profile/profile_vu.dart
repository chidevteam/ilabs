import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'profile_vm.dart';

class ProfileScreen extends ViewModelBuilderWidget<ProfileViewModel> {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, ProfileViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Profile')),
      ),
      body: viewModel.isBusy
          ? const Text('Getting DAta')
          : Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Column(
                children: [
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: Image.network(
                            'https://cdn.pixabay.com/photo/2015/03/04/22/35/head-659652_1280.png',
                            height: 120,
                            width: 120,
                          )),
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: Text(viewModel.profile!.session!.user!.name!)),
                  Expanded(
                      flex: 2,
                      child: Text(viewModel.profile!.session!.user!.role!)),
                ],
              ),
            ),
    );
  }

  @override
  ProfileViewModel viewModelBuilder(BuildContext context) {
    final vm = ProfileViewModel();
    vm.onGetProfile();
    return vm;
  }
}
