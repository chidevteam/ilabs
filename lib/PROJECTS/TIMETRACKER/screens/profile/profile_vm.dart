import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import '../../models/session/model.dart';
import '../../services/api_client.dart';

class ProfileViewModel extends BaseViewModel {
  SessionResponse? profile;

  onGetProfile() async {
    setBusy(true);
    Response resp = await ApiClient.post<SessionResponse>(
        request: {},
        endPoint: "/api/app/session",
        fromJson: SessionResponse.fromMap);
    debugPrint('respose  $resp');
    setBusy(false);
    SessionResponse profileData = resp["data"];
    profile = profileData;
    debugPrint('user idd${profile!.session!.userId!}');
    debugPrint('prfileee$profileData');
  }
}
