import 'dart:async';
import 'package:stacked/stacked.dart';
import 'package:flutter/material.dart';
import '../../models/time_status/model.dart';
import '../../models/time_start/model.dart';
import '../../models/time_end/model.dart';
import '../../services/api_client.dart';
import '../../utils/time_helper.dart';

class PunchViewModel extends BaseViewModel {
  String timePassed = "=====";
  int? punchIntime;
  Timer? timer;

  PunchViewModel() {
    debugPrint("HOme viewmodel made");
    timeStatusAPI();
  }

  Future timeStatusAPI() async {
    Request req = {};
    Response resp = await ApiClient.post<TimeStatusResponse>(
        request: req,
        endPoint: "/api/app/time_status",
        fromJson: TimeStatusResponse.fromMap);
    TimeStatusResponse? iList = resp["data"];

    if (iList!.data == null) {
      punchIntime = null;
    } else {
      punchIntime = iList.data!.start;
      startTimer();
    }
    debugPrint("$iList");
    // startTimer();
  }

  startTimer() {
    timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      int currTime = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      // debugPrint("$currTime $punchIntime ${currTime - punchIntime!}");
      timePassed = convertToHrMinSec(currTime - punchIntime!);
      notifyListeners();
    });
  }

  onClickPunchButton() async {
    if (punchIntime == null) {
      await timeStartAPI();
    } else {
      timer!.cancel();
      await timeEndApi();
      punchIntime = null;
      timePassed = "=====";
    }
    debugPrint("punch -- $punchIntime");
    timeStatusAPI();
    notifyListeners();
  }

  Future timeStartAPI() async {
    Request req = {};
    Response resp = await ApiClient.post<TimeStartResponse>(
        request: req,
        endPoint: "/api/app/time_start",
        fromJson: TimeStartResponse.fromMap);
    TimeStartResponse iList = resp["data"];
    debugPrint("$iList");
  }

  Future timeEndApi() async {
    Request req = {};
    Response resp = await ApiClient.post<TimeEndResponse>(
        request: req,
        endPoint: "/api/app/time_stop",
        fromJson: TimeEndResponse.fromMap);
    TimeEndResponse iList = resp["data"];
    debugPrint("$iList");
  }
}
