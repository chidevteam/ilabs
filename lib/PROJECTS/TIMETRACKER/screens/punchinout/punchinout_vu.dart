import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'punchinout_vm.dart';
import '../../utils/time_helper.dart';

class PunchInOutScreen extends ViewModelBuilderWidget<PunchViewModel> {
  const PunchInOutScreen({Key? key}) : super(key: key);

  @override
  Widget builder(
      BuildContext context, PunchViewModel viewModel, Widget? child) {
    String stTime = "--";
    //timeStampToTime(viewModel.punchIntime)
    if (viewModel.punchIntime != null) {
      stTime = timeStampToTime(viewModel.punchIntime!);
    }
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(
                viewModel.timePassed,
                style: const TextStyle(color: Colors.blueAccent, fontSize: 72),
              ),
              Text(
                stTime,
                style: const TextStyle(color: Colors.black38, fontSize: 42),
              )
            ],
          ),
          SizedBox(
            height: 52,
            width: 140,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: viewModel.punchIntime == null
                        ? Colors.green
                        : Colors.red),
                onPressed: () {
                  viewModel.onClickPunchButton();
                },
                child: Text(
                  viewModel.punchIntime == null ? 'Punch In' : 'Punch Out',
                  style: const TextStyle(fontSize: 18),
                )),
          ),
          SizedBox(
            height: 44,
            width: 140,
            child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/addbreak');
                },
                child: const Text('Add Break ')),
          )
        ],
      )),
    );
  }

  @override
  PunchViewModel viewModelBuilder(BuildContext context) {
    PunchViewModel vm = PunchViewModel();

    return vm;
  }
}
