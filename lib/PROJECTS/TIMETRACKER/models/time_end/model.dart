class TimeEndResponse {
  final String? status;
  final String? data;
  final String? message;

  TimeEndResponse(
    this.status,
    this.data,
    this.message,
  );

  static TimeEndResponse fromMap(Map<String, dynamic> resp) {
    return TimeEndResponse(
      resp['status'] as String?,
      resp['data'] as String?,
      resp['message'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "status:: $status\n";
    st += "data:: $data\n";
    st += "message:: $message\n";
    return st;
  }
}

