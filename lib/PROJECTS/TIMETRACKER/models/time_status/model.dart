class TimeStatusResponse {
  final String? status;
  final TimeStatus? data;
  final String? message;

  TimeStatusResponse(
    this.status,
    this.data,
    this.message,
  );

  static TimeStatusResponse fromMap(Map<String, dynamic> resp) {
    return TimeStatusResponse(
      resp['status'] as String?,
      resp['data'] != null ? TimeStatus.fromMap(resp['data']) : null,
      resp['message'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "status:: $status\n";
    st += "data:: ${data.toString()}\n";
    st += "message:: $message\n";
    return st;
  }
}

class TimeStatus {
  final int? entryId;
  final int? start;
  final String? notes;

  TimeStatus(
    this.entryId,
    this.start,
    this.notes,
  );

  static TimeStatus fromMap(Map<String, dynamic> resp) {
    return TimeStatus(
      resp['entry_id'] as int?,
      resp['start'] as int?,
      resp['notes'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "entry_id:: ${entryId.toString()}\n";
    st += "start:: ${start.toString()}\n";
    st += "notes:: $notes\n";
    return st;
  }
}
