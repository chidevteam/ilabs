class SessionResponse {
  final String? status;
  final Session? session;
  final String? message;

  SessionResponse(
    this.status,
    this.session,
    this.message,
  );

  static SessionResponse fromMap(Map<String, dynamic> resp) {
    return SessionResponse(
      resp['status'] as String?,
      Session.fromMap(resp['data']),
      resp['message'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "status:: ${status}\n";
    st += "session:: ${session?.toString()}\n";
    st += "message:: ${message}\n";
    return st;
  }
}

class Session {
  final String? sessionId;
  final num? userId;
  final num? exp;
  final User? user;
  final String? ipAddress;

  Session(
    this.sessionId,
    this.userId,
    this.exp,
    this.user,
    this.ipAddress,
  );

  static Session fromMap(Map<String, dynamic> resp) {
    return Session(
      resp['session_id'] as String?,
      resp['user_id'] as num?,
      resp['exp'] as num?,
      User.fromMap(resp['user']),
      resp['ip_address'] as String?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "sessionId:: ${sessionId}\n";
    st += "userId:: ${userId}\n";
    st += "exp:: ${exp}\n";
    // st += "user:: ${user?.toString()}\n";
    st += "ipAddress:: ${ipAddress}\n";
    return st;
  }
}

class User {
  final num? userId;
  final String? name;
  final String? role;
  final Detail? detail;

  User(
    this.userId,
    this.name,
    this.role,
    this.detail,
  );

  static User fromMap(Map<String, dynamic> resp) {
    return User(
      resp['user_id'] as num?,
      resp['name'] as String?,
      resp['role'] as String?,
      Detail.fromMap(resp["detail"]),
    );
  }

  @override
  String toString() {
    String st = '';
    st += "userId:: ${userId}\n";
    st += "name:: ${name}\n";
    st += "role:: ${role}\n";
    st += "detail:: ${detail?.toString()}\n";
    return st;
  }
}

class Detail {
  final num? organizationId;
  final String? organizationName;
  final String? industry;
  final String? logo;
  final dynamic address;
  final num? startOfWeek;
  final num? dateAdded;
  final num? dateUpdated;

  Detail(
    this.organizationId,
    this.organizationName,
    this.industry,
    this.logo,
    this.address,
    this.startOfWeek,
    this.dateAdded,
    this.dateUpdated,
  );

  static Detail fromMap(Map<String, dynamic> resp) {
    return Detail(
      resp['organization_id'] as num?,
      resp['organization_name'] as String?,
      resp['industry'] as String?,
      resp['logo'] as String?,
      resp['address'] as dynamic,
      resp['start_of_week'] as num?,
      resp['date_added'] as num?,
      resp['date_updated'] as num?,
    );
  }

  @override
  String toString() {
    String st = '';
    st += "organizationId:: ${organizationId}\n";
    st += "organizationName:: ${organizationName}\n";
    st += "industry:: ${industry}\n";
    st += "logo:: ${logo}\n";
    st += "address:: ${address}\n";
    st += "startOfWeek:: ${startOfWeek}\n";
    st += "dateAdded:: ${dateAdded}\n";
    st += "dateUpdated:: ${dateUpdated}\n";
    return st;
  }
}
