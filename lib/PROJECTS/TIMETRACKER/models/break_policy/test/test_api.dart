// ignore_for_file: avoid_print
import '../../../services/api_client.dart';
import '../model.dart';

main() async {
  ApiClient.create("https://time-track.cognitivehealthintl.com");
  String email = "ahmad@email.com";
  String password = "Ahmad@123";
  Response resp = await ApiClient.login(email, password);
  print(resp);
  await testApi();
}

Future testApi() async {
  Response resp = await ApiClient.post<BreakPolicyResponse>(
      request: {},
      endPoint: "/api/app/policies",
      fromJson: BreakPolicyResponse.fromMap);
  BreakPolicyResponse iList = resp["data"];
  print(iList);
}
