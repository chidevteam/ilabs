import 'package:flutter/cupertino.dart';
import 'screens/app/app_vu.dart';
import 'screens/profile/profile_vu.dart';
import 'screens/time_sheet/add_time_vu.dart';
import 'screens/login/login_vu.dart';
// import 'screens/add_item/add_item_vu.dart';

Map<String, WidgetBuilder> appRoutes = {
  '/': (context) => const LoginScreen(),
  '/appscreen': (context) => const AppScreen(),
  '/profile': (context) => const ProfileScreen(),
  '/addbreak': (context) => const AddTimeEntry()
};
