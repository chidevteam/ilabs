// // ignore_for_file: avoid_print

// import 'package:flutter/material.dart';
// import 'routes.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:ilabs/firebase_options.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

// // void main2() {
// //   runApp(const MyApp());
// // }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       routes: appRoutes,
//       theme: ThemeData(primarySwatch: Colors.orange),
//       initialRoute: "/",
//     );
//   }
// }

// void main() async {
//   print("Starting");
//   WidgetsFlutterBinding.ensureInitialized();
//   await Firebase.initializeApp(
//     options: DefaultFirebaseOptions.currentPlatform,
//   );
//   await addUser();
//   runApp(const MyApp());
// }

// Future<void> addUser() {
//   // Call the user's CollectionReference to add a new user
//   CollectionReference users = FirebaseFirestore.instance.collection('users');
//   return users
//       .add({
//         'full_name': "Salman Khan", // John Doe
//         'company': "NUST CHI", // Stokes and Sons
//         'age': 22 // 42
//       })
//       .then((value) => print("User Added"))
//       .catchError((error) => print("Failed to add user: $error"));
// }
