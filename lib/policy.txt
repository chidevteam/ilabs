
- No constructor in APIClient accessible from outside
  APIClient.func()

-  Easy switching to production/dev
    (http://34.230.31.204 /api)
    http(s)://invoicelabs.co/api
    NOTE ========== one is http, other is https

    Portal at: (http://34.230.31.204:4000
    

FORM POLICY:
- For the model 

- Input would be Either model, or null
  In case of null, it will make an object, The VM file will look like:
  
  final formKey = GlobalKey<FormState>();
  PaymentInstructions? inputModel;
  PaymentInstructions outputModel = PaymentInstructions();

  PaymentInstructionVM(this.inputModel) {
    if (inputModel != null) outputModel = inputModel!.copy();
  }


- MAKE A COPY OF OBJECT WHEN WE COME IN.
- RETURN A NULL CANCELLED, onsave return object
- RETURN a NULL if no change in input and output object (Minor improvement)
- Shouldn't change the ID field (which is nullable)

MODEL POLICY
============
- All fields except 'id' are non-nullable
- toMap will append only the "valid" fields

IKRAM:
======
- Signature
- App rating
- Full register control flow (guest, etc)
- Fix the Edit button in Drawer business info.
- IMportant 4-5 structures in preferences

