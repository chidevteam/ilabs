import json
import requests
from lib.read_model import readDict


def callAPI(item, siteCode, token):

    endPoint = item["request"]["url"]["raw"]
    url = endPoint.replace("{{base_url}}", "")
    url = 'https://' + siteCode + '.cognitivehealthintl.com'+url
    print("END POINT:", endPoint)  
    
    if "body" in item["request"]:
        req = item["request"]["body"]["raw"]
        
    else:
        req = '{}'
    #url = /auth/Login2
    # req = '{"username":"Kashifd","password":"Kashif123@","pn_type":"None","pn_token":"None","app_version":"4.0.1","app_type":"Doctor","lang":"en","device_id":"Ahmad_WEB","device_name":"Kashif Limited","device_type":"Android","device_model":"A131","os_name":"Android","os_version":"13.2"}'
    # print(req)
    if req == '':
        req = '{}'
    
    req1 = json.loads(req)
    # readDict(-1, req1)

    reqKeys = [k for k in req1.keys()]
    # print(reqKeys[0:4])  +++
    
    # print(json.dumps(req1, sort_keys=True, indent=4))
    # exit()
    header={"Authorization": token}
    # headers={"key": "Content-Type", "value": "application/json"}
    response = requests.post(url, data = req, headers=header)

    if response.status_code == 500:
        return response.status_code, json.loads(response.content)
    
    if response.status_code == 403:
        return response.status_code, json.loads(response.content)

    if response.status_code == 405:
        return response.status_code, {"error_html": str(response.content)}

    # rq = json.loads(req)
    return url, req1, response.status_code, response.json()
