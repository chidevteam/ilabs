# from concurrent.futures import process
import json
from .read_model import readDict
from .utils import fileNameFromAPIname, writeJsonToFile, replaceDataDataKeys, writeStringToFile
from .write_model import ModelWrite

def processResponse(reqType, email, passwd, folder, baseURL, endPoint, params, fomData):

    
    fullPath = folder + "/json/response.json"
    f = open(fullPath, 'r')
    data = f.read()
    resp = json.loads(data)
    # writeJsonToFile(folder, "model_array.json", modelArray)

    
    writeStringToFile(folder+"test/", "response.dart", "Map<String, dynamic> response = "+data+";")
    # modelFileName = fileNameFromAPIname(folderName)

    modelArray = []
    readDict(-1, resp, "General", modelArray)
    writeJsonToFile(folder+"json/", "model_array.json", modelArray)
    modelChangeName = processNames(modelArray)
    model = ModelWrite(modelArray[::-1], modelChangeName)
    writeStringToFile(folder, "model.dart", model.st)

    main_model_string = writeTestForModel(modelArray, modelChangeName)
    writeStringToFile(folder+"test/", "test_model.dart", main_model_string)

    if reqType == "GET":
        main_model_string = writeGetAPI(email, passwd, modelArray, modelChangeName, baseURL, endPoint, params)
    elif reqType == "POST":
        main_model_string = writePostAPI(email, passwd, modelArray, modelChangeName, baseURL, endPoint, params)
    elif reqType == "PATCH":
        main_model_string = writePostAPI(email, passwd, modelArray, modelChangeName, baseURL, endPoint, params)
    elif reqType == "FORMDATA":
        main_model_string = writePostAPI(email, passwd, modelArray, modelChangeName, baseURL, endPoint, params)
    else:
        print("unknown request type" + reqType)
        exit()
        
    writeStringToFile(folder+"test/", "test_api.dart", main_model_string)

def processNames(modelArray):
    modelChangeName = {}
    print("Renaming Models ========================")
    for model in modelArray:
        modelName = model["modelName"]
        newName = input("New name for " + modelName + " > ")
        if newName == "": newName = modelName
        modelChangeName[modelName] = newName
    
    # print(modelChangeName)
    return modelChangeName
    # exit()

def writeTestForModel(modelArray, modelChangeName):
    modelName = modelArray[-1]["modelName"]
    
    st = ""
    st += "// ignore_for_file: avoid_print\n"
    st += 'import "response.dart";\n'
    st += 'import "../model.dart";\n'
    st += '\n'
    st += 'main() {\n'
    st += '  // print(response);\n'
    st += '  '+modelName+' iList = '+modelName+'.fromMap(response);\n'
    st += '  print(iList);\n'
    st += '}\n'
    return st


def writeGetAPI(email, passwd, modelArray, modelChangeName, baseURL, endPoint, params):
    modelName = modelArray[-1]["modelName"]
    # print(modelChangeName)
    # print(mName)
    # exit()
    # modelName = modelChangeName[mName]
    st = '// ignore_for_file: avoid_print\n'
    st += "import '../../../services/api_client.dart';\n"
    st += "import '../model.dart';\n"
    st += '\n'
    st += 'main() async {\n'
    st += '  ApiClient.create("'+baseURL+'");\n'
    st += '  String email = "'+email+'";\n'
    st += '  String password = "'+passwd+'";\n'
    st += '  Response resp = await ApiClient.login(email, password);\n'
    st += '  print(resp);\n'
    st += '  await testApi();\n'
    st += '}\n'
    st += '\n'
    st += 'Future testApi() async {\n'
    st += '  Response resp = await ApiClient.get<'+modelName+'>(\n'
    st += '      endPoint: "'+endPoint+'",\n'
    st += '      params: "'+params+'",\n'
    st += '      fromJson: '+modelName+'.fromMap);\n'
    st += '  '+modelName+' iList = resp["data"];\n'
    st += '  print(iList);\n'
    st += '}\n'
    return st

def writePostAPI(email, passwd, modelArray, modelChangeName, baseURL, endPoint, params):
    modelName = modelArray[-1]["modelName"]
    # print(modelChangeName)
    # print(mName)
    # exit()
    # modelName = modelChangeName[mName]
    st = '// ignore_for_file: avoid_print\n'
    st += "import '../../../services/api_client.dart';\n"
    st += "import '../model.dart';\n"
    st += '\n'
    st += 'main() async {\n'
    st += '  ApiClient.create("'+baseURL+'");\n'
    st += '  String email = "'+email+'";\n'
    st += '  String password = "'+passwd+'";\n'
    st += '  Response resp = await ApiClient.login(email, password);\n'
    st += '  print(resp);\n'
    st += '  await testApi();\n'
    st += '}\n'
    st += '\n'
    st += 'Future testApi() async {\n'
    st += '  Response resp = await ApiClient.post<'+modelName+'>(\n'
    st += '      request: {},\n'
    st += '      endPoint: "'+endPoint+'",\n'
    st += '      fromJson: '+modelName+'.fromMap);\n'
    st += '  '+modelName+' iList = resp["data"];\n'
    st += '  print(iList);\n'
    st += '}\n'
    return st
