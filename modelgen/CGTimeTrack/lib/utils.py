
import json
from pathlib import Path
import os


def typeName(v):
    return type(v).__name__

def fileNameFromAPIname(st):
    
    if st[:3].lower() == "get":
        st = st[3:]

    if st[-1].lower() == "s":
        st = st[:-1]
    print(st.lower())
    return st.lower()

def replaceDataDataKeys(resp, apiName):
    if type(resp) == dict:
        if "data" in resp:
            if type(resp['data']) == dict:
                if "data" in resp["data"]:
                    v = resp["data"].pop("data")
                    resp["data"][apiName] = v
                    resp[apiName+"List"] = resp.pop("data")
            
    print(json.dumps(resp, indent=4))        

def to_camel_case(snake_str):
        snake_str = snake_str.replace(".", "_")
        components = snake_str.split('_')
        # We capitalize the first letter of each component except the first one
        # with the 'title' method and join them together.
        return components[0] + ''.join(x.title() for x in components[1:])    


def createPath(path):
    if not os.path.exists(path):
        os.makedirs(path)
    # Path(path).mkdir(parents=True, exist_ok=True)

def writeJsonToFile(path, fName, jsonData):
    fileName = path+fName
    createPath(path)
    f = open(fileName, "w")
    f.write(json.dumps(jsonData, indent=4))
    f.close()

def writeStringToFile(path, fName, data):
    fileName = path+fName
    createPath(path)
    f = open(fileName, "w")
    f.write(data)
    f.close()
